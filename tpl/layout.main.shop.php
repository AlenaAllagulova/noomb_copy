<?php
/**
 * Layout: основной каркас
 * @var $centerblock string содержимое (HTML)
 */
?>
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?= SEO::i()->metaRender() ?>
<?

?>
<? if( User::id() ){ ?><meta name="csrf_token" content="<?= bff::security()->getToken() ?>" /><? } ?>
<link rel="shortcut icon" href="<?= bff::url('/favicon.ico') ?>" type="image/x-icon" />
<? include bff::path('/tpl/css.php'); ?>
</head>
<? $background = Shops::getShopBackground(); ?>
<body class="q<?= bff::database()->statQueryCnt(); ?> l-layout-main-shop" style="<?= (!empty($background)) ? ' background-image: url(' . $background . '); ' : ''?>">
<a href="#" id="tgg-style-btn"></a>
<? include bff::path('/tpl/alert.php'); ?>
<div id="wrap">
    <? include bff::path('/tpl/header.php'); ?>
    <!-- BEGIN main content -->
    <div id="main">
        <div class="content">
            <div class="container-fluid">
            <!-- BEGIN filter -->
            <? include bff::path('/tpl/filter.php'); ?>
            <!-- END filter -->
            <?= $centerblock; ?>
            </div>
        </div>
    </div>
    <!-- END main content -->
    <div id="push"></div>
</div>
<? include bff::path('/tpl/footer.php'); ?>
</body>
</html>