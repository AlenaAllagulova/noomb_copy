<?php

class Theme_Custom extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'theme_title'   => 'Доработки',
            'theme_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
    }
}