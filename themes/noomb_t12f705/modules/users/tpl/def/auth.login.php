<?php
/**
 * Авторизация
 * @var $this Users
 * @var $back string URL страницы возврата (после успешной авторизации)
 * @var $providers array провайдеры авторизации через соц. сети
 */
?>

<div class="empty-bg" style="background: url(<?= bff::url('/img/empty-bg.png') ?>);">
    <div class="container">
        <div class="form-enter">
            <div class="text-center">
                <a href="<?= bff::urlBase() ?>" class="form-enter__logo">
                    <img src="<?= Site::logoURL('header') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>">
                </a>
            </div>

            <div class="form-enter__title text-center">
                <?=_t('','Welcome back!')?>
                <div class="mrgt5 form-enter__title-second">
                    <?= _t('','Sign in to Noomb')?>
                </div>
            </div>

            <form class="" id="j-u-login-form" action="">
                <input type="hidden" name="back" value="<?= HTML::escape($back) ?>"/>
                <div class="form-group">
                    <label class=" control-label" for="j-u-login-email">
                        <?= _t('users', 'Электронная почта') ?>
                        <span class="required-mark">*</span>
                    </label>
                    <div class="">
                        <input type="email" name="email" class="form-control" id="j-u-login-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="flex flex_sb">
                        <label class="control-label" for="j-u-login-pass">
                            <?= _t('users', 'Пароль') ?>
                            <span class="required-mark">*</span>
                        </label>
                        <a href="<?= Users::url('forgot') ?>" class=""><?= _t('users', 'Восстановить пароль?') ?></a>
                    </div>
                    <div class="">
                        <input type="password" name="pass" class="form-control" id="j-u-login-pass" placeholder="<?= _te('users', 'Введите ваш пароль') ?>" maxlength="100"/>
                    </div>
                </div>
                <?php if (Users::loginRemember()) { ?>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember"/> <?= _t('users', 'Запомнить меня'); ?>
                            </label>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <div class="">
                        <button type="submit" class="btn btn-block btn-default-accent j-submit">
                            <?= _t('users', 'Войти на сайт') ?>
                        </button>
                    </div>
                </div>
                <div class="text-center">
                    <span class="">
                        <?= _t('users', 'Впервые на нашем сайте?') ?>
                    </span>
                    <a href="<?= Users::url('register') ?>" class="bold mrgl5">
                        <?= _t('users', 'Зарегистрируйтесь') ?>
                    </a>
                </div>
                <div class="">
                    <?php foreach ($providers as $v) {

                        ?><a href="#" class="btn btn-sm btn-social btn-<?= $v['class'] ?> j-u-login-social-btn"
                             data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?></a><?php

                    } ?>
                </div>
            </form>
        </div>
    </div>


</div>

<script type="text/javascript">
    <?php js::start(); ?>
    $(function () {
        jUserAuth.login(<?= func::php2js(array(
            'login_social_url' => Users::url('login.social'),
            'login_social_return' => $back,
            'lang' => array(
                'email' => _t('users', 'E-mail адрес указан некорректно'),
                'pass' => _t('users', 'Укажите пароль'),
            ),
        )) ?>);
    });
    <?php js::stop(); ?>
</script>