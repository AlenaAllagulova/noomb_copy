<?php
/**
 * Восстановление пароля: Шаг 1
 * @var $this Users
 * @var $social integer инициировано ли восстановление на этапе авторизации через соц. сеть
 */
?>


<div class="empty-bg" style="background: url(<?= bff::url('/img/empty-bg.png') ?>);">
    <div class="container">
        <div class="form-enter">
            <div class="text-center">
                <a href="<?= bff::urlBase() ?>" class="form-enter__logo">
                    <img src="<?= Site::logoURL('header') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>">
                </a>
            </div>

            <div class="form-enter__title text-center">
                <?=_t('','Welcome back!')?>
                <div class="mrgt5 form-enter__title-second">
                    <?= _t('','Sign in to Noomb')?>
                </div>
            </div>

            <form action="" id="j-u-forgot-start-form-<?= bff::DEVICE_DESKTOP ?>" class="">
                <input type="hidden" name="social" value="<?= $social ?>"/>
                <div class="form-group">
                    <label for="j-u-forgot-start-desktop-email" class="control-label">
                        <?= _t('users', 'Электронная почта') ?>
                    </label>
                    <div class="">
                        <input class="form-control j-required" type="email" name="email" id="j-u-forgot-start-desktop-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>
                <div class="">
                    <button type="submit" class="btn btn-default-accent btn-block">
                        <?= _t('users', 'Восстановить пароль') ?>
                    </button>
                </div>
                <div class="text-center mrgt20">
                    <span class="">
                        <?= _t('users', 'Вы уже зарегистрированы?') ?>
                    </span>
                    <a href="<?= Users::url('login') ?>" class="bold mrgl5">
                        <?= _t('users', 'Войдите на сайт') ?>
                    </a>
                </div>
            </form>

        </div>
    </div>


</div>

<script type="text/javascript">
    <?php js::start(); ?>
    $(function () {
        jUserAuth.forgotStart(<?= func::php2js(array(
            'lang' => array(
                'email' => _t('users', 'E-mail адрес указан некорректно'),
                'success' => _t('users', 'На ваш электронный ящик были высланы инструкции по смене пароля.'),
            ),
        )) ?>);
    });
    <?php js::stop(); ?>
</script>