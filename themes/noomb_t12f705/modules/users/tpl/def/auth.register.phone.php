<?php
/**
 * Регистрация с подтверждением номера телефона
 * @var $this Users
 */
?>

<div class="empty-bg" style="background: url(<?= bff::url('/img/empty-bg.png') ?>);">
    <div class="container">
        <div class="form-enter">
            <div class="text-center">
                <a href="<?= bff::urlBase() ?>" class="form-enter__logo">
                    <img src="<?= Site::logoURL('header') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>">
                </a>
            </div>

            <div class="text-center">
                <p>
                    <?= _t('users', 'На номер [phone] отправлен код подтверждения.', array('phone'=>'<strong id="j-u-register-phone-current-number">+'.$phone.'</strong>')) ?>
                </p>
            </div>

            <div id="j-u-register-phone-block-code">

                <form action="" class=" mrgt30 mrgb30">
                    <div class="form-group">
                        <label for="phone-code-input" class=" control-label"><?= _t('users', 'Код подтверждения') ?></label>
                        <div class=" mrgb10">
                            <input type="text" class="form-control j-u-register-phone-code-input" id="phone-code-input" placeholder="<?= _te('users', 'Введите код') ?>" />
                        </div>
                    </div>
                    <div class="mrgt30">
                        <button type="submit" class="btn btn-default-accent btn-block j-u-register-phone-code-validate-btn"><?= _t('users', 'Подтвердить') ?></button>
                    </div>
                    <div class="text-center mrgt10">
                        <a href="#" class="bold j-u-register-phone-code-resend-btn">
                            <span><?= _t('users', 'Выслать новый код подтверждения') ?></span>
                        </a>
                    </div>
                </form>

                <div class="text-center">
                    <span>
                        <?= _t('users', 'Не получили код подтверждения? Возможно ваш номер написан с ошибкой.') ?>
                    </span>
                    <div class="">
                        <a href="#" class=" j-u-register-phone-change-step1-btn">
                            <span>
                                <?= _t('users', 'Изменить номер телефона') ?>
                            </span>
                        </a>
                    </div>
                </div>

            </div>

            <div id="j-u-register-phone-block-phone" style="display: none;">

                <form action="" class=" ">
                    <div class="form-group">
                        <label class=" control-label"><?= _t('users', 'Номер телефона') ?></label>
                        <div class=" mrgb10">
                            <?= $this->registerPhoneInput(array('name'=>'phone', 'id'=>'j-u-register-phone-input')) ?>
                        </div>
                        <div class="mrgt30">
                            <button type="button" class="btn btn-default-accent btn-block j-u-register-phone-change-step2-btn"><?= _t('users', 'Выслать код') ?></button>
                        </div>
                    </div>
                </form>

            </div>


        </div>

    </div>
</div>

<script type="text/javascript">
  <?php js::start(); ?>
  $(function(){
    jUserAuth.registerPhone(<?= func::php2js(array(
      'lang' => array(
        'resend_success' => _t('users', 'Код подтверждения был успешно отправлен повторно'),
        'change_success' => _t('users', 'Код подтверждения был отправлен на указанный вами номер'),
        ),
      )) ?>);
  });
  <?php js::stop(); ?>
</script>