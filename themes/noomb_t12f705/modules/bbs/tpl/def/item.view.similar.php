<?php

/**
 * Просмотр объявления: Список похожих объявлений
 * @var $this BBS
 * @var $similar array список объявлений
 */
if (empty($similar)) return '';

?>
<div class="ad-similar mrgt20" style="background: #d8e7fb; padding: 10px;">
    <div class="index-latest" id="j-bbs-index-similar-block">
        <div class="flex flex_sb mrgb20">
            <div class="">
                <div class="l-blockHeading-title ff-extra-bold fz-22">
                    <?= _t('view', 'Другие похожие объявления') ?>
                </div>
            </div>
            <div class="customNavigation">
                <a class="prev j-prev"><i class="fa fa-chevron-left"></i></a>
                <a class="next j-next"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        <div class="spacer"></div>
        <div class="sr-page__gallery sr-page__gallery_desktop">
            <div id="j-bbs-index-similar-carousel" class="thumbnails owl-carousel">
                <?php
                foreach ($similar as &$v) {
                    echo View::template('search.item.gallery', array('item'=>&$v, ['attr' => ['class' => 'owl-item index-latest__item']]) , 'bbs');
                } unset($v);
                ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    <?php
    tpl::includeCSS('owl.carousel', true);
    tpl::includeJS('owl.carousel.min', false);
    ?>
    <?php js::start(); ?>
    $(function(){
        var $block = $('#j-bbs-index-similar-block');

        var $carousel = $block.find('#j-bbs-index-similar-carousel');
        if ($carousel.length) {
            $carousel.owlCarousel({
                rewind: true,
                margin: 15,
                nav: false,
                dots: false,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsive: {
                    0: {items: 1},
                    767: {items: 3},
                    991: {items: 3},
                    1199: {items: 3}
                }
            });
            $block.on('click', '.j-prev', function () {
                $carousel.owlCarousel().trigger('prev.owl.carousel');
            });
            $block.on('click', '.j-next', function () {
                $carousel.owlCarousel().trigger('next.owl.carousel');
            });
        }
    });
    <?php js::stop(); ?>
</script>

