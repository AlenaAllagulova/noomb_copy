<?php
/**
 * Список объявлений: вид галерея
 * @var $this BBS
 * @var $item array данные объявления
 */
?>
<?// print_r($aData) ?>
<div class="gallery__box">
    <div class="">
        <a href="<?= $item['link'] ?>" title="<?= $item['title'] ?>" class="gallery__img <? if ($item['svc_marked']): ?>selected<? endif; ?>" style="background: url(<?= !empty($item['img_g'])? $item['img_g']: $item['img_m'] ?>)" ></a>
        <div class="">
            <a href="<?= $item['link'] ?>" class="gallery__title">
                <?php if ($item['svc_quick']) { ?>
                    <i class="icon-fire"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
                <?php } ?>
                <?= $item['title'] ?>
            </a>
        </div>
        <span class="fz-12">
            <?= $item['cat_title'] ?>
        </span>
    </div>
    <div class="mrgt15">


        <div class="flex flex_sb">
            <div class="gallery__price">
                <?php if ($item['price_on']) { ?>
                    <?= $item['price'] ?>
                    <span class="c-price-sub">
                        <?= $item['price_mod'] ?>
                    </span>
                <?php } ?>
            </div>
            <div class="">
                <?php if ($item['fav']) { ?>
                    <a href="javascript:void(0);" class="gallery__star active j-i-fav" data="{id:<?= $item['id'] ?>}" title="<?= _te('bbs', 'Удалить из избранного') ?>">
                        <i class="fa fa-star j-i-fav-icon"></i>
                    </a>
                <?php } else { ?>
                    <a href="javascript:void(0);" class="gallery__star j-i-fav" data="{id:<?= $item['id'] ?>}" title="<?= _te('bbs', 'Добавить в избранное') ?>">
                        <i class="fa fa-star-o j-i-fav-icon"></i>
                    </a>
                <?php } ?>
            </div>
        </div>

        <span class="fz-12 color-light">
            <?php if (!empty($item['city_title'])): ?>
                <?= $item['city_title'] ?>
                <?= !empty($item['district_title']) ? ', ' . $item['district_title'] : '' ?>
            <?php endif; ?>
        </span>
    </div>
</div>
