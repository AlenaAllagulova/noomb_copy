<?php
/**
 * Список объявлений: вид карта
 * @var $this BBS
 * @var $item array данные объявления
 * @var $index $index порядковый номер в списке
 */
?>
<div class="list list_map">
    <div class="list__photo">
        <a href="<?= $item['link'] ?>" title="<?= $item['title'] ?>" class="list__img <?php if ($item['svc_marked']) { ?> selected<?php } ?> " style="background: url(<?= $item['img_m'] ?>)"></a>
    </div>
    <div class="list__body">
        <div class="list__body-flex">
            <div class="">
                <div class="">
                    <a href="<?= $item['link'] ?>" class="list__title mrgb10">
                        <?php if ($item['svc_quick']) { ?>
                            <i class="icon-fire"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
                        <?php } ?>
                        <?= $item['title'] ?>
                    </a>
                </div>
                <span class="fz-12 color-light">
                    <?= $item['cat_title'] ?>
                </span>
            </div>
            <div class="">
                <div class="list__price mrgt10">
                    <?php if ($item['price_on']) { ?>
                        <?= $item['price'] ?>
                        <span class="c-price-sub">
                            <?= $item['price_mod'] ?>
                        </span>
                    <?php } ?>
                </div>
                <span class="fz-12 color-light">
                    <?php if (!empty($item['city_title'])): ?>
                        <?= $item['city_title'] ?>
                        <?= !empty($item['district_title']) ? ', ' . $item['district_title'] : '' ?>
                    <?php endif; ?>
                    <span class="j-maplist-item list__show-map" data-index="<?= $index ?>">
                        <?= _t('','Показать на карте')?>
                    </span>
                </span>
            </div>
        </div>
        <div class="list__body-flex">
            <div class="text-right">
                <?php if ($item['fav']) { ?>
                    <a href="javascript:void(0);" class="list__star active j-i-fav" data="{id:<?= $item['id'] ?>}"
                       title="<?= _te('bbs', 'Удалить из избранного') ?>"><i class="fa fa-star j-i-fav-icon"></i></a>
                <?php } else { ?>
                    <a href="javascript:void(0);" class="list__star j-i-fav" data="{id:<?= $item['id'] ?>}"
                       title="<?= _te('bbs', 'Добавить в избранное') ?>"><i class="fa fa-star-o j-i-fav-icon"></i></a>
                <?php } ?>
            </div>
            <span class="fz-12 color-light">
                <?= $item['publicated'] ?>
            </span>
        </div>
    </div>
</div>