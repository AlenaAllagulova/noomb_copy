<?php
/**
 * Блок премиум объявлений
 * @var $this BBS
 * @var $items array объявления
 */
?>
<div class="gallery gallery_sr-vip">
    <div class="gallery__article-vip mrgb20">
        <?= _t('search', 'Премиум объявления'); ?>
    </div>

    <?php foreach ($items as $v): ?>
        <div class="gallery__box">
            <div class="">
                <a href="<?= $v['link'] ?>" title="<?= $v['title'] ?>" class="gallery__img <? if ($v['svc_marked']): ?>selected<? endif; ?>" style="background: url(<?= !empty($v['img_g'])? $v['img_g']: $v['img_m'] ?>)" ></a>
                <div class="">
                    <a href="<?= $v['link'] ?>" class="gallery__title">
                        <?= $v['title'] ?>
                    </a>
                </div>
            </div>
            <div class="mrgt15">


                <div class="flex flex_sb">
                    <div class="gallery__price">
                        <?php if ($v['price_on']) { ?>
                            <?= $v['price'] ?>
                            <span class="c-price-sub">
                                <?= $v['price_mod'] ?>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="fz-12 color-light">
                </span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
