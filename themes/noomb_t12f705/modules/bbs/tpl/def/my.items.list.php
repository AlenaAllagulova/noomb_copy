<?php

/**
 * Кабинет пользователя: Мои объявления - список
 * @var $this BBS
 * @var $items array объявления
 * @var $device string текущее устройство bff::DEVICE_
 */

$lng_from = _t('bbs.my', 'С');
$lng_to = _t('bbs.my', 'По');
$lng_a_view = _t('bbs.my', 'Посмотреть');
$lng_a_edit = _t('bbs.my', 'Редактировать');
$lng_a_edit_phone = _t('bbs.my', 'Изменить');
$lng_a_unpublicate = _t('bbs.my', 'Деактивировать');
$lng_a_publicate = _t('bbs.my', 'Активировать');
$lng_a_promote = _t('bbs.my', 'Рекламировать');
$lng_a_up_free = _t('bbs.my', 'Поднять бесплатно');
$lng_a_delete = _t('bbs.my', 'Удалить');
$lng_st = _t('bbs.my', 'Статистика');
$lng_st_views = _t('bbs.my', 'просмотры');
$lng_st_contacts = _t('bbs.my', 'контакты');
$lng_st_messages = _t('bbs.my', 'сообщения');
$lng_blocked = _t('bbs.my', 'ЗАБЛОКИРОВАНО');
$lng_up_auto_edit = _t('bbs.my', 'Настроить автоподнятие');
$lng_up_auto_on = _t('bbs.my', 'Включить автоподнятие');
$lng_active_svc = _t('bbs.my', 'Активные рекламные услуги');

if ($device == bff::DEVICE_DESKTOP || $device == bff::DEVICE_TABLET || $device == bff::DEVICE_PHONE) {
    foreach ($items as $v):
        $ID = $v['id'];
        $messages_url = InternalMail::url('item.messages', array('item' => $ID));
        $moderated = ($v['moderated'] > 0 || !BBS::premoderation());
        ?>
        <div class="list list_with-checkbox mrgb40">
            <div class="list__checkbox">
                <label><input type="checkbox" name="i[]" class="j-check-desktop" value="<?= $ID ?>"/></label>
            </div><!-- /.sr-list-item-left -->

            <div class="list__photo">
                <a href="<?= $v['link'] ?>" title="<?= $v['title'] ?>" class="list__img  " style="background: url(<?= !empty($v['img_g'])? $v['img_g']: $v['img_s'] ?>)"></a>
            </div>
            <div class="list__body">
                <div class="list__body-flex">
                    <div class="">
                        <div class="">
                            <a href="<?= $v['link'] . '?from=my' ?>" class="list__title mrgb10">
                                <?php if ($v['svc_quick']) { ?>
                                    <i class="icon-fire"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></i>
                                <?php } ?>
                                <?= $v['title'] ?>
                            </a>
                            <?php if ($v['status'] == BBS::STATUS_BLOCKED) { ?>
                                <span class="text-danger">
                                    (<?= $lng_blocked ?>)
                                </span>
                            <?php } ?>
                        </div>
                        <span class="fz-12 color-light">
                            <?= $v['cat_title'] ?>
                        </span>
                        <div class="mrgt10">
                            <?php if ($v['status'] == BBS::STATUS_PUBLICATED && $moderated && bff::servicesEnabled()) { ?>
                                <a href="<?= BBS::url('item.promote', array('id' => $ID, 'from' => 'my')) ?>" class="btn btn-sm btn-success">
                                    <?= $lng_a_promote ?>
                                </a>
                                <?php if ($upfree_days) { ?>
                                    <a href="#" data-id="<?= $ID ?>" class="btn btn-sm btn-info <?= strtotime($v['svc_up_free']) >= $upfree_to ? 'disabled' : '' ?> j-i-up-free">
                                        <i class="fa fa-arrow-up"></i>
                                        <?= $lng_a_up_free ?>
                                    </a>
                            <?php } ?>
                            <?php } else if ($v['status'] == BBS::STATUS_PUBLICATED_OUT && $moderated) { ?>
                                <a href="#" data-id="<?= $ID ?>" data-act="publicate" class="btn btn-sm btn-default-accent j-i-status">
                                    <?= $lng_a_publicate ?>
                                </a>
                            <?php } ?>
                            <?php if (!$v['messages_total']) { ?>
                                <a href="#" onclick="return false;" class="btn btn-sm btn-default-accent disabled">
                                    <i class="fa fa-envelope"></i> 0&nbsp;
                                </a>
                            <?php } else {
                                if ($v['messages_new']) { ?>
                                    <a href="<?= $messages_url ?>" class="btn btn-sm btn-success">
                                        <i class="fa fa-envelope"></i>
                                        +<?= $v['messages_new'] ?>
                                    </a>
                                <?php } else { ?>
                                    <a href="<?= $messages_url ?>" class="btn btn-sm btn-default-accent">
                                        <i class="fa fa-envelope"></i>
                                        <?= $v['messages_total'] ?>
                                    </a>
                                <?php }
                            } ?>
                        </div>

                        <?php if (BBS::svcUpAutoEnabled()) { ?>
                            <div class="sr-list-item-autoup">
                                <a href="#" class="link-ajax j-i-up-auto" data-id="<?= $ID ?>">
                                    <i class="fa fa-refresh <?= $v['svc_upauto_on'] ? 'text-blue' : '' ?>"></i>
                                    <span><?= $v['svc_upauto_on'] ? $lng_up_auto_edit : $lng_up_auto_on ?></span>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="">
                        <div class=" mrgt10">
                            <ul class="sr-list-item-stats">
                                <? if ($v['svc_dd']) { ?>
                                    <li class="dropdown usr-ads-list-dropdown pull-right">
                                        <a class="dropdown-toggle" id="pay-services" role="button" data-toggle="dropdown" href="#">
                                            <i class=" icon-megaphone " aria-hidden="true"></i>
                                            <span><?= $lng_active_svc ?> </span>
                                            <b class="caret"></b>
                                        </a>
                                        <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="pay-services">
                                            <? foreach ($svc as $kk => $vv) {
                                                if (!($kk & $v['svc_dd'])) continue; ?>
                                                <li role="presentation">
                                                    <a role="menuitem" tabindex="-1" href="<?= $svc_url($kk, $v) ?>">
                                                        <div class="usr-ads-list-dropdown-ico">
                                                            <img src="<?= $vv['icon_s'] ?>" alt="">
                                                        </div>
                                                        <span><?= $vv['title'] ?></span>
                                                        <small class="usr-ads-list-dropdown-time"><?= $svc_to_date($kk, $v) ?></small>
                                                    </a>
                                                </li>
                                            <? } ?>
                                        </ul>
                                    </li>
                                <? } ?>
                                <?php if ($v['status'] != BBS::STATUS_BLOCKED && $moderated) { ?>
                                    <li>
                                        <a href="<?= $v['link'] . '?from=my' ?>" class="link-ico">
                                            <i class="fa fa-check"></i>
                                            <span>
                                                <?= $lng_a_view ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="<?= BBS::url('item.edit', array('id' => $ID, 'from' => 'my')) ?>" class="link-ico">
                                        <i class="fa fa-edit"></i>
                                        <span>
                                            <?= $lng_a_edit ?>
                                        </span>
                                    </a>
                                </li>
                                <?php if ($v['status'] == BBS::STATUS_PUBLICATED && $moderated) { ?>
                                    <li>
                                        <a href="#" data-id="<?= $ID ?>" data-act="unpublicate" class="link-ico link-red j-i-status">
                                            <i class="fa fa-times"></i>
                                            <span>
                                                <?= $lng_a_unpublicate ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php } else if ($v['status'] == BBS::STATUS_PUBLICATED_OUT || $v['status'] == BBS::STATUS_BLOCKED) { ?>
                                    <li>
                                        <a href="#" data-id="<?= $ID ?>" data-act="delete" class="link-ico link-red j-i-status">
                                            <i class="fa fa-times"></i>
                                            <span>
                                                <?= $lng_a_delete ?>
                                            </span>
                                        </a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </div>
                        <span class="fz-12 color-light">
                            <?php if (!empty($v['city_title'])): ?>
                                <?= $v['city_title'] ?>
                                <?= !empty($v['district_title']) ? ', ' . $v['district_title'] : '' ?>
                            <?php endif; ?>
                        </span>
                    </div>
                </div>
                <div class="list__body-flex">
                    <div class="">
                        <div class="list__price text-right">
                            <?php if ($v['price_on']) { ?>
                                <?= $v['price'] ?>
                                <span class="c-price-sub">
                                    <?= $v['price_mod'] ?>
                                </span>
                            <?php } ?>
                        </div>
                        <span class="fz-12 color-light">
                            <div class="sr-list-item-date c-date text-right">
                                <span class="sr-list-item-date-i"><?= tpl::dateFormat($v['publicated']) ?> - <?= tpl::dateFormat($v['publicated_to']) ?></span>
                            </div>
                        </span>
                    </div>
                    <div class="">
                        <ul class="sr-list-item-stats text-right nowrap">
                            <li class="sr-list-item-stats__li" data-placement="top" title="<?= $lng_st_views ?>" data-toggle="tooltip">
                                <i class="fa fa-eye"></i>
                                <b>
                                    <?= $v['views_item_total'] ?>
                                </b>
                            </li>
                            <li class="sr-list-item-stats__li" data-placement="top" title="<?= $lng_st_contacts ?>" data-toggle="tooltip">
                                <i class="fa fa-mobile" style="font-size: 18px;"></i>
                                <b>
                                    <?= $v['views_contacts_total'] ?>
                                </b>
                            </li>
                            <li class="sr-list-item-stats__li" data-placement="top" title="<?= $lng_st_messages ?>" data-toggle="tooltip">
                                <i class="fa fa-comment" ></i>
                                <b>
                                    <?= $v['messages_total'] ?>
                                </b>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php
    endforeach;
    if (empty($items)) {
        echo $this->showInlineMessage(_t('bbs.my', 'Список объявлений пустой'));
    }
}

if (false && $device == bff::DEVICE_PHONE) {
    foreach ($items as $v):
        $ID = $v['id'];
        $messages_url = InternalMail::url('item.messages', array('item' => $ID));
        $moderated = ($v['moderated'] > 0);
        ?>

        <!-- Mobile List Layout (if needed) -->

        <?php
    endforeach;
    if (empty($items)) {
        echo $this->showInlineMessage(_t('bbs.my', 'Список объявлений пустой'));
    }
}