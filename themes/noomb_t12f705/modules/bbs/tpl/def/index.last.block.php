<?php
/**
 * Блок объявлений на главной
 * @var $this BBS
 * @var $title string заголовок блока
 * @var $items array данные об объявлениях
 * @var $type string тип объявлений: 'last', 'premium'
 */
$lng_fav_in = _te('bbs', 'Добавить в избранное');
$lng_fav_out = _te('bbs', 'Удалить из избранного');
$lng_quick = _t('bbs', 'срочно');
?>
<div class="">
    <div class="gallery-general <? if (DEVICE_DESKTOP && (Banners::view('index-right-with-premium'))): ?>banner<? endif; ?>">

        <div class="gallery">
            <? if (DEVICE_DESKTOP && empty(Banners::view('index-right-with-premium'))): ?>
                <? $i = 0; ?>
                <?php foreach ($items as &$v): ?>
                    <?= View::template('search.item.gallery', array('item' => &$v), 'bbs');?>
                    <? $i++; ?>
                    <? if($i > 7): ?>
                        <? break; ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? else: ?>
                <? $i = 0; ?>
                <?php foreach ($items as &$v): ?>
                    <?= View::template('search.item.gallery', array('item' => &$v), 'bbs');?>
                    <? $i++; ?>
                    <? if($i > 8): ?>
                        <? break; ?>
                    <? endif; ?>
                <? endforeach; ?>
            <? endif; ?>

        </div>

        <?php if (DEVICE_DESKTOP && ($banner = Banners::view('index-right-with-premium'))) { ?>
            <div class="l-banner">
                <?= $banner; ?>
            </div>
        <?php } ?>
    </div>
</div>

