<div class="ad-author">
    <div class="">
        <?php if ($shop['logo']) { ?>
            <div class="ad-author-shop-logo">
                <a href="<?= $shop['link'] ?>">
                    <img src="<?= $shop['logo'] ?>" alt=""/>
                </a>
            </div>
        <?php } ?>

        <div class="">
            <?php if (!empty($shop['addr_addr'])) {
                if ($shop['addr_map']) {
                    Geo::mapsAPI(false);
                } ?>
                <div class="ad-author-shop-location-info"><?= $shop['region_title'] . ', ' . $shop['addr_addr'] ?></div>
                <?php if ($shop['addr_map']) { ?>
                    <a href="#" class="link-ajax" id="j-shop-view-map-toggler">
                        <i class="fa fa-map-marker"></i>
                        <span>
                            <?= _t('shops', 'Показать на карте') ?>
                        </span>
                    </a>
                <?php } ?>
                <?php if ($shop['addr_map']) { ?>
                    <div id="j-shop-view-map-popup" class="ad-author-map" style="display: none;">
                        <div id="j-shop-view-map-container" class="ad-author-map-container"></div>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>
    </div><!-- /.ad-author-shop -->

    <?php if ($shop['has_contacts']) { ?>
        <div class="">
            <div class="ad-author-contact-row">
                <div class="ad-author-contact-row-label">
                    <?= _t('shops', 'Контакты') ?>:
                </div>
                <div class="ad-author-contact-row-content">
                    <a href="#" class="link-ajax j-shop-view-c-toggler">
                        <span>
                            <?= _t('shops', 'показать контакты') ?>
                        </span>
                    </a>
                </div>
            </div>
            <?php if (!empty($shop['phones'])) { ?>
                <div class="ad-author-contact-row">
                    <div class="ad-author-contact-row-label">
                        <?= _t('users', 'Тел.') ?>:
                    </div>
                    <div class="ad-author-contact-row-content j-shop-view-c-phones">
                        <?php foreach ($shop['phones'] as $v) { ?>
                            <div><?= $v['m'] ?></div><?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if (!empty($shop['contacts'])): ?>
                <?php foreach (Users::contactsFields($shop['contacts']) as $contact): ?>
                    <div class="ad-author-contact-row">
                        <div class="ad-author-contact-row-label">
                            <?= $contact['title'] ?>:
                        </div>
                        <div class="ad-author-contact-row-content j-shop-view-c-<?= $contact['key'] ?>">
                            <?= tpl::contactMask($contact['value']) ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if (!empty($shop['social']) && $social) { ?>
                <div class="mrgt20">
                    <?php foreach ($shop['social'] as $v) {
                        if ($v && isset($social[$v['t']])) { ?>

                            <a href="<?= bff::urlAway($v['v']) ?>" rel="nofollow noreferrer noopener" target="_blank"
                               class="sh-social sh-social_<?= $social[$v['t']]['icon'] ?>"></a>
                            <?php
                        }
                    } ?>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div><!-- /.ad-author-in -->

    <?php } # $shop['has_contacts'] ?>
    <?php if (!empty($shop['site'])) { ?>
        <div class="ad-author-shop-website">
            <a href="<?= bff::urlAway($shop['site']) ?>" target="_blank" rel="nofollow" class="link-ico j-away">
                <i class="fa fa-globe"></i>
                <span>
                    <?= $shop['site'] ?>
                </span>
            </a>
        </div>
    <?php } ?>

    <?php if (!$is_owner && $has_owner) { ?>
        <div class="mrgt10">
            <a class="btn btn-block btn-default-accent" href="<?= Shops::urlContact($shop['link']) ?>">
                <?= _t('users', 'Написать сообщение') ?>
            </a>
        </div>
    <?php } ?>

</div><!-- /.ad-author -->