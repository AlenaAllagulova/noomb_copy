<?php
/**
 * Поиск магазинов: список - простой список
 * @var $this Shops
 * @var $items array список магазинов
 */

$socialTypes = Shops::socialLinksTypes();
$lang_shop_items = _t('shops', 'объявление;объявления;объявлений');
$lang_contacts_show = _t('shops', 'Показать контакты');
$listBanner = function ($positionNumber) use ($device) {
    if ($device == bff::DEVICE_PHONE) {
        $html = Banners::view('shops_search_list_mobile', array('list_pos' => $positionNumber));
        if ($html) {
            return '<div class="banner-m">' . $html . '</div>';
        }
    } else {
        $html = Banners::view('shops_search_list', array('list_pos' => $positionNumber));
        if ($html) {
            return '<div class="sh-list-item">' . $html . '</div>';
        }
    }
    return '';
};

?>
<div class="gallery">
    <? $n = 1;
    foreach ($items as $v): ?><?= $listBanner($n++); ?>
        <div class="gallery__box j-shop" data-ex="<?= $v['ex'] ?>">
        <div class="">
            <a href="<?= $v['link'] ?>" title="<?= $v['title'] ?>" class="gallery__img <?php if ($v['svc_marked']) { ?> selected<?php } ?>" style="background: url(<?= $v['logo'] ?>)" ></a>
            <div class="">
                <a href="<?= $v['link'] ?>" class="gallery__title" title="<?= $v['title'] ?>">
                    <?= $v['title'] ?>
                </a>
            </div>
            <?php if ($v['region_id']) { ?>
                <span class="fz-12 color-light">
                    <?= $v['region_title'] ?>
                </span>
            <?php } ?>
            <div class="">
                <?php if ($v['has_contacts']) { ?>
                    <div class="sh-list-item-contact-item mrgt10">
                        <a href="#" class="j-contacts-ex" data-device="<?= bff::DEVICE_DESKTOP ?>">
                            <span><?= $lang_contacts_show ?></span>
                        </a>
                        <div class="dropdown-menu shown sh-list-item-contact-dropdown hide j-contacts"></div>
                    </div>
                <?php } ?>
            </div>
            <div class="gallery__des mrgt10">
                <?= tpl::truncate($v['descr'], 100, '...', true) ?>
            </div>
        </div>
        <div class="mrgt10">
            <a href="<?= $v['link'] ?>">
                <?= tpl::declension($v['items'], $lang_shop_items) ?>
            </a>
        </div>
    </div>
    <? endforeach; ?>
    <?= ($last = $listBanner(Banners::LIST_POS_LAST)); ?>
    <?= (!$last ? $listBanner($n) : '') ?><?php
    if (empty($items)) {
        echo $this->showInlineMessage(_t('shops', 'Список магазинов пустой'));
    } ?>
</div>