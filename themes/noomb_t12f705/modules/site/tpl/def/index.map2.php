<?php
    /**
     * Главная страница: вид с картой №2
     * @var $this Site
     * @var $cats array категории
     * @var $map string карта
     */
    $visibleMainCategories = 3; // Кол-во категорий выводимых до ссылки "Все категории"
    $regionID = 0;
    if (Geo::ipLocationConfirm()) {
        Geo::filterUser(0);
        $regionData = Geo::regionFilterByIp();
    } else {
        $regionData = Geo::filter(); # user
    }
    if ( ! empty($regionData['id'])) {
        $regionID = $regionData['id'];
    }
    $dataMap = [
            'clientLang' => Site::getClientLang(),
            'countriesList'=> Geo::countriesList(),
    ];
    foreach ($dataMap['countriesList'] as &$v) {
        $v['link'] = bff::urlBase(true, $dataMap['clientLang']).$v['keyword'].'/search/';
    } unset($v);
$defSumCat = 5;
?>

<div class="container-cat">

    <div class="index-cat">
        <div class="fz-22 ff-black mrgt20 mrgb30 color-white w100p text-center">
            <?= _t('header','Find anything here')?>
        </div>
        <? foreach ($cats as $k => $v): ?>
            <div class="index-cat__item">
                <a href="<?= $v['l'] ?>" class="index-cat__link">
                    <?= $v['t'] ?>
                </a>
            </div>
        <? endforeach; ?>
        <div class=" w100p text-center ">
            <a href="<?= BBS::url('item.add') ?>" class="link-green-add">
                <img src="<?= bff::url('/img/shield.png')?>" alt="">
                <span>
                    <?= _t('header','commence maintenant et publier votre annonce ici...')?>
                </span>
            </a>
        </div>
    </div>
</div>

<div class="container ">
    <div class="banner-cat mrgt40 <? if(empty(Banners::view('index-right-with-cat')) && (empty(Banners::view('index-right-with-cat-second')))): ?>no-banner<? endif; ?>">

    <div class="hidden-phone cat-list">
        <? foreach ($cats as $k => $v): ?>
            <div class="cat-list__box">
                <div class="mrgb15">
                    <a href="<?= $v['l'] ?>" class="cat-list__title ff-black">
                        <?= $v['t'] ?>
                    </a>
                </div>
                <? if ($v['subn']): ?>
                    <ul class="cat-list__list">
                        <? $i = 0; ?>
                        <? $t = true; ?>
                        <? foreach ($v['sub'] as $vv): ?>

                            <? if(sizeof($v['sub']) > $defSumCat && $i == $defSumCat): ?>
                                <? $t = false; ?>
                                <li class="cat-list__hover">
                                    <a href="<?= $v['l'] ?>" class="cat-list__more">
                                        <?= _t('','SHOW MORE >')?>
                                    </a>
                                <ul class="cat-list__list-sub">
                            <? endif;?>

                            <li>
                                <a href="<?= $vv['l'] ?>" class="cat-list__item">
                                    <?= $vv['t'] ?>
                                </a>
                            </li>
                                    <? $i++; ?>
                        <? endforeach; ?>
                        <? if(sizeof($v['sub']) > $defSumCat && $i > $defSumCat): ?>
                            </ul>
                            </li>
                        <? endif;?>
                        <? if ($t): ?>
                        <li>
                            <a href="<?= $v['l'] ?>" class="cat-list__more">
                                <?= _t('','SHOW MORE >')?>
                            </a>
                        </li>
                        <? endif; ?>
                    </ul>
                <? endif; ?>
            </div>
        <? endforeach; ?>
    </div>
    <? if (DEVICE_DESKTOP_OR_TABLET ): ?>
        <div class="l-banner">
            <?php if(($banner = Banners::view('index-right-with-cat')) ) { ?>
                <?= $banner; ?>
                <br> <br>
            <?php } ?>
            <?php if(($banner = Banners::view('index-right-with-cat-second')) ) { ?>
                <?= $banner; ?>
            <?php } ?>
        </div>
    <? endif; ?>

</div>
</div>

<div class="map-bg">
    <div class="container ">
        <? if (empty($map)) {?>
            <? if(empty($regionID)){?>
                <? if(DEVICE_DESKTOP): ?>
                    <div class="map-country j-show-country">
                        <?= $this->viewPHP($dataMap, 'map.modal')?>
                    </div>
                <? endif;?>
            <? } else {?>
                <div class="index-map__nomap"><?= _t('site','Для данного региона карта еще недоступна.') ?></div>
            <? } ?>
        <? } else { ?>
            <? if(empty($regionID)){?>
                <? if(DEVICE_DESKTOP): ?>
                    <div class="map-country j-show-country">
                        <?= $this->viewPHP($dataMap, 'map.modal')?>
                    </div>
                <? endif;?>
            <? } else {?>
                <div class="index-map  hidden-phone">
                    <?= $map ?>
                </div>
            <? } ?>
        <? } ?>

        <? if (!empty($regions)) { ?>
            <ul class="index-cities index-cities-right">
                <? $i = 0;
                foreach ($regions as $k => $reg) { ?>
                    <li>
                        <?php if ($reg['items'] > 0) { ?>
                            <a href="<?= $reg['l'] ?>"><strong><?= $reg['title'] ?></strong></a>
                        <?php } else { ?>
                            <span class="hidden-link" data-link="<?= $reg['l'] ?>"><strong><?= $reg['title'] ?></strong></span>
                        <?php } ?>
                    </li>
                    <? unset($regions[$k]);
                } ?>
            </ul>

            <? if($catCnt > 8): $catCnt -= 8; $catCnt = ceil($catCnt / 2) * 8; $i = 0; ?>
                <ul class="index-cities index-cities-right">
                    <? foreach ($regions as $k => $reg) {
                        if($reg['numlevel'] != Geo::lvlCity) continue;
                        if(++$i > $catCnt) break;
                        ?>
                        <li>
                            <?php if ($reg['items'] > 0) { ?>
                                <a href="<?= $reg['l'] ?>"><?= $reg['title'] ?></a>
                            <?php } else { ?>
                                <span class="hidden-link" data-link="<?= $reg['l'] ?>"><?= $reg['title'] ?></span>
                            <?php } ?>
                        </li>
                    <? } ?>
                </ul>
            <? endif; ?>
        <? } ?>
    </div>
</div>