<?php
/**
 * Статические страницы: просмотр
 * @var $this Site
 * @var $title string заголовок
 * @var $content string содержание
 */
?>

<?= tpl::getBreadcrumbs(array(array('title' => $title, 'active' => true))); ?>

<div class="l-mainLayout">

    <!-- Content -->
    <div class="l-mainLayout-content">
        <div class="l-pageHeading">
            <h1 class="l-pageHeading-title"><?= $title ?></h1>
        </div>
        <div class="l-pageContent">
            <?= $content ?>
            <? if ($aData['filename'] === 'adv'): ?>
                <div class="row">
                    <div class="col-md-10  col-md-offset-1">
                        <div class="mrgt30 mrgb10">
                            <div class="flex flex_sb flex_center flex_row_sm">
                                <h3 class="mrgt10">
                                    <?= _t('', 'Цены на размещение рекламы') ?>
                                </h3>
                                <div class="flex flex_center">
                                    <span class="mrgr5">
                                        <?= _t('', 'Цены содержат налог с оборота:') ?>
                                    </span>
                                    <strong>
                                        <a href="#" class="j-count-tax  j-count-tax-minus hidden">
                                            <?= _t('', 'Да') ?>
                                        </a>
                                    </strong>
                                    <strong>
                                        <a href="#" class="j-count-tax j-count-tax-plus">
                                            <?= _t('', 'Нет') ?>
                                        </a>
                                    </strong>
                                </div>
                            </div>
                        </div>
                        <div class="prices">
                            <div class="prices__header">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 prices__img-box ">
                                        <?= _t('','Баннер')?>
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-sm-5 prices__des-box ">
                                        <?= _t('','Описание')?>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 prices__time-box ">
                                        <?= _t('','Продолжительность')?>
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-2 prices__price-box  nowrap text-right">
                                        <?= _t('','Цена')?>, €
                                    </div>
                                </div>
                            </div>
                            <a class="prices__item collapsed" data-toggle="collapse" href="#collapseOne">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 prices__img-box ">
                                        <img src="<?= bff::url('/img/banner1.png')?>" alt="">
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-sm-5 prices__des-box ">
                                        <?= _t('','Баннер на странице объявления <br> 728 x 90')?>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 prices__time-box ">
                                        <p>
                                            1 <?= _t('','неделя')?>
                                        </p>
                                        <p>
                                            1 <?= _t('','месяц')?>
                                        </p>

                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-2 prices__price-box ">
                                        <p class="j-price text-right" data-def="2 500">
                                            2 500
                                        </p>
                                        <p class="j-price text-right" data-def="10 000">
                                            10 000
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div class="collapse" id="collapseOne">
                                <div class="prices__text row">
                                    <div class="col-md-8">
                                        <?= _t('','Баннер размещается на странице каждого объявления в десктоп-версии сайта и расположен над объявлением. Если в ротации находится несколько баннеров, показы распределяются равномерно. Средний CTR — 0,13%')?>
                                    </div>
                                </div>
                            </div>
                            <hr class="prices__bb">

                            <a class="prices__item collapsed" data-toggle="collapse" href="#collapseTwo">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 prices__img-box ">
                                        <img src="<?= bff::url('/img/banner2.png')?>" alt="">
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-sm-5 prices__des-box ">
                                        <?= _t('','Баннер в разделах объявлений и подразделах 160 x 600')?>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 prices__time-box ">
                                        <p>
                                            1 <?= _t('','неделя')?>
                                        </p>
                                        <p>
                                            1 <?= _t('','месяц')?>
                                        </p>

                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-2 prices__price-box ">
                                        <p class="j-price text-right" data-def="1 000">
                                            1 000
                                        </p>
                                        <p class="j-price text-right" data-def="4 000">
                                            4 000
                                        </p>
                                    </div>
                                </div>
                            </a>
                            <div class="collapse" id="collapseTwo">
                                <div class="prices__text row">
                                    <div class="col-md-8">
                                        <?= _t('','Баннер размещается в десктоп-версии сайта на странице каждого раздела объявлений и в подразделах. Если в ротации находится несколько баннеров, показы распределяются равномерно. Средний CTR — 0,05%')?>
                                    </div>
                                </div>
                            </div>
                            <hr class="prices__bb">

                            <a class="prices__item collapsed" data-toggle="collapse" href="#collapseThree">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 prices__img-box ">
                                        <img src="<?= bff::url('/img/banner3.png')?>" alt="">
                                    </div>
                                    <div class="col-lg-6 col-sm-6 col-sm-5 prices__des-box ">
                                        <?= _t('','Индивидуальное предложение Нестандартное размещение, размер и оформление рекламной кампании')?>
                                    </div>
                                </div>
                            </a>
                            <div class="collapse" id="collapseThree">
                                <div class="prices__text row">
                                    <div class="col-md-8">
                                        <?= _t('','Предлагаем обсудить возможность проведения рекламной кампании, которая будет оформлена и размещена нестандартно.')?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mrgt20">

                            <ul>
                                <li><?= _t('','Свяжитесь с нами при желании разместить рекламу на срок более одного месяца или в форме, иной от предложенной.')?></li>
                                <li><?= _t('','Баннеры могут быть трёх форматов: gif, jpeg, swf (flash). Размер файла не должен превышать 75 KB.')?></li>
                                <li><?= _t('','В стоимость рекламы не входит изготовление баннера.')?></li>
                            </ul>

                            <p>
                                <?= _t('','Если вы заинтересованы в размещении рекламы на Noomb.com или у вас есть дополнительные вопросы,')?>
                                <a href="<?= Contacts::url('form')?>" target="_blank"><?= _t('','свяжитесь с нами')?></a>
                            </p>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>

</div>
<? if ($aData['filename'] === 'adv'): ?>
<script type="text/javascript">
    <?php js::start(); ?>
    $(function () {
       $('.j-count-tax-plus').on('click', function (e) {
           e.preventDefault();
           $('.j-count-tax').toggleClass('hidden');
           $('.j-price').map(function (i, el) {
               var str = $(el).text();
               str = str.replace(/\s/g, '');
               $(el).text(str * 1.2);
           })
       });
        $('.j-count-tax-minus').on('click', function (e) {
            e.preventDefault();
            $('.j-count-tax').toggleClass('hidden');
            $('.j-price').map(function (i, el) {
                var str = $(el).data('def');
                str = str.replace(/\s/g, '');
                $(el).text(str);
            })
        });

    });
    <?php js::stop(); ?>
</script>
<? endif; ?>