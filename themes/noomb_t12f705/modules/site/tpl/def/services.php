<?php
/**
 * Страница "Услуги"
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $svc_bbs array данные об услугах для объявлений
 * @var $svc_shops array данные об услугах для магазинов
 * @var $shop_opened boolean открыт ли у текущего пользователя магазин
 * @var $shop_promote_url string URL на страницу продвижения магазина
 * @var $shop_open_url string URL на страницу открытия магазина
 * @var $user_logined boolean авторизован ли пользователь
 * @var $seotext string SEO-текст
 */
?>

    <div class="l-pageHeading text-center">
        <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('svc', 'Как продать быстрее?')) ?></h1>
    </div>

<?php if (!empty($svc_shops)) { ?>
    <ul class="nav nav-tabs text-center mrgb15" role="tablist">
        <li class="active"><a href="#tab-ads" aria-controls="tab-ads" role="tab" data-toggle="tab">
                <?= _t('svc', 'Объявления') ?>
            </a>
        </li>
        <li><a href="#tab-shops" aria-controls="tab-shops" role="tab" data-toggle="tab">
                <?= _t('svc', 'Магазин') ?>
            </a>
        </li>
    </ul>
<?php } ?>
<div class="row mrgt40">
    <div class="tab-content col-md-10 col-md-offset-1">

        <!-- Ads tab -->
        <div role="tabpanel" class="tab-pane active fade in" id="tab-ads">
            <div class=" promotion">
                <?php foreach ($svc_bbs as $v) { ?>
                    <div class="promotion__box">

                        <div class="promotion__label">
                            <span class="">
                                <img src="<?= $v['icon_s'] ?>" alt="">
                                <h3 class="promotion__title">
                                    <?= $v['title_view'] ?>
                                </h3>
                                <span class="promotion__des">
                                    <?= nl2br($v['description_full']) ?>
                                </span>
                            </span>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="l-serviceAds">
                <div class="l-serviceAds-item">
                    <div class="l-serviceAds-item-l">
                        <?= _t('svc', 'Подайте новое объявление и сделайте его заметным') ?>
                    </div>
                    <div class="l-serviceAds-item-r">
                        <a class="btn btn-success btn-block" href="<?= BBS::url('item.add') ?>">
                            <?= _t('svc', 'Добавить объявление') ?>
                        </a>
                    </div>
                </div>
                <?php if ($user_logined) { ?>
                    <div class="l-serviceAds-item">
                        <div class="l-serviceAds-item-l">
                            <?= _t('svc', 'Рекламируйте уже существующие объявления') ?>
                        </div>
                        <div class="l-serviceAds-item-r">
                            <a class="btn btn-default btn-block" href="<?= BBS::url('my.items') ?>">
                                <?= _t('svc', 'Мои объявления') ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>

        </div><!-- /#tab-ads -->

        <?php if (!empty($svc_shops)) { ?>
            <!-- Shops tab -->
            <div role="tabpanel" class="tab-pane fade" id="tab-shops">
                <div class="promotion">
                    <?php foreach ($svc_shops as $v) { ?>
                        <div class="promotion__box">

                            <div class="promotion__label">
                                <span class="">
                                    <img src="<?= $v['icon_s'] ?>" alt="">
                                    <h3 class="promotion__title">
                                        <?= $v['title_view'] ?>
                                    </h3>
                                    <span class="promotion__des">
                                        <?= nl2br($v['description_full']) ?>
                                    </span>
                                </span>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="l-serviceAds">
                    <?php if ($shop_opened) { ?>
                        <div class="l-serviceAds-item">
                            <div class="l-serviceAds-item-l">
                                <?= _t('shops', 'Рекламируйте свой магазин') ?>
                            </div>
                            <div class="l-serviceAds-item-r">
                                <a class="btn btn-success btn-block" href="<?= $shop_promote_url ?>">
                                    <?= _t('shops', 'Рекламировать') ?>
                                </a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="l-serviceAds-item">
                            <div class="l-serviceAds-item-l">
                                <?= _t('shops', 'Откройте свой магазин') ?>
                            </div>
                            <div class="l-serviceAds-item-r">
                                <a class="btn btn-success btn-block" href="<?= $shop_open_url ?>">
                                    <?= _t('shops', 'Открыть магазин') ?>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            </div><!-- /#tab-shops -->
        <?php } ?>

    </div>
</div>
<?php if (!empty($seotext)) { ?>
    <div class="l-seoText">
        <?= $seotext ?>
    </div>
<?php } ?>