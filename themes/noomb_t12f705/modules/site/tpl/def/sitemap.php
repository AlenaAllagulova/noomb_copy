<?php
/**
 * Карта сайта
 * @var $this Site
 * @var $breadcrumb string название хлебной крошки или пустая строка
 * @var $titleh1 string заголовок H1 или пустая строка
 * @var $cats array категории
 * @var $seotext string SEO-текст
 */
?>

<?php
echo tpl::getBreadcrumbs(array(array('title' => (!empty($breadcrumb) ? $breadcrumb : _t('', 'Карта сайта')), 'link' => '#', 'active' => true)));
?>

<div class="l-mainLayout">

    <!-- Content -->
    <div class="l-mainLayout-content">
        <div class="l-pageHeading">
            <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('', 'Карта сайта')) ?></h1>
        </div>
        <div class="cat-list">
            <?php foreach ($cats as &$row): ?>
                <? foreach ($row as &$c): ?>
                    <div class="cat-list__box">
                        <div class="mrgb15">
                            <a href="<?= $c['link'] ?>" class="cat-list__title ff-black">
                                <?= $c['title'] ?>
                            </a>
                        </div>

                        <ul class="cat-list__list">
                            <?php foreach ($c['subs'] as &$cc) { ?>
                                <li>
                                    <a href="<?= $cc['link'] ?>" class="cat-list__item">
                                        <?= $cc['title'] ?>
                                    </a>
                                </li>
                            <?php } unset($cc); ?>
                            <a href="<?= $c['link'] ?>" class="cat-list__more">
                                <?= _t('', 'SHOW MORE') ?> &gt;
                            </a>
                        </ul>
                    </div>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>


        <div class="l-seoText"><?= $seotext; ?></div>

    </div>

</div>