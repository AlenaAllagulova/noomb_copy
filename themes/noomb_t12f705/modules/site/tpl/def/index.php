<?php
/**
 * Главная страница
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $centerBlock string центральный блок
 * @var $last string блок последних / премиум объявлений (HTML)
 * @var $seotext string SEO-текст
 */
View::setLayout('general');
?>

<!-- Desktop or Tablet -->
<?php if (DEVICE_DESKTOP_OR_TABLET) { tpl::includeJS('site.index', false, 1); ?>
  <?php if ( ! empty($titleh1) ) { ?>
  <div class="index-title">
    <h1 class="align-center hidden-xs"><?= $titleh1; ?></h1>
  </div>
  <?php } ?>

    <?= $centerBlock ?>


    <?php if($banner = Banners::view('site_index_last_before')) { ?>
        <div class="container">
            <div class="l-banner-h mrgb30 mrgt20">
                <?= $banner; ?>
            </div>
        </div>
    <?php } ?>
    <div class="premium-general">
        <div class="container relative">
            <img src="<?= bff::url('/img/premium.png')?>" alt="" class="premium-general__img">
          <?= $last; // /modules/bbs/tpl/def/index.last.block.php ?>
          <?= $lastBlog // /modules/blog/tpl/def/index.last.block.php ?>
        </div>
    </div>

    <div class="container">
      <?php if($banner = Banners::view('site_index_last_after')) { ?>
      <div class="l-banner-h mrgb0 mrgt40">
        <?= $banner; ?>
      </div>
      <?php } ?>

      <div class="l-seoText hidden"><?= $seotext; ?></div>
    </div>

<?php } ?>

<!-- Mobile -->
<?php if (DEVICE_PHONE) { ?>
    <div class="container">
  
      <?php if($banner = Banners::view('index_mobile')) { ?>
      <div class="l-banner-m">
        <?= $banner; ?>
      </div>
      <?php } ?>

      <!-- Mobile Home Page Categories -->
      <div class="mobile-container">
        <div id="j-f-cat-phone-index-step1">
          <?= BBS::i()->catsList('index', bff::DEVICE_PHONE, 0); ?>
        </div>
        <div id="j-f-cat-phone-index-step2" class="hide"></div>
      </div>
    </div>
    <div class="premium-general">
        <div class="container relative">
            <img src="<?= bff::url('/img/premium.png')?>" alt="" class="premium-general__img">
          <?= $last ?>
        </div>
    </div>
<?php } ?>