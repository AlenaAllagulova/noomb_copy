<?php
/**
 * Layout: основной каркас
 * @var $centerblock string содержимое (HTML)
 */
?>
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" class="no-js">
<head>
<?php View::blockStart('head'); ?>
<?= SEO::i()->metaRender(array('content-type'=>true,'csrf-token'=>true)) ?>
<?= View::template('css'); ?>
<?php View::blockEnd(); ?>
</head>
<body>
<?php View::blockStart('body'); ?>
  <?= View::template('alert'); ?>
  <div class="l-page">
    <!-- Top Banner -->
    <?php if( DEVICE_DESKTOP_OR_TABLET && ($bannerTop = Banners::view('site_top')) ) { ?>
    <div class="l-banner-top">
      <?= $bannerTop; ?>
    </div>
    <?php } ?>
    <!-- Header -->
    <?= View::template('header'); ?>
    <!-- Filter -->
      <? if(DEVICE_DESKTOP_OR_TABLET): ?>
          <section class="video-box" role="banner" style="background-image:url(<?= bff::url('/video/videoplayback_000.jpg')?>);">
              <video poster="<?= bff::url('/video/videoplayback_000.jpg')?>" autoplay="" loop="" muted="" class="video-box__video">
                  <source src="<?= bff::url('/video/videoplayback.mp4')?>" type="video/mp4">
                  <source src="<?= bff::url('/video/videoplayback.ogg')?>" type="video/ogg">
              </video>
          </section>
      <? endif; ?>

    <?= View::template('filter'); ?>
    <!-- Content -->

      <?php if( DEVICE_DESKTOP_OR_TABLET && ($bannerTop = Banners::view('site_top_after_header')) ) { ?>
          <div class="l-banner-top mrgt10">
              <?= $bannerTop; ?>
          </div>
      <?php } ?>

    <div class="l-content">
        <?= $centerblock; ?>
    </div>
  </div>

  <?php if(DEVICE_DESKTOP_OR_TABLET) { ?>
    <!-- Back to top -->
    <p class="c-scrolltop" id="j-scrolltop" style="display: none;">
      <a href="#"><span><i class="fa fa-arrow-up"></i></span></a>
    </p>
  <?php } ?>

  <!-- Footer -->
  <?= View::template('footer'); ?>
<?php View::blockEnd(); ?>
</body>
</html>