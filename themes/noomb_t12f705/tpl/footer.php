<?php
/**
 * Футер сайта
 */
if (config::get('tpl.footer.hide', false, TYPE_BOOL)) {
    return;
}
$footerMenu = Sitemap::view('footer');
$footerLink = function ($item, $extraClass = '') {
    if (!empty($item['a'])) {
        return '<li>' . $item['title'] . '</li>';
    }
    return '<li><a href="' . $item['link'] . '"' . ($item['target'] === '_blank' ? ' target="_blank"' : '') . ' class="' . (!empty($item['style']) ? $item['style'] : '') . (!empty($extraClass) ? ' ' . $extraClass : '') . '">' . $item['title'] . '</a></li>';
};
$footerText = Site::footerText();
$counters = Site::i()->getCounters();
$suffixCountryCode = 'footer';

?>
<!-- BEGIN footer -->
<? if (DEVICE_DESKTOP_OR_TABLET): ?>
    <div class="l-footer">
        <div class="container">

            <div class="l-footer__top-nav">
                <div class="flex flex_center">
                    <a href="<?= bff::urlBase() ?>" class="footer-logo">
                        <img src="<?= Site::logoURL('header') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>"/>
                    </a>
                    <ul class="footer-nav">
                        <li>
                            <a href="">
                                <?= _t('','Transport')?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <?= _t('','Real estate')?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <?= _t('','Jobs')?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <?= _t('','More...')?>
                            </a>
                        </li>
                    </ul>
                </div>
                <ul class="footer-social">
                    <li>
                        <a href="https://www.facebook.com/" class="fb" target="_blank">
                            <i class=" icon-facebook-logo"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/" class="tw" target="_blank">
                            <i class=" icon-twitter "></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/" class="insta" target="_blank">
                            <i class=" icon-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <?php if (!empty($footerText)) { ?>
                        <div  class="app-box__input-box">
                            <?= $footerText; ?>
                        </div>
                    <?php } ?>
                    <div class="app-box">
                        <div class="app-box__btn mrgb10">
                            <a href="https://itunes.apple.com/us/app/noomb/id1440614401?ls=1&mt=8" target="_blank" class="btn btn-app">
                                <i class=" icon-apple "></i>
                                <?= _t('app_link_apple','App Store')?>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=noomb.com" class="btn btn-app" target="_blank">
                                <i class=" icon-google-play "></i>
                                <?= _t('app_link_google','Google Play')?>
                            </a>
                        </div>
                        <? if(Site::isSendAppLink()): $sIdInputAppSendLink = 'j-s-phone-app-link'; ?>
                        <div class="app-box__input-box">
                            <span class="fz-12 color-light mrgb5">
                                <?= _t('app-get-link-title','Enter your mobile number to get the app')?>
                            </span>
                            <div class="flex">
                                <?= Users::i()->registerPhoneInput(
                                        [
                                            'id'=>$sIdInputAppSendLink,
                                            'name'=>'phone_link',
                                            'type' => 'tel',
                                            'placeholder' => _t('',
                                                                '+[code] (___) ___ - __ - __',
                                                                ['code'=> Geo::i()->countriesList()[Geo::i()->defaultCountry()]['phone_code']]
                                                                ),
                                        ],
                                        [
                                            'suffix' => $suffixCountryCode,
                                        ]
                                ); ?>
                                <button class="btn btn-default-accent btn_small-width" onclick="AppLink.send('<?=$sIdInputAppSendLink;?>')" >
                                    <?= _t('app-link-send-btn','SEND')?>
                                </button>
                            </div>
                        </div>
                        <? endif;?>
                    </div>
                </div>
                <?php $footerCols = array('col1' => ['w' => '3'], 'col2' => ['w' => '3'], 'col3' => ['w' => '3', 'last' => true]);
                foreach ($footerCols as $colKey => $colData):
                    if (!empty($footerMenu[$colKey]['sub'])) { ?>
                        <div class="col-sm-<?= $colData['w'] ?>">
                            <ul class="l-footer-menu"><?php
                                foreach ($footerMenu[$colKey]['sub'] as $v):
                                    echo $footerLink($v);
                                endforeach; ?>
                            </ul>
                        </div>
                    <?php }
                endforeach; ?>
                <div class="col-sm-2 text-right">
                    <?= Site::languagesSwitcher(); # Выбор языка  ?>
                    <div class="l-footer-counters">
                        <?php if (!empty($counters)) { ?>
                            <?php foreach ($counters as $v) { ?>
                                <div class="item"><?= $v['code'] ?></div><?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="footer-partner pdb30 pdt30">
                    <a target="_blank" href="https://www.mastercard.com">
                        <img src="<?= bff::url('/img/pay/mastercard.png')?>" alt="">
                    </a>
                    <a target="_blank" href="http://visa.com">
                        <img src="<?= bff::url('/img/pay/visa.png')?>" alt="">
                    </a>
                    <a target="_blank" href="https://www.paypal.com ">
                     <img src="<?= bff::url('/img/pay/paypal.png')?>" alt="">
                    </a>
                    <a target="_blank" href="https://www.apple.com/apple-pay/">
                        <img src="<?= bff::url('/img/pay/apple-pay-logo.png')?>" alt="">
                    </a>
                    <a target="_blank" href="https://www.samsung.com/us/samsung-pay/">
                        <img src="<?= bff::url('/img/pay/Samsung_Pay_Logo.png')?>" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="l-footer__end">
            <div class="container text-center">
                <div class="footer-partner">
                    <a href="https://about.holvi.com " target="_blank">
                        <img style="max-width: 80%" src="<?= bff::url('/img/partner/12.png') ?>" alt="">
                    </a>
                    <a href="https://e-estonia.com" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/3.png') ?>" alt="">
                    </a>
                    <a href="http://wallonia.be/fr" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/9.png') ?>" alt="">
                    </a>
                    <a href="https://e-resident.gov.ee" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/4.png') ?>" alt="">
                    </a>
                    <a href="http://digitalbelgium.be" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/1.png') ?>" alt="">
                    </a>
                    <a href="https://e-resident.gov.ee/become-an-e-resident/" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/2.png') ?>" alt="">
                    </a>
                    <a href="http://invest-export.brussels/fr_FR/" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/5.png') ?>" alt="">
                    </a>
                    <a href="https://vatlayer.com" target="_blank">
                        <img style="max-width: 85%" src="<?= bff::url('/img/partner/8.png') ?>" alt="">
                    </a>
                    <a href="https://www.intrum.be/nl/klanten-van-opdrachtgevers/" target="_blank">
                        <img style="max-width: 80%" src="<?= bff::url('/img/partner/10.png') ?>" alt="">
                    </a>
                    <a href="https://www.digitalwallonia.be/fr/publications/dwhub" target="_blank">
                        <img width="70" src="<?= bff::url('/img/partner/7.png') ?>" alt="">
                    </a>
                    <a href="https://www.sofort.com/" target="_blank">
                        <img src="<?= bff::url('/img/pay/sofort_logo-weiss.png')?>" alt="">
                    </a>
                    <a href="https://eugdpr.org" target="_blank">
                        <img src="<?= bff::url('/img/partner/11.png') ?>" alt="">
                    </a>
                    <a href="https://startups.be" target="_blank">
                        <img src="<?= bff::url('/img/partner/6.png') ?>" alt="">
                    </a>
                </div>
                <?= Site::copyright(); ?>
            </div>
        </div>
    </div>
<? else: ?>
    <div class="l-footer">
        <div class="container pdb20">

            <div class="row">
                <div class="col-sm-4 mrgb20 pdt20">
                    <div class="app-box">
                        <div class="app-box__btn mrgb10">
                            <a href="https://itunes.apple.com/us/app/noomb/id1440614401?ls=1&mt=8" class="btn btn-app" target="_blank">
                                <i class=" icon-apple "></i>
                                <?= _t('app_link_apple_mob','App Store')?>
                            </a>
                            <a href="#" class="btn btn-app">
                                <i class=" icon-google-play "></i>
                                <?= _t('app_link_google_mob','Google Play')?>
                            </a>
                        </div>
                        <? if(Site::isSendAppLink()): $sIdInputAppSendLink = 'j-app-link-footer'; ?>
                        <div class="app-box__input-box">
                            <span class="fz-12 color-light mrgb5">
                                <?= _t('','Enter your mobile number to get the app')?>
                            </span>
                            <div class="flex">
                                <?= Users::i()->registerPhoneInput(
                                    [
                                        'id'=>$sIdInputAppSendLink,
                                        'name'=>'phone_link',
                                        'type' => 'tel',
                                        'placeholder' => _t('',
                                            '+[code] (___) ___ - __ - __',
                                            ['code'=> Geo::i()->countriesList()[Geo::i()->defaultCountry()]['phone_code']]
                                        ),
                                    ],
                                    [
                                        'suffix' => $suffixCountryCode,
                                    ]
                                ); ?>
                                <button class="btn btn-default-accent btn_small-width" onclick="AppLink.send('<?=$sIdInputAppSendLink;?>')" >
                                    <?= _t('app-link-send-btn','SEND')?>
                                </button>
                            </div>
                        </div>
                        <?endif;?>
                    </div>

                </div>
                <?php $footerCols = array('col1' => ['w' => '3'], 'col2' => ['w' => '3'], 'col3' => ['w' => '3', 'last' => true]);
                foreach ($footerCols as $colKey => $colData):
                    if (!empty($footerMenu[$colKey]['sub'])) { ?>
                        <div class="col-sm-<?= $colData['w'] ?>">
                            <ul class="l-footer-menu"><?php
                                foreach ($footerMenu[$colKey]['sub'] as $v):
                                    echo $footerLink($v);
                                endforeach; ?>
                            </ul>
                        </div>
                    <?php }
                endforeach; ?>
                <div class="col-sm-2 ">
                    <?= Site::languagesSwitcher(); # Выбор языка  ?>
                    <div class="l-footer-counters">
                        <?php if (!empty($counters)) { ?>
                            <?php foreach ($counters as $v) { ?>
                                <div class="item"><?= $v['code'] ?></div><?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="l-footer__end">
            <div class="container text-center">
                <?= Site::copyright(); ?>
            </div>
        </div>
    </div>
<? endif; ?>
<!-- Scripts -->
<?= View::template('js'); ?>
<?= js::renderInline(js::POS_FOOT); ?>
<script>
    // Tooltips and popovers
    $(document).ready(function () {
        $('.has-tooltip').tooltip();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
    <? if(Site::isSendAppLink()):?>
        AppLink.initPhoneCodes('<?=$suffixCountryCode?>');
    <? endif;?>
</script>
