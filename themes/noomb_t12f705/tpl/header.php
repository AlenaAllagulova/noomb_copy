<?php
/**
 * Website Header
 */
$url = array(
    'item.add' => BBS::url('item.add'),
    'user.login' => Users::url('login'),
    'user.register' => Users::url('register'),
    'user.logout' => Users::url('logout'),
);
$clientLang = Site::getClientLang();

$countriesList = Geo::countriesList();
foreach ($countriesList as &$v) {
    $v['link'] = bff::urlBase(true, $clientLang).$v['keyword'].'/search/';
} unset($v);
$suffixCountryCode = 'header';

#Icons for currency
$currencyData = Site::model()->currencyData(false);
foreach ($currencyData as &$currency){
    $currency['fa-icon'] = '';
    if($currency['keyword'] == 'usd') $currency['fa-icon'] = '$';
    if($currency['keyword'] == 'eur') $currency['fa-icon'] = '€';

}unset($currency);
$dataMap = [
    'clientLang' => $clientLang,
    'countriesList'=> $countriesList,
];
?>

<? if(!DEVICE_PHONE): ?>
<div class="banner-top-apps ready j-banner-top-aps-block hidden">
    <div class="container">
        <div class="banner-top-apps__box">
            <div class="banner-top-apps__left">
                <div class="banner-top-apps__title mrgr20">
                    <?= _t('header-baner-app-title',' Get your FREE Noomb app here')?>
                </div>
                <div class="flex">
                    <a href="https://itunes.apple.com/us/app/noomb/id1440614401?ls=1&mt=8" target="_blank" class="banner-top-apps__link mrgr20">
                        <i class=" icon-apple "></i>
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=noomb.com" target="_blank" class="banner-top-apps__link mrgr20">
                        <i class=" icon-google-play "></i>
                    </a>
                </div>
            </div>

            <? if(Site::isSendAppLink()): $sIdInputAppSendLink = 'j-app-link-header'; ?>
                <div class="flex flex_center banner-top-apps__right">
                    <span class="banner-top-apps__text mrgr10">
                        <?= _t('app-get-link-title', 'Enter your mobile number to get the app')?>
                    </span>
                    <div class="flex">
                        <?= Users::i()->registerPhoneInput(
                            [
                                'id'=>$sIdInputAppSendLink,
                                'name'=>'phone_link',
                                'type' => 'tel',
                                'placeholder' => _t('',
                                    '+[code] (___) ___ - __ - __',
                                    ['code'=> Geo::i()->countriesList()[Geo::i()->defaultCountry()]['phone_code']]
                                ),
                            ],
                            [
                                'suffix' => $suffixCountryCode,
                                'input_class' => 'banner-top-apps__input',
                            ]
                        ); ?>
                        <button class="btn banner-top-apps__btn" onclick="AppLink.send('<?=$sIdInputAppSendLink;?>')" >
                            <?= _t('app-link-send-btn','SEND')?>
                        </button>
                    </div>
                </div>
            <? endif;?>
        </div>
    </div>
    <span class="banner-top-apps__close j-close-banner-top-aps">
        <i class="icon-close "></i>
    </span>
</div>
<? endif; ?>
<div class="l-header mrgb0">
    <div class="l-header__top">
        <div class="container">
            <div class="l-header__top-container">
                <div class="flex flex_center l-header__top-first">
                    <a href="<?= bff::urlBase() ?>" class="l-header__logo" style="margin-top: -4px;">
                        <img src="<?= bff::url('/img/do-logo-white.png') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>"/>
                    </a>

                    <div class="dropdown">
                        <a id="menu-drop" href="#" class="l-header__burger" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </a>

                        <ul class="dropdown-menu mrgt15" aria-labelledby="menu-drop">
                            <? $mainMenu = Sitemap::view('main');
                            foreach ($mainMenu as $k => $v): ?>
                                <li<? if ($v['a']): ?> class="active"<? endif; ?>>
                                    <a class="main-link" href="<?= $v['link'] ?>"<?= ($v['target'] === '_blank' ? ' target="_blank"' : '') ?>>
                                        <?= $v['title'] ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                            <li class="visible-sm visible-xs">
                                <a href="<?= BBS::url('items.search') ?>">
                                    <?= _t('header','Offers')?>
                                </a>
                            </li>
                            <li class="visible-sm visible-xs">
                                <a href="<?= Shops::url('search') ?>">
                                    <?= _t('header','Shops')?>
                                </a>
                            </li>
                            <li class="visible-sm visible-xs">
                                <a href="<?= Site::url('page', array('filename' => 'adv'))?>">
                                    <?= _t('header','PRO')?>
                                </a>
                            </li>
                            <li class="visible-sm visible-xs">
                                <a href="<?= Help::url('search')?>">
                                    <?= _t('header','Help')?>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <ul class="l-header__top-nav visible-md visible-lg">
                        <li class="l-header__user-links">
                            <a href="<?= BBS::url('items.search') ?>">
                                <?= _t('header','Offers')?>
                            </a>
                        </li>
                        <li class="l-header__user-links">
                            <a href="<?= Shops::url('search') ?>">
                                <?= _t('header','Shops')?>
                            </a>
                        </li>
                        <li class="l-header__user-links">
                            <a href="<?= Site::url('page', array('filename' => 'adv'))?>">
                                <?= _t('header','PRO')?>
                            </a>
                        </li>
                        <li class="l-header__user-links">
                            <a href="<?= Help::url('search')?>">
                                <?= _t('header','Help')?>
                            </a>
                        </li>
                    </ul>

                </div>

                <div class="l-header__top-second">
                    <?php if (!User::id()): ?>
                        <? $favsCounter = BBS::i()->getFavorites(0, true); ?>
                        <div class="l-header__user-links">
                            <span class="relative mrgr30" style="display: inline-block;">
                                <?= Site::languagesSwitcherSmall(); # Выбор языка  ?>
                            </span>
                            <? if ($favsCounter): ?>
                                <a href="<?= BBS::url('my.favs') ?>" class="mrgr30">
                                    <i class="fa fa-star"></i>
                                    <span class="j-cnt-fav"><?= $favsCounter ?></span>
                                </a>
                            <? endif; ?>
                            <a href="<?= $url['user.login']; ?>" class="mrgr30 color-green border-right">
                                <?= _t('header','Войдите')?>
                            </a>
                            <a href="<?= $url['user.register']; ?>" class=" color-green">
                                <?= _t('header','Зарегистрируйтесь')?>
                            </a>
                        </div>
                    <? else: ?>
                        <? $userMenu = Users::i()->my_header_menu(); ?>
                        <div class="l-header-user-buttons l-header__user-links ">
                            <span class="relative mrgr20 headerlang" style="display: inline-block;">
                                <?= Site::languagesSwitcherSmall(); # Выбор языка  ?>
                            </span>
                            <? if(!empty($userMenu['user']['cnt_internalmail_new'])): ?>
                            <a href="<?= $userMenu['menu']['messages']['url'] ?>" class=" mrgr20">
                                <i class="fa fa-comment"></i>
                                <span class="<?= (!$userMenu['user']['cnt_internalmail_new'] ? ' hide' : '') ?> j-cnt-msg">
                                    <?= $userMenu['user']['cnt_internalmail_new'] ?>
                                </span>
                            </a>
                            <? endif; ?>

                            <? if(!empty($userMenu['user']['cnt_items_fav'])): ?>
                                <a href="<?= $userMenu['menu']['favs']['url'] ?>" class=" mrgr20">
                                    <i class="fa fa-star"></i>
                                    <span class="<?= (!$userMenu['user']['cnt_items_fav'] ? ' hide' : '') ?> j-cnt-fav">
                                        <?= $userMenu['user']['cnt_items_fav'] ?>
                                    </span>
                                </a>
                            <? endif; ?>
                            <a href="<?= $userMenu['menu']['bill']['url'] ?>" class=" mrgr20">
                                <i class=" icon-wallet mrgr5"></i>
                                <span><?=Bills::i()->security->getUserBalance(true)?></span>

                                <? if(!empty($currencyData[Site::currencyDefault('id')]['fa-icon'])):?>
                                    <?= $currencyData[Site::currencyDefault('id')]['fa-icon']?>
                                <? endif;?>
                            </a>
                            <div class="l-header-user-buttons-dropdown dropdown">
                                <a href="#" class="ff-bold" data-toggle="dropdown">
                                    <i class="fa fa-user"></i>
                                    <span class="hidden-xs"><?= tpl::truncate($userMenu['user']['name'], 20) ?></span>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right mrgt15">
                                    <?php foreach ($userMenu['menu'] as $k => $v): if ($v == 'D') { ?>
                                        <li class="divider"></li>
                                    <?php } else { ?>
                                        <li>
                                            <a href="<?= $v['url'] ?>" class="ico">
                                                <i class="<?= $v['i'] ?>"></i>
                                                <?= $v['t'] ?>
                                            </a>
                                        </li>
                                    <?php } endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>


</div><!-- /.l-header -->

<a href="<?= $url['item.add'] ?>" class="order-add-btn">
    <span class="order-add-btn__text">
        <?= _t('','Добавить объявление')?>
    </span>
    <i class="order-add-btn__icon fa fa-plus"></i>
</a>

<? if(DEVICE_DESKTOP): ?>
<div class="modal modal-noomb fade bs-example-modal-lg" id="j-show-country-modal" tabindex="-1" role="dialog" aria-labelledby="j-show-country-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
                <h4 class="modal-title">
                    <?= _t('','Choose your country')?>
                </h4>
            </div>
            <div class="modal-body map-country">
                <?= Site::i()->viewPHP($dataMap, 'map.modal')?>
                <ul class="country-list">
                    <li>
                        <a href="<?= BBS::url('items.search') ?>">
                            <img src="<?= bff::url('/img/flags/european-union.svg')?>" alt="">
                            <?= _t('','International')?>
                        </a>
                    </li>
                    <? foreach ($countriesList as $v):?>
                        <li>
                            <a href="<?= $v['link'] ?>"
                               data="{id:<?= $v['id'] ?>,pid:<?= $v['pid'] ?>,key:'<?= $v['keyword'] ?>', noregions:<?= !empty($v['filter_noregions']) ? 1 : 0 ?>}">
                                <img src="<?= bff::url('/img/flags/'.$v['keyword'].'.svg')?>" alt="">
                                <?= $v['title'] ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<? endif; ?>

<script>
    <? js::start(); ?>
    <? if(Site::isSendAppLink()):?>
        AppLink.initPhoneCodes('<?=$suffixCountryCode?>');
    <? endif;?>
    <? js::stop()?>
</script>
