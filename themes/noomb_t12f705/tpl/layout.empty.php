
<?php
/**
 * Layout: для страниц сокращенного вида
 * @var $centerblock string содержимое (HTML)
 */
?>
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" class="no-js">
<head>
    <?php View::blockStart('head'); ?>
    <?= SEO::i()->metaRender(array('content-type'=>true,'csrf-token'=>true)) ?>
    <?= View::template('css'); ?>
    <?php View::blockEnd(); ?>
</head>
<body>
<?php View::blockStart('body'); ?>
<?= View::template('alert'); ?>

<div class="empty-layout">
    <?= $centerblock; ?>
</div>

<?= View::template('js'); ?>
<?= js::renderInline(js::POS_FOOT); ?>
<?php View::blockEnd(); ?>
</body>
</html>