<?php
/**
 * Переключатель языка
 * @var $lang string ключ текущего языка
 * @var $languages array список языков
 */
if (sizeof($languages) > 1) { ?>
    <div class="fz-12 color-light mrgb5">
        <?= _t('lang_select','Choose your language')?>
    </div>
    <div class="l-footer-lang dropdown">
        <a data-current="<?= HTML::escape($lang) ?>" href="#" class="dropdown-select w100p flex flex_sb flex_center" data-toggle="dropdown">
            <span>
                <?= $languages[$lang]['title'] ?>
            </span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu dropdown-menu-right w100p">
            <?php foreach ($languages as $k => $v): ?>
                <li<?php if ($v['active']) { ?> class="active"<?php } ?>>
                    <a href="<?= ($v['active'] ? 'javascript:void(0);' : $v['url']) ?>">
                      <span class="country-icon-element">
                          <span class="country-icon country-icon-<?= $v['country'] ?>"></span>
                          <?= $v['title'] ?>
                      </span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php }