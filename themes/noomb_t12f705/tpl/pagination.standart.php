<?php
/**
 * Постраничная навигация: стандартный вид
 * @var $pages array ссылки на страницы
 * @var $first array первая страница
 * @var $last array последняя страница
 * @var $total integer общее кол-во страниц
 * @var $settings array дополнительные настройки
 */

# данные отсутствуют (общее их кол-во == 0)
if( $total <= 1 || ! $settings['pages'] ) return;

?>
<!-- START pagination.standart -->
<div class="j-pgn-pages hidden-xs text-center">
  <ul class="pagination">
    <?php if($first) { ?><li><a<?= $first['attr'] ?>><?= $first['page'] ?></a></li><li><a<?= $first['dots'] ?>>...</a></li><?php } ?>
    <?php foreach($pages as $v) { ?>
    <li<?php if($v['active']){ ?> class="active"<?php } ?>><a<?= $v['attr'] ?>><?= $v['page'] ?></a></li>
    <?php } ?>
    <?php if($last) { ?><li><a<?= $last['dots'] ?>>...</a></li><?php if ($settings['pagelast']) { ?><li><a<?= $last['attr'] ?>><?= $last['page'] ?></a></li><?php } } ?>
  </ul>
</div>
<!-- END pagination.standart -->