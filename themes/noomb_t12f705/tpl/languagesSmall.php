<?php
/**
 * Переключатель языка
 * @var $lang string ключ текущего языка
 * @var $languages array список языков
 */
if (sizeof($languages) > 1) { ?>
    <? $langsTitles = empty(config::sys('locale.short.titles')) ? [] : config::sys('locale.short.titles'); ?>
    <div class="dropdown">
        <a data-current="<?= HTML::escape($lang) ?>" href="#" class="" data-toggle="dropdown">
            <span>
                <?= (array_key_exists($lang, $langsTitles) && ! empty($langsTitles[$lang])) ? $langsTitles[$lang] : $languages[$lang]['country'] ?>
            </span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu dropdown-menu-right" style="min-width: auto;">
            <?php foreach ($languages as $k => $v): ?>
                <li <?= $v['active'] ? 'class="active"' : '' ?>>
                    <a href="<?= ($v['active'] ? 'javascript:void(0);' : $v['url']) ?>">
                      <span>
                          <?= (array_key_exists($k, $langsTitles) && ! empty($langsTitles[$k])) ? $langsTitles[$k] : $v['country'] ?>
                      </span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php }