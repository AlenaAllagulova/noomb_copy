<?php

class Theme_Noomb_t12f705 extends Theme
{

    const MIN_ALLOWED_SUBCATS = 5;
    const MAX_ALLOWED_SUBCATS = 200;

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'  => 't12f7054a48e719e8775f277f442f3170f8e0158',
            'theme_title'   => 'noomb',
            'theme_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            # максимально допустимое видимое кол-во подкатегорий, integer BBS $nSubSlice
            'bbs.index.subcats.limit' => [
                'title' => $this->lang('Max allowed visible number of subcategories'),
                'input' => 'number',
                'min' => self::MIN_ALLOWED_SUBCATS, // Минимально допустимое значение
                'max' => self::MAX_ALLOWED_SUBCATS, // Максимально допустимое значение
                'onChange'  => function($newValue, $oldValue) {
                    # проверкa нового значения:
                    if (empty($newValue) || $newValue > self::MAX_ALLOWED_SUBCATS || $newValue < self::MIN_ALLOWED_SUBCATS) {
                        $this->errors->set($this->lang(
                            'Error max allowed number of subcategories, valid values ​​from [min] to [max] items',
                            [
                                'min' => self::MIN_ALLOWED_SUBCATS,
                                'max' => self::MAX_ALLOWED_SUBCATS,
                            ]
                        ));
                        return false;
                    }
                    return $newValue;
                },
            ],
        ));
        
        $this->cssEdit(array(
            static::CSS_FILE_MAIN => ['path' => $this->path('/static/css/main.css', false), 'save' => true],
            static::CSS_FILE_CUSTOM => ['path' => $this->path('/static/css/custom.css', false), 'save' => true],
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        \bff::hookAdd('css.extra', function() {
            tpl::includeCSS('dist/noomb');
        });

        \bff::hookAdd('js.extra', function() {
            tpl::includeJS(['dist/noomb', 'dist/applink']);
        });
    }
}
