'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AppLink = function () {
    function AppLink() {
        _classCallCheck(this, AppLink);
    }

    _createClass(AppLink, null, [{
        key: 'initPhoneCodes',
        value: function initPhoneCodes(suffix) {
            app.user.phoneInput('.j-phone-number-' + suffix);
        }
    }, {
        key: 'send',
        value: function send(input_id) {
            bff.ajax(bff.ajaxURL('site&ev=sendAppLink'), {
                phone: $('#' + input_id).val()
            }, function (data, errors) {
                if (data && data.success) {
                    app.alert.success(data.success_msg);
                } else {
                    app.alert.error(errors);
                }
            });
        }
    }]);

    return AppLink;
}();
//# sourceMappingURL=applink.js.map
