class AppLink{

    static initPhoneCodes(suffix){
        app.user.phoneInput('.j-phone-number-'+suffix);
    }

    static send(input_id){
        bff.ajax(bff.ajaxURL('site&ev=sendAppLink'),
            {
                phone: $('#'+input_id).val(),
            },
            function (data, errors) {
                if (data && data.success) {
                    app.alert.success(data.success_msg);
                }else {
                    app.alert.error(errors);
                }
            }
        );
    }
}