$(function(){
    function BannerTopAps() {
        var box = $('.j-banner-top-aps-block');
        var btn = $('.j-close-banner-top-aps');

        // if(sessionStorage.getItem("banner-top-apps") !== "true"){
        //     box.removeClass('hidden');
        // }https://clazion.myjetbrains.com/youtrack/issue/NMBC-75
        btn.on('click', function () {
            sessionStorage.setItem("banner-top-apps", "true");
            box.remove('');
        });
    }
    BannerTopAps();

    $(document).ready(function(){
        $('.j-link-country-map').on('mouseover',function (e) {
            var region_data = $(this).data('key');
            var b = $('body');
            var modal = $('#j-show-country-modal');
            $('<div class="index-map__info country-info"><div>'+
                region_data +
                '</div></div>'
            ).appendTo(b);
            $('<div class="index-map__info country-info"><div>'+
                region_data +
                '</div></div>'
            ).appendTo(modal);
        }).on('mouseleave',function () {
            $('.index-map__info.country-info').remove();
        }).on('mousemove',function(e) {
            var mouseX = e.pageX,
                mouseY = e.pageY;
            var $mapInfo = $('.index-map__info.country-info');
            $mapInfo.css({
                top: mouseY + 20,
                left: mouseX + 20
            });
        }).on('click',function(e){
            var region_data = $(this).data('region');
            window.location.href = region_data + '/search/';
        });

        function ShowCountryModal() {
            $('#j-show-country-modal').modal({
                show: true
            });
            localStorage.setItem('ShowCountryModal', 'true');
        }
        if(localStorage.getItem("ShowCountryModal") !== "true"){
            ShowCountryModal();
        }

    });
});