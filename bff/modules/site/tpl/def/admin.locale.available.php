<?php

/**
 * @var $this Site
 * @var $adminSettings bool
 */

$locale = bff::locale();
$languageDefault  = $locale->getDefaultLanguage();
$languageDefaultAdmin = config::sysAdmin('locale.default.admin', $languageDefault);
$languagesList    = $locale->getLanguages(false);
$languagesListAll = $locale->getLanguageSettingsAll();
$languagesListAdmin = $locale->getAdminLocalesList(); # Доступные в админ. панели
$languagesHidden = config::sysAdmin('locale.hidden', array(), TYPE_ARRAY); # Скрытые от пользователей сайта
foreach ($languagesList as $k=>&$v) {
    $v['default'] = ($k === $languageDefault);
    $v['admin'] = array_key_exists($k, $languagesListAdmin);
    $v['hidden'] = in_array($k, $languagesHidden);
} unset($v);

tpl::includeJS(['ui.sortable','tablednd']);

?>

<div id="j-site-locale-available-block" class="well well-small">
    <table class="table table-condensed table-striped table-hover admtbl j-selected-block">
        <thead>
            <tr class="nodrag nodrop">
                <th class="left"><?= _t('site', 'Язык'); ?></th>
                <th width="150" colspan="2" class="bold"><?= ($adminSettings ? _t('site', 'Публичный сайт') : ''); ?></th>
                <?php if ($adminSettings) { ?>
                <th width="150" colspan="2" class="bold"><?= _t('site', 'Админ панель'); ?></th>
                <?php } ?>
                <th width="65"></th>
            </tr>
            <tr class="nodrag nodrop">
                <td></td>
                <td class="center"><?= _t('site', 'По умолчанию'); ?></td>
                <td class="center"><?= _t('site', 'Отображать'); ?></td>
                <?php if ($adminSettings) { ?>
                <td class="center"><?= _t('site', 'По умолчанию'); ?></td>
                <td class="center"><?= _t('site', 'Отображать'); ?></td>
                <?php } ?>
                <td></td>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($languagesList as $k=>$v): ?>
            <tr class="row1">
                <td>
                    <input type="hidden" name="languages_selected[]" value="<?= $k ?>" />
                    <span class="lang-icon country-icon country-icon-<?= $v['country'] ?> j-lang-icon" style="margin-right: 5px;"></span><span class="j-lang-title"><?= $v['title'] ?></span>
                </td>
                <td class="center">
                    <label class="radio inline"><input type="radio" name="languages_default" value="<?= $k ?>" <?php if ($v['default']) { ?> checked="checked"<?php } ?> /></label>
                </td>
                <td class="center">
                    <label class="checkbox inline"><input type="checkbox" name="languages_hidden[]" value="<?= $k ?>" <?php if (!$v['hidden']) { ?> checked="checked"<?php } ?> /></label>
                </td>
                <?php if ($adminSettings) { ?>
                <td class="center">
                    <label class="radio inline"><input type="radio" name="languages_default_admin" <?php if ($languageDefaultAdmin === $k) { ?> checked="checked"<?php } ?> /></label>
                </td>
                <td class="center">
                    <label class="checkbox inline"><input type="checkbox" name="languages_available_admin[]" value="<?= $k ?>" <?php if ($v['admin']) { ?> checked="checked"<?php } ?> /></label>
                </td>
                <?php } ?>
                <td class="center">
                    <a href="javascript:void(0);" class="but del"></a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="j-available-block" style="display: none;">
        <select name="languages_add">
            <?php foreach ($languagesListAll as $k=>$v) { ?>
            <option <?php if(array_key_exists($k, $languagesList)) { ?> disabled="disabled"<?php } ?> value="<?= $k ?>"><?= $v['title'] ?></option>
            <?php } ?>
        </select>
        <a href="javascript:void(0);" class="btn btn-mini btn-success" style="margin-top:-1px;"><?= _t('', 'добавить') ?></a>
        <a href="javascript:void(0);" class="btn btn-mini j-available-block-hide" style="margin-top:-1px;"><?= _t('', 'отмена') ?></a>
    </div>
    <div>
        <a href="javascript:void(0);" class="btn btn-mini j-available-block-show"><?= _t('site','Добавить язык'); ?></a>
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var $bl = $('#j-site-locale-available-block');
        $bl.on('click', '.j-available-block-show', function(){
            $(this).hide();
            $bl.find('.j-available-block').show();
        });
        $bl.on('click', '.j-available-block-hide', function(){
            $bl.find('.j-available-block-show').show();
            $bl.find('.j-available-block').hide();
        });
        $bl.on('click', '.j-lang-hidden', function(){
            $(this).find('span').toggleClass('icon-eye-open icon-eye-close');
        });
        $bl.find('.j-selected-block').tableDnD();
    });
</script>