<?php namespace bff\extend;

/**
 * Плагинизация: Блок расширения
 * @version 0.2
 * @modified 29.oct.2018
 * @copyright Tamaranga
 */

class ExtensionBlock
{
    /** @var Extension объект расширения */
    protected $extension;

    /** @var string ID тега */
    protected $tag = '';

    /** @var array параметры тега */
    protected $params = [];

    /** @var callable функция обработчик */
    protected $callback = '';

    /** @var string название блока */
    protected $title = '';

    /** @var string описание блока */
    protected $description = '';

    /** @var array список шаблонов в которые рекомендуется вставка блока */
    protected $templates = [];

    /**
     * Регистрация блока
     * @param Extension|ExtensionInterface $extension объект расширения
     * @param string $tag ID тега
     * @param callable $callback функция обработчик, отвечающая за отрисовку блока
     */
    public function __construct(ExtensionInterface $extension, $tag, callable $callback)
    {
        $this->extension = $extension;
        $this->tag = $tag;
        $this->callback = $callback;

        if ($extension->isEnabled()) {
            \bff::tagAdd($tag, $callback);
        }
    }

    /**
     * Получаем ID тега
     * @param boolean $htmlView
     * @return string
     */
    public function getTag($htmlView = false)
    {
        if ($htmlView) {
            $params = '';
            if ( ! empty($this->params)) {
                $params = json_encode($this->params, JSON_FORCE_OBJECT);
            }
            return '<!-- ['.$this->tag.$params.'] -->';
        }
        return $this->tag;
    }

    /**
     * Добавляем параметр тега, например значение которое необходимо передать из шаблона в который будет вставлен блок:
     *  key='id', value='echo (isset($v[\'id\']) ? $v[\'id\'] : 0);'
     * @param string $key ключ параметра
     * @param string|array $value значение параметра
     * @param mixed $wrap тип обвертки:
     *   'php' - php код
     * @return self
     */
    public function param($key, $value, $wrap = 'php')
    {
        if (is_string($value) && $wrap === 'php') {
            $value = (mb_stripos($value, '$') === 0 ? '<?= '.$value.' ?>' : '<?php '.$value.' ?>');
        }
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * Устанавливаем название блока
     * @param string $title
     * @return self
     */
    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Получаем название блока
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Устанавливаем описание блока
     * @param string $text
     * @return self
     */
    public function description($text)
    {
        $this->description = $text;

        return $this;
    }

    /**
     * Получаем описание блока
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Устанавливаем шаблон в котором рекомендуется выполнить вставку блока
     * @param string $path путь к файлу шаблона
     * @param string $options доп. параметры
     * @return self
     */
    public function template($path, array $options = array())
    {
        $options['path'] = $path;
        $this->templates[] = $options;

        return $this;
    }

    /**
     * Получаем список шаблонов
     * @return array
     */
    public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * Удаление блока
     * @return bool
     */
    public function remove()
    {
        return \bff::tagRemove($this->tag);
    }
}