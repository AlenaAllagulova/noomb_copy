<?php namespace bff\extend;

/**
 * Плагинизация: плагин
 * @abstract
 * @version 2.0
 * @modified 6.nov.2018
 * @copyright Tamaranga
 */

abstract class Plugin extends \Module implements ExtensionInterface
{
    use Extension {
        viewPHP as viewPHPBase;
    }

    /**
     * Внутреннее название плагина
     * @var string
     */
    protected $plugin_name = '';
    /**
     * Алиас внутреннего названия плагина
     * @var string
     */
    protected $plugin_alias = '';
    /**
     * Название плагина отображаемое в админ. панели
     * @var string
     */
    protected $plugin_title = '?';
    /**
     * Версия плагина
     * @var string
     */
    protected $plugin_version = '?';
    /**
     * Выполнялась ли установка плагина
     * @var bool
     */
    protected $plugin_installed = false;
    /**
     * Выполнялось ли включение плагина
     * @var bool
     */
    protected $plugin_enabled = false;
    /**
     * Директория шаблонов плагина
     * @var string
     */
    protected $plugin_templates_dir = 'tpl';
    /**
     * Шаблоны страниц (coding)
     * @var array
     */
    protected $plugin_coding_templates = [];

    /**
     * Инициализация
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->extension_type = EXTENSION_TYPE_PLUGIN;
        $this->module_name  = $name = $this->getName();
        $this->module_title = $this->getTitle();
        $this->module_dir   = $this->extension_path;

        if (\bff::adminPanel()) {
            foreach (array('install','uninstall','enable','disable') as $action) {
                \bff::hookAdd('plugins.' . $action . '.' . $name, function () use ($action) {
                    if ($action !== 'disable') {
                        $this->refreshStatic(in_array($action, ['install', 'enable']));
                    }
                    return $this->$action();
                }, 4);
            }
        }
        \bff::hookAdd('plugins.update.'.$name, function ($return, $context) {
            $this->refreshStatic(true);
            return $this->update($context);
        }, 4);
        \bff::hookAdd('plugins.start.'.$name, function($obj, $testMode) {
            static $started = false;
            if (!$started && $this->isActive($testMode)) {
                $started = true;
                if (BFF_DEBUG) {
                    $this->refreshStatic(true);
                }
                $this->extensionStart();
                $this->start();
            }
        }, 4);
    }

    /**
     * Запуск плагина (если был установлен и включен)
     */
    protected function start()
    {
    }

    /**
     * Установка плагина
     * Метод вызываемый при инсталяции плагина администратором
     * @return bool
     */
    protected function install()
    {
        return true;
    }

    /**
     * Удаление плагина
     * Метод вызываемый при удалении плагина администратором
     * @return bool
     */
    protected function uninstall()
    {
        return true;
    }

    /**
     * Установлен ли плагин
     * @return bool
     */
    public function isInstalled()
    {
        return $this->plugin_installed;
    }

    /**
     * Включение плагина
     * Метод вызываемый при включении плагина администратором
     * @return bool
     */
    protected function enable()
    {
        return true;
    }

    /**
     * Выключение плагина
     * Метод вызываемый при выключении плагина администратором
     * @return bool
     */
    protected function disable()
    {
        return true;
    }

    /**
     * Обновление плагина
     * Метод вызываемый при обновлении плагина
     * @param array $context [
     *    'version_from' => 'версия до обновления (X.X.X)',
     *    'version_to' => 'версия обновления (X.X.X)',
     *    'date' => 'дата обновления (d.m.Y)'
     * ]
     * @return bool
     */
    protected function update($context)
    {
        return true;
    }

    /**
     * Включен ли плагин
     * @param bool|null $testMode проверяем включен ли плагин в режиме тестирования
     * @return bool
     */
    public function isEnabled($testMode = null)
    {
        if ( ! is_bool($testMode)) {
            $testMode = \bff::dev()->extensionsTestMode();
        }
        return ($testMode ? $this->isTestmode() : $this->plugin_enabled);
    }

    /**
     * Плагин был установлен и включен
     * @param bool $testMode проверяем включен ли плагин в режиме тестирования
     * @return bool
     */
    public function isActive($testMode = false)
    {
        return $this->isInstalled() && $this->isEnabled($testMode);
    }

    /**
     * Внутренее название плагина
     * @return string
     */
    public function getName()
    {
        return $this->plugin_name;
    }

    /**
     * Алиас внутреннего названия плагина
     * @return string
     */
    public function getAlias()
    {
        return $this->plugin_alias;
    }

    /**
     * Видимое название плагина
     * @return string
     */
    public function getTitle()
    {
        if ($this->plugin_title === '?'
         || $this->plugin_title === '{TITLE}') {
            return $this->getName();
        }
        return $this->plugin_title;
    }

    /**
     * Версия плагина
     * @return string
     */
    public function getVersion()
    {
        return $this->plugin_version;
    }

    /**
     * Расписание запуска крон задач плагина:
     * [
     *   'название публичного метода плагина' => ['period'=>'* * * * *'],
     *   ...
     * ]
     * @return array
     */
    public function cronSettings()
    {
        return array();
    }

    /**
     * Coding: Добавляем шаблон страницы
     * @param string $title название страницы
     * @param string $template название файла php шаблона (file.php, /dir/file.php)
     * @param bool $admin шаблон для админ. панели
     * @param array $opts [
     *     'admin' => true, # шаблон для админ. панели
     *     'query' => ['key'=>'value', ...]
     * ]
     */
    public function codingTemplateAdd($title, $template, array $opts = array())
    {
        \func::array_defaults($opts, array(
            'admin' => true,
            'query' => array(),
        ));
        $this->plugin_coding_templates[] = [
            'title'    => $title,
            'template' => $template,
            'admin'    => !empty($opts['admin']),
            'opts'     => $opts,
        ];
    }

    /**
     * Coding: Формируем ссылку на файл шаблона
     * @param string $template название файла php шаблона (file.php, /dir/file.php)
     * @param array $query параметры ссылки
     * @param bool $admin шаблон для админ. панели
     * @return string
     */
    public function codingTemplateUrl($template, $query = [], $admin = true)
    {
        $query['template'] = $template;
        return $this->urlAction('codingTemplateView', ['query' => $query], !empty($admin));
    }

    /**
     * Coding: Просматриваем шаблон страницы
     * @param array $data данные передаваемые в шаблон
     */
    public function codingTemplateView(array $data = array())
    {
        $template = $this->input->get('template', TYPE_NOTAGS);
        if ( ! empty($template)) {
            $template = trim($template, '/'.DS);
            if (mb_stripos($template, $this->plugin_templates_dir . DS) !== 0) {
                $template = $this->plugin_templates_dir . DS . $template;
            }
            if (mb_substr($template, -4) === '.php') {
                $template = mb_substr($template, 0, mb_strlen($template) - 4);
            }
            return $this->viewPHP($data, $template);
        }
        $this->errors->impossible();
        return '';
    }

    /**
     * Coding: Получаем список шаблонов
     * @return array
     */
    public function codingTemplatesList()
    {
        return $this->plugin_coding_templates;
    }

    /**
     * {@inheritdoc}
     */
    public function viewPHP(array &$aData, $templateName, $templateDir = false, $display = false)
    {
        # Поиск шаблона для текущей активной темы / coding:
        do {
            if ( ! empty($templateDir)) {
                break;
            }
            $templateName = ltrim($templateName, DS);
            if (mb_stripos($templateName, $this->plugin_templates_dir . DS) !== 0) {
                break;
            }
            # No theme:
            if (($theme = \bff::theme()) === false ||
                ($themeId = $theme->getExtensionId(false, true)) === '') {
                # Coding:
                if (BFF_DEBUG &&
                    ! file_exists($this->extension_path . $templateName . '.php') &&
                    is_file($this->extension_path . 'coding' . DS . $templateName . '.php')) {
                    $templateName = 'coding' . DS . $templateName;
                }
                break;
            }
            # Theme:
            $templateNameThemed = $this->plugin_templates_dir . DS . $themeId . DS .
                mb_substr($templateName, mb_strlen($this->plugin_templates_dir . DS));
            if (is_file($this->extension_path . $templateNameThemed . '.php')) {
                # tpl/file.php => tpl/themeID/file.php
                # tpl/subdir/file.php => tpl/themeID/subdir/file.php
                $templateName = $templateNameThemed;
            }
            # Coding:
            if (BFF_DEBUG && $templateName !== $templateNameThemed &&
                ! file_exists($this->extension_path . $templateName . '.php')) {
                if (is_file($this->extension_path . 'coding' . DS . $templateNameThemed . '.php')) {
                    $templateName = 'coding' . DS . $templateNameThemed;
                } else if (is_file($this->extension_path . 'coding' . DS . $templateName . '.php')) {
                    $templateName = 'coding' . DS . $templateName;
                }
            }
        } while (false);

        return $this->viewPHPBase($aData, $templateName, $templateDir, $display);
    }
}