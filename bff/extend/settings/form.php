<?php namespace bff\extend\settings;

use \bff\extend\Extension as Extension;

/**
 * Конструктор для таба настройки
 * @version 0.1
 * @modified 26.sep.2017
 */

class Form extends \Module
{
    # типы полей
    const FIELD_TEXT            = 1;    # Текстовое поле
    const FIELD_TEXT_LANG       = 2;    # Текстовое поле с мультиязычностью
    const FIELD_JWYSIWYG        = 3;    # jwysiwyg
    const FIELD_JWYSIWYG_LANG   = 4;    # jwysiwyg с мультиязычностью
    const FIELD_IMAGES          = 5;    # Аплоадер изображений сохраняем в формате CImagesUploaderField
    const FIELD_CHECKBOX_LIST   = 6;    # Список чекбоксов с сортировкой
    const FIELD_TAGS            = 7;    # Список тегов с автокомплитером для выбора
    const FIELD_GROUP_FIELDS    = 8;    # Группа полей, поля определенные внутри группы добавляются как well
    const FIELD_IMAGES_LINK     = 9;    # Ссылка на изображения внутри группы полей
    const FIELD_SELECT          = 10;   # input type select
    const FIELD_AUTOCOMPLETE    = 11;   # input type text с автокомплитером
    const FIELD_CHECKBOX        = 12;   # input type checkbox
    const FIELD_FILES           = 13;   # Аплоадер файлов
    const FIELD_FILES_LINK      = 14;   # Ссылка на файл внутри группы полей
    const FIELD_CUSTOM          = 100;  # кастомное поле с произвольным html

    /** @var string ID формы */
    protected $id;

    /** @var string Название таба */
    public $name;

    /** @var Extension */
    protected $extension;

    # Табы
    protected $tabs = array();
    protected $tabActive = false;       # активный таб (для селектора)

    # Поля
    protected $fields = array();
    protected $fieldActive = false;     # активное поле (для селектора)
    protected $groupActive = false;     # активная группа
    protected $optionActive = false;    # активный option для FIELD_CHECKBOX_LIST

    /**
     * Конструктор
     * @param integer $id ID формы
     * @param string $name название формы
     * @param Extension $extension
     */
    public function __construct($id, $name, $extension)
    {
        $this->id = $id;
        $this->name = $name;
        $this->extension = $extension;

        # Инициализируем модуль в качестве компонента (неполноценного модуля)
        $this->initModuleAsComponent('extensionSettingsForm', PATH_CORE . 'extend' . DS . 'settings');
        $this->init();

    }

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        //
    }

    /**
     * Получаем ID формы
     * @return integer
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * Построение формы с конструктором
     * @return string HTML
     */
    public function content()
    {
        $data = array(
            'tabs'      => & $this->tabs,
            'fields'    => & $this->fields,
            'img'       => $this->img(),
            'fls'       => $this->fls(),
        );

        foreach($this->fields as $k => & $v) {
            $this->tabActive = $v['tab'];
            $this->fieldActive = $k;
            $v['value'] = $this->value('', false);
            $this->fillValues($v);
        } unset($v);

        return $this->viewPHP($data, 'admin.form');
    }

    /**
     *  Сохранение значений для формы, событие submit
     */
    public function submit()
    {
        $params = array();
        foreach($this->tabs as $v) {
            $params[ $v['name'] ] = TYPE_ARRAY;
        }
        $data = $this->input->postm($params);

        foreach($this->fields as $v) {
            if ( ! isset($this->tabs[ $v['tab'] ])) continue;
            if ( ! empty($v['group'])) continue;
            $tab = $this->tabs[ $v['tab'] ]['name'];
            $name = $v['name'];
            switch($v['type']) {
                case static::FIELD_IMAGES: # для изображений
                    $record = $v['id'];
                    $images = $this->img($record);
                    $images->setSizes($v['sizes']);
                    if (isset($data[ $tab ][ $name ])) {
                        $files = $data[ $tab ][ $name ];
                        if (is_array($files)){
                            $images->saveOrder($files);   # сохраним порядок
                        }
                    }
                    $deleted = $this->input->post('delete_image_'.$record, TYPE_ARRAY);
                    if ( ! empty($deleted)) {
                        $images->deleteImages($deleted);  # удалим лишние
                    }
                    break;
                case static::FIELD_FILES: # для файлов
                    $record = $v['id'];
                    $fls = $this->fls($record, $v['public']);
                    if (isset($data[ $tab ][ $name ])) {
                        $files = $data[ $tab ][ $name ];
                        if (is_array($files)){
                            $fls->saveOrder($files);   # сохраним порядок
                        }
                    }
                    $deleted = $this->input->post('delete_file_'.$record, TYPE_ARRAY);
                    if ( ! empty($deleted)) {
                        $fls->deleteFiles($deleted);  # удалим лишние
                    }
                    break;
                case static::FIELD_GROUP_FIELDS: # значения для групированных полей с учетом порядка
                    if ( ! isset($data[ $tab ][ $name ])) break;
                    $group = $data[ $tab ][ $name ];
                    $result = array();
                    foreach($group as $g) {
                        $r = array();
                        foreach($this->fields as $vv){
                            if ($vv['group'] != $v['id']) continue;
                            if ( ! isset($g[ $vv['name'] ])) continue;
                            $val = $g[ $vv['name'] ];
                            if (is_callable($vv['clean'])) {
                                $val = call_user_func($vv['clean'], $val, $vv);
                            } else {
                                $this->input->clean($val, $vv['clean']);
                            }
                            $r[ $vv['name'] ] = $val;
                        }
                        if ( ! empty($r)) {
                            $result[] = $r;
                        }
                    }
                    if ( ! empty($result)) {
                        $this->configUpdate($v['id'], $result);
                    }
                    break;
                default:
                    $val = isset($data[ $tab ][ $name ]) ? $data[ $tab ][ $name ] : false;
                    if (is_callable($v['clean'])) {
                        $val = call_user_func($v['clean'], $val, $v);
                    } else {
                        $this->input->clean($val, $v['clean']);
                    }
                    $this->configUpdate($v['id'], $val);
                    break;
            }
        }
        $this->ajaxResponseForm();
    }

    /**
     * Получение URL для обработки ajax запросов
     * @return string
     */
    public function ajaxUrl()
    {
        return $this->adminLink('extensions&name='.$this->extension->getName().'&type='.$this->extension->getExtensionType().'&form_id='.$this->id.'&act=settings.form.ajax', 'dev');
    }

    /**
     * Обработчик ajax запросов
     */
    public function ajax()
    {
\bff::log(__FUNCTION__);
        $act = $this->input->getpost('form_action', TYPE_STR);
        $response = array();
        switch($act) {
            case 'upload-img': # загрузка изображения
                $id = $this->input->getpost('record', TYPE_UINT);
                if (empty($id)) break;
                if ( ! isset($this->fields[$id])) break;
                $field = $this->fields[$id];
                if ($field['type'] != static::FIELD_IMAGES) break;
                $images = $this->img($id);
                if (empty($field['sizes'])) break;
                $images->setSizes($field['sizes']);

                $result = $images->uploadQQ();
                $response = array('success' => ($result !== false && $this->errors->no()));

                if ($result !== false) {
                    $response = array_merge($response, $result);
                    $response = array_merge($response, $images->getURL($result, ['o', 's']));
                }
                $response['errors'] = $this->errors->get();
                $this->ajaxResponse($response, true);
                break;
            case 'upload-file': # загрузка изображения
                $id = $this->input->getpost('record', TYPE_UINT);
                if (empty($id)) break;
                if ( ! isset($this->fields[$id])) break;
                $field = $this->fields[$id];
                if ($field['type'] != static::FIELD_FILES) break;
                $files = $this->fls($id, $field['public']);
                if ( ! empty($field['extensions'])) {
                    $files->setAllowedExtensions($field['extensions']);
                }

                $result = $files->uploadQQ();
                $response = array('success' => ($result !== false && $this->errors->no()));

                if ($result !== false) {
                    $response = array_merge($response, $result);
                    $response['url'] = $files->getUrl($result);
                    $response['filesize'] = \tpl::filesize($result['filesize']);
                }
                $response['errors'] = $this->errors->get();
                $this->ajaxResponse($response, true);
                break;
            case 'autocomplete': # значения для автокомплитера тегов
                $id = $this->input->getpost('id', TYPE_UINT);
                if ( ! isset($this->fields[$id])) break;
                $f = $this->fields[$id];
                if ( ! in_array($f['type'], [static::FIELD_TAGS, static::FIELD_AUTOCOMPLETE])) break;
                $q = $this->input->post('q', TYPE_NOTAGS);
                if (is_callable($f['autocomplete'])) {
                    $response = call_user_func($f['autocomplete'], array('q' => $q));
                }
                $this->ajaxResponse($response);
                break;
            case 'group-fields-block': # добавления нового блока для полей типа FIELD_GROUP_FIELDS
                $id = $this->input->postget('id', TYPE_UINT);
                $name = $this->input->postget('name', TYPE_STR);
                $index = $this->input->postget('index', TYPE_UINT);

                if ( ! isset($this->fields[$id])) break;
                $f = $this->fields[$id];
                if ($f['type'] != static::FIELD_GROUP_FIELDS) break;

                foreach($this->fields as $k => & $v) {
                    $v['value'] = $v['default'];
                    $this->fillValues($v);
                } unset($v);

                $data = array(
                    'tabs'          => & $this->tabs,
                    'fields'        => & $this->fields,
                    'img'           => $this->img(),
                    'fls'           => $this->fls(),
                    'groupBlock'    => false,
                    'groupAddMode'  => 1,
                    'index'         => $index,
                );
                $this->viewPHP($data, 'admin.form');

                if ( ! is_callable($data['groupBlock'])) break;

                $response = array(
                    'index' => $index,
                    'html' => call_user_func($data['groupBlock'], $id, $name),
                );
                break;
        }
        $this->ajaxResponseForm($response);
    }

    /**
     * Инмциализация компонент работы с изображениями
     * @param int $recordID id поля типа FIELD_IMAGES
     * @return Images
     */
    public function img($recordID = 0)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new Images();
            $i->setExtension($this->extension, $this);
        }
        $i->setRecordID($recordID);
        return $i;
    }

    /**
     * Инмциализация компонент работы с файлами
     * @param int $recordID id поля типа FIELD_FILES
     * @param bool $public см publicStore для Files
     * @return Files
     */
    public function fls($recordID = 0, $public = true)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new Files();
            $i->setExtension($this->extension, $this);
        }
        $i->setRecordID($recordID, $public);
        return $i;
    }

    /**
     * Формирование имени для сохранения настроек
     * @param integer $fieldID ID поля
     * @return string
     */
    public function configName($fieldID)
    {
        $name = '_form_'.$this->id.'_';
        do {
            if ( ! isset($this->fields[ $fieldID ])) break;
            $field = $this->fields[ $fieldID ];
            if ($field['group'] !== false) break;
            if ( ! isset($this->tabs[ $field['tab'] ])) break;
            $tab = $this->tabs[ $field['tab'] ];
            $name .= $tab['name'].'_'.$field['name'];
        } while(false);
        return $name;
    }

    /**
     * Добавление полей к конфигу расширения
     * @param $config
     */
    public function appendConfigSettings(& $config)
    {
        foreach($this->fields as $k => $v) {
            if ($v['group'] !== false) continue;
            $config[ $this->configName($k) ] = array(
                'input'     => 'custom',
                'type'      => isset($v['clean']) && is_scalar($v['clean']) ? $v['clean'] : TYPE_ARRAY,
                'default'   => $v['default'],
            );
        }
    }

    /**
     * Получение сохраненных данных из конфига расширения
     * @param integer $fieldID ID поля
     * @param mixed $default значение по умолчанию
     * @return mixed
     */
    public function config($fieldID, $default = '')
    {
        return $this->extension->config($this->configName($fieldID), $default);
    }

    /**
     * Сохранение данных в конфигк расширения
     * @param integer $fieldID ID поля
     * @param mixed $save данные
     */
    public function configUpdate($fieldID, $save)
    {
        $this->extension->configUpdate($this->configName($fieldID), $save);
    }

    # === добавление табов и полей ===

    /**
     * Добавление или выбор таба
     * @param string $name имя таба
     * @param string $title заголовок
     * @return $this
     */
    public function tab($name, $title = '')
    {
        do {
            foreach($this->tabs as $k => $v) {
                if ($v['name'] == $name) {
                    $this->tabActive = $k;
                    $this->groupActive = false;
                    break 2;
                }
            }

            $this->tabs[] = array(
                'name' => $name,
                'title' => $title,
            );
            $tabs = array_keys($this->tabs);
            $this->tabActive = end($tabs);
            $this->tabs[ $this->tabActive ]['id'] = $this->tabActive;
            $this->groupActive = false;
            $this->optionActive = false;

        } while(false);
        return $this;
    }

    /**
     * Добавление или выбор поля
     * @param string $name имя
     * @param string $title заголовок
     * @param int $type тип
     * @param mixed $default значение по умолчанию
     * @return Form
     */
    public function field($name, $title = '', $type = 0, $default = false)
    {
        # значения для любого типа поля
        $data = array(
            'name'      => $name,
            'title'     => $title,
            'type'      => $type,
            'tab'       => $this->tabActive,
            'group'     => $this->groupActive,
            'value'     => false,
            'default'   => $default,
            'clean'     => TYPE_NOCLEAN,
        );

        do {
            if (empty($name)) break;                                # c пустым именем нельзя
            if ( ! isset($this->tabs[ $this->tabActive ])) break;   # поля могут добавлятся только внутри таба

            # назначим поле активным, если есть поле с именем $name в активном табе
            foreach($this->fields as $k => $v) {
                if ($v['tab'] !== $this->tabActive) continue;
                if ($v['group'] !== $this->groupActive) continue;
                if ($v['name'] == $name) {
                    $this->fieldActive = $k;

                    if ($v['type'] == static::FIELD_GROUP_FIELDS) {
                        $this->groupActive = $this->fieldActive;
                    }
                    break 2;
                }
            }

            # добавляем поле
            switch($type) {
                case static::FIELD_IMAGES:                          # Аплоадер изображений сохраняем в формате CImagesUploaderField
                    $data = array_merge(array(
                        'limit'     => 1,                           # максимальное кол изображений
                        'sizes'     => ['o' => ['o' => true] ],     # массив размеров
                        'hidden'    => false,                       # флаг скрытое поле (если загрузка нужна внутри group-fields)
                    ), $data);
                    if ( ! isset($data['sizes']['s'])) {
                        $data['sizes']['s'] = ['width' => 100, 'height' => 100]; # для привью в админке используем 100x100
                    }
                    break;
                case static::FIELD_FILES:                           # Аплоадер файлов
                    $data = array_merge(array(
                        'limit'     => 1,                           # максимальное кол файлов
                        'hidden'    => false,                       # флаг скрытое поле (если загрузка нужна внутри group-fields)
                        'public'    => true,                        # см publicStore для Files
                        'extensions'=> [],                          # разрешенные расширения
                    ), $data);
                    break;
                case static::FIELD_CHECKBOX_LIST:                   # Список чекбоксов с сортировкой
                    $data = array_merge(array(
                        'options'     => [],                        # перечень возможных значений
                    ), $data);
                    break;
                case static::FIELD_CHECKBOX:                        # чекбокс
                    $data = array_merge(array(
                        'label'     => '',
                    ), $data);
                    break;
                case static::FIELD_SELECT:                          # <select>
                    $data = array_merge(array(
                        'options'     => [],                        # перечень возможных значений
                        'empty'       => false,                     # false - не добавлять вариант "не выбран"; string - название; array(id,title) - id + название
                    ), $data);
                    break;
                case static::FIELD_AUTOCOMPLETE:                    # input текст с автокомплитером
                    $data = array_merge(array(
                        'autocomplete'  => false,                   # callable closure для поиска значений в автокомплитере
                        'getValues'     => false,                   # callable closure для получения названия значений
                        'placeholder'   => '',                      # плейсхолдер в инпуте
                    ), $data);
                    break;
                case static::FIELD_TAGS:                            # список тегов с автокомплитером
                    $data = array_merge(array(
                        'autocomplete'  => false,                   # callable closure для поиска значений в автокомплитере
                        'getValues'     => false,                   # callable closure для получения названий ранее сохраненых значений
                        'placeholder'   => '',                      # плейсхолдер в инпуте
                    ), $data);
                    break;
                case static::FIELD_GROUP_FIELDS:                    # группа полей, поля определенные внутри группы добавляются как well
                    $data = array_merge(array(
                        'text_add'  => '+ add',                     # такст кнопки для добавления группы
                    ), $data);
                    break;
                case static::FIELD_IMAGES_LINK:                     # ссылка на изображение внутри группы полей
                    $data = array_merge(array(
                        'uploader'  => false,                       # id поля с аплоадером (type=images) объявленного вне группы
                        'limit'     => 100,                         # максимальное кол изображений
                    ), $data);
                    break;
                case static::FIELD_FILES_LINK:                      # ссылка на изображение внутри группы полей
                    $data = array_merge(array(
                        'uploader'  => false,                       # id поля с аплоадером (type=files) объявленного вне группы
                        'limit'     => 100,                         # максимальное кол файлов
                    ), $data);
                    break;
                case static::FIELD_CUSTOM:                          # кастомное поле
                    $data = array_merge(array(
                        'html'      => '',                          # функция или код для построения html
                        'onValue'     => false,                     # функция для получения значения
                    ), $data);
                    break;
            }

            if ( ! $data['type']) break;

            $uploaderID = false;
            $uploaderType = 0;
            if ($type == static::FIELD_IMAGES && $data['group']) {  # Если добавить аплоадера внутрь группы, то аплоадер добавляем вне группы,
                                                                    # а внутрь группы добавляем FIELD_IMAGES_LINK с именем как у аплоадера
                $data['name'] = 'upl_'.$data['name'];
                $data['limit'] = 100;
                $data['group'] = 0;
                $data['hidden'] = true;
                $uploaderID = true;
                $uploaderType = static::FIELD_IMAGES_LINK;
            }
            if ($type == static::FIELD_FILES && $data['group']) {  # Если добавить аплоадера внутрь группы, то аплоадер добавляем вне группы,
                $data['name'] = 'upl_'.$data['name'];
                $data['limit'] = 100;
                $data['group'] = 0;
                $data['hidden'] = true;
                $uploaderID = true;
                $uploaderType = static::FIELD_FILES_LINK;
            }

            $this->fields[] = $data;
            $fields = array_keys($this->fields);
            $this->fieldActive = end($fields);                      # добавленное поле - активное
            $this->fields[ $this->fieldActive ]['id'] = $this->fieldActive; # добавим id поля

            if ($type == static::FIELD_GROUP_FIELDS) {
                $this->groupActive = $this->fieldActive;            # делаем активным группу (последующие поля будут добавлятся в группу, прерывается по $this->tab или $this->endGroup )
            }

            $this->optionActive = false;

            if ($uploaderID === true) {                             # аплоадер внутри группы
                $uploaderID = $this->fieldActive;
                $this->field($name, $title, $uploaderType)  # добавляем FIELD_IMAGES_LINK
                        ->param('uploader', $uploaderID);               # указываем ранее созданный аплоадер
                                                                    # последующие параметры меняем для поля типа $uploaderType
            }

        } while(false);

        return $this;
    }

    /**
     * Добавление поля типа FIELD_TEXT
     * @param string $name имя
     * @param string $title заголовок
     * @param string $default значение по умолчанию
     * @return Form
     */
    public function text($name, $title = '', $default = '')
    {
        return $this->field($name, $title, static::FIELD_TEXT, $default)->param('clean', TYPE_STR);
    }

    /**
     * Добавление поля типа FIELD_TEXT_LANG
     * @param string $name имя
     * @param string $title заголовок
     * @param array $default значение по умолчанию
     * @return Form
     */
    public function textLang($name, $title = '', $default = array())
    {
        return $this->field($name, $title, static::FIELD_TEXT_LANG, $default)->param('clean', TYPE_ARRAY_STR);
    }

    /**
     * Добавление поля типа FIELD_JWYSIWYG
     * @param string $name имя
     * @param string $title заголовок
     * @param string $default значение по умолчанию
     * @return Form
     */
    public function jwysiwyg($name, $title = '', $default = '')
    {
        return $this->field($name, $title, static::FIELD_JWYSIWYG, $default)->param('clean', TYPE_STR);
    }

    /**
     * Добавление поля типа FIELD_JWYSIWYG_LANG
     * @param string $name имя
     * @param string $title заголовок
     * @param array $default значение по умолчанию
     * @return Form
     */
    public function jwysiwygLang($name, $title = '', $default = array())
    {
        return $this->field($name, $title, static::FIELD_JWYSIWYG_LANG, $default)->param('clean', TYPE_ARRAY_STR);
    }

    /**
     * Добавление поля типа FIELD_IMAGES
     * @param string $name имя
     * @param string $title заголовок
     * @return Form
     */
    public function images($name, $title = '')
    {
        return $this->field($name, $title, static::FIELD_IMAGES);
    }

    /**
     * Добавление поля типа FIELD_FILES
     * @param string $name имя
     * @param string $title заголовок
     * @return Form
     */
    public function files($name, $title = '')
    {
        return $this->field($name, $title, static::FIELD_FILES);
    }

    /**
     * Добавление поля типа FIELD_CHECKBOX_LIST
     * @param string $name имя
     * @param string $title заголовок
     * @return Form
     */
    public function checkboxList($name, $title = '')
    {
        return $this->field($name, $title, static::FIELD_CHECKBOX_LIST, array())
            ->param('clean', function($val, $f) {
                $result = array();
                if (empty($f['options'])) return $result;
                if ( ! is_array($val)) $val = array();
                $o = $f['options'];
                # включенные согласно порядка
                foreach($val as $k => $v) {
                    if ( ! isset($o[ $k ])) continue;
                    unset($o[$k]);
                    $result[ $k ] = array(
                        'checked'   => ! empty($v) ? 1 : 0,
                    );
                }
                if ( ! empty($o)) {
                    # отключенные
                    foreach($o as $k => $v) {
                        $result[ $k ] = array(
                            'checked'   => 0,
                        );
                    }
                }
                return $result;
            });
    }

    /**
     * Добавление поля типа FIELD_CHECKBOX
     * @param string $name имя
     * @param string $title заголовок
     * @param bool $default checked
     * @return Form
     */
    public function checkbox($name, $title = '', $default = false)
    {
        return $this->field($name, $title, static::FIELD_CHECKBOX, $default)->param('clean', TYPE_BOOL);
    }

    /**
     * Добавление поля типа FIELD_TAGS
     * @param string $name имя
     * @param string $title заголовок
     * @return Form
     */
    public function tags($name, $title = '')
    {
        return $this->field($name, $title, static::FIELD_TAGS)->param('clean', TYPE_ARRAY_UINT);
    }

    /**
     * Добавление поля типа FIELD_TEXT
     * @param string $name имя
     * @param string $title заголовок
     * @return Form
     */
    public function group($name, $title = '')
    {
        return $this->field($name, $title, static::FIELD_GROUP_FIELDS, array())->param('clean', TYPE_ARRAY);
    }

    /**
     * Добавление поля типа FIELD_SELECT
     * @param string $name имя
     * @param string $title заголовок
     * @param mixed $default ID выбранного элемента
     * @return Form
     */
    public function select($name, $title = '', $default = false)
    {
        return $this->field($name, $title, static::FIELD_SELECT, $default);
    }

    /**
     * Добавление поля типа FIELD_AUTOCOMPLETE
     * @param string $name имя
     * @param string $title заголовок
     * @param string $default значение по умолчанию
     * @return Form
     */
    public function autocomplete($name, $title = '', $default = '')
    {
        return $this->field($name, $title, static::FIELD_AUTOCOMPLETE, $default)->param('clean', TYPE_STR);
    }

    /**
     * Добавление поля типа FIELD_CUSTOM
     * @param string $name имя
     * @param string $title заголовок
     * @param mixed $default
     * @return Form
     */
    public function custom($name, $title = '', $default = false)
    {
        return $this->field($name, $title, static::FIELD_CUSTOM, $default);
    }

    /**
     * Завершения добавления полей в группу
     * @return Form
     */
    public function endGroup()
    {
        $this->groupActive = false;
        return $this;
    }

    /**
     * Завершения добавления параметров в option
     * @return Form
     */
    public function endOption()
    {
        $this->optionActive = false;
        return $this;
    }

    # === добавление параметров к полям ===

    /**
     * Чтение или запись параметра для активного поля
     * @param string $name имя параметра
     * @param mixed $value значение, если null - чтение
     * @return Form|mixed
     */
    public function param($name, $value = null)
    {
        do {
            if ( ! isset($this->fields[ $this->fieldActive ])) break;
            if ( ! isset($this->fields[ $this->fieldActive ][ $name ])) {
                # если для поля указан аплодер и параметра нет у поля, то меняем для аплоадера
                if (isset($this->fields[ $this->fieldActive ]['uploader'])) {
                    $uploader = $this->fields[ $this->fieldActive ]['uploader'];
                    if (isset($this->fields[ $uploader ][ $name ])) {
                        if (is_null($value)) {
                            return $this->fields[ $uploader ][ $name ];
                        }
                        $this->fields[ $uploader ][ $name ] = $value;
                    }
                }
                break;
            }

            if ($name == 'options' && in_array($this->fields[ $this->fieldActive ]['type'], [static::FIELD_CHECKBOX_LIST, static::FIELD_SELECT])
            && isset($this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ])) {
                if (is_null($value)) {
                    return $this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ];
                }
                $this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ] = $value;
                break;
            }

            if (is_null($value)) {
                return $this->fields[ $this->fieldActive ][ $name ];
            }

            $this->fields[ $this->fieldActive ][ $name ] = $value;

        } while(false);

        return $this;
    }

    /**
     * Добавление options для поля типов FIELD_CHECKBOX_LIST, FIELD_SELECT
     * @param mixed $name имя или id или callable функция для генерации
     * @param string $title заголовок
     * @param bool $default значение по умолчанию (checked)
     * @return Form
     */
    public function option($name, $title = '', $default = false)
    {
        do {
            if (empty($name)) break;
            if ( ! isset($this->fields[ $this->fieldActive ])) break;
            $f = $this->fields[ $this->fieldActive ];
            if (is_callable($name)) {
                $this->fields[ $this->fieldActive ]['options'] = call_user_func($name);
                break;
            }

            switch($f['type']) {
                case static::FIELD_CHECKBOX_LIST:
                    $this->fields[ $this->fieldActive ]['options'][ $name ] = array('title' => $title);
                    $this->fields[ $this->fieldActive ]['default'][ $name ] = array('checked' => $default);
                    $this->optionActive = $name;
                    break;
                case static::FIELD_SELECT:
                    $this->fields[ $this->fieldActive ]['options'][ $name ] = array('id' => $name, 'title' => $title);
                    $this->optionActive = $name;
                    break;
            }
        } while(false);
        return $this;
    }


    # === Получение значения для полей

    /**
     * Вернуть значение для активного поля
     * @param string $name имя поля или таб/поле
     * @param string $lang если указанно вернуть для языка (false - для всех языков)
     * @param bool $default значение по умолчанию
     * @return mixed
     */
    public function value($name = '', $lang = LNG, $default = false)
    {
        $result = $default;
        $tabActive = $this->tabActive;
        $fieldActive = $this->fieldActive;

        if ( ! empty($name)) {
            $name = explode('/', $name);
            if (count($name) == 2) { # таб/поле
                $tabActive = false;
                foreach($this->tabs as $k => $v) {
                    if ($v['name'] == $name[0]) {
                        $tabActive = $k;
                        break;
                    }
                }
                $fieldName = $name[1];
            } else {    # название поля в активном табе
                $fieldName = $name[0];
            }
            foreach($this->fields as $k => $v) {
                if ($v['tab'] != $tabActive) continue;
                if ($v['name'] == $fieldName) {
                    $fieldActive = $k;
                    break;
                }
            }
        }

        do {
            if ( ! isset($this->fields[ $fieldActive])) break;     # нет активного поля
            $field = $this->fields[ $fieldActive ];
            if ($field['tab'] != $tabActive) break;

            # сохраненные значения
            $result = $this->config($fieldActive, $field['default']);

            # для полей типа FIELD_IMAGES добавим данные о загруженных изображениях
            if ($field['type'] == static::FIELD_IMAGES) {
                $images = $this->img($field['id']);
                $images->setSizes($field['sizes']);
                $result = $images->getData();
                foreach($result as & $v) {
                    foreach($field['sizes'] as $s => $vv) {
                        $v['url_'.$s] = $images->getURL($v, $s);
                    }
                } unset($v);
            }


            # для мультиязычных полей вернем на конкретном языке, если указан
            if (in_array($field['type'], [static::FIELD_TEXT_LANG, static::FIELD_JWYSIWYG_LANG])) {
                if (is_array($result) && $lang !== false) {
                    $result = isset($result[ $lang ]) ? $result[ $lang ] : reset($result);
                }
            }

            if ($field['type'] == static::FIELD_GROUP_FIELDS && ! empty($result)) {
                # для группы полей вернем массив со всеми значениями
                #       для типов static::FIELD_TEXT_LANG и static::FIELD_JWYSIWYG_LANG вернем на указанном языке, остальные типы - как есть
                #       если значение не сохранено - вернем значение по умолчанию
                $groupResult = array();
                foreach($result as $v) {
                    $r = array();
                    $img = array();
                    foreach($this->fields as $f) {
                        if ($f['group'] != $fieldActive) continue;
                        if (isset($v[ $f['name'] ])) {
                            $res = $v[ $f['name'] ];
                        } else {
                            $res = $f['default'];
                        }
                        if (in_array($f['type'], [static::FIELD_TEXT_LANG, static::FIELD_JWYSIWYG_LANG])) {
                            if (is_array($res) && $lang !== false) {
                                $res = isset($res[ $lang ]) ? $res[ $lang ] : reset($res);
                            }
                        }
                        if ($f['type'] == static::FIELD_IMAGES_LINK && ! empty($res)) {
                            if ( ! isset($img[ $f['uploader'] ])) {
                                $img[ $f['uploader'] ] = $this->value($this->fields[ $f['uploader'] ]['name']);
                            }
                            foreach($res as & $vv) {
                                foreach($img[ $f['uploader'] ] as $u) {
                                    if ($u['filename'] != $vv) continue;
                                    $vv = $u;
                                    break;
                                }
                            } unset($vv);
                        }
                        $r[ $f['name'] ] = $res;
                    }
                    if (empty($r)) continue;
                    $groupResult[] = $r;
                }
                return $groupResult;
            }

            if ($field['type'] == static::FIELD_CUSTOM) {
                if (is_callable($field['onValue'])) {
                    $result = call_user_func($field['onValue'], $result, $field);
                }
            }

        } while(false);
        return $result;
    }

    /**
     * Дополнение значений для поля (выполнение closure функции getValues )
     * @param array $field поле в массиве полей $this->fields
     */
    public function fillValues(& $field)
    {
        if (isset($field['getValues']) && is_callable($field['getValues'])) {
            $field['values'] = call_user_func($field['getValues'], $field['value']);
        }
    }


}