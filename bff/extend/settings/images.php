<?php namespace bff\extend\settings;

use bff\utils\Files as UtilsFiles;
use \bff\extend\Extension as Extension;

/**
 * Работа с изображениями конструктора для таба настройки
 * @version 0.1
 * @modified 26.sep.2017
 */
class Images extends \CImagesUploaderField
{
    /** @var Extension */
    protected $extension;

    /** @var Form */
    protected $form;

    function initSettings()
    {
        $this->filenameLetters = 10;
        $this->sizes = [];
        $this->limit = 1000;
    }

    /**
     * Инициализация компонента для работы с формой
     * @param Extension $extension
     * @param Form $form
     */
    public function setExtension($extension, $form)
    {
        $this->extension = $extension;
        $this->form = $form;

        $folder = $this->extension->getExtensionId();

        $this->path = $this->pathTmp = \bff::path($folder, 'images');
        $this->url  = $this->urlTmp  = \bff::url($folder,  'images');

        if ( ! file_exists($this->path)) {
            UtilsFiles::makeDir($this->path);
        }
        if ( ! file_exists($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t create folder: [path]', array('path'=>$this->path)));  return; }
        if ( ! is_writable($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t write to folder: [path]', array('path'=>$this->path))); return; }
    }

    /**
     * Получаем данные о записи
     * @param integer $recordID ID записи
     * @return array
     */
    protected function loadRecordData($recordID)
    {
        $saved = $this->form->config($recordID, array());
        $data = array();
        $data[$this->field_images] = $saved;
        $data[$this->field_count]  = count($saved);
        return $data;
    }

    /**
     * Сохраняем данные о записи
     * @param integer $recordID ID записи
     * @param array $recordData данные
     * @return mixed
     */
    protected function saveRecordData($recordID, array $recordData)
    {
        if (isset($recordData[$this->field_images]) && is_array($recordData[$this->field_images])) {
            $this->form->configUpdate($recordID, $recordData[$this->field_images]);
        }
    }
}