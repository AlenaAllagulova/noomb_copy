<?php namespace bff\tpl\admin;

use bff\utils\Files as UtilsFiles;
use \bff\extend\Extension as Extension;

/**
 * Работа с изображениями конструктора для таба настройки
 * @version 0.1
 * @modified 03.oct.2018
 */
class FormImages extends \CImagesUploaderField
{
    /** @var Extension */
    protected $extension;

    /** @var Form */
    protected $form;

    protected $preload = false;

    function initSettings()
    {
        $this->filenameLetters = 10;
        $this->sizes = [];
        $this->limit = 1000;
        $this->extensionsAllowed = ['jpg', 'jpeg', 'gif', 'png', 'svg'];
    }

    /**
     * Инициализация компонента для работы с формой
     * @param Extension $extension
     * @param Form $form
     */
    public function setExtension($extension, $form)
    {
        $this->extension = $extension;
        $this->form = $form;

        $folder = $this->extension->getExtensionId();

        $this->path = $this->pathTmp = \bff::path($folder, 'images');
        $this->url  = $this->urlTmp  = \bff::url($folder,  'images');

        if ( ! file_exists($this->path)) {
            UtilsFiles::makeDir($this->path);
        }
        if ( ! file_exists($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t create folder: [path]', array('path'=>$this->path)));  return; }
        if ( ! is_writable($this->path)) { $this->errors->set($this->extension->langAdmin('Can\'t write to folder: [path]', array('path'=>$this->path))); return; }
    }

    /**
     * Получаем данные о записи
     * @param integer $recordID ID записи
     * @return array
     */
    protected function loadRecordData($recordID)
    {
        $saved = $this->form->config($recordID, array());
        if ($this->preload && empty($saved)) {
            $saved = $this->preloadImages();
        }
        unset($saved['empty']);
        if ( ! is_array($saved)) $saved = array();
        $data = array();
        $data[$this->field_images] = $saved;
        $data[$this->field_count]  = count($saved);
        return $data;
    }

    /**
     * Сохраняем данные о записи
     * @param integer $recordID ID записи
     * @param array $recordData данные
     * @return mixed
     */
    protected function saveRecordData($recordID, array $recordData)
    {
        if (isset($recordData[$this->field_images]) && is_array($recordData[$this->field_images])) {
            if (empty($recordData[$this->field_images])) {
                $recordData[$this->field_images]['empty'] = 1;
            }
            $this->form->configUpdate($recordID, $recordData[$this->field_images]);
        }
    }

    /**
     * Применение настроек поля с ID
     * @param integer $fieldID ID поля
     */
    public function setField($fieldID)
    {
        $field = $this->form->getField($fieldID);
        if (empty($field['type']) || $field['type'] != Form::FIELD_IMAGES) return;
        $this->setRecordID($fieldID);
        $this->setSizes($field['sizes']);
        if ( ! empty($field['maxSize'])) {
            $this->setMaxSize($field['maxSize']);
        }
        $this->preload = ! empty($field['preload']);
    }

    protected function preloadImages()
    {
        do {
            $field = $this->form->getField($this->recordID);
            if (empty($field['preload'])) break;
            $this->preload = false;
            foreach ($field['preload'] as $v) {
                if ( ! file_exists($v)) continue;
                $this->uploadFromFile($v);
            }
            return $this->form->config($this->recordID, array());
        } while(false);
        return array();
    }
}