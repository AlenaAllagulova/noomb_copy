<?php namespace bff\tpl\admin;

use \bff\extend\Extension as Extension;

/**
 * Компонент построения формы в админ. панели
 */
class Form extends Block
{
    # типы полей
    const FIELD_TEXT                = 1;    # Текстовое поле
    const FIELD_TEXT_LANG           = 2;    # Текстовое поле с мультиязычностью
    const FIELD_WYSIWYG             = 3;    # jwysiwyg
    const FIELD_WYSIWYG_LANG        = 4;    # jwysiwyg с мультиязычностью
    const FIELD_IMAGES              = 5;    # Аплоадер изображений сохраняем в формате CImagesUploaderField
    const FIELD_CHECKBOX_LIST       = 6;    # Список чекбоксов с сортировкой
    const FIELD_TAGS                = 7;    # Список тегов с автокомплитером для выбора
    const FIELD_GROUP_FIELDS        = 8;    # Группа полей, поля определенные внутри группы добавляются как well
    const FIELD_IMAGES_LINK         = 9;    # Ссылка на изображения внутри группы полей
    const FIELD_SELECT              = 10;   # input type select
    const FIELD_AUTOCOMPLETE        = 11;   # input type text с автокомплитером
    const FIELD_CHECKBOX            = 12;   # input type checkbox
    const FIELD_FILES               = 13;   # Аплоадер файлов
    const FIELD_FILES_LINK          = 14;   # Ссылка на файл внутри группы полей
    const FIELD_WYSIWYG_FCK         = 15;   # wysiwyg FCKEditor
    const FIELD_WYSIWYG_FCK_LANG    = 16;   # wysiwyg FCKEditor с мультиязычностью
    const FIELD_PASSWORD            = 17;   # input type password
    const FIELD_NUMBER              = 18;   # input type number
    const FIELD_DIVIDER             = 19;   # Разделитель полей
    const FIELD_TEXTAREA            = 20;   # текстэриа
    const FIELD_TEXTAREA_LANG       = 21;   # текстэриа с мультиязычностью
    const FIELD_CUSTOM              = 100;  # кастомное поле с произвольным html

    /** @var string ID формы */
    protected $id;

    # Табы
    protected $tabs = array();
    protected $tabActive = false;       # активный таб (для селектора)

    # Поля
    protected $fields = array();
    protected $fieldActive = false;     # активное поле (для селектора)
    protected $groupActive = false;     # активная группа
    protected $optionActive = false;    # активный option для FIELD_CHECKBOX_LIST

    /**
     * Конструктор
     * @param integer $id ID формы
     * @param string $name название формы
     * @param Extension|\Module $controller объект контроллера
     * @param string $action название метода контроллера обрабатывающего запросы формы
     */
    public function __construct($id, $controller, $action = null)
    {
        $this->id = $id;

        $this->setController($controller, $action);

        $this->init();
    }

    /**
     * Инициализация
     */
    public function init()
    {
        parent::init();

        $this->setTemplateName('form');
    }

    /**
     * Получаем ID формы
     * @return integer
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * Построение формы с конструктором
     * @return string HTML
     */
    public function content()
    {
        $data = array(
            'tabs'      => & $this->tabs,
            'fields'    => & $this->fields,
            'img'       => $this->imagesInit(),
            'fls'       => $this->filesInit(),
        );

        foreach ($this->fields as $k => & $v) {
            $this->tabActive = $v['tab'];
            $this->fieldActive = $k;
            $v['value'] = $this->value('', false);
            $this->fillValues($v);
        } unset($v);

        return $this->render($data);
    }

    /**
     *  Сохранение значений для формы, событие submit
     */
    public function submit()
    {
        $params = array();
        foreach ($this->tabs as $v) {
            $params[ $v['name'] ] = TYPE_ARRAY;
        }
        $data = $this->input->postm($params);

        foreach ($this->fields as $v) {
            if ( ! isset($this->tabs[ $v['tab'] ])) continue;
            if ( ! empty($v['group'])) continue;
            $tab = $this->tabs[ $v['tab'] ]['name'];
            $name = $v['name'];
            switch ($v['type']) {
                case static::FIELD_IMAGES: # для изображений
                    $record = $v['id'];
                    $images = $this->imagesInit($record);
                    if (isset($data[ $tab ][ $name ])) {
                        $files = $data[ $tab ][ $name ];
                        if (is_array($files)){
                            $images->saveOrder($files);   # сохраним порядок
                        }
                    }
                    $deleted = $this->input->post('delete_image_'.$record, TYPE_ARRAY);
                    if ( ! empty($deleted)) {
                        $images->deleteImages($deleted);  # удалим лишние
                    }
                    break;
                case static::FIELD_FILES: # для файлов
                    $record = $v['id'];
                    $fls = $this->filesInit($record);
                    if (isset($data[ $tab ][ $name ])) {
                        $files = $data[ $tab ][ $name ];
                        if (is_array($files)){
                            $fls->saveOrder($files);   # сохраним порядок
                        }
                    }
                    $deleted = $this->input->post('delete_file_'.$record, TYPE_ARRAY);
                    if ( ! empty($deleted)) {
                        $fls->deleteFiles($deleted);  # удалим лишние
                    }
                    break;
                case static::FIELD_GROUP_FIELDS: # значения для групированных полей с учетом порядка
                    if ( ! isset($data[ $tab ][ $name ])) {
                        $this->configUpdate($v['id'], array('empty' => 1));
                        break;
                    };
                    $group = $data[ $tab ][ $name ];
                    $result = array();
                    foreach ($group as $g) {
                        $r = array();
                        foreach ($this->fields as $vv){
                            if ($vv['group'] != $v['id']) continue;
                            if ( ! isset($g[ $vv['name'] ])) continue;
                            $val = $g[ $vv['name'] ];
                            if (is_callable($vv['clean'], true)) {
                                $val = call_user_func($vv['clean'], $val, $vv);
                            } else {
                                $this->input->clean($val, $vv['clean']);
                            }
                            $r[ $vv['name'] ] = $val;
                        }
                        if ( ! empty($r)) {
                            $result[] = $r;
                        }
                    }
                    if ( ! empty($result)) {
                        $this->configUpdate($v['id'], $result);
                    }
                    break;
                case static::FIELD_DIVIDER:
                    break;
                default:
                    $val = isset($data[ $tab ][ $name ]) ? $data[ $tab ][ $name ] : false;
                    if (is_callable($v['clean'], true)) {
                        $val = call_user_func($v['clean'], $val, $v);
                    } else {
                        $this->input->clean($val, $v['clean']);
                    }
                    $this->configUpdate($v['id'], $val);
                    break;
            }
        }
        $this->ajaxResponseForm();
    }

    /**
     * Получение URL для обработки ajax запросов
     * @return string
     */
    public function ajaxUrl()
    {
        if (is_a($this->controller, '\\Extension')) {
            return $this->adminLink('extensions&name=' . $this->controller->getName() . '&type=' . $this->controller->getExtensionType() . '&form_id=' . $this->id . '&act=settings.form.ajax', 'dev');
        } else {
            return $this->controller->adminLink($this->controllerAction);
        }
    }

    /**
     * Обработчик ajax запросов
     */
    public function ajax()
    {
        $act = $this->input->getpost('form_action', TYPE_STR);
        $response = array();
        switch ($act) {
            case 'upload-img': # загрузка изображения
                $id = $this->input->getpost('record', TYPE_UINT);
                if (empty($id)) break;
                if ( ! isset($this->fields[$id])) break;
                $field = $this->fields[$id];
                if ($field['type'] != static::FIELD_IMAGES) break;
                $images = $this->imagesInit($id);

                $result = $images->uploadQQ();
                $response = array('success' => ($result !== false && $this->errors->no()));

                if ($result !== false) {
                    $response = array_merge($response, $result);
                    $response = array_merge($response, $images->getURL($result, ['o', 's']));
                }
                $response['errors'] = $this->errors->get();
                $this->ajaxResponse($response, true);
                break;
            case 'upload-file': # загрузка изображения
                $id = $this->input->getpost('record', TYPE_UINT);
                if (empty($id)) break;
                if ( ! isset($this->fields[$id])) break;
                $field = $this->fields[$id];
                if ($field['type'] != static::FIELD_FILES) break;
                $files = $this->filesInit($id);

                $result = $files->uploadQQ();
                $response = array('success' => ($result !== false && $this->errors->no()));

                if ($result !== false) {
                    $response = array_merge($response, $result);
                    $response['url'] = $files->getUrl($result);
                    $response['filesize'] = \tpl::filesize($result['filesize']);
                }
                $response['errors'] = $this->errors->get();
                $this->ajaxResponse($response, true);
                break;
            case 'autocomplete': # значения для автокомплитера тегов
                $id = $this->input->getpost('id', TYPE_UINT);
                if ( ! isset($this->fields[$id])) break;
                $f = $this->fields[$id];
                if ( ! in_array($f['type'], [static::FIELD_TAGS, static::FIELD_AUTOCOMPLETE])) break;
                $q = $this->input->post('q', TYPE_NOTAGS);
                if (is_callable($f['autocompleteCallback'], true)) {
                    $response = call_user_func($f['autocompleteCallback'], $q);
                }
                $this->ajaxResponse($response);
                break;
            case 'group-fields-block': # добавления нового блока для полей типа FIELD_GROUP_FIELDS
                $id = $this->input->postget('id', TYPE_UINT);
                $name = $this->input->postget('fname', TYPE_STR);
                $index = $this->input->postget('index', TYPE_UINT);

                if ( ! isset($this->fields[$id])) break;
                $f = $this->fields[$id];
                if ($f['type'] != static::FIELD_GROUP_FIELDS) break;

                foreach ($this->fields as $k => & $v) {
                    $v['value'] = $v['default'];
                    $this->fillValues($v);
                } unset($v);

                $data = array(
                    'tabs'          => & $this->tabs,
                    'fields'        => & $this->fields,
                    'img'           => $this->imagesInit(),
                    'fls'           => $this->filesInit(),
                    'groupBlock'    => false,
                    'groupAddMode'  => 1,
                    'index'         => $index,
                );
                $this->render($data);

                if ( ! is_callable($data['groupBlock'], true)) break;

                $response = array(
                    'index' => $index,
                    'html' => call_user_func($data['groupBlock'], $id, $name),
                );
                break;
        }
        $this->ajaxResponseForm($response);
    }

    /**
     * Инмциализация компонент работы с изображениями
     * @param int|bool $fieldID id поля типа FIELD_IMAGES
     * @return FormImages
     */
    protected function imagesInit($fieldID = false)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new FormImages();
            $i->setExtension($this->controller, $this);
        }
        $i->setField($fieldID);
        return $i;
    }

    /**
     * Инмциализация компонент работы с файлами
     * @param int $fieldID id поля типа FIELD_FILES
     * @return FormFiles
     */
    protected function filesInit($fieldID = false)
    {
        static $i;
        if ( ! isset($i)) {
            $i = new FormFiles();
            $i->setExtension($this->controller, $this);
        }
        $i->setField($fieldID);
        return $i;
    }

    /**
     * Формирование имени для сохранения настроек
     * @param integer $fieldID ID поля
     * @return string
     */
    public function configName($fieldID)
    {
        $name = '_form_'.$this->id.'_';
        do {
            if ( ! isset($this->fields[ $fieldID ])) break;
            $field = $this->fields[ $fieldID ];
            if ($field['group'] !== false) break;
            if ( ! isset($this->tabs[ $field['tab'] ])) break;
            $tab = $this->tabs[ $field['tab'] ];
            $name .= $tab['name'].'_'.$field['name'];
        } while(false);
        return $name;
    }

    /**
     * Добавление полей к конфигу расширения
     * @param $config
     */
    public function appendConfigSettings(& $config)
    {
        foreach ($this->fields as $k => $v) {
            if ($v['group'] !== false) continue;
            if ($v['type'] == static::FIELD_DIVIDER) continue;
            $c = array(
                'input'     => 'custom',
                'type'      => isset($v['clean']) && is_scalar($v['clean']) ? $v['clean'] : TYPE_ARRAY,
                'default'   => $v['default'],
            );
            if ($v['type'] == static::FIELD_PASSWORD) {
                $c['encrypt'] = true;
            }
            $config[ $this->configName($k) ] = $c;
        }
    }

    /**
     * Получение сохраненных данных из конфига расширения
     * @param integer $fieldID ID поля
     * @param mixed $default значение по умолчанию
     * @return mixed
     */
    public function config($fieldID, $default = '')
    {
        return $this->controller->config($this->configName($fieldID), $default);
    }

    /**
     * Сохранение данных в конфигк расширения
     * @param integer $fieldID ID поля
     * @param mixed $save данные
     */
    public function configUpdate($fieldID, $save)
    {
        $this->controller->configUpdate($this->configName($fieldID), $save);
    }

    # === добавление табов и полей ===

    /**
     * Добавление или выбор таба
     * @param string $name имя таба
     * @param string $title заголовок
     * @return $this
     */
    public function tab($name, $title = '')
    {
        do {
            foreach ($this->tabs as $k => $v) {
                if ($v['name'] == $name) {
                    $this->tabActive = $k;
                    $this->groupActive = false;
                    break 2;
                }
            }

            $this->tabs[] = array(
                'name'      => $name,
                'title'     => $title,
                'hidden'    => false,
            );
            $tabs = array_keys($this->tabs);
            $this->tabActive = end($tabs);
            $this->tabs[ $this->tabActive ]['id'] = $this->tabActive;
            $this->groupActive = false;
            $this->optionActive = false;

        } while(false);
        return $this;
    }

    /**
     * Добавление или выбор поля
     * @param string $name имя
     * @param string $title заголовок
     * @param int $type тип
     * @param mixed $params параметры
     * @return Form
     */
    public function field($name, $title = '', $type = 0, $params = array())
    {
        # значения для любого типа поля
        $data = array_merge(array(
            'name'      => $name,
            'title'     => $title,
            'type'      => $type,
            'tab'       => $this->tabActive,
            'group'     => $this->groupActive,
            'value'     => false,
            'default'   => false,
            'clean'     => TYPE_NOCLEAN,
            'attr'      => [],
            'htmlAfter' => '',
            'preload'   => [],
            'tip'       => [],
        ), $params);

        do {
            if (empty($data['name'])) break;                                # c пустым именем нельзя

            if ( ! isset($this->tabs[ $this->tabActive ])) {                # поля могут добавлятся только внутри таба
                if (empty($this->tabs) && empty($this->fields)) {           # в режиме без табов создадим один скрытый таб
                    $this->tab('h', 'hidden');
                    $this->tabs[ $this->tabActive ]['hidden'] = true;
                } else {
                    break;
                }
            }

            # назначим поле активным, если есть поле с именем $name в активном табе
            foreach ($this->fields as $k => $v) {
                if ($v['tab'] !== $this->tabActive) continue;
                if ($v['group'] !== $this->groupActive) continue;
                if ($v['name'] == $name) {
                    $this->fieldActive = $k;

                    if ($v['type'] == static::FIELD_GROUP_FIELDS) {
                        $this->groupActive = $this->fieldActive;
                    }
                    break 2;
                }
            }

            # добавляем поле
            switch ($type) {
                case static::FIELD_WYSIWYG:
                case static::FIELD_WYSIWYG_LANG:
                    $data = array_merge(array(
                        'width'         => 0,                       # ширина число или "100%" (0,false = 100%)
                        'height'        => 100,                     # высота число или "100%" (0,false = 100%)
                        'params'        => [],                      # параметры инициализации
                        'JSObjectName'  => '',                      # имя js объекта, для дальнейшего управления компонентом
                    ), $data);
                    break;
                case static::FIELD_WYSIWYG_FCK:
                case static::FIELD_WYSIWYG_FCK_LANG:
                    $data = array_merge(array(
                        'width'         => '',                      # ширина
                        'height'        => '',                      # высота
                        'toolbarMode'   => '',                      # режим панели
                        'theme'         => '',                      # тема
                    ), $data);
                    break;
                case static::FIELD_IMAGES:                          # Аплоадер изображений сохраняем в формате CImagesUploaderField
                    $data = array_merge(array(
                        'limit'     => 1,                           # максимальное кол изображений
                        'sizes'     => ['o' => ['o' => true] ],     # массив размеров
                        'hidden'    => false,                       # флаг скрытое поле (если загрузка нужна внутри group-fields)
                        'maxSize'   => false,                       # максимальный размер файла
                    ), $data);
                    if ( ! isset($data['sizes']['s'])) {
                        $data['sizes']['s'] = ['width' => 100, 'height' => 100]; # для привью в админке используем 100x100
                    }
                    $data['clean'] = TYPE_ARRAY;
                    break;
                case static::FIELD_IMAGES_LINK:                     # ссылка на изображение внутри группы полей
                    $data = array_merge(array(
                        'uploader'  => false,                       # id поля с аплоадером (type=images) объявленного вне группы
                        'limit'     => 100,                         # максимальное кол изображений
                    ), $data);
                    break;
                case static::FIELD_FILES:                           # Аплоадер файлов
                    $data = array_merge(array(
                        'limit'     => 1,                           # максимальное кол файлов
                        'hidden'    => false,                       # флаг скрытое поле (если загрузка нужна внутри group-fields)
                        'public'    => true,                        # см publicStore для \bff\tpl\admin\FormFiles
                        'extensions'=> '',                          # разрешенные расширения
                        'maxSize'   => false,                       # максимальный размер файла
                    ), $data);
                    $data['clean'] = TYPE_ARRAY;
                    break;
                case static::FIELD_FILES_LINK:                      # ссылка на изображение внутри группы полей
                    $data = array_merge(array(
                        'uploader'  => false,                       # id поля с аплоадером (type=files) объявленного вне группы
                        'limit'     => 100,                         # максимальное кол файлов
                    ), $data);
                    break;
                case static::FIELD_CHECKBOX_LIST:                   # Список чекбоксов с сортировкой
                    $data = array_merge(array(
                        'options'   => [],                          # перечень возможных значений
                        'sortable'  => false,                       # возможность сортировки
                    ), $data);
                    break;
                case static::FIELD_CHECKBOX:                        # чекбокс
                    $data = array_merge(array(
                        'label'     => '',
                    ), $data);
                    break;
                case static::FIELD_SELECT:                          # <select>
                    $data = array_merge(array(
                        'options'     => [],                        # перечень возможных значений
                        'empty'       => false,                     # false - не добавлять вариант "не выбран"; string - название; array(id,title) - id + название
                    ), $data);
                    break;
                case static::FIELD_AUTOCOMPLETE:                    # input текст с автокомплитером
                    $data = array_merge(array(
                        'autocomplete'  => false,                   # callable closure для поиска значений в автокомплитере
                        'getValues'     => false,                   # callable closure для получения названия значений
                        'placeholder'   => '',                      # плейсхолдер в инпуте
                    ), $data);
                    break;
                case static::FIELD_TAGS:                            # список тегов с автокомплитером
                    $data = array_merge(array(
                        'autocompleteCallback'  => null,            # callable closure для поиска значений в автокомплитере
                        'valuesCallback'        => null,            # callable closure для получения названий ранее сохраненых значений
                        'placeholder'           => '',              # плейсхолдер в инпуте
                        'sortable'              => false,           # возможность сортировки
                    ), $data);
                    break;
                case static::FIELD_GROUP_FIELDS:                    # группа полей, поля определенные внутри группы добавляются как well
                    if (empty($data['title'])) {
                        $data['title'] = '+ add';                   # текст кнопки для добавления группы
                    }
                    break;
                case static::FIELD_CUSTOM:                          # кастомное поле
                    $data = array_merge(array(
                        'htmlCallback'  => null,                    # функция для построения html
                        'valueCallback' => null,                    # функция для получения значения
                    ), $data);
                    break;
            }

            if ( ! $data['type']) break;

            $uploaderID = false;
            $uploaderType = 0;
            $fieldLimit = 0;
            if ($type == static::FIELD_IMAGES && $data['group']) {  # Если добавить аплоадера внутрь группы, то аплоадер добавляем вне группы,
                                                                    # а внутрь группы добавляем FIELD_IMAGES_LINK с именем как у аплоадера
                $data['name'] = 'upl_'.$data['name'];
                $fieldLimit = $data['limit'];
                $data['limit'] = 100;
                $data['group'] = false;
                $data['hidden'] = true;
                $uploaderID = true;
                $uploaderType = static::FIELD_IMAGES_LINK;
            }
            if ($type == static::FIELD_FILES && $data['group']) {  # Если добавить аплоадера внутрь группы, то аплоадер добавляем вне группы,
                $data['name'] = 'upl_'.$data['name'];
                $fieldLimit = $data['limit'];
                $data['limit'] = 100;
                $data['group'] = false;
                $data['hidden'] = true;
                $uploaderID = true;
                $uploaderType = static::FIELD_FILES_LINK;
            }

            $this->fields[] = $data;
            $fields = array_keys($this->fields);
            $this->fieldActive = end($fields);                      # добавленное поле - активное
            $this->fields[ $this->fieldActive ]['id'] = $this->fieldActive; # добавим id поля

            if ($type == static::FIELD_GROUP_FIELDS) {
                $this->groupActive = $this->fieldActive;            # делаем активным группу (последующие поля будут добавлятся в группу, прерывается по $this->tab или $this->endGroup )
            }

            $this->optionActive = false;

            if ($uploaderID === true) {                             # аплоадер внутри группы
                $uploaderID = $this->fieldActive;
                $this->field($name, $title, $uploaderType)          # добавляем FIELD_IMAGES_LINK
                        ->param('uploader', $uploaderID);           # указываем ранее созданный аплоадер
                                                                    # последующие параметры меняем для поля типа $uploaderType
                if ($fieldLimit) $this->param('limit', $fieldLimit);
            }

        } while(false);

        return $this;
    }

    /**
     * Добавление поля типа FIELD_TEXT_LANG или FIELD_TEXT
     * @param string $name имя
     * @param string $title заголовок
     * @param array|string $default значение по умолчанию
     * @param bool $lang использовать мультиязычность
     * @return Form
     */
    public function text($name, $title = '', $default = array(), $lang = true)
    {
        if ($lang) {
            return $this->field($name, $title, static::FIELD_TEXT_LANG, [
                'default'   => $default,
                'clean'     => TYPE_ARRAY_STR,
            ]);
        } else {
            if (empty($default)) $default = '';
            return $this->field($name, $title, static::FIELD_TEXT, [
                'default'   => $default,
                'clean'     => TYPE_STR,
            ]);
        }
    }

    /**
     * Добавление поля типа FIELD_TEXTAREA_LANG или FIELD_TEXTAREA
     * @param string $name имя
     * @param string $title заголовок
     * @param array|string $default значение по умолчанию
     * @param bool $lang использовать мультиязычность
     * @return Form
     */
    public function textarea($name, $title = '', $default = array(), $lang = true)
    {
        if ($lang) {
            return $this->field($name, $title, static::FIELD_TEXTAREA_LANG, [
                'default'   => $default,
                'clean'     => TYPE_ARRAY_STR,
            ]);
        } else {
            if (empty($default)) $default = '';
            return $this->field($name, $title, static::FIELD_TEXTAREA, [
                'default'   => $default,
                'clean'     => TYPE_STR,
            ]);
        }
    }

    /**
     * Добавление поля типа FIELD_WYSIWYG_LANG или FIELD_WYSIWYG
     * @param string $name имя
     * @param string $title заголовок
     * @param array|string $default значение по умолчанию
     * @param bool $lang использовать мультиязычность
     * @param mixed $width ширина число или "100%" (0,false = 100%)
     * @param mixed $height высота число или "100%" (0,false = 100%)
     * @param mixed $params параметры инициализации
     * @param mixed $JSObjectName имя js объекта, для дальнейшего управления компонентом
     * @return Form
     */
    public function wysiwyg($name, $title = '', $default = array(), $lang = true, $width = 0, $height = 100, $params = array(), $JSObjectName = '')
    {
        $init = array(
            'width'         => $width,
            'height'        => $height,
            'params'        => $params,
            'JSObjectName'  => $JSObjectName,
        );
        if ($lang) {
            $init['clean'] = TYPE_ARRAY_STR;
            $init['default'] = $default;
            return $this->field($name, $title, static::FIELD_WYSIWYG_LANG, $init);
        } else {
            if (empty($default)) $default = '';
            $init['clean'] = TYPE_STR;
            $init['default'] = $default;
            return $this->field($name, $title, static::FIELD_WYSIWYG, $init);
        }
    }

    /**
     * Добавление поля типа FIELD_WYSIWYG_FCK_LANG или FIELD_WYSIWYG_FCK
     * @param string $name имя
     * @param string $title заголовок
     * @param array|string $default значение по умолчанию
     * @param bool $lang использовать мультиязычность
     * @param string|int $width ширина
     * @param string|int $height высота
     * @param string $toolbarMode режим панели: average, ...
     * @param string $theme тема: sd
     * @return Form
     */
    public function wysiwygFCK($name, $title = '', $default = array(), $lang = true, $width = '650px', $height = '300px', $toolbarMode = 'average', $theme = 'sd')
    {
        $init = array(
            'width'         => $width,
            'height'        => $height,
            'toolbarMode'   => $toolbarMode,
            'theme'         => $theme,
        );
        if ($lang) {
            $init['clean'] = TYPE_ARRAY_STR;
            $init['default'] = $default;
            return $this->field($name, $title, static::FIELD_WYSIWYG_FCK_LANG, $init);
        } else {
            if (empty($default)) $default = '';
            $init['clean'] = TYPE_STR;
            $init['default'] = $default;
            return $this->field($name, $title, static::FIELD_WYSIWYG_FCK, $init);
        }
    }

    /**
     * Добавление поля типа FIELD_IMAGES
     * @param string $name имя
     * @param string $title заголовок
     * @param integer $limit максимальное количество изображений
     * @param integer $maxSize максимальный размер одного файла в байтах
     * @param array $sizes массив с размерами
     * @return Form
     */
    public function images($name, $title = '', $limit = 100, $maxSize = 4194304, $sizes = array())
    {
        $params = array(
            'limit'     => $limit,
            'maxSize'   => $maxSize,
            'clean'     => TYPE_ARRAY,
        );
        if ( ! empty($sizes)) {
            $params['sizes'] = $sizes;
        }
        return $this->field($name, $title, static::FIELD_IMAGES, $params);
    }

    /**
     * Добавление поля типа FIELD_FILES
     * @param string $name имя
     * @param string $title заголовок
     * @param integer $limit максимальное количество файлов
     * @param integer $maxSize максимальный размер одного файла в байтах
     * @param bool $public публичное хранение @see $publicStore \bff\tpl\admin\FormFiles
     * @param string $extensions список разрешенных расширений, через ','
     * @return Form
     */
    public function files($name, $title = '', $limit = 100, $maxSize = 10485760, $public = true, $extensions = '')
    {
        $params = array(
            'limit'     => $limit,
            'maxSize'   => $maxSize,
            'public'    => $public,
            'clean'     => TYPE_ARRAY,
            'extensions'=> $extensions,
        );
        return $this->field($name, $title, static::FIELD_FILES, $params);
    }

    /**
     * Добавление поля типа FIELD_CHECKBOX_LIST
     * @param string $name имя
     * @param string $title заголовок
     * @param callable $optionsCallback функция для генерации массива options
     * @param bool $sortable возможность перетягивания
     * @return Form
     */
    public function checkboxList($name, $title = '', callable $optionsCallback = null, $sortable = false)
    {
        $params = array(
            'sortable' => $sortable,
        );
        $params['clean'] = function($val, $f) {
            $result = array();
            if (empty($f['options'])) return $result;
            if ( ! is_array($val)) $val = array();
            $o = $f['options'];
            # включенные согласно порядка
            foreach ($val as $k => $v) {
                if ( ! isset($o[ $k ])) continue;
                unset($o[$k]);
                $result[ $k ] = array(
                    'checked'   => ! empty($v) ? 1 : 0,
                );
            }
            if ( ! empty($o)) {
                # отключенные
                foreach ($o as $k => $v) {
                    $result[ $k ] = array(
                        'checked'   => 0,
                    );
                }
            }
            return $result;
        };
        if (is_callable($optionsCallback, true)) {
            $params['options'] = call_user_func($optionsCallback);
        }
        return $this->field($name, $title, static::FIELD_CHECKBOX_LIST, $params);
    }

    /**
     * Добавление поля типа FIELD_CHECKBOX
     * @param string $name имя
     * @param string $title заголовок
     * @param bool $checked
     * @return Form
     */
    public function checkbox($name, $title = '', $checked = false)
    {
        $params = array(
            'clean' => TYPE_BOOL,
            'default' => $checked,
        );
        return $this->field($name, $title, static::FIELD_CHECKBOX, $params);
    }

    /**
     * Добавление поля типа FIELD_TAGS
     * @param string $name имя
     * @param string $title заголовок
     * @param callable $autocompleteCallback closure для поиска значений в автокомплитере
     * @param callable $valuesCallback closure для получения названий ранее сохраненых значений
     * @param string $sortable возможность перетягивания
     * @param string $placeholder плейсхолдер
     * @return Form
     */
    public function tags($name, $title, callable $autocompleteCallback, callable $valuesCallback, $sortable = false, $placeholder = '')
    {
        $params = array(
            'clean'                 => TYPE_ARRAY_UINT,
            'autocompleteCallback'  => $autocompleteCallback,
            'valuesCallback'        => $valuesCallback,
            'sortable'              => $sortable,
            'placeholder'           => $placeholder,
        );
        return $this->field($name, $title, static::FIELD_TAGS, $params);
    }

    /**
     * Добавление поля типа FIELD_TEXT
     * @param string $name имя
     * @param string $title текст ссылки с добавлением элемента группы +add
     * @return Form
     */
    public function group($name, $title = '')
    {
        $params = array(
            'clean' => TYPE_ARRAY,
        );
        return $this->field($name, $title, static::FIELD_GROUP_FIELDS, $params);
    }

    /**
     * Добавление поля типа FIELD_SELECT
     * @param string $name имя
     * @param string $title заголовок
     * @param mixed $default ID выбранного элемента
     * @param callable $optionsCallback функция для генерации массива options
     * @return Form
     */
    public function select($name, $title = '', $default = false, callable $optionsCallback = null)
    {
        $params = array(
            'default' => $default,
        );
        if (is_callable($optionsCallback, true)) {
            $params['options'] = call_user_func($optionsCallback);
        }
        return $this->field($name, $title, static::FIELD_SELECT, $params);
    }

    /**
     * Добавление поля типа FIELD_AUTOCOMPLETE
     * @param string $name имя
     * @param string $title заголовок
     * @param callable $autocompleteCallback closure для поиска значений в автокомплитере
     * @param callable $valuesCallback closure для получения названий ранее сохраненых значений
     * @param string $placeholder плейсхолдер
     * @return Form
     */
    public function autocomplete($name, $title, callable $autocompleteCallback, callable $valuesCallback, $placeholder = '')
    {
        $params = array(
            'clean' => TYPE_STR,
            'autocompleteCallback'  => $autocompleteCallback,
            'valuesCallback'        => $valuesCallback,
            'placeholder'           => $placeholder,
        );
        return $this->field($name, $title, static::FIELD_AUTOCOMPLETE, $params);
    }

    /**
     * Добавление поля типа FIELD_PASSWORD
     * @param string $name имя
     * @param string $title заголовок
     * @param string $default значение по умолчанию
     * @return Form
     */
    public function password($name, $title = '', $default = '')
    {
        return $this->field($name, $title, static::FIELD_PASSWORD, [
            'default'   => $default,
            'clean'     => TYPE_NOCLEAN,
        ]);
    }

    /**
     * Добавление поля типа FIELD_NUMBER
     * @param string $name имя
     * @param string $title заголовок
     * @param integer $min минимальное значение
     * @param integer $max максимальное значение 0 - без ограничений
     * @param integer $step шаг
     * @param integer $default значение по умолчанию
     * @return Form
     */
    public function number($name, $title = '', $min = 1, $max = 0, $step = 1, $default = 0)
    {
        $this->field($name, $title, static::FIELD_NUMBER, [
            'clean'     => TYPE_UINT,
            'default'   => $default,
        ])->attr('min', $min)->attr('step', $step);
        if ($max) {
            $this->attr('max', $max);
        }
        return $this;
    }

    /**
     * Добавление поля типа FIELD_DIVIDER
     * @param string $content контент
     * @param string $align выравнивание  left, right, center
     * @return Form
     */
    public function divider($content = '<hr />', $align = 'left')
    {
        return $this->field('divider'.(count($this->fields) + 1), '', static::FIELD_DIVIDER, [
            'content'   => $content,
            'align'     => $align,
        ]);
    }

    /**
     * Добавление поля типа FIELD_CUSTOM
     * @param string $name имя
     * @param callable $htmlCallback closure для генерации HTML кода аргументы функции($name, $field) $name - значение атрибута name для input; $field - массив данных о поле
     * @param mixed $clean тип или closure для валидации значения
     * @param callable $valueCallback closure для получения значения
     * @param string $title заголовок
     * @return Form
     */
    public function custom($name, callable $htmlCallback, $clean = TYPE_NOCLEAN, callable $valueCallback = null, $title = '')
    {
        $params = array(
            'clean'         => $clean,
            'htmlCallback'  => $htmlCallback,
            'valueCallback' => $valueCallback,
        );
        return $this->field($name, $title, static::FIELD_CUSTOM, $params);
    }

    /**
     * Завершения добавления полей в группу
     * @return Form
     */
    public function endGroup()
    {
        $this->groupActive = false;
        return $this;
    }

    /**
     * Завершения добавления параметров в option
     * @return Form
     */
    public function endOption()
    {
        $this->optionActive = false;
        return $this;
    }

    /**
     * Получение параметров поля по ID
     * @param $fieldID
     * @return array|boolean
     */
    public function getField($fieldID)
    {
        if ( ! isset($this->fields[ $fieldID ])) {
            return false;
        }
        return $this->fields[ $fieldID ];
    }

    # === добавление параметров к полям ===

    /**
     * Чтение или запись параметра для активного поля
     * @param string $name имя параметра
     * @param mixed $value значение, если null - чтение
     * @return Form|mixed
     */
    public function param($name, $value = null)
    {
        do {
            if ( ! isset($this->fields[ $this->fieldActive ])) break;
            if ( ! isset($this->fields[ $this->fieldActive ][ $name ])) {
                # если для поля указан аплодер и параметра нет у поля, то меняем для аплоадера
                if (isset($this->fields[ $this->fieldActive ]['uploader'])) {
                    $uploader = $this->fields[ $this->fieldActive ]['uploader'];
                    if (isset($this->fields[ $uploader ][ $name ])) {
                        if (is_null($value)) {
                            return $this->fields[ $uploader ][ $name ];
                        }
                        $this->fields[ $uploader ][ $name ] = $value;
                    }
                }
                break;
            }

            if ($name == 'options' && in_array($this->fields[ $this->fieldActive ]['type'], [static::FIELD_CHECKBOX_LIST, static::FIELD_SELECT])
            && isset($this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ])) {
                if (is_null($value)) {
                    return $this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ];
                }
                $this->fields[ $this->fieldActive ][ $name ]['options'][ $this->optionActive ] = $value;
                break;
            }

            if (is_null($value)) {
                return $this->fields[ $this->fieldActive ][ $name ];
            }

            $this->fields[ $this->fieldActive ][ $name ] = $value;

        } while(false);

        return $this;
    }

    /**
     * Добавление атрибутов для инпута
     * @param string $key название атрибута
     * @param mixed $value значение
     * @return Form
     */
    public function attr($key, $value)
    {
        do {
            if (empty($key)) break;
            if ( ! isset($this->fields[ $this->fieldActive ])) break;
            \HTML::attributeAdd($this->fields[ $this->fieldActive ]['attr'], $key, $value);
        } while(false);
        return $this;
    }

    /**
     * Добавление options для поля типов FIELD_CHECKBOX_LIST, FIELD_SELECT
     * @param mixed $name имя или id
     * @param string $title заголовок
     * @param bool $checked отмечено/выбрано по умолчанию
     * @param callable $optionsCallback функция для определения доступна option(return true) или нет (return false)
     * @return Form
     */
    public function option($name, $title = '', $checked = false, callable $enabledCallback = null )
    {
        do {
            if (empty($name)) break;
            if ( ! isset($this->fields[ $this->fieldActive ])) break;
            $f = $this->fields[ $this->fieldActive ];
            if (is_callable($enabledCallback, true)) {
                if ( ! call_user_func($enabledCallback)) break;
            }

            switch ($f['type']) {
                case static::FIELD_CHECKBOX_LIST:
                    $this->fields[ $this->fieldActive ]['options'][ $name ] = array('title' => $title);
                    $this->fields[ $this->fieldActive ]['default'][ $name ] = array('checked' => $checked);
                    $this->optionActive = $name;
                    break;
                case static::FIELD_SELECT:
                    $this->fields[ $this->fieldActive ]['options'][ $name ] = array('id' => $name, 'title' => $title);
                    $this->optionActive = $name;
                    break;
            }
        } while(false);
        return $this;
    }

    /**
     * Добавление размера для нарезки изображения
     * @param string $key ключ размера
     * @param integer $width ширина
     * @param integer $height высота
     * @param bool $original сохранить оригинал
     * @param mixed $options другие параметры
     * @return Form
     */
    public function imageSize($key, $width, $height, $original = false, $options = array())
    {
        $options['width'] = $width;
        $options['height'] = $height;
        $options['o'] = $original;

        $fieldActive = $this->fieldActive;
        do {
            if ( ! isset($this->fields[ $fieldActive ])) break;
            if ($this->fields[ $fieldActive ]['type'] == static::FIELD_IMAGES_LINK) {
                $fieldActive = $this->fields[ $fieldActive ]['uploader'];
            }
            if ( ! isset($this->fields[ $fieldActive ])) break;
            if ($this->fields[ $fieldActive ]['type'] != static::FIELD_IMAGES) break;
            $this->fields[ $fieldActive ]['sizes'][ $key ] = $options;
        } while(false);

        return $this;
    }

    /**
     * Добавление подски справа от <input>
     * @param string $content HTML контент
     * @param string $trigger how popover is triggered - click | hover
     * @param string $placement position the popover - top | bottom | left | right
     * @param callable $wrapperCallback функция для генерации HTML обвертки, аргументы ($html) -  $html код с подсказкой
     * @return $this
     */
    public function tip($content, $trigger = 'hover', $placement = 'right', callable $wrapperCallback = null)
    {
        $this->param('tip', [
            'content'   => $content,
            'placement' => $placement,
            'trigger'   => $trigger,
            'wrapper'   => $wrapperCallback,
        ])->attr('class', 'c');
        return $this;
    }

    /**
     * Вывод HTML кода, после <input>
     * @param string $html HTML код
     * @return Form
     */
    public function htmlAfter($html = '')
    {
        $this->param('htmlAfter', $html);
        return $this;
    }

    /**
     * Добавление предзагруженных данных (изображения, файлы, наборы групповых полей)
     * @param mixed $data
     * @param integer|bool $fieldID ID поля к которому добавить false - к активному ($this->fieldActive)
     * @return $this
     */
    public function preload($data, $fieldID = false)
    {
        do {
            if (empty($data)) break;
            if ($fieldID === false) {
                $fieldID = $this->fieldActive;
            }
            if ( ! isset($this->fields[ $fieldID ])) break;
            if ( ! empty($this->fields[ $fieldID ]['group'])) {
                $fieldID = $this->fields[ $fieldID ]['group'];
            } else
            if (in_array($this->fields[ $fieldID ]['type'], [static::FIELD_IMAGES_LINK, static::FIELD_FILES_LINK])) {
                $fieldID = $this->fields[ $fieldID ]['uploader'];
            }
            if ( ! isset($this->fields[ $fieldID ])) break;
            switch ($this->fields[ $fieldID ]['type']) {
                case static::FIELD_IMAGES:
                case static::FIELD_FILES:
                    if ( ! file_exists($data))  break;
                    $this->fields[ $fieldID ]['preload'][] = $data;
                    break;
                case static::FIELD_GROUP_FIELDS:
                    $this->fields[ $fieldID ]['preload'][] = $data;
                    foreach ($this->fields as $f) {
                        if ($f[ 'group' ] != $fieldID) continue;
                        if (in_array($f['type'], [static::FIELD_IMAGES_LINK, static::FIELD_FILES_LINK])
                        && isset($data[ $f['name'] ])) {
                            if (is_array($data[ $f['name'] ])) {
                                foreach ($data[ $f['name'] ] as $v) {
                                    $this->preload($v, $f['uploader']);
                                }
                            } else {
                                $this->preload($data[ $f['name'] ], $f['uploader']);
                            }
                        }
                    }
                    break;
            }
        } while(false);
        return $this;
    }


    # === Получение значения для полей ===

    /**
     * Вернуть значение для активного поля
     * @param string $name имя поля или таб/поле
     * @param string $lang если указанно вернуть для языка (false - для всех языков)
     * @param bool $default значение по умолчанию
     * @return mixed
     */
    public function value($name = '', $lang = LNG, $default = false)
    {
        $result = $default;
        $tabActive = $this->tabActive;
        $fieldActive = $this->fieldActive;

        if ( ! empty($name)) {
            $name = explode('/', $name);
            if (count($name) == 2) { # таб/поле
                $tabActive = false;
                foreach ($this->tabs as $k => $v) {
                    if ($v['name'] == $name[0]) {
                        $tabActive = $k;
                        break;
                    }
                }
                $fieldName = $name[1];
            } else {    # название поля в активном табе
                $fieldName = $name[0];
            }
            foreach ($this->fields as $k => $v) {
                if ($v['tab'] != $tabActive) continue;
                if ($v['name'] == $fieldName) {
                    $fieldActive = $k;
                    break;
                }
            }
        }

        do {
            if ( ! isset($this->fields[ $fieldActive])) break;     # нет активного поля
            $field = $this->fields[ $fieldActive ];
            if ($field['tab'] != $tabActive) break;

            # сохраненные значения
            $result = $this->config($fieldActive, $field['default']);

            # для полей типа FIELD_IMAGES добавим url о загруженных изображениях
            if ($field['type'] == static::FIELD_IMAGES) {
                $images = $this->imagesInit($field['id']);
                $result = $images->getData();
                foreach ($result as & $v) {
                    foreach ($field['sizes'] as $s => $vv) {
                        $v['url_'.$s] = $images->getURL($v, $s);
                    }
                } unset($v);
            }

            # для полей типа FIELD_FILES добавим url о загруженных файлах
            if ($field['type'] == static::FIELD_FILES) {
                $files = $this->filesInit($field['id']);
                $result = $files->getData();
                foreach ($result as & $v) {
                    $v['url'] = $files->getUrl($v);
                } unset($v);
            }

            # для мультиязычных полей вернем на конкретном языке, если указан
            if (in_array($field['type'], [static::FIELD_TEXT_LANG, static::FIELD_WYSIWYG_LANG, static::FIELD_TEXTAREA_LANG, static::FIELD_WYSIWYG_FCK_LANG])) {
                if (is_array($result) && $lang !== false) {
                    $result = isset($result[ $lang ]) ? $result[ $lang ] : reset($result);
                }
            }

            if ($field['type'] == static::FIELD_GROUP_FIELDS) {
                # для группы полей вернем массив со всеми значениями
                #       для типов static::FIELD_TEXT_LANG и static::FIELD_WYSIWYG_LANG вернем на указанном языке, остальные типы - как есть
                #       если значение не сохранено - вернем значение по умолчанию
                $groupResult = array();
                $preload = false;
                if (empty($result) && ! empty($field['preload'])) {
                    $result = $field['preload'];
                    $preload = true;
                }
                if ( ! is_array($result)) $result = array();
                unset($result['empty']);
                $img = array();
                foreach ($result as $v) {
                    $r = array();
                    foreach ($this->fields as $f) {
                        if ($f['group'] != $fieldActive) continue;
                        if (isset($v[ $f['name'] ])) {
                            $res = $v[ $f['name'] ];
                        } else {
                            $res = $f['default'];
                        }
                        if (in_array($f['type'], [static::FIELD_TEXT_LANG, static::FIELD_WYSIWYG_LANG, static::FIELD_TEXTAREA_LANG, static::FIELD_WYSIWYG_FCK_LANG])) {
                            if (is_array($res) && $lang !== false) {
                                $res = isset($res[ $lang ]) ? $res[ $lang ] : reset($res);
                            }
                        }
                        if (in_array($f['type'], [static::FIELD_IMAGES_LINK, static::FIELD_FILES_LINK]) && ! empty($res)) {
                            if ( ! isset($img[ $f['uploader'] ])) {
                                $img[ $f['uploader'] ] = $this->value($this->fields[ $f['uploader'] ]['name']);
                            }
                            if ($preload) {
                                if ( ! empty($img[ $f[ 'uploader' ] ])) {
                                    if (is_array($res)) {
                                        $cnt = count($res);
                                        $res = array();
                                        for ($i = 0; $i < $cnt; $i++) {
                                            $res[] = array_shift($img[ $f[ 'uploader' ] ]);
                                        }
                                    } else {
                                        $res = array();
                                        $res[] = array_shift($img[ $f[ 'uploader' ] ]);
                                    }
                                }
                            } else {
                                foreach ($res as & $vv) {
                                    foreach ($img[ $f[ 'uploader' ] ] as $u) {
                                        if ($u[ 'filename' ] != $vv) continue;
                                        $vv = $u;
                                        break;
                                    }
                                } unset($vv);
                            }
                        }
                        if ($f['type'] == static::FIELD_CHECKBOX) {
                            $res = ! empty($v[ $f['name'] ]);
                        }
                        $r[ $f['name'] ] = $res;
                    }
                    if (empty($r)) continue;
                    $groupResult[] = $r;
                }
                return $groupResult;
            }

            if ($field['type'] == static::FIELD_CUSTOM) {
                if (is_callable($field['valueCallback'], true)) {
                    $result = call_user_func($field['valueCallback'], $result, $field);
                }
            }

        } while(false);
        return $result;
    }

    /**
     * Дополнение значений для поля (выполнение closure функции getValues )
     * @param array $field поле в массиве полей $this->fields
     */
    protected function fillValues(& $field)
    {
        if (isset($field['valuesCallback']) && is_callable($field['valuesCallback'], true)) {
            $field['values'] = call_user_func($field['valuesCallback'], $field['value']);
        }
    }

    # ===

    /**
     * Добавление кода javascript в форму
     * @param string $position 'onReady', 'group.append', 'object', 'return'
     * @param callable $callback функция, генерирующая javascript, аргументы(Form $form, int $u)  $u - случайно сгенерированный номер объекта
     * @return Form
     */
    public function customJavascript($position, callable $callback)
    {
        \bff::hookAdd('extension.settings.form.js.'.$position, $callback);
        return $this;
    }

}