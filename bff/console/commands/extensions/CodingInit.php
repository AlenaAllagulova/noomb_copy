<?php namespace bff\console\commands\extensions;

/**
 * Консоль: команда инициализации coding структуры файлов и директорий в расширении
 * @version 0.1
 * @modified 6.nov.2018
 * Examples:
 *   php bffc extensions/codingInit -x plugin/name
 *   php bffc extensions/codingInit -x theme/name
 */

use bff\console\commands\Command;
use bff\utils\Files;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CodingInit extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        parent::configure();

        $this->setName('extensions/codingInit')
             ->setDescription('Extension coding init')
             ->addOption('--extension', '-x', InputOption::VALUE_REQUIRED, 'Extension name: "plugin/name", "theme/name"')
            ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $extension = $this->getExtensionByOption('extension', $input, $output);
        if ($extension === false) {
            return 1;
        }

        # create files & dirs
        $errors = \bff::errors();
        $chmod = 0775;
        $dir = 'coding';
        $list = array(
            $dir => false,
            $dir.'/index.php' => "<?php\n/**\n * @var \$this Plugin\n */\n\n\$this->codingTemplateAdd('Пример admin шаблона', 'example-admin-file.php');\n\$this->codingTemplateAdd('Пример frontend шаблона', 'example-frontend-file.php', ['admin'=>false]);",
            $dir.'/less' => false,
            $dir.'/scss' => false,
            $dir.'/tpl' => false,
            $dir.'/tpl/_admin.php' => "<?php\n/**\n * @var \$this Plugin\n */\n\n//\$this->cssAdmin('/css/admin.css');\n\n?>\n",
            $dir.'/tpl/example-admin-file.php' => "<?php\n/**\n * @var \$this Plugin\n */\n\nrequire_once('_admin.php');\n\ntplAdmin::adminPageSettings([\n    'title' => 'Список',\n    'link'  => ['title'=>'+ добавить', 'href'=>'#'],\n]);\n?>\n",
            $dir.'/tpl/_frontend.php' => "<?php\n/**\n * @var \$this Plugin\n */\n\n//\$this->cssAdmin('/css/frontend.css');\n\n?>\n\n<script type=\"text/javascript\">\n    <?php js::start() ?>\n    $(function(){\n        // common frontend js code\n    });\n    <?php js::stop() ?>\n</script>",
            $dir.'/tpl/example-frontend-file.php' => "<?php\n/**\n * @var \$this Plugin\n */\n\nrequire_once('_frontend.php');\n\n?>\n\n<div>Example frontend file</div>\n\n<script type=\"text/javascript\">\n    <?php js::start() ?>\n    $(function(){\n        // example js code\n    });\n    <?php js::stop() ?>\n</script>",
        );

        foreach ($list as $k=>$v) {
            $path = $extension->path($k, false);
            if ($v === false) { # dir
                if ( ! file_exists($path) && ! Files::makeDir($path, $chmod)) {
                    $errors->set(_t('dev', 'Невозможно создать директорию "[dir]"', array('dir' => $path)));
                    break;
                }
            } else { # file
                if ( ! file_exists($path) && ! Files::putFileContent($path, $v)) {
                    $errors->set(_t('dev','Невозможно создать файл "[file]"', array('file'=>$path)));
                    break;
                }
            }
        }

        if ( ! $errors->no()) {
            foreach ($errors->get(true, false) as $error) {
                $output->writeln('<error>' . $error . '</error>');
            }
            return 1;
        }
    }
}