<?php
/**
 * Системные настройки
 */

$config = array(
    'site.host'   => 'noomb.com',
    'site.static' => '//noomb.com',
    'site.title'  => 'Noomb.com', // название сайта, для подобных случаев: "Я уже зарегистрирован на {Noomb.com}"
    /**
     * Доступ к базе данных
     */
    'db.type' => 'mysql', // варианты: pgsql, mysql
    'db.host' => 'localhost', // варианты: localhost, ...
    'db.port' => '3306', // варианты: pgsql - 5432, mysql - 3306
    'db.name' => 'noomb_com',
    'db.user' => 'noomb_com',
    'db.pass' => '5B0s4F0p',
    'db.charset' => 'UTF8',
    'db.prefix' => 'bff_',
    /**
     * Локализация
     * Подробности добавления дополнительных локализаций описаны в файле /install/faq.txt
     */
     'locale.available' => array( // список языков (используемых на сайте)
        // ключ языка => название языка
//        'ru' => 'Русский',
        //'uk' => 'Українська',
        'en' => 'English',
        'fr' => 'Français',
        'de' => 'Deutsch',
//        'es' => 'Español',
//        'it' => 'Italiano',
//        'no' => 'Norsk',
        'nl' => 'Nederlands',
     ),
     'locale.short.titles' => [
        'en' => 'EN',
        'fr' => 'FR',
        'de' => 'DE',
        'nl' => 'NL',
     ],
     'locale.default' => 'en', // язык по-умолчанию
     //'locale.default.admin' => 'ru', // язык по-умолчанию в админ. панели
     'locale.hidden' => array( // языки скрытые от пользователей сайта
        //'en',
     ),
    'locale.client.default' => 'en', // язык по-умолчанию при определении языка браузера
    /**
     * Debug (для разработчика)
     */
    'php.errors.reporting' => -1, // all
    'php.errors.display'   => 0, // отображать ошибки (варианты: 1|0)
    'debug' => false, // варианты:true|false - включить debug-режим
    /**
     * Дополнительные настройки:
     * ! Настоятельно не рекомендуется изменять после запуска проекта
     */
    'date.timezone' => 'Europe/Kiev', // часовой пояс
    'config.sys.admin' => true, // Возможность редактирования большей части системных настроек через админ. панель в "режиме разработчика"
    'site.static.minify' => true, // Минимизация файлов статики: js, css
    'site.app.link'      => 'https://goo.gl/7jVCG5 https://goo.gl/JGYx6A',    // Ссылка на моб приложение (варианты: string|false, false-не использовать функционал)
    # SEO
    'seo.landing.pages.enabled' => true, // Задействовать посадочные страницы (варианты: true|false)
    'seo.landing.pages.fields'  => array(
        'titleh1' => array(
            't'=>'Заголовок H1',
            'type'=>'text',
        ),
        'seotext' => array(
            't'=>'SEO текст',
            'type'=>'wy',
        ),
    ),
    'seo.redirects' => true, // Задействовать редиректы (варианты: true|false)
    'geo.filter.url' => true, // Фильтр по региону из указанного в URL
    'device.desktop.responsive' => false, // Responsive для desktop версии сайта (false - выключен)
    # Хуки
    'hooks' => array(
        # Контакты (поля ввода)
        # Выключая уже используемые поля вы скрываете/удаляете контактные данные указанные пользователями ранее
        'users.contacts.fields' => function($list) {
            return config::merge($list, array(
                'skype'    => ['enabled' => 1],
                'icq'      => ['enabled' => 0],
                'whatsapp' => ['enabled' => 0],
                'viber'    => ['enabled' => 0],
                'telegram' => ['enabled' => 0],
                'example'  => [ # Ключ должен быть уникальным и содержать символы a-z
                    'title'    => _te('', 'Noomb.com title'),
                    'icon'     => 'fa fa-comment', # http://fontawesome.io/icons/
                    'priority' => 1,
                    'enabled'  => false, # включен - true, выключен - false
                ],
            ));
        },
        # Системы оплаты доступные пользователю:
        # 'enabled' => true, # включено
        # 'enabled' => false, # выключено
        # currency_id - ID валюты в разделе "Настройки сайта / Валюты"
        'bills.pay.systems.user' => function($list, $extra) {
            $list = config::merge($list, array(
                'robox' => array( # Robokassa
                    'enabled' => false,
                    'title'   => _t('bills', 'Robokassa'),
                ),
                'wm' => array( # Webmoney WMZ
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney'),
                ),
                'wmr' => array( # Webmoney WMR
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney WMR'),
                    'logo_desktop' => $extra['logoUrl'] . 'wm.png',
                    'logo_phone'   => $extra['logoUrl'] . 'wm.png',
                    'way'     => 'wmr',
                    'id'      => Bills::PS_WM,
                    'currency_id' => 2, # рубли
                ),
                'wmu' => array( # Webmoney WMU
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney WMU'),
                    'logo_desktop' => $extra['logoUrl'] . 'wm.png',
                    'logo_phone'   => $extra['logoUrl'] . 'wm.png',
                    'way'     => 'wmu',
                    'id'      => Bills::PS_WM,
                    'currency_id' => 1, # гривны
                ),
                'terminal' => array( # W1
                    'enabled' => false,
                    'title'   => _t('bills', 'Терминал'),
                ),
                'paypal' => array( # PayPal
                    'enabled' => false,
                    'title'   => _t('bills', 'Paypal'),
                ),
                'liqpay' => array( # LiqPay
                    'enabled' => false,
                    'title'   => _t('bills', 'Liqpay'),
                ),
                'yandex' => array( # Yandex.Деньги
                    'enabled' => false,
                    'title'   => _t('bills', 'С кошелька'),
                ),
                'yandexAC' => array( # Yandex.Деньги
                    'enabled' => false,
                    'title'   => _t('bills', 'Банковская карта'),
                ),
            ));
            return $list;
        },
        # Блок премиум/последних на главной:
        'bbs.index.last.blocks' => array(
            //'premium', 'last',
        ),
    ),
);

$config['locale.available.admin'] = array_keys($config['locale.available']);

if (file_exists(PATH_BASE.'config'.DIRECTORY_SEPARATOR.'sys-local.php')) {
    $local = include 'sys-local.php';
    return array_merge($config, $local);
}


return $config;
