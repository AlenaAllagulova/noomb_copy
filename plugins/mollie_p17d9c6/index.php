<?php

class Plugin_Mollie_p17d9c6 extends Plugin
{
    const ID = 2048; # PS_MOLLIE
    const KEY = 'mollie';

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id' => 'p17d9c68748e5d777dcfa22fcbe4d5b9557fc7a8',
            'plugin_title' => 'Плагин оплаты "Mollie"',
            'plugin_version' => '1.0.0',
        ));

        $aEnableSiteCurrency = Site::model()->currencyData(false, false, true);
        $aMollieCurrernsy = ['eur', 'usd'];
        foreach ($aEnableSiteCurrency as $cur) {
            if (in_array($cur['keyword'], $aMollieCurrernsy)) {
                $aCurrencyConfig[$cur['keyword']] = ($cur['keyword'] == 'usd' ? ['title' => 'United states dollar'] : ['title' => $cur['title']]);
            }
        }
        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'live_key' => array(
                'title' => 'Live API key',
                'input' => 'password',
            ),
            'test_key' => array(
                'title' => 'Test API key',
                'input' => 'password',
            ),
            'test_mode' => array(
                'type' => TYPE_BOOL,
                'title' => 'Тестовый режим',
                'input' => 'checkbox',
            ),
            'currency' => array(
                'title' => 'Валюта',
                'input' => 'select',
                'type' => TYPE_STR,
                'default' => in_array(Site::currencyDefault('keyword'), $aMollieCurrernsy) ? Site::currencyDefault('keyword') : 'eur',
                'options' => $aCurrencyConfig,
            ),
            'process_url' => array( #webhook
                'title' => 'Страница обработки оплаты',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('process', array('ps' => static::KEY)),
            ),
            'in-progress_url' => array(
                'title' => 'Страница ответа платежной системы',
                'input' => 'text',
                'readonly' => true,
                'default' => Bills::url('in-progress'),
            ),
        ));
    }

    protected function start()
    {
        bff::hookAdd('bills.pay.systems.user', array($this, 'user_list'));
        bff::hookAdd('bills.pay.systems.data', array($this, 'system_list'));
        bff::hookAdd('bills.pay.form', array($this, 'form'));
        bff::hookAdd('bills.pay.process', array($this, 'process'));
    }

    /**
     * Дополняем список доступных пользователю способов оплаты
     * @param array $list список систем оплат
     * @param array $extra : 'logoUrl', 'balanceUse'
     * @return array
     */
    public function user_list($list, $extra)
    {
        $list['mollie'] = array(
            'id' => static::ID,
            'logo_desktop' => $this->url('/mollie-logo.png'),
            'logo_phone' => $this->url('/mollie-logo.png'),
            'way' => '',
            'title' => _t('bills', 'Mollie'), # Название способа оплаты
            'currency_id' => Site::currencyDefault('id'), # Тэнге (ID валюты в системе)
            'enabled' => true, # Способ доступен пользователю
            'priority' => 0, # Порядок: 0 - последняя, 1+
        );
        return $list;
    }

    /**
     * Дополняем данными о системе оплаты
     * @param array $list
     * @return array
     */
    public function system_list($list)
    {
        $list[static::ID] = array(
            'id' => static::ID,
            'key' => static::KEY,
            'title' => _t('bills', 'Mollie'), # Название системы для описания счета в админ. панели
            'desc' => '',
        );
        return $list;
    }

    /**
     * Форма выставленного счета, отправляемая системе оплаты
     * @param string $form HTML форма
     * @param integer $paySystem ID системы оплаты для которой необходимо сформировать форму
     * @param array $data дополнительные данные о выставляемом счете:
     *  amount - сумма для оплаты
     *  bill_id - ID счета
     *  bill_description - описание счета
     * @return string HTML
     */
    public function form($form, $paySystem, $data)
    {
        if ($paySystem != static::ID) return $form;

        $key = $this->getApiKey();

        if (empty($key)) {
            $this->log('Api key for Payment system Mollie not found');
            return $form;
        }
        $mollie = new Mollie\Api\MollieApiClient();
        $mollie->setApiKey($key);

        $amount = number_format($data['amount'], 2);
        $currency = strtoupper($this->config('currency'));
        $payment = $mollie->payments->create([
            "amount" => [
                "currency" => $currency,
                "value" => $amount,
            ],
            "description" => "Payment for a bill #" . $data['bill_id'],
            "redirectUrl" => $this->config('in-progress_url'),
            "webhookUrl" => $this->config('process_url'),
            "metadata" => [
                "bill_id" => $data['bill_id'],
                'amount' => $amount
            ],
        ]);

        $aParams = [];
        $aParams['url'] = $payment->getCheckoutUrl();

        return $this->formCreate($aParams);
    }

    public function formCreate($aParams)
    {

        $form = '<form action="' . $aParams['url'] . '" method="POST">';
        foreach ($aParams as $key => $param) {
            $form .= '<input type="hidden" name="' . $key . '" value="' . $param . '" />';
        }
        $form .= '</form>';
        return $form;
    }

    /**
     * Обработка запроса / webhook от системы оплаты
     * Метод должен завершать поток путем вызова bff::shutdown();
     * @param string $system ключ обрабываемой системы оплаты
     */
    public function process($system)
    {
        if ($system != static::KEY) return;

        $key = $this->getApiKey();

        if (empty($key)) {
            $this->log('Api key for Payment system Mollie not found');
            return;
        }

        $aRequest = $_POST;
        unset($aRequest['s']);
        unset($aRequest['ev']);

        $mollie = new Mollie\Api\MollieApiClient();
        $mollie->setApiKey($key);

        $payment = $mollie->payments->get($aRequest["id"]);

        $bill_id = $payment->metadata->bill_id;
        $amount = $payment->metadata->amount;
        $isPaid = $payment->isPaid();

        if (!$isPaid) {
            $this->log('Bill #' . $bill_id . ' wasn`t paid');
            die('Bill #' . $bill_id . ' wasn`t paid');
        }

        if (empty($bill_id) || is_null($amount)) {
            $this->log('Wrong bill data from payment system Mollie');
            die('Wrong bill data from payment system Mollie');
        }
        $bResult = $this->bills()->processBill($bill_id, $amount, self::ID);

        if (empty($bResult)) {
            $this->log('Something wrong during saving data bill #' . $bill_id);
            die('Something wrong during saving data bill #' . $bill_id);
        }

        bff::shutdown();
    }

    protected function getValue($name)
    {
        $value = false;
        if (isset($_POST[$name])) $value = $_POST[$name];
        if (isset($_GET[$name])) $value = $_GET[$name];
        return $value;
    }

    protected function getApiKey()
    {
        if ($this->config('test_mode') && !empty($this->config('test_key'))) {
            return $this->config('test_key');
        }
        return $this->config('live_key');
    }

}