<?php
/**
 * @var $this Plugin
 */
    $aData['submitButtons'] = '';
?>

<?php if ($this->isEnabled()) { ?>
<div id="j-settings-testsms-form">
    <input type="text" name="phone" value="<?= HTML::escape($this->config('from')); ?>" placeholder="Phone number" />
    <textarea class="stretch" type="text" name="message" style="height: 100px;" placeholder="<?= $this->langAdmin('Test message', array(), true); ?>">Test message</textarea>

    <div style="margin-top: 5px;">
        <input type="button" class="j-submit-testsms-form btn btn-small btn-success button" value="<?= $this->langAdmin('Отправить SMS', array(), true); ?>" />
    </div>
</div>

<script type="text/javascript">
    $(function(){
        var $block = $('#j-settings-testsms-form');
        $block.on('click', '.j-submit-testsms-form', function(e){
            var $button = $(this);
            bff.ajax('<?= $this->adminLink('testSms', $this->getName()) ?>', {
                phone: $block.find('[name="phone"]').val(),
                message: $block.find('[name="message"]').val()
            }, function(data){
                if (data && data.success) {
                    //
                }
            }, function(){ $button.toggleClass('disabled'); });
        });
    });
</script>
<?php } else { ?>
    <div class="alert alert-info"><?= $this->langAdmin('Включите плагин для возможности его тестирования'); ?></div>
<?php } ?>