CREATE TABLE IF NOT EXISTS `/*TABLE_PREFIX*/push_onesignal_devices` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`token` VARCHAR(255) NULL DEFAULT NULL,
	`success` TINYINT(3) NULL DEFAULT 1,
	`created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `token` (`token`),
	INDEX `user_id` (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;