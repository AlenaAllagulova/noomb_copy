<link rel="manifest"  href="<?php echo $this->url('/js/manifest.json.php?gcm_sender_id=' . $this->config('gcm_sender_id')); ?>" />
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script> 
	var OneSignal = window.OneSignal || [];
	OneSignal.push( function() {
		OneSignal.SERVICE_WORKER_UPDATER_PATH = "OneSignalSDKUpdaterWorker.js.php";
		OneSignal.SERVICE_WORKER_PATH = "OneSignalSDKWorker.js.php";
		OneSignal.SERVICE_WORKER_PARAM = { scope: '/' };	
		OneSignal.setDefaultNotificationUrl("<?php echo $host; ?>");
		
		var oneSignal_options = {};
		window._oneSignalInitOptions = oneSignal_options;
		
		oneSignal_options['wordpress'] = false;
		oneSignal_options['appId'] = '<?php echo $this->config('app_id'); ?>';
		oneSignal_options['autoRegister'] = false;
		
		<?php if(!empty($this->config('safari_web_id'))) { ?>
			  oneSignal_options['safari_web_id'] = "<?php echo $this->config('safari_web_id'); ?>";
		<?php } ?>
		
		<?php if(!empty($this->config('http.permission_request'))) { ?>
			oneSignal_options['httpPermissionRequest'] = { };
			oneSignal_options['httpPermissionRequest']['enable'] = true;
			oneSignal_options['httpPermissionRequest']['modalTitle'] = "<?php echo $this->config('http.permission_request_modal_title'); ?>";
			oneSignal_options['httpPermissionRequest']['modalMessage'] = "<?php echo $this->config('http.permission_request_modal_message'); ?>";
			oneSignal_options['httpPermissionRequest']['modalButtonText'] = "<?php echo  $this->config('http.permission_request_modal_button_text'); ?>";
		<?php } ?>
		
		<?php if (!empty($this->config('welcome.notification'))) { ?>
			oneSignal_options['welcomeNotification'] = { };
			oneSignal_options['welcomeNotification']['title'] = "<?php echo $this->config('welcome.notification.title'); ?>";
			oneSignal_options['welcomeNotification']['message'] = "<?php echo $this->config('welcome.notification.message'); ?>";
         <?php if (!empty($this->config('welcome.notification.url'))) { ?>
            oneSignal_options['welcomeNotification']['url'] = "<?php echo $this->config('welcome.notification.url'); ?>";
         <?php }
        }
        else { ?>
			oneSignal_options['welcomeNotification'] = { };
			oneSignal_options['welcomeNotification']['disable'] = true;
        <?php } ?>		
		
		<?php if(!empty($this->config('http.subdomain'))) { ?>
			  oneSignal_options['subdomainName'] ="<?php echo $this->config('http.subdomain'); ?>";
			  oneSignal_options['promptOptions'] = {
					siteName: "<?php echo $sitename; ?>",
					actionMessage: "<?php echo $this->config('prompt_options.message'); ?>",
					exampleNotificationTitle: "<?php echo $this->config('prompt_options.ntitle'); ?>",
					exampleNotificationMessage: "<?php echo $this->config('prompt_options.nmessage'); ?>",
					exampleNotificationCaption: "<?php echo $this->config('prompt_options.ncaption'); ?>",
					acceptButtonText: "<?php echo $this->config('prompt_options.accept'); ?>",
					cancelButtonText: "<?php echo $this->config('prompt_options.cancel'); ?>"
				};
				oneSignal_options['httpPermissionRequest'] = {
					modalTitle: "<?php echo $this->config('http_permission_request.title'); ?>",
					modalMessage: "<?php echo $this->config('http_permission_request.message'); ?>",
					modalButtonText: "<?php echo $this->config('http_permission_request.button'); ?>"
				};
		<?php } else { ?>
			  oneSignal_options['path'] = "<?php echo $this->url('/js/'); ?>";
			  oneSignal_options['promptOptions'] = {
					actionMessage: "<?php echo $this->config('prompt_options.message'); ?>",
					acceptButtonText: "<?php echo $this->config('prompt_options.accept'); ?>",
					cancelButtonText: "<?php echo $this->config('prompt_options.cancel'); ?>"
				};
		<?php } ?>
		
		<?php if(!empty($notify_button)) { ?>
			oneSignal_options['notifyButton'] = { };
			oneSignal_options['notifyButton']['enable'] = <?php echo $notify_button;?>;
			oneSignal_options['notifyButton']['position'] = "<?php echo $this->config('notify_button.possition'); ?>";
			oneSignal_options['notifyButton']['text'] = {
				'tip.state.unsubscribed': "<?php echo $this->config('tip.state.unsubscribed'); ?>",
				'tip.state.subscribed': "<?php echo $this->config('tip.state.subscribed'); ?>",
				'tip.state.blocked': "<?php echo $this->config('tip.state.blocked'); ?>",
				'message.prenotify': "<?php echo $this->config('message.prenotify'); ?>",
				'message.action.subscribed': "<?php echo $this->config('message.action.subscribed'); ?>",
				'message.action.resubscribed': "<?php echo $this->config('message.action.resubscribed'); ?>",
				'message.action.unsubscribed': "<?php echo $this->config('message.action.unsubscribed'); ?>",
				'dialog.main.title': "<?php echo $this->config('dialog.main.title'); ?>",
				'dialog.main.button.subscribe': "<?php echo $this->config('dialog.main.button.subscribe'); ?>",
				'dialog.main.button.unsubscribe': "<?php echo $this->config('dialog.main.button.unsubscribe'); ?>",
				'dialog.blocked.title': "<?php echo $this->config('dialog.blocked.title'); ?>",
				'dialog.blocked.message': "<?php echo $this->config('dialog.blocked.message'); ?>"
			};		
		<?php } ?>
		
		OneSignal.init(window._oneSignalInitOptions);

		OneSignal.on('subscriptionChange', function (isSubscribed) {
			var action = 'delete_device';
			if(isSubscribed){ action = 'add_device'; }
			OneSignal.getUserId().then(function(userId) {
				$.post('?s=<?php echo $plugin_name; ?>&ev=onesignal', {act:action, token:userId});
			});
		});
	});

	function documentInitOneSignal() {
		 var oneSignal_elements = document.getElementsByClassName("OneSignal-prompt");
		 var oneSignalLinkClickHandler = function(event) { OneSignal.push(['registerForPushNotifications']); event.preventDefault(); };
		 for(var i = 0; i < oneSignal_elements.length; i++) oneSignal_elements[i].addEventListener('click', oneSignalLinkClickHandler, false);
	}
	 
	if (document.readyState === 'complete') {
		documentInitOneSignal();
	}
	else {
		window.addEventListener("load", function(event){
			documentInitOneSignal();
		});
	}
</script>