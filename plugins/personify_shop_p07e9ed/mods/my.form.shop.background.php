<div class="control-group">
                    <label class="control-label">
                        <?= _t('shops', 'Фон') ?>
    <br /><small><?= _t('shops', 'Установите фоновое изображения для вашего магазина') ?></small>
<? if ($shopBackgroundPayable): ?>
    <br /><small><?= _t('shops', '[_b]Платная настройка[b_]', $boldText) ?></small>
<? endif; ?>
    </label>
    <div class="controls">
        <div class="u-cabinet__settings__photo span6">
            <a class="v-author__background" href="#" onclick="return false;">
                <? if ( ! $is_edit): ?><input type="hidden" name="background" value="" id="j-shop-background-fn" /><? endif; ?>
                <img alt="" src="<?= $background_preview ?>" id="j-shop-background-preview" />
            </a>
        </div>
        <div class="u-cabinet__settings__photo_upload">
            <a href="javascript:void(0);" class="btn" id="<?= empty($shopBackgroundPayable)  ? 'j-shop-background-upload' : 'j-shop-background-upload-payable' ?>"><?= _t('shops', 'Загрузить фон') ?></a>
            <a href="javascript:void(0);" id="j-shop-background-delete" class="pseudo-link-ajax ajax-ico mrgl10 remove<? if(!$background){ ?> hide<? } ?>"><i class="fa fa-times"></i> <span><?= _t('shops', 'удалить') ?></span></a>
            <div class="help-block"><?= _t('shops', 'Максимальный размер файла - [size]', array('size'=>$background_maxsize_format)) ?></div>
        </div>
    </div>
    </div>

<? if ( ! empty($shopBackgroundPayable)): ?>
    <div id="shop-background-payable-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?= _t('shops', 'Фоновое изображение для магазина') ?></h3>
        </div>
        <div class="modal-body">
            <p>
                <?= _t('shops','Установка обложки (фонового избражения) магазина является ') .
                _t('shops', '[_b]платной настройкой[b_] и составляет [_b][price][b_] Данная сумма будет списана с Вашего счёта.',
                    array_merge(['price' => $shopBackgroundPayable . ' ' . Site::currencyDefault()], $boldText)) ?>
            </p>
            <p>
                <?= _t('shops','Если Вы согласны и желаете [_b]использовать платную настройку[b_], нажмите кнопку "[_b]Продолжить[b_]", иначе закройте это окно.', $boldText) ?>
            </p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><?= _t('shops','Отмена') ?></button>
            <a href="javascript:void(0);" class="btn btn-success" id="j-shop-background-upload"><?= _t('shops', 'Продолжить') ?></a>
        </div>
    </div>
<? endif; ?>