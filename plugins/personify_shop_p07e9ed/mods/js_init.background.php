<script type="text/javascript">
<? js::start() ?>
jShopsFormBackground.init(<?= func::php2js(array(
    'edit'       => $is_edit,
    'url_submit' => $url_submit,
    'lang'       => [
        'saved_success'              => _t('', 'Настройки успешно сохранены'),
        'background_upload_messages' => [
            'typeError'    => _t('shops', 'Допустимы только следующие типы файлов: {extensions}'),
            'sizeError'    => _t('shops', 'Файл {file} слишком большой, максимально допустимый размер {sizeLimit}'),
            'minSizeError' => _t('shops', 'Файл {file} имеет некорректный размер'),
            'emptyError'   => _t('shops', 'Файл {file} имеет некорректный размер'),
            'onLeave'      => _t('shops', 'Происходит загрузка изоражения, если вы покинете эту страницу, загрузка будет прекращена'),
        ],
        'background_upload'          => _t('shops', 'Загрузка фонового изображения'),
    ],
    'backgroundMaxSize'     => $background_maxsize,
    'shopBackgroundPayable' => ! empty($shopBackgroundPayable),
    'uploadProgress'        => '<div class="align-center j-progress" style="width:200px; height:80px; float:left; line-height:80px;"> <img alt="" src="'.bff::url('/img/loading.gif').'"> </div>',
)) ?>);
<? js::stop() ?>
</script>