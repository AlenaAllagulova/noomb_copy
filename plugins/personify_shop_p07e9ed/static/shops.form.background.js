var jShopsFormBackground = (function(){
    var inited = false, o = {edit:true, lang:{}, url_submit:''}, $cont, form;

    function init()
    {
        $cont = $('#j-shops-form');

        // background
        var $background = $cont.find('#j-shop-background-preview');

        if (typeof o.shopBackgroundPayable !== 'boolean') {
            o.shopBackgroundPayable = false;
        }

        if (o.shopBackgroundPayable) {
            $('#j-shop-background-upload-payable').on('click', function() {
                $('#shop-background-payable-modal').modal('show');
            });
            $cont.find('#j-shop-background-upload').on('click', function() {
                $('#shop-background-payable-modal').modal('hide');
            });
        }

        var $backgroundFilename = $cont.find('#j-shop-background-fn');
        var $backgroundDelete = $cont.find('#j-shop-background-delete');
        var backgroundUpload = new qq.FileUploaderBasic({
            button: $cont.find('#j-shop-background-upload').get(0),
            action: bff.ajaxURL('shops&ev=background', 'upload'),
            uploaded: 0, multiple: false, sizeLimit: o.backgroundMaxSize,
            allowedExtensions: ['jpeg','jpg','png','gif'],
            onSubmit: function(id, fileName) {
                var params = {hash: app.csrf_token};
                if( ! o.edit) params['tmp'] = $backgroundFilename.val();
                backgroundUpload.setParams(params);
                $background.parent().addClass('hidden').after(o.uploadProgress);
                return true;
            },
            onComplete: function(id, fileName, resp) {
                $background.parent().removeClass('hidden').next('.j-progress').remove();
                if(resp && resp.data.success) {
                    $background.attr('src', resp.data.preview);
                    if( ! o.edit) $backgroundFilename.attr('value', resp.data.filename);
                    $backgroundDelete.show();
                } else {
                    if(resp.errors) {
                        app.alert.error(resp.errors, {title: o.lang.background_upload});
                    }
                }
                return true;
            },
            messages: o.lang.background_upload_messages,
            showMessage: function(message, code) {
                app.alert.error(message, {title: o.lang.background_upload});
            }
        });
        // background delete
        $backgroundDelete.on('click', function(e){ nothing(e);
            $backgroundDelete.hide();
            bff.ajax(bff.ajaxURL('shops', 'delete&ev=background'),
                {fn:$backgroundFilename.val(), hash: app.csrf_token}, function(data, errors){
                    if(data && data.success) {
                        $background.attr('src', data.preview);
                    } else {
                        $backgroundDelete.show();
                        if(errors) app.alert.error(errors);
                    }
                });
        });
    }

    return {
        init: function(options)
        {
            if(inited) return; inited = true;
            o = bff.filter('shops.form.background.settings', $.extend(o, options || {}));
            $(function(){ init(); });
        },
    };
}());