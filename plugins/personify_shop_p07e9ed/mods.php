<?php
return [
    [
        'file' => '/modules/shops/tpl/def/admin.shops.form.info.php',
        'search' => '$(\'a[rel=shop-logo-fancybox]\', $container).fancybox();',
        'replace' => '$(\'a[rel=shop-background-fancybox]\', $container).fancybox();
                    <? if ( ! empty(Shops::$shopBackgroundPayable)): ?>
                    $(\'#j-shop-background-payable\').on(\'click\', function () {
                        var conf = confirm(\'<?= _t(\'shops\',\'Установка обложки (фонового избражения) магазина является платной настройкой и составляет [price] Данная сумма будет списана с Вашего счёта.\',
                            [\'price\' => Shops::$shopBackgroundPayable . \' \' . Site::currencyDefault()]) ?>\');
                        if ( ! conf) {
                            return false;
                        }
                    });
                    <? endif; ?>',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 1,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/tpl/def/admin.shops.form.info.php',
        'search' => 'return {',
        'replace' => 'deleteBackground: function(link)
                    {
                        if (confirm(\'<?= _t(\'shops\', \'Удалить фоновое изображение?\'); ?>\')) {
                            var $block = $(link).parent();
                            $block.hide().find(\'#shop_background_delete_flag\').val(1);
                            $block.prev().show();
                            return false;
                        }
                    },',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/tpl/def/my.form.php',
        'search' => '$show_titles = $is_open;',
        'replace' => '$boldText = [\'_b\' => \'<b>\', \'b_\' => \'</b>\'];',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/tpl/def/my.form.php',
        'search' =>  '<div class="u-cabinet__settings__block__form">',
        'replace' => $this->path('mods/my.form.shop.background.php'),
        'replace-file' => true,
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/tpl/def/my.form.php',
        'search' =>  '</script>',
        'replace' => $this->path('mods/js_init.background.php'),
        'replace-file' => true,
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/shops.adm.class.php',
        'search' =>  '# сохраняем настройки магазина',
        'replace' => '                        if (\bff::hooksAdded(\'user.shop.form.data\')){
                            $aData = bff::filter(\'user.shop.form.data\', $aData, [\'shop_id\'  => $nShopID,
                                    \'user_id\'  => $nUserID,
                                    \'response\' => &$aResponse]
                            );
                        }',
        'replace-file' => false,
        'position' => 'before',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/shops/shops.class.php',
        'search' =>  '$this->abonementOpen($aResponse, $nShopID);',
        'replace' => '                    if (\bff::hooksAdded(\'shop.open.settings.data\')){
                        $aResponse = bff::filter(\'shop.open.settings.data\', [\'shop_id\' => $nShopID,
                            \'data\' => &$aData,
                            \'response\' => &$aResponse,
                        ]);
                    }',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 1,
        'index' => 2,
    ],
    [
        'file' => '/modules/shops/shops.class.php',
        'search' =>  '$aData[\'url_submit\'] = ($aData[\'is_edit\'] ? Users::url(\'my.settings\', array(\'act\'=>\'shop\')) : static::url(\'my.open\') );',
        'replace' => '        if (\bff::hooksAdded(\'shops.form.data\')){
            $aData = bff::filter(\'shops.form.data\', $aData);
        }',
        'replace-file' => false,
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ],
    [
        'file' => '/modules/users/users.adm.class.php',
        'search' =>  'Shops::model()->shopSave($nShopID, $aShopData);',
        'replace' => '                        if (\bff::hooksAdded(\'user.shop.form.data\')){
                            $aShopData = bff::filter(\'user.shop.form.data\', $aShopData, [\'shop_id\'  => $nShopID,
                                                                                         \'user_id\'  => $nUserID,
                                                                                         \'response\' => &$aResponse]
                            );
                        }',
        'replace-file' => false,
        'position' => 'before',
        'offset' => 0,
        'index' => 1,
    ],

];