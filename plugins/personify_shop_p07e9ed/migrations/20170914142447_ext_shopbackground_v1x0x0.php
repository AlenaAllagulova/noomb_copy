<?php

use bff\db\migrations\Migration as Migration;

class ExtShopbackgroundV1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(DB_PREFIX.'shops')
            ->addColumn('background', 'string', [ 'null' => true, 'limit' => 50])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $table = $this->table(DB_PREFIX.'shops');
        $table->removeColumn('background')
            ->update();
    }
}
