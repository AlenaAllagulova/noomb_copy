<?php
class Plugin_ShopBackground_ShopsBase extends Plugin_ShopBackground_ShopsBase_Base{

    # фон
    /**
     * @var
     */
    public static $shopBackground;

    # снимать деньги за фон магазина
    public static $shopBackgroundPayable;

    public function init(){

        parent::init();

        $sDirName = PluginLoaderPersonifyShop::getPluginInstance()->getMainDirName();

        bff::autoloadEx(array(
                'ShopsBackground'   => ['app', 'plugins'.DS.$sDirName.DS.'modules'.DS.'shops'.DS.'shops.background.php'],
            )
        );

        self::$shopBackground = '';
        self::$shopBackgroundPayable = PluginLoaderPersonifyShop::getPluginInstance()->config('payable');
    }

    /**
     * Инициализация компонента работы с фоном магазина
     * @param integer $nShopID ID магазина
     * @return ShopsBackground объект
     */
    public function shopBackground($nShopID = 0)
    {
        return ShopsBackground::i($nShopID);
    }

    /**
     * Устанавливает фон для магазина
     * @param $nShopID - id магазина
     * @param null $sFileName - имя файла
     * @return bool|string
     */
    public static function setShopBackground($nShopID, $sFileName = null)
    {
        if (empty($nShopID)) {
            return false;
        }
        if (empty($sFileName)) {
            $aData = self::model()->shopData($nShopID, 'background');
            $sFileName = $aData['background'];
        }
        $sBackground = ShopsBackground::url($nShopID, $sFileName, ShopsBackground::szOrigin, false, true);
        self::$shopBackground = $sBackground;
        return $sBackground;
    }

    /**
     * Возвращает фон для магазина
     * @return string
     */
    public static function getShopBackground()
    {
        return static::$shopBackground;
    }

}