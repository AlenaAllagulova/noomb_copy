<?php
class Plugin_ShopBackground_Shops extends Plugin_ShopBackground_Shops_Base{

    /**
     * Управление состоянием (загрузка / удаление) фонового изображения магазина
     */
    public function background()
    {
        $this->security->setTokenPrefix('my-settings');
        $nShopID = User::shopID();
        $oShopBackground = ShopsBackground::i($nShopID);
        $aResponse = [];
        switch ($this->input->get('act', TYPE_NOTAGS)) {
            case 'upload': # загрузка
            {
                if (!$this->security->validateToken()) {
                    $this->errors->reloadPage();
                    break;
                }

                if ( ! empty(static::$shopBackgroundPayable)) {
                    $aBillsResult = Bills::model()->changeUserBalance(User::id(), User::balance(),
                                                                        static::$shopBackgroundPayable,
                                                                        PluginLoaderPersonifyShop::getPluginInstance()->lang('Установка фонового изображения для магазина'));
                    if ( ! $aBillsResult['state']) {
                        $this->errors->set($aBillsResult['msg']);
                        break;
                    }
                }

                $aResponse = $this->backgroundUpload($nShopID, $oShopBackground);
            }
            break;
            case 'delete': # удаление
            {
                $aResponse = $this->backgroundDelete($nShopID, $oShopBackground);
            }
            break;
        }
        $this->ajaxResponseForm($aResponse);
    }

    /**
     * Загрузка фонового изображения магазина
     * @param $nShopID
     * @param $oShopBackground
     * @return array
     */
    public function backgroundUpload($nShopID, $oShopBackground)
    {
        $bTmp = !$nShopID;
        $mResult = $oShopBackground->uploadQQ(true, !$bTmp);
        $aResponse = [];

        if (!empty($mResult)) {
            $aResponse = array_merge($aResponse, $mResult);
            $aResponse['preview'] = ShopsBackground::url($nShopID, $mResult['filename'], ShopsBackground::szView, $bTmp);
            if ($bTmp) {
                $tmp = $this->input->postget('tmp', TYPE_STR);
                $oShopBackground->deleteTmp($tmp);
            } else {
                # отправляем на пост-модерацию: смена фона
                $this->model->shopSave($nShopID, array('moderated' => 2));
            }
        }

        return $aResponse;

    }

    /**
     * Удаление фонового изображения магазина
     * @param $nShopID
     * @param $oShopBackground
     * @return array
     */
    public function backgroundDelete($nShopID, $oShopBackground)
    {
        $aResponse = [];

        if ($this->security->validateToken(true, false)) {
            if ($nShopID) {
                $oShopBackground->delete(true);
            } else {
                $oShopBackground->deleteTmp($this->input->post('fn', TYPE_NOTAGS));
            }
            if ($this->errors->no()) {
                $aResponse['preview'] = $oShopBackground->urlDefault();
            }
        }

        return $aResponse;
    }

}