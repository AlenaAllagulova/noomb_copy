<?php

class ShopsBackground extends CImageUploader
{
    const szView = 'v'; # view - просмотр
    const szOrigin = 'o'; # origin - оригинал

    const FILE_NAME_LETTERS = 6;
    const MAX_SIZE = 3145728; // 3мб
    const MIN_WIDTH = 1;
    const MIN_HEIGHT = 1;
    const MAX_WIDTH = 1500;
    const MAX_HEIGHT = 1500;
    protected $size_view_width = 218; // ширина предпросмотра фонового изображеня в настройках магазина и админ панели
    const SIZE_VIEW_HEIGHT = false;
    const SIZE_VIEW_ORIGIN = true;

    protected static $instance = null;


    public static function i($nRecordID = 0)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
            self::$instance->setRecordID($nRecordID);
        }
        return self::$instance;
    }

    private function __constructor()
    {
        $this->initSettings();
    }

    public function setBackgroundViewWidth(){
        $this->size_view_width = PluginLoaderPersonifyShop::getPluginInstance()->config('view_width');
    }

    function initSettings()
    {
        $this->path = bff::path('shop' . DS . 'background', 'images');
        $this->pathTmp = bff::path('tmp', 'images');

        $this->url = bff::url('shop/background', 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->table = TABLE_SHOPS;
        $this->fieldID = 'id';
        $this->fieldImage = 'background';
        $this->filenameLetters = static::FILE_NAME_LETTERS;
        $this->folderByID = true;
        $this->maxSize = static::MAX_SIZE;
        $this->minWidth = static::MIN_WIDTH;
        $this->minHeight = static::MIN_HEIGHT;
        $this->maxWidth = static::MAX_WIDTH;
        $this->maxHeight = static::MAX_HEIGHT;
        $this->setBackgroundViewWidth();
        $this->sizes = [
            self::szView  => [
                'width' => $this->size_view_width,
                'height' => static::SIZE_VIEW_HEIGHT
            ],
            self::szOrigin  => [
                'o' => static::SIZE_VIEW_ORIGIN
            ],
        ];
    }

    public static function url($nShopID, $sFilename, $sSizePrefix, $bTmp = false, $bDefault = false)
    {
        $i = self::i($nShopID);
        if (empty($sFilename)) {
            return ($bDefault ? $i->urlDefault() : false);
        }
        return $i->getURL($sFilename, $sSizePrefix, $bTmp);
    }

    public function urlDefault()
    {
        return $this->url . 'default.gif';
    }

    /**
     * Обработка формы загрузки логотипа
     * @param bool $bOnCreate загрузка логитипа при создании магазина
     * @param string $sInputFile имя поля загрузки логотипа (input="file")
     * @param string $sInputDeleteCheckbox имя поля удаления логотипа (input="checkbox")
     * @return mixed
     */
    public function onSubmit($bOnCreate, $sInputFile = 'shop_background', $sInputDeleteCheckbox = 'shop_background_del')
    {
        if ($bOnCreate) {
            $this->setAssignErrors(false);
            $aUpload = $this->uploadFILES($sInputFile, false, false);
            if (!empty($aUpload['filename'])) {
                return $aUpload['filename'];
            }
        } else {
            $aUpload = $this->uploadFILES($sInputFile, true, false);
            if (!empty($aUpload['filename'])) {
                return $aUpload['filename'];
            } else {
                if ($this->input->postget($sInputDeleteCheckbox, TYPE_BOOL)) {
                    if ($this->delete(false)) {
                        return '';
                    }
                }
            }

            return false;
        }
    }
}