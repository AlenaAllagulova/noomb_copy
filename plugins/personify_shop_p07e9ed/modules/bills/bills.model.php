<?php
class Plugin_ShopBackground_BillsModel extends Plugin_ShopBackground_BillsModel_Base
{

    /**
     * Изменение счета пользователя
     * @param $nUserID - uid
     * @param $fUserBalance - баланс пользователя
     * @param $fAmount - списываемая виртуальная сумма
     * @param string $sDescription - описание платежа
     * @return array - ['state' - состояние платежа, 'msg' - сообщение о платеже]
     */
    public function changeUserBalance($nUserID, $fUserBalance, $fAmount, $sDescription = '')
    {
        $aResult = [
            'state' => false,
            'msg' => _t('orders', 'Не удается создать счет.')
        ];
        if ($fUserBalance >= $fAmount) {
            $nBillID = Bills::model()->billSave(0, [
                    'user_id'      => $nUserID,
                    'user_balance' => $fUserBalance - $fAmount,
                    'type'         => Bills::TYPE_OUT_SERVICE,
                    'status'       => Bills::STATUS_COMPLETED,
                    'amount'       => $fAmount,
                    'currency_id'  => Site::currencyDefault('id'),
                    'description'  => $sDescription,
                    'payed'        => $this->db->now(),
                ]
            );
            if ( ! empty($nBillID) && BillsModuleBase::i()->updateUserBalance($nUserID, $fAmount, false)) {
                $aResult = [
                    'state' => true,
                    'msg' => _t('orders', 'Оплата [description] прошла успешно.', ['description' => $sDescription])
                ];
            }
        } else {
            $aResult = [
                'state' => false,
                'msg' => _t('orders', 'На Вашем счету должно быть больше [money] [currency]', ['money' => $fAmount, 'currency' => Site::currencyDefault()])
            ];
        }
        return $aResult;
    }


}