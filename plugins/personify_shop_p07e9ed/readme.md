1. Установите и Включите плагин в [списке плагинов](?s=site&ev=extensionsmanager&type=1).

2. Настройка плагина: 
 * Не снимать деньги за фон магазина - значение по умолчанию (0 - бесплатно, > 0 - платно)
 * Ширина предпросмотра фонового изображеня магазина в настройках магазина и админ панели

3. Фильтр данных карточки магазина при редактировании из админ панели, в данном плагине необходим для удаления 
   или сохранения фона магазина.
   
   Добавьте в файл класса **{file:/modules/shops/shops.adm.class.php}** следующий код:

```
if (\bff::hooksAdded('user.shop.form.data')){
        $aData = bff::filter('user.shop.form.data', $aData, ['shop_id'  => $nShopID,
                'user_id'  => $nUserID,
                'response' => &$aResponse]
        );
}
```
Рекомендуемое расположение - перед строкой: `# сохраняем настройки магазина
                                             $res = $this->model->shopSave($nShopID, $aData);`
                                             
4. Добавлять фоновое изображение можно при открытии магазина и далеее при редактировании его настроек, а так же 
    в админ панели в меню пользователи (в карточке пользователя, таб "Магазин") и в меню магазины (в карточке магазина)

5. Фильтр данных карточки магазина при отбражении в админ панели, в данном плагине необходим для вывода 
фонового изображения магазина.
   
   Добавьте в файл класса **{file:/modules/shops/shops.adm.class.php}** следующий код:

```
if (\bff::hooksAdded('shops.adm.formInfo.data')){
            $aData = bff::filter('shops.adm.formInfo.data', $aData);
        }
```
Рекомендуемое расположение - перед строкой: `return $this->viewPHP($aData, 'admin.shops.form.info');`

6. Фильтр настроек магазина в кабинете пользователя при его открытии, в данном плагине необходим для возможности
сохранения фонового изображения в момент открытия магазина.
   
   Добавьте в файл класса **{file:/modules/shops/shops.class.php}** следующий код:

```
if (\bff::hooksAdded('shops.adm.formInfo.data')){
            $aData = bff::filter('shops.adm.formInfo.data', $aData);
        }
```
Рекомендуемое расположение - после строк: `# Абонемент
                                           if (static::abonementEnabled()) {
                                               $this->abonementOpen($aResponse, $nShopID);
                                           }`
                                           
7. Фильтр настроек магазина в кабинете пользователя при редактировании, в данном плагине необходим для возможности
вывода фонового изображения, если оно имеется.
   
   Добавьте в файл класса **{file:/modules/shops/shops.class.php}** следующий код:

```
if (\bff::hooksAdded('shops.form.data')){
            $aData = bff::filter('shops.form.data', $aData);
        }
```
Рекомендуемое расположение - перед строкой: `return $this->viewPHP($aData, 'my.form');`

8. Фильтр настроек магазина в админ панели в карточке пользователя в табе "Магазин" при редактировании, 
в данном плагине необходим для возможности сохраненния или удаления фонового изображения.
   
   Добавьте в файл класса **{file:/modules/shops/shops.class.php}** следующий код:

```
if (\bff::hooksAdded('user.shop.form.data')){
    $aShopData = bff::filter('user.shop.form.data', $aShopData, ['shop_id'  => $nShopID,
                                                                 'user_id'  => $nUserID,
                                                                 'response' => &$aResponse]
    );
}
```
Рекомендуемое расположение - перед строкой: `Shops::model()->shopSave($nShopID, $aShopData);`
