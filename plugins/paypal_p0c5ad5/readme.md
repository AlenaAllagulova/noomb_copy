Выполните следующие настройки аккаунта в кабинете системы Paypal:

1. Перейдите в кабинете системы в раздел "Profile -> Profile and settings > My business info".

1.1. Скопируйте значение поля "Merchant account ID" в настройку плагина "**PayPal ID**".

2. Активируйте "Paypal Instant Payment Notifications". 

2.1. Для этого выберите таб "My selling tools" и перейдите в раздел "Instant payment notifications -> Update".

2.2. Укажите в поле "**Notification URL**" значение из поля настроек плагина "IPN URL".

2.3. Включите настройку "**Message delivery**" и выполните сохранение настроек.

3. Включите перенаправление пользователя после завершения платежа.

3.1. Для этого выберите таб "My selling tools" и перейдите в раздел "Website preferences -> Update".

3.2. Укажите настройку **Auto Return** в значение ON.

3.3. Укажите в поле "**Return URL**" значение из поля настроек плагина "Return URL" и выполните сохранение настроек.

4. Включите плагин в [списке плагинов](?s=site&ev=extensionsmanager&type=1).
