<?php

use bff\db\migrations\Migration as Migration;
use Phinx\Db\Adapter\MysqlAdapter;

class ExtP03da668dc2001b9d128eafb3a35ef793f16597dV1x0x0 extends Migration
{
    public function migrate()
    {
        $this->table(TABLE_BBS_ITEMS_FAV)
            ->addColumn('pricedrop_default', 'decimal', array('signed'=>false, 'precision' => 11, 'scale' => 2, 'default' => 0))
            ->addColumn('pricedrop_price', 'decimal', array('signed'=>false, 'precision' => 11, 'scale' => 2, 'default' => 0))
            ->addColumn('pricedrop_change', 'datetime', array('null' => true))
            ->addColumn('pricedrop_sent', 'datetime', array('null' => true))
            ->update();

        $this->table(TABLE_USERS)
            ->addColumn('enotify_pricedrop_bbs', 'integer',  ['limit' => MysqlAdapter::INT_TINY, 'default' => 1, 'signed' => false, 'after' => 'enotify'])
            ->update();
    }

    public function rollback()
    {
        $this->table(TABLE_BBS_ITEMS_FAV)
            ->removeColumn('pricedrop_default')
            ->removeColumn('pricedrop_price')
            ->removeColumn('pricedrop_change')
            ->removeColumn('pricedrop_sent')
            ->update();

        $this->table(TABLE_USERS)
            ->removeColumn('enotify_pricedrop_bbs')
            ->update();
    }
}