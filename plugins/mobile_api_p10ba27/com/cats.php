<?php
require_once 'inc/Api_Abstract.php';

class Api_FN extends Api_Abstract 
{
    public $FN='cats';
    
    function FNget_tree_cats($pa) {
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        $inf = $this->Model_Api->modelTreeCats($pa);
	    $this->rez['RESULT'] = 1;
	    $this->rez['cats_tree']['rows'] = $inf;
	    return true;
    }

    function FNget_cat_dyns($pa) {
        require_once $this->SET['base_site_path'].'bff.php';

        if(!isset($pa['id'])) {
            $this->errAddMsg('NOT_CAT_ID');
        } else {
            if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
            $inf = $this->Model_Api->getAllCatDynProperties($pa);
            $cat = $this->Model_Api->modelLoneCatInfo($pa['id'], $pa['lang']);
            $this->rez['RESULT'] = 1;
        }	
    
        if($this->rez['RESULT'] == 0) {
            $this->rez['MESSAGE'] = $this->errTxtAr;
        } else {
            $this->rez['add_info'] = $inf;
            $this->rez['cat_info'] = $cat;
            $this->rez['currency_list'] = $this->Model_Api->modelCurrenciesList($pa);
        }
        return true;
    }

    function start() {}
}