<?php
require_once 'inc/Api_Abstract.php';

class Api_FN extends Api_Abstract {
	public $FN = 'user';
	public $loginMinLength = 6;
	public $loginMaxLength = 50;

	function FNboard_settings($pa) {
		$this->rez['RESULT'] = 1;
		$this->rez['settings'] = [
			'users.profile.phones' => config::sysAdmin('users.profile.phones', 5, TYPE_UINT),
			'currency.default' => config::sysAdmin('currency.default', 1, TYPE_UINT),
			'users.register.social.email.activation' => config::sysAdmin('users.register.social.email.activation', 1, TYPE_UINT),
			'users.register.phone' => config::sysAdmin('users.register.phone', false, TYPE_UINT),
			'bbs.premoderation' => config::sysAdmin('bbs.premoderation', true, TYPE_UINT),
			'shops.exist' => (integer)bff::moduleExists('shops'),
			'shops.categories.enabled' => config::sysAdmin('shops.categories', false, TYPE_UINT)
		];

        if (bff::moduleExists('shops')) {
            if (config::sysAdmin('shops.categories', false, TYPE_BOOL)) {
                $this->rez['settings']['shops.categories.limit'] = Shops::categoriesLimit();
            }
            $this->rez['settings']['shops.abonement'] = config::sysAdmin('shops.abonement', 0, TYPE_UINT);
            if ($this->rez['settings']['shops.abonement']) {
                $this->rez['settings']['shops.abonement.default.limit'] = config::sysAdmin('shops.abonement.default.limit', 100, TYPE_UINT);
            }
        }
	}

	function FNuser_login($pa) {
		$inf = $this->Model_Api->modelUserLogin($pa);
		if (isset($inf['err'])) {
			$this->errAddMsg($inf['err']);
			if (!empty($inf['details'])) $this->rez['details'] = $inf['details'];
			return;
		}

		if (!isset($pa['imcode'])) $pa['imcode'] = '';
		$r = $this->session_starting(array('type'=>'n', 'user_id' => $inf['id'], 'imcode' => @$pa['imcode']));
		if (false === $r)
			return false;
		$this->rez['RESULT'] = 1;
		$this->rez['SESID'] = $r['sid'];
		$this->rez['user_info'] = $inf;

        $this->Model_Api->modelUpStat($inf['id'], $r['sid']);

		return true;
	}

	function FNuser_social_connect($pa) {
		$inf = $this->Model_Api->modelUserSocialConnect($pa);
		if (isset($inf['err'])) {
			$this->errAddMsg($inf['err']);
			return;
		}

		if (!isset($pa['imcode'])) $pa['imcode'] = '';
		$r = $this->session_starting(array('type'=>'n', 'user_id' => $inf['id'], 'imcode' => @$pa['imcode']));
		if (false === $r)
			return false;
		$this->rez['RESULT'] = 1;
		$this->rez['SESID'] = $r['sid'];
		$this->rez['user_info'] = $inf;

		$this->Model_Api->modelUpStat($inf['id'], $r['sid']);

		return true;
	}

	function FNuser_logout($pa) {
		if (!isset($this->sesrow['user_id'])) return;
		$this->Model_Api->modelUserLogout($pa);
		$this->session_end($this->sesrow['sid']);
		$this->rez['RESULT'] = 1;
		return true;
	}

	function FNget_info($pa){
		if (isset($this->sesrow['user_id'])) $pa['id'] = $this->sesrow['user_id'];
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$inf = $this->Model_Api->modelUserGetInfo($pa['id'], $pa['lang']);
		if (!isset($inf['id']))
			return;
		$this->rez['user_info'] = $inf;
		$this->rez['RESULT'] = 1;
	}

	function FNedit_info($pa){
		$this->rez['RESULT'] = (int)$this->Model_Api->modelUserEdit($pa);

		if ($this->rez['RESULT']) {
			$this->rez['user_info'] = $this->Model_Api->modelUserGetInfo($this->sesrow['user_id']);
		} else {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
	}

	function FNchange_password($pa){
		$this->rez['RESULT'] = (int)$this->Model_Api->modelChangePassword($pa);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
		return true;
	}

	function FNchange_email($pa){
		$this->rez['RESULT'] = (int)$this->Model_Api->modelChangeEmail($pa);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
		return true;
	}

	function FNavatar($pa){
		$this->rez['RESULT'] = (int)$this->Model_Api->modelUserAvatarFromFile($pa['img']);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
		return true;
	}

    function FNcheck_code($pa){

        if (!empty($pa['code']) && !empty($pa['user_id'])) {

            $inf = $this->Model_Api->modelCheckCode($pa);

            if (isset($inf['err'])) {
                $this->errAddMsg($inf['err']);
                return;
            }

            $this->rez['RESULT'] = 1;
            $this->rez['SESID'] = $this->sesrow['sid'];
            $this->rez['user_info'] = $this->Model_Api->modelUserGetInfo($pa['user_id']);
            return true;

        } else {
			$this->rez['RESULT'] = 0;
            return false;
        }

    }

    function FNget_code($pa){

        $inf = $this->Model_Api->modelGet_code($pa); //$pa['phone_number'], $pa['user_id']
        if (isset($inf['err'])) {
            $this->errAddMsg($inf['err']);
            return;
        }

        $this->rez['RESULT'] = 1;
        return true;

    }

    function FNadd_soc($pa){

        $userData = $this->Model_Api->GetSocialData($pa['provider_id'], $pa['profile_id'], $pa['token']);

        if (isset($userData['err'])) {
            $this->errAddMsg($userData['err']);
            return;
        }

        $response = $this->Model_Api->modelAddSoc($pa, $userData);
        if (isset($response['err'])) {
            $this->errAddMsg($response['err']);
            return;
        }

        $this->rez['RESULT'] = 1;
		$this->rez['user_info'] = $response;
        return true;

    }

	function FNuser_register($pa){

		if (isset($pa['email'])) {
			$login = mb_substr($pa['email'], 0, mb_strpos($pa['email'], '@'));
			$login = preg_replace('/[^a-z0-9\_]/ui', '', $login);
			$login = mb_strtolower(trim($login, '_ '));

			if (mb_strlen($login) >= $this->loginMinLength) {
				if (mb_strlen($login) > $this->loginMaxLength) {$login = mb_substr($login, 0, $this->loginMaxLength);}
			}
			$pa['login'] = $this->Model_Api->modelUserLoginGenerate($login);
		}

		$pa['activated'] = 1;
		$pa['enotify'] = 0;
		$pa['admin'] = 0;
		$pa['member'] = 1;
		$pa['user_id_ex'] = substr(md5(uniqid(mt_rand(), true)), 0, 6);
		$pa['created_ip'] = ip2long($this->get_user_ip());

		$inf = $this->Model_Api->modelUserAdd($pa);
		if (isset($inf['err'])) {
			if (!empty($inf['min_length'])) {
				$this->errAddMsg(['msg' => $inf['err'], 'min_length' => $inf['min_length']]);
			} else {
				$this->errAddMsg($inf['err']);
			}
			return;
		}

		if (!isset($pa['imcode'])) $pa['imcode'] = ''; // code device
		$r = $this->session_starting(array('type'=>'n', 'user_id' => $inf['user_id'], 'imcode' => @$pa['imcode']));
		if (false === $r)
			return false;
		$this->rez['RESULT'] = 1;
		$this->rez['SESID'] = $r['sid'];
		$this->rez['user_info'] = $inf;

        $this->Model_Api->modelUpStat($inf['id'], $r['sid']);

		return true;
	}

	function FNintmail_list($pa) {
		if (isset($pa['recipient'])) {
			$pa['recipient'] = $this->sesrow['user_id'];
		} elseif(isset($pa['author'])) {
			$pa['author'] = $this->sesrow['user_id'];
		} elseif(isset($pa['all_my'])) {
			$pa['all_my'] = $this->sesrow['user_id'];
		} else {
			return;
		}

		if (!isset($pa['page']))
			$pa['page'] = 0;
		if (!isset($pa['onpage']))
			$pa['onpage'] = 10;

		$this->rez['intmail_list'] = $this->Model_Api->modelIntMailList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Отправляем сообщение другому пользователю
	 */
	function FNintmail_add($pa){
		if(!isset($pa['shop_id'])) $pa['shop_id'] = 0;
		if(!isset($pa['interlocutor'])) $pa['interlocutor'] = 0;
		if(!isset($pa['message'])) $pa['message'] = '';
		if(!isset($pa['from_shop'])) $pa['from_shop'] = 0;
		if(!isset($pa['item_id'])) $pa['item_id'] = 0;

		if ($pa['item_id'] > 0) {
			$aData = BBS::model()->itemData($pa['item_id'], array('user_id', 'shop_id'), true);
			if (empty($aData)) {
				$this->rez['RESULT'] = 0;
				$this->rez['MESSAGE'] = 'ITEM_NOT_FOUND';
			}
			$nResult = $this->Model_Api->modelIntMailAddNew($aData['user_id'], $aData['shop_id'], $pa['message'], $pa['from_shop'], $pa['item_id']);
		} else {
			$nResult = $this->Model_Api->modelIntMailAddNew($pa['interlocutor'], $pa['shop_id'], $pa['message'], $pa['from_shop']);
		}
		if (empty($nResult)) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'UNKNOWN_ERROR';
		} else {
			$this->rez['intmail_id'] = $nResult;
			$this->rez['RESULT'] = 1;
		}
	}

	/**
	 * Получаем список переписок
	 * Необходимые поля во входящем объекте:
	 * (option) integer page - номер страницы
	 * (option) integer onpage - чатов на странице
	 */
	function FNchats_list($pa){
		if(!isset($pa['page'])) $pa['page'] = 0;
		if(!isset($pa['onpage'])) $pa['onpage'] = 10;
		if(!isset($pa['shop_id'])) $pa['shop_id'] = 0;
		if(!isset($pa['folder_id'])) $pa['folder_id'] = 0;
		if(!isset($pa['search'])) $pa['search'] = '';

		# Формируем лимит выборки
		$limit = ' LIMIT '.(intval($pa['page'])*intval($pa['onpage'])).','.intval($pa['onpage']);

		//$this->rez['chats_list']=$this->Model_Api->modelContactsList($this->sesrow['user_id'], $pa['shop_id'], $limit);
		$this->rez['chats_list']=$this->Model_Api->modelContactsListNew($this->sesrow['user_id'], $pa['shop_id'], $pa['folder_id'], $pa['search'], $limit);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Получаем сообщения переписки
	 * Необходимые поля во входящем объекте:
	 * (option) integer interlocutor - айди пользователя, переписку с которым надо открыть
	 * (option) integer page - номер страницы
	 * (option) integer onpage - чатов на странице
	 */
	function FNchat_messages($pa){
		if(!isset($pa['page'])) $pa['page'] = 0;
		if(!isset($pa['onpage'])) $pa['onpage'] = 10;
		if(!isset($pa['interlocutor'])) $pa['interlocutor'] = 0;
		if(!isset($pa['shop_id'])) $pa['shop_id'] = 0;
		if(!isset($pa['from_shop'])) $pa['from_shop'] = 0;

		if (empty($pa['interlocutor']) && empty($pa['shop_id'])){
			$this->rez['MESSAGE'] = 'RECEIVER_NOT_DEFINED';
			return;
		}

		# Формируем лимит выборки
		$limit = ' LIMIT '.(intval($pa['page'])*intval($pa['onpage'])).','.intval($pa['onpage']);

		//$this->rez['chats_list']=$this->Model_Api->modelConversationMessages($this->sesrow['user_id'], $pa['interlocutor'], $pa['sort'], $limit);
		$aResult = $this->Model_Api->modelConversationMessagesNew($pa['interlocutor'], $pa['shop_id'], $pa['from_shop'], $limit);
		$this->rez['RESULT'] = 1;
		if (empty($aResult) && !empty($this->Model_Api->errTxtAr)) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'unknown error';
		} else {
			$this->rez['chat'] = empty($aResult) ? new stdClass() : $aResult;
		}
	}

	function FNuser_balance($pa) {
		$this->rez['RESULT'] = 0;
		if (!isset($this->sesrow['user_id'])) return;
		$this->rez['user_balance'] = $this->Model_Api->modelUserBalance($pa);
		$this->rez['RESULT'] = 1;
		return true;
	}

	function FNuser_set_android_push_key($pa) {
		$this->rez['RESULT'] = 0;
		if (!isset($pa['key'])) {
			$this->errAddMsg('PUSH_KEY_NOT_DEFINED');
			return;
		}
		$this->rez['RESULT'] = $this->Model_Api->modelSetUserAndroidPushKey($pa['key']);
		return true;
	}

	function FNshops_list($pa) {
		$result = $this->Model_Api->modelShopsList($pa);

		if (empty($result)) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'unknown error';
		} else {
			$this->rez['RESULT'] = 1;
			$this->rez['shops_list'] = $result;
		}
		return true;
	}

	function FNshop_open($pa) {
		$result = $this->Model_Api->modelOpenShop($pa);

		if (empty($result)) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'unknown error';
		} else {
			$this->rez['RESULT'] = 1;
		}
		return true;
	}

	function FNshop_edit($pa) {
		$result = $this->Model_Api->modelEditShop($pa);

		if (empty($result)) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'unknown error';
		} else {
			$this->rez['RESULT'] = 1;
		}
		return true;
	}

	function FNshop_categories($pa) {
		$result = $this->Model_Api->shopCategoriesList($pa);

		if ($result === 0) {
			$this->rez['RESULT'] = 0;
			$this->rez['MESSAGE'] = !empty($this->Model_Api->errTxtAr) ? $this->Model_Api->errTxtAr : 'unknown error';
		} else {
			$this->rez['RESULT'] = 1;
			$this->rez['shops_categories_list'] = $result;
		}
		return true;
	}

	function FNshop_svc_list($pa) {
        $this->rez['RESULT'] = 1;
        $this->rez['shop_svc_list'] = $this->Model_Api->shopSvcList($pa);
    }

    /**
     * Покупка премиум услуг магазина
     */
    function FNshop_svc_buy($pa) {
        $this->rez['RESULT'] = 0;
        if (!isset($this->sesrow['user_id'])) {
            $this->errAddMsg('NOT_LOGGED');
        } else if (!isset($pa['id'])) {
            $this->rez['MESSAGE'] = 'SHOP_ID_NOT_DEFINED';
            return;
        } else if (!isset($pa['svc_id'])) {
            $this->rez['MESSAGE'] = 'SVC_ID_NOT_DEFINED';
            return;
        }
        if (empty($pa['ps'])) {
            $pa['ps'] = 'balance';
        }
        $this->rez['RESULT'] = $this->Model_Api->promoteShop($pa);
        if ($this->rez['RESULT'] == 0) {
            $this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
        } else {
            $this->rez['user_balance'] = $this->Model_Api->modelUserBalance([]);
        }
    }

    function FNshop_svc_abonement_list($pa) {
        $this->rez['RESULT'] = 1;
        $this->rez['shop_svc_abonement_list'] = $this->Model_Api->shopSvcAbonementList($pa);
    }

    function FNshop_svc_abonement_buy($pa) {
        $lang = '';
        $this->rez['RESULT'] = 0;
        if (!isset($this->sesrow['user_id'])) {
            $this->errAddMsg('NOT_LOGGED');
        } else if (!isset($pa['shop_id'])) {
            $this->rez['MESSAGE'] = 'SHOP_ID_NOT_DEFINED';
            return;
        } else if (!isset($pa['abonement_id'])) {
            $this->rez['MESSAGE'] = 'ABONEMENT_ID_NOT_DEFINED';
            return;
        } else if (!isset($pa['abonement_period'])) {
            $this->rez['MESSAGE'] = 'ABONEMENT_PERIOD_NOT_DEFINED';
            return;
        }
        if (empty($pa['ps'])) {
            $pa['ps'] = 'balance';
        }

        $this->rez['RESULT'] = $this->Model_Api->buyAbonement(
            $pa['shop_id'],
            $pa['abonement_id'],
            $pa['abonement_period'],
            $pa['ps']
        );
        if ($this->rez['RESULT'] == 0) {
            $this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
        } else {
            $this->rez['user_balance'] = $this->Model_Api->modelUserBalance([]);
        }
    }

	function start() {}

}