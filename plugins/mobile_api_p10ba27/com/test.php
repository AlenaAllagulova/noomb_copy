<?php
require_once 'inc/Api_Abstract.php';

class Api_FN extends Api_Abstract {

	function run_init() {}

	function mk_fnCom($com, $sesid) {
		$de = array();
		$de['SESID'] = $sesid;
		$de['VERSION'] = 'test2.0';

		$fn = explode('.', $com);
		if (isset($this->com_ar[$fn[0]])) {
			if(isset($fn[1])) {
				if(isset($this->com_ar[$fn[0]]['fns'][$fn[1]])) {
					$this->mk_fnComItem($de, $this->com_ar[$fn[0]]['fns'][$fn[1]]);
				}
			} else {
				$this->mk_fnComItem($de, $this->com_ar[$fn[0]]);
			}
		}
		$inp_req = json_encode($de);
		return $inp_req;
	}

	function mk_fnComItem(&$de, $pa) {
		foreach ($pa as $k => $v) {
			if ($k == 'SESID') continue;
			if ($k == 'fns') continue;
			if ($v[0] == 's') $de[$k] = '';
			if ($v[0] == 'i') $de[$k] = '';
			if ($v[0] == 'a') $de[$k] = new stdClass();
		}
		return $de;
	}

	/*
	 * Получаем список доступых функций
	 * @return array
	 */
	function get_fnList(){
		$ar = array();
		foreach ($this->com_ar as $k => $v){
			if (isset($v['fns'])) {
				foreach($v['fns'] as $k2=>$v2){
					$ar[$k.'.'.$k2]=1;
				}
			} else {
				$ar[$k]=1;
			}
		}
		return $ar;
	}
        
	function start()
    {
        $m = $this;
        if (get_magic_quotes_gpc())
            foreach ($_POST as $k => $v)
                $_POST[$k] = stripslashes($v);
        $inp_com = isset($_POST['inp_com']) ? $_POST['inp_com'] : '';
        $inp_req = isset($_POST['inp_req']) ? $_POST['inp_req'] : '';
        $sesid = isset($_POST['sesid']) ? $_POST['sesid'] : '';
        $inp_rez = '';
        if (isset($_POST['submitsel'])) {
            $inp_req = $m->mk_fnCom($inp_com, $sesid);
        }
        if (isset($_POST['submitgo'])) {

            $se = explode('.', $inp_com);
            $fn = $m->SET['domain_api'] . $se[0]; //.'.php';

            $options = array(
                'http' => array(
                    'method' => 'POST',
                    'content' => $inp_req,
                    'timeout' => 800,
                    'header' => "Content-Type: application/json",
                ),
            );
            $context = stream_context_create($options);

            $inp_rez = file_get_contents($fn, false, $context);

            $h = unpack('H*', substr($inp_rez, 0, 16));

            $ar = @json_decode($inp_rez, true);
            if ($ar === NULL) $ar = $inp_rez;
            if (is_array($ar) && isset($ar['SESID']))
                $sesid = $ar['SESID'];
            $inp_rez = preg_replace("/=>[\r\n  ]+array/", '=>array', preg_replace("/Array[\r\n  ]+\(/", 'Array(', var_export($ar, true)));

        }

        echo '<!DOCTYPE html>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
					<title>test</title>
				</head>
				<body>
					<div style="padding-bottom: 5px; margin-bottom: 5px; border-bottom: 1px solid black;">
						Test &gt;  <!--.php-->
						<a href="test">test</a> |
					</div>
					<div id="diverror"></div>
					test';

        if ($inp_com != 'bbs.item_add') {
            $action = 'test';
        } else {
            $action = 'bbs';
        }

        echo '<FORM METHOD=POST ACTION="' . $action . '" enctype="multipart/form-data">';

        if ($action == 'test'){
            echo '<div>Команда: <select name="inp_com">';
            $ar = $m->get_fnList();
            foreach ($ar as $k => $v) {
                echo '<option value="' . $k . '" ' . (($k == $inp_com) ? 'selected' : '') . '>' . $k . '</option>';
            }
            echo '</select><INPUT TYPE="submit" name="submitsel" value="sel" />';
        } else {
            echo '<input name="inp_com" type="hidden" value="'.$inp_com.'"/>';
        }

        echo '</div>
						<div>SESID:<input type="text" name="SESID" value="'. $sesid . '" size=40 /><INPUT TYPE="submit" name="submitgo" value="run" /></div>';

        if($inp_com != 'bbs.item_add'){
            echo '<TEXTAREA NAME="inp_req" ROWS="8" COLS="100">' . $inp_req . '</TEXTAREA>
					<TEXTAREA NAME="rez" ROWS="27" COLS="100">' . $inp_rez . '</TEXTAREA><hr />';
        } else {
            echo '<div>
                    cat_id:<INPUT TYPE="text" name="item_add[cat_id]" value="" /><br>
                    title:<INPUT TYPE="text" name="item_add[title]" value="" /><br>
                    descr:<INPUT TYPE="text" name="item_add[descr]" value="" /><br>
                    name:<INPUT TYPE="text" name="item_add[name]" value="" /><br>
                    city_id:<INPUT TYPE="text" name="item_add[city_id]" value="" /><br>
                    images[]:<INPUT TYPE="file" name="images[]" value="" /><br>
                    images[]:<INPUT TYPE="file" name="images[]" value="" /><br>
                    </div>';
        }

        echo '</FORM><hr /></body>
			</html>';
	}
}