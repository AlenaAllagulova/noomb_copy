<?php
require_once 'inc/Api_Abstract.php';

class Api_FN extends Api_Abstract {
	public $FN = 'bbs';

	/**
	 * Список категорий
	 */
	function FNcateg_list($pa) {
		$pa['icon'] = 'b'; // b или s
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$this->rez['categ_list'] = $this->Model_Api->modelCatsList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Данные объявления
	 */
	function FNitem_info($pa) {
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$pa['icon'] = 'b'; // b или s

		$r = $this->Model_Api->modelItemDetail($pa);
		if (isset($r['id'])) {
			$this->rez['item_info'] = $r;
			$this->rez['currency_list'] = $this->Model_Api->modelCurrenciesList($pa);
			$this->rez['RESULT'] = 1;
		} else {
			$this->errAddMsg($r['err']);
			$this->rez['RESULT'] = 0;
		}
	}

	/**
	 * Список объявлений
	 */
	function FNitems_list($pa){
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		if (!isset($pa['page'])) $pa['page'] = 0;
		if (!isset($pa['onpage'])) $pa['onpage'] = 100;

		$this->rez['items_list'] = $this->Model_Api->modelItemsList($pa);
		$this->rez['currency_list'] = $this->Model_Api->modelCurrenciesList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Пожаловаться на объявление
	 */
	function FNitem_claim($pa){
		$this->rez['RESULT'] = 0;
		if (isset($this->sesrow['user_id']))
			$pa['user_id'] = $this->sesrow['user_id'];
		$pa['user_ip'] = $this->get_user_ip();
		$r = $this->Model_Api->modelItemClaimAdd($pa);
		if (isset($r['err'])) {
			$this->errAddMsg($r['err']);
		} else {
			$this->rez['RESULT'] = 1;
		}
	}

	/**
	 * Список доступных валют
	 */
	function FNcurrency_list($pa) {
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$this->rez['currency_list'] = $this->Model_Api->modelCurrenciesList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Создание объявления
	 */
	function FNitem_add($pa) {
		require_once $this->SET['base_site_path'].'bff.php';

		$inf = null;
		if (!isset($this->sesrow['user_id'])) {
			$this->errAddMsg('AUTHOR_NOT_FOUND');
		} else if (empty($pa)) {
			$this->errAddMsg('NOT_DATA');
		} else {
			$pa['user_id'] = $this->sesrow['user_id'];

			$inf = $this->Model_Api->modelItemAdd($pa);

			$this->rez['RESULT'] = $this->Model_Api->rez['RESULT'];
			$this->errTxtAr = $this->Model_Api->errTxtAr;
		}

		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->errTxtAr;
		} else {
			$this->rez['add_info'] = $inf;
		}
	}

	/**
	 * Обновление объявления
	 */
	function FNitem_update($pa) {
		require_once $this->SET['base_site_path'].'bff.php';

		$inf = null;
		if (!isset($this->sesrow['user_id'])) {
			$this->errAddMsg('AUTHOR_NOT_FOUND');
		} else if(empty($pa)) {
			$this->errAddMsg('NOT_DATA');
		} else {
			$pa['user_id']=$this->sesrow['user_id'];
			$inf = $this->Model_Api->modelItemUpdate($pa);
			$this->rez['RESULT'] = $this->Model_Api->rez['RESULT'];
			$this->errTxtAr = $this->Model_Api->errTxtAr;
		}

		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->errTxtAr;
		} else {
			$this->rez['item'] = $this->Model_Api->modelItemDetail(['id' => $pa['id'], 'icon' => 'b']);
			$this->rez['update_info'] = $inf;
		}
	}

	/**
	 * Получаем список избранных объявлений
	 */
	function FNfav_list($pa) {
		$pa['user_id'] = $this->sesrow['user_id'];
		$this->rez['fav_list'] = $this->Model_Api->modelFavList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Обновление избранных объявлений
	 */
	function FNfav_update($pa) {
		$pa['user_id'] = $this->sesrow['user_id'];
		$this->rez['items_fav'] = $this->Model_Api->modelFavUpdate($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Получаем список регионов
	 */
	function FNregion_list($pa) {
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$this->rez['region_list'] = $this->Model_Api->modelRegionList($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Получаем объявление для редактирования
	 * Необходимые поля во входящем объекте:
	 * (mandatory) integer id - айди объявления
	 * (option) string lang - язык
	 */
	function FNitem_load($pa){
		if (!isset($pa['id'])){
			return;
		}
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$this->rez['item_data']=$this->Model_Api->modelItemload($this->sesrow['user_id'], $pa['id'], $pa['lang']);
		$this->rez['RESULT']=1;
	}

	/**
	 * Смена статуса объявления
	 */
	function FNitem_change_status($pa) {
		if (!isset($this->sesrow['user_id'])) {
			$this->errAddMsg('NOT_LOGGED');
		} else if (empty($pa)) {
			$this->errAddMsg('NOT_DATA');
		} else if (!isset($pa['id'])) {
			$this->errAddMsg('ITEM_NOT_SPECIFIED');
		} else if (!isset($pa['status'])) {
			$this->errAddMsg('NEW_STATUS_NOT_SPECIFIED');
		} else if (!in_array($pa['status'], ['unpublicate', 'publicate', 'refresh', 'delete'])) {
			$this->errAddMsg('NEW_STATUS_NOT_VALID');
		} else {
			if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
			$pa['user_id'] = $this->sesrow['user_id'];
			$this->rez['item_data']=$this->Model_Api->modelItemChangeStatus($pa);
			$this->rez['RESULT'] = $this->Model_Api->rez['RESULT'];
			$this->errTxtAr = $this->Model_Api->errTxtAr;
		}

		if ($this->rez['RESULT'] == 0) {
			unset($this->rez['item_data']);
			$this->rez['MESSAGE'] = $this->errTxtAr;
		}
	}

	/**
	 * Увеличиваем счетчик просмотра контактов
	 * Необходимые поля во входящем объекте:
	 * (mandatory) integer id - айди объявления
	 */
	function FNitem_viewup($pa){
		if (!isset($pa['id'])){
			$this->rez['MESSAGE'] = 'ITEM_ID_NOT_DEFINED';
			return;
		}
		$this->rez['RESULT'] = $this->Model_Api->modelItemContactsViewUp($pa['id']);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
	}

	function FNcountry_code($pa) {
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$this->rez['country_code'] = $this->Model_Api->modelCountryCode($pa);
		$this->rez['RESULT'] = 1;
	}

	/**
	 * Бесплатный подъем вверх своего объявления
	 * Необходимые поля во входящем объекте:
	 * (mandatory) integer id - айди объявления
	 */
	function FNitem_freeup($pa){
		$this->rez['RESULT'] = 0;
		if (!isset($this->sesrow['user_id'])) {
			$this->errAddMsg('NOT_LOGGED');
		} else if (!isset($pa['id'])){
			$this->rez['MESSAGE'] = 'ITEM_ID_NOT_DEFINED';
			return;
		}
		$this->rez['RESULT'] = $this->Model_Api->modelFreeItemUp($pa['id']);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		}
	}

	function FNreg_info($pa){
		$this->rez['RESULT'] = 0;
		if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
		$info = $this->Model_Api->getRegionDataById($pa);

		if (empty($info)) {
			$this->rez['MESSAGE'] = empty($this->Model_Api->errTxtAr) ? [['msg' => 'NOTHING_FOUND']] : $this->Model_Api->errTxtAr;
		} else {
			$this->rez['RESULT'] = 1;
			$this->rez['region_info'] = $info;
		}
	}

	function FNsvc_list($pa){
		$this->rez['RESULT'] = 1;
		$this->rez['svc_list'] = $this->Model_Api->svcList($pa);
	}

	/**
	 * Покупка премиум услуг
	 */
	function FNsvc_buy($pa){
		$this->rez['RESULT'] = 0;
		if (!isset($this->sesrow['user_id'])) {
			$this->errAddMsg('NOT_LOGGED');
		} else if (!isset($pa['id'])){
			$this->rez['MESSAGE'] = 'ITEM_ID_NOT_DEFINED';
			return;
		} else if (!isset($pa['svc_id'])){
			$this->rez['MESSAGE'] = 'ITEM_ID_NOT_DEFINED';
			return;
		}
		if (empty($pa['ps'])) $pa['ps'] = 'balance';
		$this->rez['RESULT'] = $this->Model_Api->itemSvc($pa);
		if ($this->rez['RESULT'] == 0) {
			$this->rez['MESSAGE'] = $this->Model_Api->errTxtAr;
		} else {
			$this->rez['user_balance'] = $this->Model_Api->modelUserBalance([]);
		}
	}

	function start() {}
}