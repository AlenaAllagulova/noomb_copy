<?php
require 'Model_Api.php';

define('TABLE_SESSIONS', 'api_sess_v2');
define('TABLE_APPREGID', 'api_appregid_v2');
define('TABLE_APPLEREGID', 'api_appleregid_v2');

abstract class Api_Abstract {
	protected $rez = array('RESULT' => 0, 'VERSION' => 'api_2.0');
	public $DB;
	public $SET;
	public $Model_Api;
	protected $req = array();
	protected $sesrow = array();
	protected $errTxtAr = array();
	protected $languages = array();
	protected $languageDefault = '';

    const TABLE_SHOPS = 'bff_shops';

	protected $com_ar = array(
		'user' => array(' ',
            'fns' => array(
				'user_login' => array(' ', 'user_login' => 'a'),
				'user_logout' => array(' ', 'user_logout' => 'a'),
				'edit_info' => array(' ', 'SESID' => 's', 'edit_info' => 'a'),
				'get_info' => array(' ', 'SESID' => 's', 'get_info' => 'a'),
                'check_code' => array(' ', 'SESID' => 's', 'check_code' => 'a'),
                'get_code' => array(' ', 'SESID' => 's', 'get_code' => 'a'),
                'add_soc' => array(' ', 'SESID' => 's', 'add_soc' => 'a'),
				'user_register' => array(' ', 'user_register' => 'a'),
				'intmail_list' => array(' ', 'SESID' => 's', 'intmail_list' => 'a'),
				'intmail_add' => array(' ', 'SESID' => 's', 'intmail_add' => 'a'),
				'chats_list' => array(' ', 'SESID' => 's', 'chats_list' => 'a'),
				'chat_messages' => array(' ', 'SESID' => 's', 'chat_messages' => 'a'),
				'change_password' => array(' ', 'SESID' => 's', 'change_password' => 'a'),
				'change_email' => array(' ', 'SESID' => 's', 'change_email' => 'a'),
				'avatar' => array(' ', 'SESID' => 's', 'avatar' => 'a'),
				'user_balance' => array(' ', 'SESID' => 's', 'user_balance' => 'a'),
				'user_set_android_push_key' => array(' ', 'SESID' => 's', 'user_set_android_push_key' => 'a'),
				'user_social_connect' => array(' ', 'user_social_connect' => 'a'),
				'board_settings' => array(' ', 'board_settings' => 'a'),
				'shops_list' => array(' ', 'shops_list' => 'a'),
				'shop_open' => array(' ', 'SESID' => 's', 'shop_open' => 'a'),
				'shop_edit' => array(' ', 'SESID' => 's', 'shop_edit' => 'a'),
				'shop_categories' => array(' ', 'shop_categories' => 'a'),
                'shop_svc_list' => array(' ', 'shop_svc_list' => 'a'),
                'shop_svc_buy' => array(' ', 'SESID' => 's', 'svc_buy' => 'a'),
                'shop_svc_abonement_list' => array(' ', 'shop_svc_abonement_list' => 'a'),
                'shop_svc_abonement_buy' => array(' ', 'SESID' => 's', 'shop_svc_abonement_buy' => 'a'),
			)
		),
        'bbs'=>array(' ',
            'fns'=>array(
				'region_list' => array(' ', 'region_list' => 'a'),
				'categ_list' => array(' ', 'categ_list' => 'a'),
				'items_list' => array(' ', 'items_list' => 'a'),
				'item_info' => array(' ', 'item_info' => 'a'),
				'fav_list' => array(' ', 'SESID' => 's','fav_list' => 'a'),
				'fav_update' => array(' ', 'SESID' => 's', 'fav_update' => 'a'),
				'item_claim' => array(' ', 'item_claim' => 'a'),
				'item_add' => array(' ', 'SESID' => 's', 'item_add' => 'a'),
				'currency_list' => array(' ', 'currency_list' => 'a'),
				'item_update' => array(' ', 'SESID' => 's', 'item_update' => 'a'),
				'item_change_status' => array(' ', 'SESID' => 's', 'item_change_status' => 'a'),
				'country_code' => array(' ', 'country_code' => 'a'),
				'item_viewup' => array(' ', 'item_viewup' => 'a'),
				'item_freeup' => array(' ', 'SESID' => 's', 'item_freeup' => 'a'),
				'reg_info' => array(' ', 'reg_info' => 'a'),
				'svc_list' => array(' ', 'svc_list' => 'a'),
				'svc_buy' => array(' ', 'SESID' => 's', 'svc_buy' => 'a'),
			)
		),
        'cats' => array(' ',
            'fns' => array(
                'get_tree_cats' => array(' ', 'get_tree_cats' => 'a'),
                'get_cat_dyns' => array(' ', 'get_cat_dyns' => 'a'),
			)
		),
	);


	public function __construct() {
		require_once 'setting.php';

		$this->SET = $set_;
		$this->languages = array_keys(config::sys('locale.available'));
		$this->languageDefault = config::sys('locale.default');

        $this->DB = bff::database();
	}

	public function __destruct() {}


	public function incom_raw_post() {

		$post_data = json_decode(file_get_contents( 'php://input' ), true);

		if (!empty($post_data) && !is_array($post_data)) {
            $this->errAddMsg('JSON_ERROR');
            return false;
        }

        //Add post array
        if (empty($post_data) || !is_array($post_data)) {
            $post_data = $_POST;
        }

        //Add files array
        foreach($post_data as $key => $val){
		    if(!in_array($key,['SESID','VERSION','s','ev','submitgo','inp_com'])){ $post_data[$key]['files'] = $_FILES; break; }
        }

        //print_r($post_data); exit;

		if (!isset($post_data['SESID'])) $post_data['SESID'] = '';
		$this->req = $post_data;
		return true;
	}

	public function run_init() {
		$this->run_init2();
		if (is_object($this->DB)) $this->DB->disconnect();
		$this->display();
		return true;
	}
        
	public function run_init2() {
		if (!$this->incom_raw_post())
			return false;
		if (!isset($this->com_ar[$this->FN])) {
			$this->errAddMsg('ERROR');
			return false;
		}

		if (isset($this->req['SESID']) && ($this->req['SESID'] != '')) {
			if (!$this->session_isset($this->req['SESID']))
				return false;
		}

		$com = $this->com_ar[$this->FN];

		if (isset($com['SESID'])) {
			if (!isset($this->sesrow['time']))
				return false;
		}
		$this->Model_Api = new Model_Api();
		$this->Model_Api->DB = &$this->DB;
		$this->Model_Api->SET = &$this->SET;
		$this->Model_Api->sesrow = $this->sesrow;

		foreach ($com['fns'] as $k => $v) {
			if (isset($this->req[$k])) {
				if (isset($v['SESID'])) {
					if (!isset($this->sesrow['time'])) {
						$this->errAddMsg('NO_PERMISSION');
						return false;
					}
				}
				if ($v[$k] == 'a')
					if (!is_array($this->req[$k]))
						continue;
				$FN2 = 'FN' . $k;
				$this->$FN2($this->req[$k]);
			}
		}

		return true;
	}

	public function session_starting($pa){
		if ($pa['user_id'] > 0) {
			$this->DB->delete(TABLE_SESSIONS, ['user_id' => (int)$pa['user_id']]);
		}
		$this->DB->delete(TABLE_SESSIONS, ['time<' . (time()-60*60*24*180)]);
		$sid = $this->str_genrand(32);
		$de = array(
			'sid' => $sid,
			'time' => time(),
			'type' => substr($pa['type'], 0, 1),
			'user_id' => (int)$pa['user_id'],
			'ip' => $this->get_user_ip(),
			'imcode' => substr(@$pa['imcode'], 0, 32),
		);
		$id = $this->DB->insert(TABLE_SESSIONS, $de);
		if ($id === false) {
			$this->errAddMsg('INIT_SESSION_FAILD');
			return false;
		}
		return array('sid' => $sid);
	}

	public function session_end($sid) {
		$this->DB->delete(TABLE_SESSIONS, ['sid' => $sid]);
	}

	public function session_isset($sid){
		if (strlen($sid) < 10)
			return false;
		$row = $this->DB->one_array('SELECT * from ' . TABLE_SESSIONS . ' WHERE sid = :sid', ['sid' => $sid]);
		if (false === $row) {
			$this->errAddMsg('NO_SESSION');
			return false;
		}
		$this->DB->update(TABLE_SESSIONS, ['time' => time()], ['sid' => $row['sid']]);
		$this->sesrow = $row;
		return $row;
	}

	public function session_update($sid, $ar) {
		$aQueryArgs = array();
		if (isset($ar['type'])) $aQueryArgs['type'] = $ar['type'];
		if (isset($ar['user_id'])) $aQueryArgs['user_id'] = (int)$ar['user_id'];
		if (empty($aQueryArgs))
			return false;
		return $this->DB->update(TABLE_SESSIONS, $aQueryArgs, ['sid' => $sid]);
	}

	public function session_isutypes($t){
		if (!isset($this->sesrow['havetype']))
			return false;
		$se = array_flip(str_split($this->sesrow['havetype']));
		foreach (str_split($t) as $v) {
			if (isset($se[$v]))
				return true;
		}
		return false;
	}

	public function date2time($t) {
		$date = DateTime::createFromFormat('Y-m-d H:i:s', $t);
		return $date->getTimestamp();
	}

	public function errAddMsg($pa){
		if (!is_array($pa))
			$pa = array('msg' => $pa);
		$this->errTxtAr[] = $pa;
	}

	public function get_user_ip(){
		if (isset($_SERVER['HTTP_CLIENT_IP']) && !empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		if (strpos($ip, ',')) {
			$se = explode(',', $ip);
			$ip = $se[0];
		}
		return $ip;
	}

	public function str_genrand($len, $type = '') {
		$t = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
		if ($type == 'human')
			$t = '1234567890qwertyupasdfghjkzxcvbnm';
		$ml = strlen($t) - 1;
		$s = '';
		for ($i = 0; $i < $len; $i++) {
			$s .= $t[rand(0, $ml)];
		}
		return $s;
	}

	public function display() {
		# типа тест вывода?
		/*if (isset($this->FN)) {
			if (in_array($this->FN, array('------zakaz_add'))) {
				$post_data = file_get_contents( 'php://input' );
				$s = '';
			}
		}*/

		header('Content-Type: application/json; charset=UTF-8', true);

		if (sizeof($this->errTxtAr) > 0)
			$this->rez['MESSAGE'] = $this->errTxtAr;
		$sJson = json_encode($this->rez);
		echo $sJson;
	}

}
