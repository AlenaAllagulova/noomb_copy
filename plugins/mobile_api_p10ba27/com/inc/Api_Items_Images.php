<?php
class ApiBBSItemImages extends BBSItemImages //CImagesUploaderTable
{
    public function getMinWidth() {
        return $this->minWidth;
    }
    public function getMinHeight() {
        return $this->minHeight;
    }
    public function getMaxWidth() {
        return $this->maxWidth;
    }
    public function getMaxHeight() {
        return $this->maxHeight;
    }

    public function getUser() {
        return $this->userID;
    }

    public function getRecord() {
        return $this->recordID;
    }
}