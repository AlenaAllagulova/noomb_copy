<?php

/**
 * Получаем системные настройки
 * @param string|array $key ключ настройки, ключи строек
 * @param string|mixed $default значение по-умолчанию
 * @param string $keyPrefix префикс ключа настройки
 * @param boolean $keyPrefixCut true - отрезать префикс ключа, false - оставить
 * @examples:
 *   config::sys('db.host') => 'data', - настройка по ключу
 *   config::sys('host', '', 'db') => 'data', - настройка по ключу, с префиксом
 *   config::sys([]) => [...] - все системные настройки
 *   config::sys([], [], 'db') => ['db.host','db.name',...] - все настройки с префиксом 'db'
 *   config::sys(['db.host','db.name']) => ['db.host','db.name'] - перечисленные настройки
 *   config::sys(['host','name'], [], 'db') => ['db.host','db.name'] - перечисленные настройки с префиксом
 * 'site.host', 'site.static', 'https.only', 'db.host', 'db.name', 'db.user', 'db.pass',
 * @return mixed
 */

    date_default_timezone_set(config::sys('date.timezone'));

    $set_ = [
        'main_site_path' => str_replace('\\','/',str_replace(':\\',':/',dirname(dirname(__FILE__)))).'/',
        'public_site_path' => PATH_PUBLIC,
        'base_site_path' => PATH_BASE,
        'premoderation' => (int)config::sysAdmin('bbs.premoderation'),
        'HTTPS' => (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off')
    ];

    $set_['http_'] = ($set_['HTTPS']?'https:':'http:');
    $set_['domain'] = $set_['http_'].'//'.config::sys('site.host').'/';
    $set_['domain_api'] = $set_['http_'].'//'.config::sys('site.host').'/api/';