<?php
/*
* @version 1.5
* @copyright
* Copyright (C) 2012-2014 Chip
*/

class UploaderFiles {
	var $set = array();
	var $msgs = array(
		'err' => 'Error upload.',
		'maxsize' => 'Max size of uploding file is {size}.',
		'alowf' => 'Allowed file extension: {ext}.',
	);
	var $rez = array(
		'r' => '',
		'fname' => '',
		'fext' => '',
		'fpath' => '',
		'old_name' => ''
	);

	function __construct($set = ''){
		$this->set = $set;
	}

	function main($file = false) {
		if (!is_array($file)) $file = $_FILES['imgfile'];

		if (isset($file)) {
			$r = $this->upload_img($file);
			if (true !== $r)
				return $r;
			if (isset($this->set['tb_size']))
				$this->img_resize_tb();
			return true;
		}
		return false;
	}

	function upload_file($se){
		if (!isset($se['name']))
			return $this->msgs['err'];
		if (is_array($se['name']))
			return $this->msgs['err'];
		if ($se['error'] != 0)
			return $this->msgs['err'];
		if (empty($se['tmp_name']))
			return $this->msgs['err'];

		$this->rez['old_name'] = urldecode($se['name']);

		$this->rez['size'] = $se['size'];
		if ($se['size'] > $this->hum_conv_size($this->set['maxsize']))
			return str_replace('{size}', $this->set['maxsize'], $this->msgs['maxsize']);

		$w = explode('.', $this->rez['old_name']);
		$ext = strtolower(array_pop($w));
		$name = implode('.', $w);
		if (!in_array($ext, $this->set['allowed_ext']))
			return str_replace('{ext}', implode(', ', $this->set['allowed_ext']), $this->msgs['alowf']);

		$this->rez['fname'] = $this->mk_newfname($name, $ext);
		$this->rez['fext'] = $ext;
		$this->rez['fpath'] = rtrim($this->set['file_dir'], '/').'/'.$this->rez['fname'];
		$to = $this->dir2full($this->rez['fpath']);
		if (!move_uploaded_file($se['tmp_name'], $to))
			return $this->msgs['err'];
		$this->rez['r'] = true;

		return true;
	}

	function upload_img($se) {
		if (!isset($se['name']))
			return false;
		if (is_array($se['name']))
			return false;
		if ($se['error']!=0)
			return $this->msgs['err'];
		if (empty($se['tmp_name']))
			return $this->msgs['err'];

		$this->rez['old_name'] = urldecode($se['name']);
		$this->rez['size'] = $se['size'];
		if ($se['size'] > $this->hum_conv_size($this->set['maxsize']))
			return str_replace('{size}', $this->set['maxsize'], $this->msgs['maxsize']);

		$w = explode('.', $this->rez['old_name']);
		$ext = strtolower(array_pop($w));
		$name = implode('.', $w);
		if (!in_array($ext,$this->set['allowed_ext']))
			return str_replace('{ext}', implode(', ',$this->set['allowed_ext']), $this->msgs['alowf']);
		$this->rez['fname'] = $this->mk_newfname($name, $ext);
		$this->rez['fext'] = $ext;
		$this->rez['fpath'] = rtrim($this->set['dir_img'], '/').'/'.$this->rez['fname'];

		$r = $this->img_detect($se['tmp_name']);
		if (false === $r)
			return str_replace('{ext}', implode(', ', $this->set['allowed_ext']), $this->msgs['alowf']);
		$this->rez['imgsize'] = $r;
		if (isset($this->set['min_w']))
			if ($r[0] < $this->set['min_w'])
				return 'SIZE_SMALL';
		if (isset($this->set['min_h']))
			if($r[1] < $this->set['min_h'])
				return 'SIZE_SMALL';

		$to = $this->dir2full($this->rez['fpath']);
		if (isset($se['copy']) && ($se['copy'] == 1)) {
			if (!copy($se['tmp_name'], $to))
				return $this->msgs['err'];
		} else {
			if(!move_uploaded_file($se['tmp_name'], $to))
				return $this->msgs['err'];
		}
		$this->rez['r'] = true;
		return true;
	}

	function mk_newfname($name,$ext) {
		if (!isset($this->set['fname']))
			return time().base_convert(substr(microtime(),2,8),10,36).'.'.$ext;
		if (strpos($this->set['fname'], '{latin}') !== false) {
			$s = MCore::exm(array('charconvert' => 'rus2lat_url'), array('s' => $name, 'from' => MCore::setting_get('site_charset')));
			if (isset($this->set['fname:latin_len']))
				$s = substr($s, 0, $this->set['fname:latin_len']);
			$t = str_replace('{latin}', $s, $this->set['fname']);
			$t = str_replace('{ext}', $ext, $t);
			$t = str_replace('{uniq}', base_convert(substr(microtime(), 2, 8), 10, 36), $t);
			return $t;
		}
		return $this->set['fname'].'.'.$ext;
	}


	function img_resize_tb() {
		$w = explode('x', $this->set['tb_size']);
		$de = array(
			$this->dir2full($this->rez['fpath']),
			$this->dir2full($this->set['dir_tb'].'/'.$this->rez['fname']),
		);
		$r = $this->img_resize_k($de[0], $de[1], $w[0], $w[1], 0, $this->set['tb_quality']);
		if (is_array($r)) {
			$this->rez['tb'] = array(
				'fpath' => $this->set['dir_tb'].'/'.$this->rez['fname'],
				'w' => $r['w'],
				'h' => $r['h']
			);
		}
		return true;
	}

	function dir2full($f){
		if (substr($f, 0, 1) == '/') return $f;
		if (substr($f, 1, 1) == ':') return $f;
		return MCore::seting_get('main_site_path').ltrim($f, '/');
	}


	function img_resize_w($src, $dest, $width, $height, $rgb = 0x000000, $quality = 100) {
		if (!file_exists($src))
			return false;
		$size = getimagesize($src);
		if ($size === false)
			return false;

		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
		$icfunc = 'imagecreatefrom'.$format;
		if (!function_exists($icfunc))
			return false;

		$x_ratio = $width  / $size[0];
		$y_ratio = $height / $size[1];

		if ($height == 0) {
			$y_ratio = $x_ratio;
			$height = $y_ratio * $size[1];
		} elseif ($width == 0) {
			$x_ratio = $y_ratio;
			$width = $x_ratio * $size[0];
		}

		$ratio = min($x_ratio, $y_ratio);
		$use_x_ratio = ($x_ratio == $ratio);

		$new_width = $use_x_ratio ? $width : floor($size[0] * $ratio);
		$new_height = !$use_x_ratio ? $height : floor($size[1] * $ratio);
		$new_left = $use_x_ratio ? 0 : floor(($width - $new_width) / 2);
		$new_top = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);

		$isrc  = $icfunc($src);
		$idest = imagecreatetruecolor($width, $height);
		$rgb2 = imagecolorallocate($idest, 245, 245, 245);
		imagefill($idest, 0, 0, $rgb2);
		imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);

		imagejpeg($idest, $dest, $quality);

		imagedestroy($isrc);
		imagedestroy($idest);
		return array('w' => $new_width, 'h' => $new_height);
	}

	function img_resize_k($path, $save, $width, $height, $rgb = 0x000000, $quality = 100) {
		$size = getimagesize($path);

		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
		$icfunc = 'imagecreatefrom'.$format;
		if (!function_exists($icfunc)) return false;
		$src = $icfunc($path);

		$thumb = imagecreatetruecolor($width, $height);

		$scaleW = $width / $size[0];
		$scaleH = $height / $size[1];
		$scale = max($scaleW, $scaleH);
		$new_width = floor($size[0] * $scale);
		$new_height = floor($size[1] * $scale);
		$dst_x = floor(($width - $new_width) / 2);
		$dst_y = floor(($height - $new_height) / 2);

		imagecopyresampled($thumb, $src, $dst_x, $dst_y, 0, 0, $new_width, $new_height, $size[0], $size[1]);

		imagejpeg($thumb, $save, $quality);
		return array('w' => $new_width, 'h' => $new_height);
	}

	function img_detect($fn){
		if (!file_exists($fn))
			return false;
		$size = getimagesize($fn);
		if (!is_array($size))
			return false;

		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
		$icfunc = 'imagecreatefrom'.$format;
		if (!function_exists($icfunc))
			return false;
		return $size;
	}


	function hum_conv_size($s) {
		$w = substr($s,-2);
		$s = (int)$s;
		if ($w == 'kb')
			return $s*1024;
		if ($w == 'mb')
			return $s*1024*1024;
		return $s;
	}
}