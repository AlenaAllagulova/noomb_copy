<?php

use bff\db\Dynprops;

class Model_Api extends Api_Abstract
{
    public $DB;

    private $androidPushKey = '';
    private $cert = 'cert.pem';//'pushcert.pem';
    private $passphrase = '';//'GaurangaApp2017';

    function __construct()
    {
        $this->languages = array_keys(config::sys('locale.available'));
        $this->languageDefault = config::sysAdmin('locale.default');

        $this->androidPushKey = config::get('ext.plugin.mapi.android_push_key', '');
    }


    private $modelUserSet = array(
        'FldPublic' => array(
            'user_id as id',
            'user_id_ex',
            'shop_id',
            'admin',
            'member',
            'login',
            'email',
            'phone_number',
            'phone',
            'phones',
            'activated',
            'phone_number_verified',
            'balance',
            'balance_spent',
            'reg1_country',
            'reg2_region',
            'reg3_city',
            'region_id',
            'name',
            'surname',
            'avatar',
            'avatar_crop',
            'blocked',
            'blocked_reason',
            'birthdate',
            'sex',
            'about',
            'skype',
            'icq',
            'site',
            'im_noreply',
            'enotify',
            'social_id'
        ),
        'AvatarSitePath' => 'files/images/avatars/',
        'ItemImgPath' => 'files/images/items/',
        'TmpImgPath' => 'files/images/tmp/',
        'ItemImgPath2' => 'images/',
        'ExtraImgPath' => ''
    );

    function modelUpStat($user_id, $sid)
    {

        $this->DB->update(TABLE_USERS_STAT, array(
            'last_login' => date('Y-m-d H:i:s'),
            'last_activity' => date('Y-m-d H:i:s'),
            'last_login_ip' => preg_replace('/[^0-9]/is', '', $_SERVER['REMOTE_ADDR']),
            'session_id' => $sid), ['user_id' => $user_id]);

    }

    function modelUserLogin($pa)
    {
        //Soc Auth
        if (!empty($pa['profile_id']) && !empty($pa['provider_id']) && !empty($pa['token'])) {

            $userData['sn'] = $this->GetSocialData($pa['provider_id'], $pa['profile_id'], $pa['token']);

            if (strpos(json_encode($userData['sn']), 'error') !== false || !is_array($userData['sn'])) return array('err' => 'ACCESS_DENIDED_SOC');

            $userId = $this->DB->one_data('SELECT user_id
					FROM ' . TABLE_USERS_SOCIAL . '
					WHERE provider_id = :prov AND profile_id = :id',
                array('prov' => $pa['provider_id'], 'id' => $pa['profile_id']));
            if ($userId) {
                $row = $this->DB->one_array('SELECT * from ' . TABLE_USERS . ' WHERE user_id = :user_id LIMIT 1', ['user_id' => $userId]);
                if ($row['blocked'] == 1)
                    return array('err' => array('msg' => 'USER_BLOCKED', 'reason' => $row['blocked_reason']));

                if (!empty($pa['appregid'])) {
                    $this->DB->exec("REPLACE into " . TABLE_APPREGID . " values(:user_id, :appregid)", ['user_id' => $row['user_id'], 'appregid' => $pa['appregid']]);
                }
                if (!empty($pa['appleregid'])) {
                    $this->DB->exec("REPLACE into " . TABLE_APPLEREGID . " values(:user_id, :appleregid)", ['user_id' => $row['user_id'], 'appleregid' => $pa['appleregid']]);
                }

                return $this->modelUserGetInfo($userId);
            } else {
                switch ($pa['provider_id']) {
                    case 4 : //ok
                        $pa['profile_id'] = !empty($userData['sn']['uid']) ? $userData['sn']['uid'] : $pa['profile_id'];
                        break;
                }
                //Registration
                return array(
                    'err' => 'SOCIAL_USER_NOT_EXIST',
                    'details' => [
                        'email' => !empty($userData['sn']['email']) ? $userData['sn']['email'] : '',
                        'profile_id' => $pa['profile_id'],
                        'provider_id' => $pa['provider_id'],
                        'token' => $pa['token']
                    ]
                );
            }
        }

        //Main Auth
        if (empty($pa['email']) || empty($pa['pass']))
            return array('err' => 'EMPTY_EMAIL_OR_PASS');

        if (strpos($pa['email'], '@') !== false) {
            $row = $this->DB->one_array('SELECT * from ' . TABLE_USERS . ' WHERE email = :email LIMIT 1', ['email' => $pa['email']]);
        } else {
            $row = $this->DB->one_array('SELECT * from ' . TABLE_USERS . ' WHERE login = :login LIMIT 1', ['login' => $pa['email']]);
        }
        if (empty($row)) {
            return array('err' => 'LOGIN_FAIL');
        }
        //If not Activated
        if ($row['activated'] == 0) {
            return array('err' => 'USER_NOT_ACTIVE');
        }

        if ($row['password'] != $this->modelUserPasswordMD5($pa['pass'], $row['password_salt']))
            return array('err' => 'PASS_FAIL');
        if ($row['blocked'] == 1)
            return array('err' => array('msg' => 'USER_BLOCKED', 'reason' => $row['blocked_reason']));

        if (!empty($pa['appregid'])) {
            $this->DB->exec("REPLACE into " . TABLE_APPREGID . " values(:user_id, :appregid)", ['user_id' => $row['user_id'], 'appregid' => $pa['appregid']]);
        }
        if (!empty($pa['appleregid'])) {
            $this->DB->exec("REPLACE into " . TABLE_APPLEREGID . " values(:user_id, :appleregid)", ['user_id' => $row['user_id'], 'appleregid' => $pa['appleregid']]);
        }

        return $this->modelUserGetInfo($row['user_id']);
    }

    function modelUserLogout($pa)
    {
        $nUserId = $this->sesrow['user_id'];
        if (!empty($pa['appregid'])) {
            $this->DB->delete(TABLE_APPREGID, ['id' => $nUserId]);
        }
        if (!empty($pa['appleregid'])) {
            $this->DB->delete(TABLE_APPLEREGID, ['id' => $nUserId]);
        }
    }

    function modelUserPasswordMD5($sPassword, $sSalt = '')
    {
        return \bff::security()->getUserPasswordMD5($sPassword, $sSalt);
    }

    function NullToEmpty($arr)
    {
        $new_arr = array();
        foreach ($arr as $k => $v) {
            $new_arr[$k] = str_replace('NULL', '', $v);
        }
        return $new_arr;
    }

    function modelUserGetInfo($id, $lang = null)
    {
        $row = $this->DB->one_array('SELECT u.' . implode($this->modelUserSet['FldPublic'], ',u.') . ', s.internalmail_new, s.items_fav, s.last_activity FROM '
            . TABLE_USERS . ' u LEFT JOIN ' . TABLE_USERS_STAT . ' s USING (user_id) WHERE u.user_id = :user_id', ['user_id' => (int)$id]);

        if (empty($row))
            return null;

        //Add soc providers
        $row_soc = $this->DB->select('SELECT provider_id FROM '
            . TABLE_USERS_SOCIAL . ' WHERE user_id = :user_id', ['user_id' => (int)$id]);
        foreach ($row_soc as $k => $v) {
            $row['soc_providers'][] = $v['provider_id'];
        }

        $row['phone_number'] = empty($row['phone_number']) ? '' : '+' . $row['phone_number'];
        $row['phones'] = (!empty($row['phones']) ? func::unserialize($row['phones']) : array());
        if (!empty($row['phones'])) {
            $aPhones = array();
            foreach ($row['phones'] as $phoneData) {
                $aPhones[] = $phoneData['v'];
            }
            $row['phones'] = $aPhones;
        }

        if ($row['activated'] == 0) return array('error' => 'ACCOUNT_NOT_ACTIVE', 'user_id' => $id, 'phone' => $row['phone_number']);

        if (!empty($row['avatar']))
            $row['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($row['id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $row['id'] . 'n' . $row['avatar'];

        $row['currency'] = Site::currencyDefault();
        $row['balance_display'] = $row['balance'] . ' ' . $row['currency'];

        if (!empty($row['shop_id'])) {
            $row['shop_data'] = Shops::model()->shopData(
                $row['shop_id'],
                array(
                    'status',
                    'title',
                    'descr',
                    'logo',
                    'region_id',
                    'addr_lat',
                    'addr_lon',
                    'addr_addr',
                    'phones',
                    'site',
                    'contacts',
                    'social',
                    'blocked_reason',
                    'items',
                    'svc_abonement_id',
                    'svc_abonement_expire',
                    'svc_abonement_termless',
                )
            );
            $row['shop_data'] = empty($row['shop_data']) ? null : $row['shop_data'];
            if (!empty($row['shop_data']['phones'])) {
                $aPhones = array();
                foreach ($row['shop_data']['phones'] as $phoneData) {
                    $aPhones[] = $phoneData['v'];
                }
                $row['shop_data']['phones'] = $aPhones;
            }
            $row['shop_data']['logo'] = !empty($row['shop_data']['logo'])
                ? $this->SET['http_'] . Shops::i()->shopLogo($row['shop_id'])->url($row['shop_id'], $row['shop_data']['logo'], ShopsLogo::szView)
                : $this->SET['http_'] . Shops::i()->shopLogo($row['shop_id'])->urlDefault(ShopsLogo::szView);
            if (empty(floatval($row['shop_data']['addr_lat'])) && empty(floatval($row['shop_data']['addr_lon'])) && !empty($row['shop_data']['region_id'])) {
                $tmp = $this->getRegionDataById(['id' => $row['shop_data']['region_id']]);
                if (!empty($tmp['yc'])) {
                    list($row['shop_data']['addr_lat'], $row['shop_data']['addr_lon']) = explode(',', $tmp['yc']);
                }
            }
            $row['shop_data']['contacts'] = empty($row['shop_data']['contacts']) ? new \stdClass() : $row['shop_data']['contacts'];
            $row['shop_data']['social'] = empty($row['shop_data']['social']) ? array() : (array)$row['shop_data']['social'];

            if (Shops::categoriesEnabled()) {
                $row['shop_data']['cats'] = Shops::model()->shopCategoriesIn($row['shop_id'], ShopsCategoryIcon::BIG);
                foreach ($row['shop_data']['cats'] as &$v) {
                    if ($v['pid'] > Shops::CATS_ROOTID) {
                        $v['icon'] = '';
                        $v['title_full'] = $v['ptitle'] . ' &raquo; ' . $v['title'];
                        $v['picon'] = stripos($v['picon'], 'default-b') === false ? $this->SET['http_'] . $v['picon'] : '';
                    }
                }
                unset($v);
            }

            // Abonement
            $abonementId = $row['shop_data']['svc_abonement_id'];
            if ($abonementId) {
                $abonement = Shops::model()->abonementData($abonementId);
                if ($abonement['enabled']) {
                    $abonement['expire'] = date("Y-m-d", strtotime($row['shop_data']['svc_abonement_expire']));
                    if ($abonement['price_free'] == 0) {
                        $abonement['termless'] = 0;
                    } else {
                        $abonement['termless'] = $abonement['price_free_period'] ? 0 : 1;
                    }
                    $itemsTotal = $abonement['items'];
                    $abonement['items_total']  = $itemsTotal;
                    $abonement['items_available'] = $itemsTotal - $row['shop_data']['items'];
                    $abonement = $this->removeLangFields($abonement, ['title'], $lang);
                    $removeFields = [
                        'price',
                        'price_free',
                        'price_free_period',
                        'discount',
                        'import',
                        'svc_mark',
                        'svc_fix',
                        'icon_b',
                        'icon_s',
                        'num',
                        'is_default',
                        'items',
                        'enabled',
                        'one_time',
                    ];
                    $abonement = array_diff_key($abonement, array_flip($removeFields));
                    $row['shop_data']['svc_abonement'] = $abonement;
                }
            }
            unset(
                $row['shop_data']['svc_abonement_id'],
                $row['shop_data']['svc_abonement_expire'],
                $row['shop_data']['svc_abonement_termless']
            );
        }

        return $this->NullToEmpty($row);
    }

    function GetSocialData($provider_id, $profile_id, $token)
    {

        if (!empty($provider_id) && !empty($profile_id) && !empty($token)) {

            $soc_config = require_once('../config/social.php');

            switch ($provider_id) {
                case 1 : //vk
                    $user_profile = file_get_contents('https://api.vk.com/method/users.get?user_id=' . $profile_id . '&v=5.21&fields=photo_400_orig&access_token=' . $token);
                    break;
                case 2 : //facebook
                    $user_profile = file_get_contents("https://graph.facebook.com/me?fields=id,name,picture.width(400).height(400),email&access_token=" . $token);
                    break;
                case 4 : //ok
                    $sign = md5("application_key={$soc_config['providers']['Odnoklassniki']['keys']['key']}format=jsonmethod=users.getCurrentUser" . md5("{$token}{$soc_config['providers']['Odnoklassniki']['keys']['secret']}"));
                    $params = array(
                        'method' => 'users.getCurrentUser',
                        'access_token' => $token,
                        'application_key' => $soc_config['providers']['Odnoklassniki']['keys']['key'],
                        'format' => 'json',
                        'sig' => $sign
                    );

                    $user_profile = file_get_contents('http://api.odnoklassniki.ru/fb.do' . '?' . urldecode(http_build_query($params)));
                    break;
                default:
                    return array('err' => 'INCORRECT_PROVIDER');
            }
            //JSON
            if (strpos($user_profile, 'error') !== false) return array('err' => 'ACCESS_DENIDED_SOC');

            //ARRAY
            $user_profile = json_decode($user_profile, true);
            switch ($provider_id) {
                case 1 : //vk
                    $user_profile = isset($user_profile['response'][0]) ? $user_profile['response'][0] : [];
                    if (empty($user_profile)) return array('err' => 'ACCESS_DENIDED_SOC');
                    $user_profile['name'] = $user_profile['first_name'] . ' ' . $user_profile['last_name'];
                    break;
            }

            return $user_profile;

        } else {

            return array('err' => 'NOT_FULL_DATA');

        }

    }

    function modelAddSoc($pa, $userData)
    {
        if (empty($pa['user_id']) || empty($pa['provider_id']) || empty($pa['profile_id']) || empty($userData))
            return array('err' => 'NOT_FULL_DATA');

        $userSoc = $this->DB->one_data('SELECT user_id FROM ' . TABLE_USERS_SOCIAL . '
					WHERE provider_id = :provider_id AND profile_id = :profile_id',
            array('provider_id' => $pa['provider_id'], 'profile_id' => $pa['profile_id']));

        // Check soc profile
        if ($userSoc) {
            if ($userSoc == $pa['user_id']) {
                return $this->modelUserGetInfo($pa['user_id']);
            } else {
                return array('err' => 'SOCIAL_ACC_ALREADY_CONNECTED_TO_ANOTHER_USER');
            }
        } else {
            $nSocialID = $this->DB->insert(TABLE_USERS_SOCIAL, array('user_id' => $pa['user_id'],
                'provider_id' => $pa['provider_id'], 'profile_id' => $pa['profile_id'], 'profile_data' => serialize($userData)));

            if ($nSocialID) {
                return $this->modelUserGetInfo($pa['user_id']);
            } else {
                # Неудалось создать соц.аккаунт
                return array('err' => 'CAN_NOT_CREATE_SOCIAL_ACC');
            }
        }
    }

    function modelUserSocialConnect($pa)
    {
        $login = $this->modelUserLogin(['email' => $pa['email'], 'pass' => $pa['pass']]);
        if (isset($login['err'])) return $login;

        $userData = $this->GetSocialData($pa['provider_id'], $pa['profile_id'], $pa['token']);

        if (isset($userData['err'])) {
            return array('err' => $userData['err']);
        }

        $pa['user_id'] = $login['id'];
        return $this->modelAddSoc($pa, $userData);
    }

    function modelUserAdd($pa)
    {
        $r = $this->modelUserDataClean($pa);
        if (isset($r['err']))
            return $r;
        $aUserDetails = array();

        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;

        if (!empty($pa['provider_id']) && !empty($pa['profile_id']) && !empty($pa['token'])) { // Social registartion if there is email in table users

            $userData = $this->GetSocialData($pa['provider_id'], $pa['profile_id'], $pa['token']);

            if (isset($userData['err'])) {
                return array('err' => $userData['err']);
            }

            //if isset in soc
            switch ($pa['provider_id']) {
                case 1 :
                    if (!empty($userData['photo_400_orig'])) $pa['avatar'] = $userData['photo_400_orig'];
                    break;
                case 2 :
                    if (!empty($userData['picture'])) $pa['avatar'] = $userData['picture']['data']['url'];
                    break;
                case 4 :
                    if (!empty($userData['pic_3'])) {
                        $pa['avatar'] = $userData['pic_3'];
                    } else if (!empty($userData['pic_2'])) {
                        $pa['avatar'] = $userData['pic_2'];
                    } else if (!empty($userData['pic_1'])) {
                        $pa['avatar'] = $userData['pic_1'];
                    }
                    break;
            }

            //if isset in request
            if (!empty($pa['email'])) $userData['email'] = $pa['email'];

            if (!empty($userData['name'])) $pa['name'] = $userData['name'];
            if (!empty($userData['email'])) $pa['email'] = $userData['email'];
            if (!empty($userData['phone']) && empty($pa['phone'])) $pa['phone'] = $userData['phone'];


            //empty main params
            if (empty($userData['email'])) return array('err' => 'NO_EMAIL_IN_PROFILE');
            if (!\bff::DI('input')->isEmail($pa['email'])) {
                return array('err' => 'USER_EMAIL_WRONG');
            }

            # [ id, email, phone, provider, name, photo ]
            $userId = $this->DB->one_data('SELECT user_id FROM ' . TABLE_USERS . ' WHERE email = :email', array('email' => $userData['email']));

            if ($userId) { // Check main profile
                /*$pa['user_id'] = $userId;
                return $this->modelAddSoc($pa, $userData); //Check Soc and Create*/
                return array(
                    'err' => 'SOCIAL_REGISTER_EMAIL_INUSE',
                    'details' => [
                        'email' => $userData['email'],
                        'profile_id' => $pa['profile_id'],
                        'provider_id' => $pa['provider_id'],
                        'token' => $pa['token']
                    ]
                );
            } else {
                $aUserDetails['email'] = $userData['email'];
                $userPassw = substr(md5(uniqid(mt_rand(), true)), 0, 8);
                $userPasswSalt = substr(md5(uniqid(mt_rand(), true)), 0, 4);
                $aUserDetails['password_salt'] = $userPasswSalt;
                $aUserDetails['password'] = $userPassw; //$this->modelUserPasswordMD5($userPassw, $userPasswSalt);
            }

            //$aUserDetails['activated'] = 1 - config::sysAdmin('users.register.social.email.activation', 1, TYPE_INT);

        } else { // Original registration

            $minPasswordLength = Users::i()->passwordMinLength;
            if (!empty($pa['pass']) && strlen(@$pa['pass']) < $minPasswordLength)
                return array('err' => 'USER_PASSWORD_LENGTH_MISSMATCH', 'min_length' => $minPasswordLength);
            if (!empty($pa['phone']) && !preg_match('/^\+\d{1,3}\d{3}\d{2}\d{2}\d{2}$/', $pa['phone']))
                return array('err' => 'PHONE_NUMBER_WRONG');

            $pa['password_salt'] = substr(md5(uniqid(mt_rand(), true)), 0, 4);
            $aUserDetails['password_salt'] = $pa['password_salt'];
            $aUserDetails['password'] = $pa['pass']; //$this->modelUserPasswordMD5($pa['pass'], $pa['password_salt']);

            if (!\bff::DI('input')->isEmail($pa['email'])) {
                return array('err' => 'USER_EMAIL_WRONG');
            }
            $aUserDetails['email'] = $pa['email'];
        }

        $aUserDetails['login'] = isset($pa['login']) ? $pa['login'] : mb_substr($pa['email'], 0, strpos($pa['email'], '@'));

        if (isset($pa['member']))
            $aUserDetails['member'] = (int)$pa['member'];
        if (isset($pa['phone'])) {
            $aUserDetails['phone_number'] = trim($pa['phone'], '+ ');
            $aUserDetails['phone'] = '+' . $aUserDetails['phone_number'];
            $aUserDetails['phones'] = serialize(array($aUserDetails['phone']));
        }
        if (isset($pa['region_id']))
            $aUserDetails['region_id'] = (int)$pa['region_id'];
        if (isset($pa['addr_addr']))
            $aUserDetails['addr_addr'] = mb_substr($pa['addr_addr'], 0, 300);
        if (isset($pa['addr_lat']))
            $aUserDetails['addr_lat'] = (float)$pa['addr_lat'];
        if (isset($pa['addr_lon']))
            $aUserDetails['addr_lon'] = (float)$pa['addr_lon'];
        if (isset($pa['name']))
            $aUserDetails['name'] = $pa['name'];
        if (isset($pa['birthdate']))
            $aUserDetails['birthdate'] = mb_substr($pa['birthdate'], 0, 12);
        if (isset($pa['sex']))
            $aUserDetails['sex'] = (int)$pa['sex'];

        $aUserDetails['created'] = $this->DB->now();
        $aUserDetails['created_ip'] = preg_replace('/[^0-9]/is', '', $_SERVER['REMOTE_ADDR']);
        $aUserDetails['user_id_ex'] = substr($aUserDetails['password'], 0, 6);
        $aUserDetails['subscribed'] = 1;
        $aUserDetails['enotify'] = 1;

        $aUserDetails['lang'] = $pa['lang'];

        $row = $this->DB->one_array('SELECT * FROM ' . TABLE_USERS . ' WHERE email = :email limit 1', ['email' => $aUserDetails['email']]);

        if ($row['user_id']) {
            return array('err' => 'USER_EMAIL_INUSE');
        } else {
            $aUserData = Users::i()->userRegister($aUserDetails);
            $id = $aUserData['user_id'];
        }

        if (!$id) return array('err' => 'ERROR_NO_CREATE_PROFILE');

        //Load Avatar
        if (isset($pa['avatar']))
            $this->modelUserAvatar($id, $pa['avatar']);

        if (!empty($pa['appregid'])) {
            $this->DB->exec("REPLACE into " . TABLE_APPREGID . " values(:user_id, :appregid)", ['user_id' => $id ? $id : $row['user_id'], 'appregid' => $pa['appregid']]);
        }
        if (!empty($pa['appleregid'])) {
            $this->DB->exec("REPLACE into " . TABLE_APPLEREGID . " values(:user_id, :appleregid)", ['user_id' => $id ? $id : $row['user_id'], 'appleregid' => $pa['appleregid']]);
        }

        $mailData = array(
            'id' => $aUserData['user_id'],
            'name' => $aUserDetails['name'],
            'password' => $aUserData['password'],
            'email' => $aUserDetails['email'],
            'activate_link' => $aUserData['activate_link']
        );
        bff::sendMailTemplate($mailData, 'users_register', $aUserDetails['email'], false, '', '', $pa['lang']);

        if (isset($userPassw)) {

            $nSocialID = $this->DB->insert(TABLE_USERS_SOCIAL, array('user_id' => $id,
                'provider_id' => $pa['provider_id'], 'profile_id' => $pa['profile_id'], 'profile_data' => serialize($userData)));

            if ($nSocialID) {
                if (!config::sysAdmin('users.register.social.email.activation', true, TYPE_BOOL)) {
                    Users::model()->userSave($aUserData['user_id'], array(
                        'activated' => 1
                    ));
                }

                $aData = $this->modelUserGetInfo($id);
                $aData['password'] = $userPassw;
                return $aData;
            } else {
                # Неудалось создать соц.аккаунт
                return array('err' => 'CAN_NOT_CREATE_SOCIAL_ACC');
            }
        }

        return $this->modelUserGetInfo($id);
    }

    function modelDailyLimitActivate($user_id)
    {

        $limit_activation = (!empty(config::sys('limit_activation'))) ? config::sys('limit_activation') : 3;

        $row = $this->DB->one_array('SELECT * FROM ' . TABLE_USERS_LIMITS_ACTIVATION . ' 
        WHERE user_id = :user_id AND date_activ = :date_activ limit 1', ['user_id' => $user_id, 'date_activ' => date('Y-m-d')]);

        if (!$row) {
            $de = array(
                'user_id' => $user_id,
                'count_activ' => 1,
                'date_activ' => date('Y-m-d'),
            );
            $this->DB->insert(TABLE_USERS_LIMITS_ACTIVATION, $de);

            return true;

        } else {

            if ($row['count_activ'] >= $limit_activation) {
                return false;
            } else {
                $this->DB->update(TABLE_USERS_LIMITS_ACTIVATION, ['count_activ' => $row['count_activ'] + 1], ['user_id' => $user_id]);
                return true;
            }

        }
    }

    function modelGet_code($pa)
    {
        if (!empty($pa['phone']) && !empty($pa['user_id'])) {
            $pa['phone'] = str_replace(' ', '', trim($pa['phone'], '+'));
            if (!$this->verifyUserPhone($pa['phone'], $pa['user_id']) && empty($pa['change_phone']))
                return array('err' => 'WRONG_PHONE_OWNER');
            if ($this->phoneInUse($pa['phone']) && !empty($pa['change_phone']))
                return array('err' => 'PHONE_INUSE');
            if (!preg_match('/^\d{1,3}\d{3}\d{2}\d{2}\d{2}$/', $pa['phone']))
                return array('err' => 'PHONE_NUMBER_WRONG');
            if (!$this->modelDailyLimitActivate($pa['user_id'])) return array('err' => 'ACTIVATION_LIMIT');

            # send sms for activation
            /*if (config::get('debug', false, TYPE_BOOL) && empty($pa['test'])) {
                // Create activate code
                $aActivation = $this->getActivationInfo();
                $aUserDetails['activate_key'] = substr($aActivation['key'], 0, 5);
                Users::i()->sms(false)->sendActivationCode($pa['phone'], $aUserDetails['activate_key']);
            } else {
                $aUserDetails['activate_key'] = '333';
            }*/

            $aActivation = $this->getActivationInfo();
            $aUserDetails['activate_key'] = substr($aActivation['key'], 0, 5);
            Users::i()->sms(false)->sendActivationCode($pa['phone'], $aUserDetails['activate_key']);

            $this->DB->update(TABLE_USERS, $aUserDetails, ['user_id' => $pa['user_id']]);
            return true;
        } else {
            return false;
        }
    }

    function modelCheckCode($pa)
    {
        if (!empty($pa['change_phone']) && empty($pa['phone'])) {
            return array('err' => 'EMPTY_PHONE');
        } else if (empty($pa['phone'])) {
            $pa['phone'] = '';
        }
        $row = (boolean)$this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_USERS . ' WHERE activate_key = :code AND user_id = :user_id limit 1', ['code' => $pa['code'], 'user_id' => $pa['user_id']]);
        if ($row) {
            $aUserDetails = ['phone_number_verified' => 1, 'activated' => 1];
            if (!empty($pa['change_phone'])) {
                $aUserDetails['phone_number'] = str_replace(' ', '', trim($pa['phone'], '+'));
            }
            $this->DB->update(TABLE_USERS, $aUserDetails, ['user_id' => $pa['user_id']]);
            return true;
        } else {
            return array('err' => 'WRONG_CODE');
        }
    }

    function verifyUserPhone($phone, $user_id)
    {
        return (boolean)$this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_USERS . ' WHERE phone_number = :phone AND user_id = :user_id limit 1', ['phone' => $phone, 'user_id' => $user_id]);
    }

    function phoneInUse($phone)
    {
        return (boolean)$this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_USERS . ' WHERE phone_number = :phone AND phone_number_verified = 1 limit 1', ['phone' => $phone]);
    }

    function modelUserEdit($pa)
    {
        if (empty($this->sesrow['user_id'])) {
            $this->errAddMsg('NO_SESSION');
            return 0;
        }
        $this->modelUserDataClean($pa);

        if (isset($pa['phone'])) {
            if (!empty($pa['phone'])) {
                if (strlen($pa['phone']) < 5) {
                    $this->errAddMsg('NOT_VALID_PHONE');
                    return 0;
                }
                $limit = config::sysAdmin('users.profile.phones', 5, TYPE_UINT);
                $tmp = Users::model()->userDataByFilter(array('user_id' => $this->sesrow['user_id']), array('phones'));
                $aPhonesNew = array(trim($pa['phone']));
                if (!empty($tmp['phones'])) {
                    $phones = unserialize($tmp['phones']);
                    if (count($phones) > 1) {
                        for ($i = 1; $i < count($phones); $i++) {
                            $aPhonesNew[] = $phones[$i]['v'];
                        }
                    }
                }
                $aPhones = Users::validatePhones($aPhonesNew, $limit);
                $de['phones'] = serialize($aPhones);
                $de['phone'] = trim($pa['phone']);
            } else {
                $de['phones'] = $de['phone'] = '';
            }
        }
        if (isset($pa['phones'])) {
            if (!empty($pa['phones'])) {
                if (!is_array($pa['phones'])) {
                    $this->errAddMsg('NOT_VALID_PHONES_ARRAY');
                    return 0;
                }
                $limit = config::sysAdmin('users.profile.phones', 5, TYPE_UINT);
                $aPhones = Users::validatePhones($pa['phones'], $limit);
                $de['phones'] = serialize($aPhones);
            } else {
                $de['phones'] = serialize([]);
            }
        }
        if (isset($pa['region_id']))
            $de['region_id'] = (int)$pa['region_id'];
        if (isset($pa['addr_addr']))
            $de['addr_addr'] = mb_substr($pa['addr_addr'], 0, 300);
        if (isset($pa['addr_lat']))
            $de['addr_lat'] = (float)$pa['addr_lat'];
        if (isset($pa['addr_lon']))
            $de['addr_lon'] = (float)$pa['addr_lon'];
        if (isset($pa['name']))
            $de['name'] = mb_substr($pa['name'], 0, 100);
        if (isset($pa['birthdate']))
            $de['birthdate'] = mb_substr($pa['birthdate'], 0, 12);
        if (isset($pa['sex']))
            $de['sex'] = (int)$pa['sex'];

        if (sizeof($de) > 0)
            return $this->DB->update(TABLE_USERS, $de, ['user_id' => (int)$this->sesrow['user_id']]);

        return 0;
    }

    function modelChangePassword($pa)
    {
        if (empty($this->sesrow['user_id'])) {
            $this->errAddMsg('NO_SESSION');
            return 0;
        }
        if (empty($pa['pass'])) {
            $this->errAddMsg('CURRENT_PASSWORD_EMPTY');
            return 0;
        }
        if (empty($pa['pass_new'])) {
            $this->errAddMsg('NEW_PASSWORD_EMPTY');
            return 0;
        }
        $row = $this->DB->one_array('SELECT password, password_salt from ' . TABLE_USERS . ' WHERE user_id = :id', ['id' => $this->sesrow['user_id']]);
        if ($row['password'] != $this->modelUserPasswordMD5($pa['pass'], $row['password_salt'])) {
            $this->errAddMsg('CURRENT_PASSWORD_WRONG');
            return 0;
        }

        $minPasswordLength = Users::i()->passwordMinLength;
        if (strlen(trim($pa['pass_new'])) < $minPasswordLength) {
            $this->errAddMsg(['msg' => 'USER_PASSWORD_LENGTH_MISSMATCH', 'min_length' => $minPasswordLength]);
            return 0;
        }
        $sPasswordSalt = substr(md5(uniqid(mt_rand(), true)), 0, 4);
        $sNewPasswordHash = $this->modelUserPasswordMD5($pa['pass_new'], $sPasswordSalt);

        return $this->DB->update(TABLE_USERS, ['password' => $sNewPasswordHash, 'password_salt' => $sPasswordSalt], ['user_id' => $this->sesrow['user_id']]);
    }

    function modelUserLoginGenerate($prefix = 'u')
    {
        $done = false;
        do {
            $login = $prefix . mt_rand(123456789, 987654321);
            $res = $this->DB->one_data('SELECT user_id FROM ' . TABLE_USERS . ' WHERE login = :login', ['login' => $login]);
            if (empty($res)) {
                $done = true;
            }
        } while (!$done);
        return $login;
    }

    function modelUserDataClean(array &$aData, $mKeys = true, array $aExtraSettings = array())
    {
        if (!is_array($mKeys))
            $mKeys = array_keys($aData);
        foreach ($mKeys as $key) {
            if (!isset($aData[$key]))
                continue;
            switch ($key) {
                case 'name': # имя                    # допустимые символы: # латиница, кирилица, тире, пробелы
                    $aData[$key] = preg_replace('/[^\.\s\[\]\-\_\p{L}0-9\w\’\']+/iu', '', $aData[$key]);
                    $aData[$key] = trim(mb_substr($aData[$key], 0, (isset($aExtraSettings['name_length']) ? $aExtraSettings['name_length'] : 50)), '- ');
                    break;
                case 'birthdate': # дата рождения
                    if (!empty($aData[$key])) {
                        $se = explode('-', $aData[$key]);
                        if (!checkdate(@$se[1], @$se[2], @$se[0]))
                            return array('err' => 'USER_BIRTHDATE_WRONG');
                    } else {
                        $aData[$key] = $aData[$key]['year'] . '-' . $aData[$key]['month'] . '-' . $aData[$key]['day'];
                    }
                    break;
                case 'site': # сайт
                    if (mb_strlen($aData[$key]) > 3) {
                        $aData[$key] = mb_substr($aData[$key], 0, 255);
                        if (stripos($aData[$key], 'http') !== 0) {
                            $aData[$key] = $this->SET['http_'] . '//' . $aData[$key];
                        }
                    } else {
                        $aData[$key] = '';
                    }
                    break;
                case 'about': # о себе
                    $aData[$key] = mb_substr($aData[$key], 0, 2500);
                    break;
                case 'phone': # телефоны
                    break;
                case 'skype': # skype
                    $aData[$key] = preg_replace('/[^\.\s\[\]\:\-\_a-zA-Z0-9]/', '', $aData[$key]);
                    $aData[$key] = trim(mb_substr($aData[$key], 0, 32), ' -');
                    break;
                case 'icq': # icq
                    $aData[$key] = preg_replace('/[^\.\-\s\_0-9]/', '', $aData[$key]);
                    $aData[$key] = trim(mb_substr($aData[$key], 0, 20), ' .-');
                    break;
                case 'region_id':
                    break;
            }
        }
    }

    function modelUserAvatar($id, $file, $type = 'link')
    {
        $row = $this->DB->one_array('SELECT user_id as id, avatar FROM ' . TABLE_USERS . ' WHERE user_id = :id', ['id' => (int)$id]);
        if (empty($row))
            return false;

        $up = array(
            'dir_img' => $this->SET['public_site_path'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($row['id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/',
            'allowed_ext' => array('jpg'),
            'maxsize' => '1mb',
            'tb_size' => '35x35',
            'o_size' => '150x150',
            'tb_quality' => 90,
        );

        $nam = substr(md5(uniqid(mt_rand(), true)), 0, 6) . '.jpg';
        $fnam = $row['id'] . 'n' . $nam;
        $snam = $row['id'] . 's' . $nam;

        if ($type == 'link') {
            $fdata = file_get_contents($file);
            if (strlen($fdata) < 50) return false;
            file_put_contents($up['dir_img'] . $fnam, $fdata);
        } else {
            if ($file['size'] < 1024) return false;
            if (!move_uploaded_file($file['tmp_name'], $up['dir_img'] . $fnam)) return false;
        }

        include_once('UploaderFiles.php');

        $m = new UploaderFiles($up);
        if (filesize($up['dir_img'] . $fnam) > $m->hum_conv_size($up['maxsize']))
            return false;

        if (($row['avatar'] != '') && is_file($up['dir_img'] . $row['id'] . 'n' . $row['avatar'])) {
            @unlink($up['dir_img'] . $row['id'] . 'n' . $row['avatar']);
            @unlink($up['dir_img'] . $row['id'] . 's' . $row['avatar']);
        }
        if (!is_dir($up['dir_img'])) {
            mkdir($up['dir_img']);
        }

        $siz = $m->img_detect($up['dir_img'] . $fnam);

        if (false === $siz) {
            @unlink($up['dir_img'] . $fnam);
            return true;
        }

        $w = explode('x', $up['tb_size']);
        $m->img_resize_k($up['dir_img'] . $fnam, $up['dir_img'] . $snam, $w[0], $w[1], 0, $up['tb_quality']);
        $w = explode('x', $up['o_size']);
        $m->img_resize_k($up['dir_img'] . $fnam, $up['dir_img'] . $fnam, $w[0], $w[1], 0, $up['tb_quality']);
        $this->DB->update(TABLE_USERS, array('avatar' => $nam), array('user_id' => $id));
        return true;
    }

    function modelUserAvatarFromFile($sImgName)
    {
        if (empty($sImgName)) {
            return Users::i()->avatar($this->sesrow['user_id'])->delete(true);
        }
        $dir_img = $this->SET['domain'] . $this->modelUserSet['TmpImgPath'];
        $rez = Users::i()->avatar($this->sesrow['user_id'])->uploadURL($dir_img . $sImgName, true, true);
        if (!$rez) {
            $this->errAddMsg('ERROR_AVATAR_FILE_UPLOAD');
        }
        unlink($dir_img . $sImgName);
        return $rez ? 1 : 0;
    }

    function modelChangeEmail($pa)
    {
        if (empty($this->sesrow['user_id'])) {
            $this->errAddMsg('NO_SESSION');
            return 0;
        }
        if (empty($pa['email'])) {
            $this->errAddMsg('NEW_EMAIL_EMPTY');
            return 0;
        }
        if (!\bff::DI('input')->isEmail($pa['email'])) {
            $this->errAddMsg('WRONG_EMAIL');
            return 0;
        }
        if (Users::model()->userEmailExists($pa['email'])) {
            $this->errAddMsg('EMAIL_IN_USE');
            return 0;
        }
        if (empty($pa['pass'])) {
            $this->errAddMsg('PASSWORD_EMPTY');
            return 0;
        }
        $user = Users::model()->userData($this->sesrow['user_id'], array('extra', 'name', 'password', 'password_salt'));
        if ($user['password'] != $this->modelUserPasswordMD5($pa['pass'], $user['password_salt'])) {
            $this->errAddMsg('CURRENT_PASSWORD_WRONG');
            return 0;
        }
        $info = Users::i()->getActivationInfo(array(), func::generator(32));
        $info['email'] = $pa['email'];
        $info['link'] = Users::url('email.change', array('key' => $info['key'], 'step' => $this->sesrow['user_id']));
        $user['extra']['email_change'] = $info;

        $res = Users::model()->userSave($this->sesrow['user_id'], array('extra' => serialize($user['extra'])));
        if (!empty($res)) {
            # отправляем письмо для активации e-mail адреса
            $mailData = array(
                'id' => $this->sesrow['user_id'],
                'name' => $user['name'],
                'email' => $pa['email'],
                'activate_link' => $info['link'],
            );
            if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
            return bff::sendMailTemplate($mailData, 'users_email_change', $pa['email'], false, '', '', $pa['lang']);
        }

        $this->errAddMsg('USER_UPDATE_ERROR');
        return 0;
    }

    function modelIntMailList($pa)
    {
        InternalMail::i();
        $filter = array();
        $aBinds = array();
        $recip = 0;
        if (isset($pa['recipient'])) {
            $filter[] = 'recipient = :recipient';
            $recip = (int)$pa['recipient'];
            $aBinds['recipient'] = (int)$pa['recipient'];
        }
        if (isset($pa['author'])) {
            $filter[] = 'author = :author';
            $aBinds['author'] = (int)$pa['author'];
        }
        if (isset($pa['item_id'])) {
            $filter[] = 'item_id = :item_id';
            $aBinds['item_id'] = (int)$pa['item_id'];
        }
        if (isset($pa['is_new'])) {
            $filter[] = 'is_new = :is_new';
            $aBinds['is_new'] = (int)$pa['is_new'];
        }
        if (isset($pa['all_my'])) {
            $filter[] = '(recipient = :all_my1 OR author = :all_my2)';
            $aBinds['all_my1'] = (int)$pa['all_my'];
            $aBinds['all_my2'] = (int)$pa['all_my'];
            $recip = (int)$pa['all_my'];
        }

        $limit = ' limit ' . (int)$pa['page'] . ',' . (int)$pa['onpage'];

        $rez = array(
            'mails' => array(),
            'items' => array(),
            'users' => array()
        );
        $usids = array();
        $itids = array();

        $aMails = $this->DB->select('SELECT * FROM ' . TABLE_INTERNALMAIL . '  WHERE ' . implode(' AND ', $filter) . ' ORDER BY id DESC ' . $limit, $aBinds);
        foreach ($aMails as $row) {
            $rez['mails'][] = $row;
            $usids[$row['author']] = $row['author'];
            $usids[$row['recipient']] = $row['recipient'];
            $itids[$row['item_id']] = $row['item_id'];
            if (($row['is_new'] == 1) && ($row['recipient'] == $recip)) {
                $this->DB->update(TABLE_INTERNALMAIL, array('is_new' => 0, 'readed' => $this->DB->now()), array('id' => $row['id']));
            }
        }
        if (count($usids)) {
            $aUsers = $this->DB->select('SELECT ' . implode($this->modelUserSet['FldPublic'], ',') . ' FROM ' . TABLE_USERS . '  WHERE user_id IN(' . implode(',', $usids) . ')');
            foreach ($aUsers as $row) {
                if (!empty($row['avatar']))
                    $row['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($row['id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $row['id'] . 'n' . $row['avatar'];
                $rez['users'][] = $row;
            }
        }
        if (count($itids)) {
            $aItems = $this->DB->select('SELECT id, status, title, img_s, img_m, link FROM ' . TABLE_BBS_ITEMS . '  WHERE id IN(' . implode(',', $itids) . ')');
            foreach ($aItems as $row) {
                $rez['items'][] = $row;
            }
        }
        if ($recip) {
            $w = $this->DB->one_data('SELECT count(*) as num FROM ' . TABLE_INTERNALMAIL . ' WHERE recipient = :recipient', ['recipient' => $recip]);
            $this->DB->update(TABLE_USERS_STAT, array('internalmail_new' => $w), array('user_id' => $recip));
        }

        return $rez;
    }

    /**
     * Получаем список контактов
     * @param integer $userID ID пользователя, просматривающего свои сообщения
     * @param string $sqlLimit
     * @return array
     */
    function modelContactsList($userID, $sqlLimit = '')
    {
        Shops::i();
        InternalMail::i();
        $aItems = $this->DB->select('SELECT C.*, M.message, U.name AS username, U.login AS userlogin,
					CONCAT(U.user_id, \'n\', IF((U.avatar > \'\'), U.avatar, NULL)) as avatar, IF((C.user_id != M.author), 0, 1) as lastmy
                   FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
                   LEFT JOIN ' . TABLE_INTERNALMAIL . ' M
                   ON C.last_message_id = M.id
                   LEFT JOIN ' . TABLE_USERS . ' U
                   ON C.interlocutor_id = U.user_id
                   WHERE (C.user_id = :id
                   OR (C.interlocutor_id = :id2 AND C.user_id NOT IN
                   		(SELECT interlocutor_id FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' WHERE user_id = :id3)))
                   ORDER BY C.last_message_date DESC ' . $sqlLimit, ['id' => $userID, 'id2' => $userID, 'id3' => $userID]);
        foreach ($aItems as $key => $item) {
            if ($item['avatar']) {
                $aItems[$key]['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($item['interlocutor_id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $item['avatar'];
            } else {
                $aItems[$key]['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . 'n.png';
            }
            if ($item['user_id'] != $userID) {
                $aItems[$key] = $this->DB->one_array(
                    'SELECT C.user_id AS interlocutor_id, C.interlocutor_id AS user_id, C.interlocutor_folders,
						C.last_message_id, C.last_message_date, C.messages_total, C.shop_id,
						M.message, U.name AS username, U.login AS userlogin,
						CONCAT(U.user_id, \'n\', IF((U.avatar > \'\'), U.avatar, NULL)) as avatar,
						1 as lastmy
						FROM ' . TABLE_INTERNALMAIL_CONTACTS . ' C
						LEFT JOIN ' . TABLE_INTERNALMAIL . ' M
						ON C.last_message_id = M.id
						LEFT JOIN ' . TABLE_USERS . ' U
						ON C.user_id = U.user_id
						WHERE C.interlocutor_id = :id AND C.user_id = :id2', ['id' => $userID, 'id2' => $item['user_id']]
                );
                $aItems[$key]['messages_new'] = '0';
                if ($aItems[$key]['avatar']) {
                    $aItems[$key]['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($aItems[$key]['user_id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $aItems[$key]['avatar'];
                } else {
                    $aItems[$key]['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . 'n.png';
                }
            }
            if ($item['shop_id'] > 0) {
                $tmp = Shops::model()->shopData($item['shop_id'], array('title'));
                $aItems[$key]['shoptitle'] = $tmp['title'];
            }
            if (empty($aItems[$key]['username'])) $aItems[$key]['username'] = $aItems[$key]['userlogin'];
        }
        return $aItems;
    }

    /**
     * Получаем список контактов
     * @param integer $userID ID пользователя, просматривающего свои сообщения
     * @param integer $shopID
     * @param integer $folderID
     * @param string $sSearch
     * @param string $sqlLimit
     * @return array
     */
    function modelContactsListNew($userID, $shopID = 0, $folderID = 0, $sSearch = '', $sqlLimit = '')
    {
        $aFolders = [
            InternalMail::FOLDER_ALL,
            InternalMail::FOLDER_SH_SHOP,
            InternalMail::FOLDER_SH_USER,
            InternalMail::FOLDER_FAVORITE,
            InternalMail::FOLDER_IGNORE
        ];
        $sSearch = bff\base\app::i()->input()->cleanSearchString($sSearch, 50);
        if (!InternalMail::foldersEnabled()) {
            $folderID = -1;
        } else if (!array_key_exists($folderID, $aFolders)) {
            $folderID = InternalMail::FOLDER_ALL;
        }
        $aContacts = InternalMail::model()->getContactsListingFront($userID, $shopID, $folderID, $sSearch, false, $sqlLimit);
        if (!empty($aContacts)) {
            foreach ($aContacts as &$v) {
                $v['shop_id_my'] = ($v['shop_id'] > 0 && $v['shop_id'] == $shopID);
                if ($v['shop_id'] && !$v['shop_id_my']) {
                    $v['c_name'] = $v['shop']['title'];
                    $v['c_logo'] = $this->SET['http_'] . $v['shop']['logo'];
                } else {
                    $v['c_name'] = (!empty($v['name']) ? $v['name'] : $v['login']);
                    $v['c_logo'] = $this->SET['http_'] . UsersAvatar::url($v['user_id'], $v['avatar'], UsersAvatar::szNormal, $v['sex']);
                }
            }
            unset($v);
        }

        return $aContacts;
    }

    /**
     * Получаем сообщения переписки
     * @param integer $userID ID пользователя, просматривающего свои сообщения
     * @param integer $interlocutorID ID собеседника
     * @param integer $shopID
     * @param string $sqlLimit
     * @return array
     */
    function modelConversationMessagesNew($interlocutorID = 0, $shopID = 0, $nFromShop = 0, $sqlLimit = '')
    {
        if ($interlocutorID > 0) {
            $userData = Users::model()->userDataByFilter(array('user_id' => $interlocutorID),
                array(
                    'user_id',
                    'avatar',
                    'sex',
                    'name',
                    'login',
                    'blocked',
                    'activated',
                    'im_noreply'
                )
            );
            if (empty($userData)) {
                $this->errAddMsg('USER_NOT_FOUND');
                return [];
            }
            if ($userData['blocked']) {
                $this->errAddMsg('USER_BLOCKED');
                return [];
            }
            $aUserData = array(
                'id' => $interlocutorID,
                'name' => (!empty($userData['name']) ? $userData['name'] : $userData['login']),
                'avatar' => $this->SET['http_'] . UsersAvatar::url($userData['user_id'], $userData['avatar'], UsersAvatar::szSmall, $userData['sex']),
                'is_shop' => 0
            );
            if ($nFromShop > 0) {
                $tmp = Users::model()->userDataByFilter(array('user_id' => $this->sesrow['user_id']), array('shop_id'));
                $shopID = $tmp['shop_id'];
                $aShopData = Shops::model()->shopData($shopID, array(
                        'id',
                        'title',
                        'logo'
                    )
                );
                $aShopData['logo'] = $this->SET['http_'] . Shops::i()->shopLogo()->url($shopID, $aShopData['logo'], ShopsLogo::szMini, false, true);
            }
        } else if ($shopID > 0) {
            $shopData = Shops::model()->shopData($shopID, array(
                    'id',
                    'user_id',
                    'title',
                    'logo',
                    'status'
                )
            );
            if (empty($shopData)) {
                $this->errAddMsg('SHOP_NOT_FOUND');
                return [];
            }
            if ($shopData['status'] == Shops::STATUS_BLOCKED) {
                $this->errAddMsg('SHOP_BLOCKED');
                return [];
            } else {
                if ($shopData['status'] != Shops::STATUS_ACTIVE) {
                    $this->errAddMsg('SHOP_NOT_ACTIVE');
                    return [];
                }
            }
            $interlocutorID = $shopData['user_id'];
            $aUserData = array(
                'id' => $shopID,
                'name' => $shopData['title'],
                'avatar' => $this->SET['http_'] . Shops::i()->shopLogo()->url($shopID, $shopData['logo'], ShopsLogo::szMini, false, true),
                'is_shop' => 1
            );
        } else {
            $this->errAddMsg('RECEIVER_NOT_DEFINED');
            return [];
        }
        $aMessages = InternalMail::model()->getConversationMessages($this->sesrow['user_id'], $interlocutorID, $shopID, false, $sqlLimit);

        $aItems = array();
        $aNewItems = array();
        $nNewViewed = 0;
        if (!empty($aMessages)) {
            foreach ($aMessages as $v) {
                if ($v['item_id'] > 0) {
                    $aItems[] = $v['item_id'];
                }
                if ($v['new']) {
                    $nNewViewed++;
                    if ($v['item_id'] > 0) {
                        if (!isset($aNewItems[$v['item_id']])) {
                            $aNewItems[$v['item_id']] = 0;
                        }
                        $aNewItems[$v['item_id']]++;
                    }
                }
            }
            if (!empty($aItems)) {
                $aItems = BBS::model()->itemsListChat($aItems);
                foreach ($aItems as &$i) {
                    $i['img_s'] = $this->SET['http_'] . $i['img_s'];
                }
            }
        } else {
            return [];
        }

        $aResult = array(
            'messages' => array_reverse($aMessages),
            'items' => $aItems,
            'user' => $aUserData
        );
        if (!empty($aShopData)) {
            $aResult['my_shop'] = $aShopData;
        }

        # mark as readed
        if ($nNewViewed > 0) {
            InternalMail::model()->setMessagesReaded($this->sesrow['user_id'], $interlocutorID, $shopID, true);
        }
        if (count($aNewItems)) {
            BBS::model()->itemsListChatSetReaded($aNewItems);
        }

        return $aResult;
    }

    /**
     * Получаем сообщения переписки
     * @param integer $userID ID пользователя, просматривающего свои сообщения
     * @param integer $interlocutorID ID собеседника
     * @param string $sqlLimit
     * @param string $lang
     * @param array|integer
     * @return array
     */
    function modelConversationMessages($userID, $interlocutorID, $sort = 1, $sqlLimit = '', $lang = 'ru')
    {
        InternalMail::i();
        if (!isset($lang) || !in_array($lang, $this->languages)) $lang = $this->languageDefault;
        $aSortDirection = $sort ? 'DESC ' : '';
        $aMessages = $this->DB->select('SELECT M.*, DATE(M.created) as created_date, (M.author = :id1) as my,
                        (M.recipient = :id2 AND M.is_new) as new, I.img_s as item_img,
                        I.title_edit as item_title, I.publicated as item_publicated, I.price_ex,
                        I.price as item_price, I.price_curr as item_price_curr, R.title_' . $lang . ' AS item_city,
                        CONCAT(I.price, \' \', CR.title_short) as item_price_with_curr
                   FROM ' . TABLE_INTERNALMAIL . ' M
                   LEFT JOIN ' . TABLE_BBS_ITEMS . ' I ON M.item_id = I.id
                   LEFT JOIN ' . TABLE_REGIONS . ' R ON I.city_id = R.id
                   LEFT JOIN ' . TABLE_CURRENCIES_LANG . ' CR ON CR.id = I.price_curr AND CR.lang = :lang
                   WHERE ( (M.author = :id3 AND M.recipient = :id4) OR (M.recipient = :id5 AND M.author = :id6) )
                   ORDER BY M.created ' . $aSortDirection . $sqlLimit, ['id1' => $userID, 'id2' => $userID, 'id3' => $userID,
            'id4' => $interlocutorID, 'id5' => $userID, 'id6' => $interlocutorID, 'lang' => $lang]);
        $aInterlocData = $this->DB->one_array('SELECT user_id, IF((name > \'\'), name, login) as username, avatar FROM ' . TABLE_USERS . ' WHERE user_id = :id',
            ['id' => $interlocutorID]);
        if ($aInterlocData['avatar']) {
            $aInterlocData['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($aInterlocData['user_id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $aInterlocData['user_id'] . 'n' . $aInterlocData['avatar'];
        } else {
            $aInterlocData['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . 'n.png';
        }
        foreach ($aMessages as $key => $value) {
            if ($value['item_img']) $aMessages[$key]['item_img'] = (substr($value['item_img'], 0, 4) === 'http' ? '' : $this->SET['http_']) . $value['item_img'];
            $aMessages[$key]['username'] = $aInterlocData['username'];
            $aMessages[$key]['avatar'] = $aInterlocData['avatar'];
            $aMessages[$key]['price_ex'] = array(
                'mod' => $aMessages[$key]['price_ex'] & 1 ? 1 : 0, # PRICE_EX_MOD
                'exchange' => $aMessages[$key]['price_ex'] & 2 ? 1 : 0, # PRICE_EX_EXCHANGE
                'free' => $aMessages[$key]['price_ex'] & 4 ? 1 : 0, # PRICE_EX_FREE
                'agreed' => $aMessages[$key]['price_ex'] & 8 ? 1 : 0, # PRICE_EX_AGREED
            );
        }

        # отмечаем сообщение как прочитанное
        $this->DB->update(TABLE_INTERNALMAIL, array('is_new' => 0, 'readed' => $this->DB->now()), array('is_new' => 1,
            'recipient' => $userID, 'author' => $interlocutorID));
        # отмечаем чат как просмотренный
        $this->DB->update(TABLE_INTERNALMAIL_CONTACTS, array('messages_new' => 0), array(
            'user_id' => $userID, 'interlocutor_id' => $interlocutorID));
        # обновляем счетчик в статистике пользователя
        $newMessagesNumb = $this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_INTERNALMAIL . '
                   WHERE recipient = :id AND is_new = 1', ['id' => $userID]);
        $this->DB->update(TABLE_USERS_STAT, array('internalmail_new' => $newMessagesNumb), array('user_id' => $userID));

        return $aMessages;
    }

    function modelSetUserAndroidPushKey($key)
    {
        if (!empty($key)) {
            return $this->DB->exec("REPLACE into " . TABLE_APPREGID . " values(:user_id, :appregid)", ['user_id' => $this->sesrow['user_id'], 'appregid' => $key]);
        } else {
            $this->errAddMsg('PUSH_KEY_NOT_DEFINED');
            return 0;
        }
    }

    /**
     * Получаем данные объявления для дальнейшего редактирования
     * @param integer $userID ID пользователя
     * @param integer $id ID объявления
     * @param string $lang
     * @param array|integer
     */
    function modelItemload($userID, $id, $lang = 'ru')
    {
        if (!isset($lang) || !in_array($lang, $this->languages)) $lang = $this->languageDefault;
        $data = $this->DB->one_array('SELECT I.id, I.user_id, I.is_publicated, I.is_moderating, I.status, I.cat_id, CAT.title AS cat_title, I.title, I.descr, I.name,
					I.city_id, R.title_' . $lang . ' AS city_title, I.district_id, D.title_' . $lang . ' AS district_title,
					I.metro_id, M.title_' . $lang . ' AS metro_title, I.addr_lat as lat, I.addr_lon as lon,
					I.addr_addr, I.price, I.price_curr, I.price_ex, I.phones, I.skype, I.icq, I.svc,
					I.video FROM ' . TABLE_BBS_ITEMS . ' I
                   LEFT JOIN ' . TABLE_BBS_CATEGORIES_LANG . ' CAT ON I.cat_id = CAT.id
                   LEFT JOIN ' . TABLE_REGIONS . ' R ON I.city_id = R.id
                   LEFT JOIN ' . TABLE_REGIONS_DISTRICTS . ' D ON I.district_id = D.id
                   LEFT JOIN ' . TABLE_REGIONS_METRO . ' M ON I.metro_id = M.id
                   WHERE I.user_id = :user_id AND I.id = :id AND CAT.lang = :lang',
            ['user_id' => $userID, 'id' => $id, 'lang' => $lang]);
        $data['price_ex'] = array(
            'mod' => $data['price_ex'] & 1 ? 1 : 0, # PRICE_EX_MOD
            'exchange' => $data['price_ex'] & 2 ? 1 : 0, # PRICE_EX_EXCHANGE
            'free' => $data['price_ex'] & 4 ? 1 : 0, # PRICE_EX_FREE
            'agreed' => $data['price_ex'] & 8 ? 1 : 0, # PRICE_EX_AGREED
        );
        return $data;
    }

    /**
     * Отправляем сообщение в чат
     * @param array|integer
     * @return mixed
     */
    function modelIntMailAdd($pa)
    {

        if (isset($pa['interlocutor']) && !empty($pa['interlocutor'])) {
            # проверяем существование такого пользователя
            $row = $this->DB->one_array('SELECT * FROM ' . TABLE_USERS . ' WHERE user_id = :id', ['id' => (int)$pa['interlocutor']]);
            if (empty($row))
                return array('err' => 'RECIPIENT_NOT_FOUND');
            $pa['recipient'] = $pa['interlocutor'];
            $pa['item_id'] = 0;
        } else if (empty($pa['answ_id'])) {
            # если пишем по объявлению
            $item = $this->DB->one_array('SELECT id, user_id FROM ' . TABLE_BBS_ITEMS . ' WHERE id = :id AND deleted = 0', ['id' => (int)@$pa['item_id']]);
            if (empty($item))
                return array('err' => 'ITEM_NOT_FOUND');
            $pa['recipient'] = $item['user_id'];
            $pa['item_id'] = $item['id'];
        } else {
            # ответ на сообщение
            $row = $this->DB->one_array('SELECT * FROM ' . TABLE_INTERNALMAIL . ' WHERE id = :id', ['id' => (int)$pa['answ_id']]);
            if (empty($row))
                return array('err' => 'ITEM_NOT_FOUND');
            $pa['recipient'] = $row['author'];
            $pa['item_id'] = $row['item_id'];
        }

        # проверяем необходимые данные перед непосредственной обработкой
        if (empty($pa['recipient']))
            return array('err' => 'RECIPIENT_NOT_FOUND');
        if (empty($pa['author']))
            return array('err' => 'AUTHOR_NOT_FOUND');
        if (empty($pa['message']) || (mb_strlen($pa['message']) < 1) || (mb_strlen($pa['message']) > 1000))
            return array('err' => 'MAIL_MESSAGE_WRONG');

        # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
        if (Site::i()->preventSpam('users-write-form', 15)) {
            return array('err' => array('msg' => 'MAIL_TIMELIMIT', 'dtime' => 15));
        }

        # отправляем сообщение владельцу ОБ
        $messageID = InternalMail::model()->sendMessage($pa['author'], $pa['recipient'], 0,
            $pa['message'], '', $pa['item_id']);
        if ($messageID > 0 && $pa['item_id']) {
            # обновляем счетчик сообщений ОБ
            BBS::model()->itemSave($pa['item_id'], array(
                    'messages_total = messages_total + 1',
                    'messages_new = messages_new + 1',
                )
            );
        }

        $aSender = Users::model()->userData($pa['author'], ['name', 'login', 'avatar']);
        $sSenderName = !empty($aSender['name']) ? $aSender['name'] : $aSender['login'];//$this->DB->one_data('SELECT login FROM ' . TABLE_USERS . ' WHERE user_id = :id', ['id' => (int)$pa['author']]);
        # ищем получателя в списке пользователей приложения и, если найден, отправляем ему извещение о новом сообщении
        $aData = [
            'message_text' => (mb_strlen($pa['message']) > 20) ? substr($pa['message'], 0, 17) . '...' : $pa['message'],// заголовок объявления
            'name_user' => $sSenderName,//имя юзера
            'avatar' => empty($aSender['avatar']) ? '' : $this->SET['http_'] . UsersAvatar::url($pa['author'], $aSender['avatar'], UsersAvatar::szNormal),
            'user_id' => $pa['author'],
            'for_user_id' => $pa['recipient'],
        ];
        $this->DB->select_iterator('SELECT appregid FROM ' . TABLE_APPREGID . ' WHERE id = :id', array('id' => (int)$pa['recipient']),
            function ($user) use ($aData) {
                $this->FCMsendNotification([$user['appregid']], $aData);
            });
        $this->DB->select_iterator('SELECT appleregid FROM ' . TABLE_APPLEREGID . ' WHERE id = :id', array('id' => (int)$pa['recipient']),
            function ($user) use ($sSenderName) {
                $this->iOSsendNotification([$user['appleregid']], 'Новое сообщение от ' . $sSenderName);
            });

        return array('id' => $messageID);
    }

    /**
     * Отправляем сообщение в чат
     * @param integer
     * @param integer
     * @param string
     * @param integer
     * @return mixed
     */
    function modelIntMailAddNew($interlocutorID = 0, $shopID = 0, $sMessage = '', $nFromShop = 0, $itemID = 0)
    {
        $userID = $this->sesrow['user_id'];
        if ($interlocutorID > 0) {
            $userData = Users::model()->userDataByFilter(array('user_id' => $interlocutorID),
                array(
                    'user_id',
                    'avatar',
                    'sex',
                    'name',
                    'login',
                    'blocked',
                    'activated',
                    'im_noreply'
                )
            );
            if (empty($userData)) {
                $this->errAddMsg('USER_NOT_FOUND');
                return 0;
            }
            if ($userData['blocked']) {
                $this->errAddMsg('USER_BLOCKED');
                return 0;
            }
            if ($nFromShop > 0) {
                $tmp = Users::model()->userDataByFilter(array('user_id' => $this->sesrow['user_id']), array('shop_id'));
                $shopID = $tmp['shop_id'];
            }
        } else if ($shopID > 0) {
            $shopData = Shops::model()->shopData($shopID, array(
                    'id',
                    'user_id',
                    'title',
                    'logo',
                    'status'
                )
            );
            if (empty($shopData)) {
                $this->errAddMsg('SHOP_NOT_FOUND');
                return 0;
            }
            if ($shopData['status'] == Shops::STATUS_BLOCKED) {
                $this->errAddMsg('SHOP_BLOCKED');
                return 0;
            } else {
                if ($shopData['status'] != Shops::STATUS_ACTIVE) {
                    $this->errAddMsg('SHOP_NOT_ACTIVE');
                    return 0;
                }
            }
            $interlocutorID = $shopData['user_id'];
        } else {
            $this->errAddMsg('RECEIVER_NOT_DEFINED');
            return 0;
        }
        $sMessage = InternalMail::model()->cleanMessage($sMessage, 4000, false);
        if (mb_strlen($sMessage) < 5) {
            $this->errAddMsg('MESSAGE_TOO_SHORT');
            return 0;
        }
        if (!empty($userData['im_noreply']) ||
            InternalMail::model()->isUserInFolder($userID, $interlocutorID, $shopID, InternalMail::FOLDER_IGNORE)
        ) {
            $this->errAddMsg('USER_DENIED_MESSAGES');
            return 0;
        }
        $messageID = InternalMail::model()->sendMessage($userID, $interlocutorID, $shopID, $sMessage, '', $itemID);

        if (empty($messageID)) {
            $this->errAddMsg('MESSAGE_NOT_SEND');
            return 0;
        }

        $aSender = Users::model()->userData($userID, ['name', 'login', 'avatar']);
        $sSenderName = !empty($aSender['name']) ? $aSender['name'] : $aSender['login'];
        # ищем получателя в списке пользователей приложения и, если найден, отправляем ему извещение о новом сообщении
        $aData = [
            'message_text' => (mb_strlen($sMessage) > 20) ? substr($sMessage, 0, 17) . '...' : $sMessage,// заголовок объявления
            'name_user' => $sSenderName,//имя юзера
            'avatar' => empty($aSender['avatar']) ? '' : $this->SET['http_'] . UsersAvatar::url($userID, $aSender['avatar'], UsersAvatar::szNormal),
            'user_id' => $userID,
            'for_user_id' => $interlocutorID,
        ];
        $this->DB->select_iterator('SELECT appregid FROM ' . TABLE_APPREGID . ' WHERE id = :id', array('id' => $interlocutorID),
            function ($user) use ($aData) {
                $this->FCMsendNotification([$user['appregid']], $aData);
            });
        $this->DB->select_iterator('SELECT appleregid FROM ' . TABLE_APPLEREGID . ' WHERE id = :id', array('id' => $interlocutorID),
            function ($user) use ($sSenderName) {
                $this->iOSsendNotification([$user['appleregid']], 'Новое сообщение от ' . $sSenderName);
            });

        return $messageID;
    }

    function modelCatsList($pa)
    {
        $counterVersion = (defined("TABLE_BBS_ITEMS_COUNTERS") && $this->DB->isTable(TABLE_BBS_ITEMS_COUNTERS));

        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;

        $rez = array(
            'rows' => array(),
            'url_path' => $this->SET['domain'] . 'search/',
            'img_path' => $this->SET['domain'] . 'files/images/cats/'
        );
        $dat = &$rez['rows'];
        $dak = array();
        $dap = array();
        $filter = array('C.pid != 0', 'C.enabled = 1');
        $aBinds = array();

        if (isset($pa['id'])) {
            $filter['id'] = 'C.id = :id';
            $aBinds['id'] = (int)$pa['id'];
        }
        if (isset($pa['pid'])) {
            $filter['pid'] = 'C.pid = :pid';
            $aBinds['pid'] = (int)$pa['pid'];
        }
        if (isset($pa['lvl:max'])) {
            $filter[] = 'C.numlevel <= :numlvl';
            $aBinds['numlvl'] = (int)$pa['lvl:max'];
        }

        $sJoinQuery = array('LEFT JOIN ' . TABLE_BBS_CATEGORIES_LANG . ' CL ON (CL.id = C.id AND CL.lang = :lang)');
        $aBinds['lang'] = $pa['lang'];
        if ($counterVersion) {
            $sJoinQuery[] = 'LEFT JOIN ' . TABLE_BBS_ITEMS_COUNTERS . ' N ON C.id = N.cat_id AND N.delivery = 0 AND N.region_id = :region';
            $aBinds['region'] = Geo::i()->defaultCountry();
        }

        $sQuery = 'SELECT C.id, C.pid, C.icon_' . $pa['icon'] . ' as i, CL.title as t, C.keyword as k, (C.numright-C.numleft) > 1 as subs, C.numlevel as lvl, C.photos, C.addr';
        $sQuery = $counterVersion ? $sQuery . ', N.items' : $sQuery . ', C.items';
        $sQuery .= ' FROM ' . TABLE_BBS_CATEGORIES . ' C ' . implode(' ', $sJoinQuery) . ' WHERE ' . implode(' AND ', $filter) . ' ORDER BY C.numleft ASC';

        $this->DB->select_iterator($sQuery, $aBinds, function ($item) use (&$dak, &$dap, &$dat) {
            $item['props'] = array();
            $dak[$item['id']] = $item;
            $dap[$item['id']] = &$dak[$item['id']]['props'];
            $dat[] = &$dak[$item['id']];
        });

        $this->DB->select_iterator('SELECT d.*, m.name_' . $pa['lang'] . ', m.value FROM '
            . TABLE_BBS_CATEGORIES_DYNPROPS . ' d LEFT JOIN ' . TABLE_BBS_CATEGORIES_DYNPROPS_MULTI
            . ' m ON m.dynprop_id = d.id WHERE 1 ORDER by d.cat_id, d.num', array(),
            function ($value) use (&$dap, $pa) {
                if (!array_key_exists($value['cat_id'], $dap)) return;
                if (!isset($dap[$value['cat_id']][$value['id']])) {
                    $dap[$value['cat_id']][$value['id']] = array(
                        'id' => $value['id'],
                        't' => $value['title_' . $pa['lang']],
                        'd' => $value['description_' . $pa['lang']],
                        'type' => $value['type'],
                        'def' => $value['default_value'],
                        'req' => $value['req'],
                        'sea' => $value['is_search'],
                        'extra' => unserialize($value['extra']),
                        'list' => array()
                    );
                    if (empty($dap[$value['cat_id']][$value['id']]['extra']['search_ranges']))
                        unset($dap[$value['cat_id']][$value['id']]['extra']['search_ranges']);
                    if (empty($dap[$value['cat_id']][$value['id']]['extra']))
                        unset($dap[$value['cat_id']][$value['id']]['extra']);
                    if (empty($value['value'])) return;
                }
                $dap[$value['cat_id']][$value['id']]['list'][] = array(
                    'n' => $value['name_' . $pa['lang']],
                    'v' => $value['value']
                );
            });

        foreach ($dap as $key => $value)
            if (empty($value))
                unset($dak[$key]['props']);

        if ($counterVersion && !empty($rez['rows'])) {
            $deliveryCountry = Geo::i()->defaultCountry();
            # добавим счетчики объявлений с доставкой по всей стране
            if ($deliveryCountry) {
                $cats = array();
                foreach ($rez['rows'] as $v) {
                    $cats[] = $v['id'];
                }
                $counters = BBS::model()->itemsCountByFilter(array('cat_id' => $cats, 'region_id' => $deliveryCountry, 'delivery' => 1),
                    array('cat_id', 'items'), false);
                if (!empty($counters)) {
                    $counters = func::array_transparent($counters, 'cat_id', true);
                    foreach ($rez['rows'] as &$v) {
                        if (!isset($counters[$v['id']])) continue;
                        $v['items'] += $counters[$v['id']]['items'];
                    }
                    unset($v);
                }
            }
        }

        return $rez;
    }

    function modelItemDetail($pa) {

        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        bff::locale()->setCurrentLanguage($pa['lang'], true);

        if(!empty($pa['id'])) {
            $row = $this->DB->one_array('SELECT I.*, R.title_' . $pa['lang'] . ' as city_title FROM ' . TABLE_BBS_ITEMS . ' I
			LEFT JOIN ' . TABLE_REGIONS . ' R ON I.city_id = R.id
			WHERE I.id = :id', array('id' => (int)$pa['id']));
            if ($row['price_curr']) {
                $row['curr_display'] = $this->DB->one_data('SELECT title_short FROM ' . TABLE_CURRENCIES_LANG . '
                WHERE id = :price_curr AND lang = :lang', array('price_curr' => $row['price_curr'], 'lang' => $pa['lang']));
            }
        } else {
            return array('err' => 'ID_NOT_FOUND');
        }

        if (empty($row))
            return array('err' => 'NOT_FOUND');

        if (!empty($row['price_curr'])) {
            array_merge($row, $this->DB->one_array('SELECT title_short as curr_display FROM ' . TABLE_CURRENCIES_LANG . '
			    WHERE id = :id AND lang = :lang', array('id' => (int)$row['price_curr'], 'lang' => $pa['lang'])));
        } else {

        }

        $this->modelItemRow($row);
        $tmpcat = $this->modelLoneCatInfo($row['cat_id'], $pa['lang']);
        $row['price_mod'] = isset($tmpcat['price_sett']['mod_title']) ? $tmpcat['price_sett']['mod_title'] : $row['price_mod'];
        $row['fav'] = !isset($this->sesrow['user_id']) ? 0 : $this->DB->one_data('SELECT COUNT(*) FROM '.TABLE_BBS_ITEMS_FAV.' F WHERE F.item_id = :item_id AND F.user_id = :user_id',
            array('item_id' => (int)@$row['id'], 'user_id' => $this->sesrow['user_id']));

        if ($row['shop_id'] != 0) {
            $row['shop_data'] = Shops::model()->shopData($row['shop_id'], array('title', 'descr', 'logo'));
            $row['shop_data']['logo'] = $this->SET['http_'] . ShopsLogo::url($row['shop_id'], $row['shop_data']['logo'], ShopsLogo::szView);
        }

        $row['cat_list'] = array();
        $row['cat_props'] = array();
        $similar = array(
            'lang' => $pa['lang'],
            'page' => 0,
            'onpage' => 3,
            'filters' => array(
                '!id' => $row['id'],
                'cats_or' => array('cat_id' => $row['cat_id']),
                'sort' => 'publicated_less',
            )
        );
        $se = array();
        $sd = array();
        $aBinds = array();
        for ($i = 1; $i < 5; $i++) {
            if (isset($row['cat_id'.$i]) && ($row['cat_id'.$i] > 0)) {
                $se[] = 'SELECT C.id, C.pid, C.icon_'.$pa['icon'].' as i, CL.title as t, C.keyword as k, C.items, (C.numright - C.numleft) > 1 as subs, C.numlevel as lvl
					FROM ' .TABLE_BBS_CATEGORIES. ' C LEFT JOIN ' .TABLE_BBS_CATEGORIES_LANG. ' CL ON (CL.id = C.id AND CL.lang = :lang'.$i.') WHERE C.id = :id'.$i;
                $aBinds['lang'.$i] = $pa['lang'];
                $aBinds['id'.$i] = $row['cat_id'.$i];
                $sd[] = $row['cat_id'.$i];
                if ($row['cat_id'] != $row['cat_id'.$i])
                    $similar['cats_or']['cat_id'.$i] = $row['cat_id'.$i];
            }
        }
        if (count($se)) {
            $row['cat_list'] = $this->DB->select('('.implode(') UNION (', $se).')', $aBinds);
        }

        $dynprops = BBS::i()->dp()->getByOwner($row['cat_id'], true);
        foreach ($dynprops as $k => $v) {
            switch ($v['type']) {
                case 1:
                    if ($row['f' . $v['data_field']] !== '') {
                        $row['cat_props'][] = [
                            'id' => $v['id'],
                            'title' => $v['title'],
                            'description' => $v['description'],
                            'type' => $v['type'],
                            'data_field' => $v['data_field'],
                            'default_value' => $v['default_value'],
                            'v' => $row['f' . $v['data_field']]
                        ];
                    }
                    break;
                case 9:
                    $aMultiProps = array();
                    $a = (int)$row['f' . $v['data_field']];
                    $tmp = $this->DB->select('SELECT name_'.$pa['lang'].' as name, value FROM ' . TABLE_BBS_CATEGORIES_DYNPROPS_MULTI
                        . ' WHERE dynprop_id = :id ORDER by num', array('id' => $v['id']));

                    foreach ($tmp as $value) {
                        if ($a & (int)$value['value']) $aMultiProps[] = $value['name'];
                    }
                    if (empty($aMultiProps)) break;
                    $row['cat_props'][] = [
                        'id' => $v['id'],
                        'title' => $v['title'],
                        'description' => $v['description'],
                        'type' => $v['type'],
                        'data_field' => $v['data_field'],
                        'default_value' => $v['default_value'],
                        'v' => implode(', ',$aMultiProps)
                    ];
                    break;
                case 10:
                    if (($row['f' . $v['data_field']] == 0) && ($v['default_value'] == '')) {
                    } else {
                        $row['cat_props'][] = [
                            'id' => $v['id'],
                            'title' => $v['title'],
                            'description' => $v['description'],
                            'type' => $v['type'],
                            'data_field' => $v['data_field'],
                            'default_value' => $v['default_value'],
                            'v' => $row['f' . $v['data_field']]
                        ];
                    }
                    break;
                default:
                    if (($row['f' . $v['data_field']] == 0) && ($v['default_value'] == '')) {
                    } else {
                        $w = $this->DB->one_array('SELECT name_'.$pa['lang'].' as name, value FROM ' . TABLE_BBS_CATEGORIES_DYNPROPS_MULTI
                            . ' WHERE dynprop_id = :id AND value = :value', array('id' => $v['id'], 'value' => (int)$row['f' . $v['data_field']]));
                        if ($w) $row['f' . $v['data_field']] = $w['name'];
                        if (empty($row['f' . $v['data_field']])) break;
                        $row['cat_props'][] = [
                            'id' => $v['id'],
                            'title' => $v['title'],
                            'description' => $v['description'],
                            'type' => $v['type'],
                            'data_field' => $v['data_field'],
                            'default_value' => $v['default_value'],
                            'v' => $row['f' . $v['data_field']]
                        ];
                    }
            }
        }
        unset($dynprops);
        foreach (preg_grep('/f(\d+)/', array_keys($row)) as $i) unset($row[$i]);

        $row['cat_props'] = array_values($row['cat_props']);

        $row['images'] = $this->modelItemImages($row['id']);
        $row['user'] = $this->modelUserGetInfo($row['user_id']);
        if (isset($pa['similar']))
            $row['similar'] = $this->modelItemsList($similar);
        if (isset($pa['need_cat_dyn_info']))
            $row['cat_dyn_info'] = $this->getAllCatDynProperties(['id' => end($row['cat_list'])['id'], 'lang' => $pa['lang']]);
        $row['category_edit'] = self::catFormEdit();

        if (empty($this->sesrow['user_id']) || $row['user_id'] != $this->sesrow['user_id']) {
            BBS::model()->itemViewsIncrement($row['id'], 'item');
        } else {
            $row['up_free'] = 0;
            $days = BBS::svcUpFreePeriod();
            if ($days > 0) {
                $row['up_free'] = strtotime($row['svc_up_free']) > strtotime('-' . $days . ' days') ? 0 : 1;
            }
        }

        return $row;
    }

    public static function catFormEdit()
    {
        return config::sysAdmin('bbs.form.category.edit', false, TYPE_BOOL) ? 1 : 0;
    }

    function modelItemsList($pa)
    {
        $flagUseOtherFilters = true;
        $flagFavouritesList = false;
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        bff::locale()->setCurrentLanguage($pa['lang'], true);

        $premoderation = BBS::premoderation();

        $rez = array(
            'rows' => array(),
            'districts' => array(),
            'domain' => $this->SET['domain']
        );
        $dat = &$rez['rows'];

        $filter = array();
        $filter[] = 'I.deleted = 0';

        $qs = array('I.id, I.status, I.title, I.cat_id, I.link, I.img_s, I.img_m, I.owner_type, I.imgcnt as imgs, I.moderated,
			I.addr_lat as lat, I.addr_lon as lon, I.addr_addr, I.price, I.price_curr, I.price_ex, I.district_id,
			((I.svc & 2) > 0) as svc_marked, ((I.svc & 4) > 0) as svc_fixed, I.svc_premium, ((I.svc & 17) > 0) as svc_turbo, ((I.svc & 32) > 0) as svc_quick,
			((I.svc & 1) > 0) as svc_up, I.descr, I.publicated, I.publicated_to, I.created, I.modified, C.price_sett, C.price as price_on,
			CL.title as cat_title,R.title_' . $pa['lang'] . ' AS city_title, I.price_search, CR.title_short as curr_display');
        $qj = array('LEFT JOIN ' . TABLE_REGIONS . ' R ON I.city_id = R.id',
            'INNER JOIN ' . TABLE_BBS_CATEGORIES . ' C ON C.id = I.cat_id',
            'INNER JOIN ' . TABLE_BBS_CATEGORIES_LANG . ' CL ON (CL.id = I.cat_id AND CL.lang = :lang)',
            'LEFT JOIN ' . TABLE_CURRENCIES_LANG . ' CR ON CR.id = I.price_curr AND CR.lang = :lang2'
        );
        $aBinds = array('lang' => $pa['lang'], 'lang2' => $pa['lang']);

        if (!empty($pa['filters']) && array_key_exists('id_favs', $pa['filters']) && !empty($pa['filters']['id_favs'])) {
            $flagUseOtherFilters = false;
            $flagFavouritesList = true;
            $filter[] = 'I.id IN (' . implode(',', $pa['filters']['id_favs']) . ')';
        } else if (!empty($pa['filters']) && array_key_exists('id_favs', $pa['filters']) && empty($pa['filters']['id_favs'])) {
            return $rez;
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['!id'])) {
            $filter[] = 'I.id != :nid';
            $aBinds['nid'] = (int)$pa['filters']['!id'];
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['cats_id'])) {
            $row = $this->DB->one_array('SELECT id, pid, numleft, numright FROM ' . TABLE_BBS_CATEGORIES . ' WHERE id = :id', ['id' => (int)$pa['filters']['cats_id']]);

            if (($row['numright'] - $row['numleft']) > 1) {

                $se = array($row['id']);
                $tmp = $this->DB->select_one_column('SELECT id FROM ' . TABLE_BBS_CATEGORIES . ' WHERE numleft > :numleft AND numright < :numright',
                    array('numleft' => $row['numleft'], 'numright' => $row['numright']));
                foreach ($tmp as $value)
                    $se[] = $value;
                $this->sql_ArrayToWhere($filter, $se, 'i', 'I.cat_id', array('>' => 0));
            } else {
                $this->sql_ArrayToWhere($filter, $pa['filters']['cats_id'], 'i', 'I.cat_id', array('>' => 0));
            }
        } else {
            $pa['filters']['cats_id'] = 0;
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['cats_or'])) {
            $se = array();
            if (!empty($pa['filters']['cats_or']['cat_id'])) {
                $se[] = 'I.cat_id = :cat_id';
                $aBinds['cat_id'] = (int)$pa['filters']['cats_or']['cat_id'];
            }
            if (!empty($pa['filters']['cats_or']['cat_id1'])) {
                $se[] = 'I.cat_id1 = :cat_id1';
                $aBinds['cat_id1'] = (int)$pa['filters']['cats_or']['cat_id1'];
            }
            if (!empty($pa['filters']['cats_or']['cat_id2'])) {
                $se[] = 'I.cat_id2 = :cat_id2';
                $aBinds['cat_id2'] = (int)$pa['filters']['cats_or']['cat_id2'];
            }
            if (!empty($pa['filters']['cats_or']['cat_id3'])) {
                $se[] = 'I.cat_id3 = :cat_id3';
                $aBinds['cat_id3'] = (int)$pa['filters']['cats_or']['cat_id3'];
            }
            if (!empty($pa['filters']['cats_or']['cat_id4'])) {
                $se[] = 'I.cat_id4 = :cat_id4';
                $aBinds['cat_id4'] = (int)$pa['filters']['cats_or']['cat_id4'];
            }
            if (count($se) > 1) {
                $filter[] = '(' . implode(' OR ', $se) . ')';
            } elseif (count($se) == 1) {
                $filter[] = implode('', $se);
            }
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['user_id'])) {
            $filter[] = 'I.user_id = :user_id';
            $filter['shop_id'] = 'I.shop_id = 0';
            $aBinds['user_id'] = (int)$pa['filters']['user_id'];

            if ($this->sesrow['user_id'] != $pa['filters']['user_id']) {
                $filter[] = 'I.status = 3';
                $filter['is_publicated'] = 'I.is_publicated = 1';
                if ($premoderation) $filter[] = 'I.moderated = 1';
            } else {
                $qs[] = 'I.views_item_total, I.views_contacts_total, I.messages_total, I.blocked_reason, I.publicated_to, I.svc_up_free';
                if (isset($pa['filters']['!status'])) $filter[] = 'I.status != ' . $pa['filters']['!status'];
                if (isset($pa['filters']['status']) && $pa['filters']['status']) $filter[] = 'I.status = ' . $pa['filters']['status'];
                $filter['shop_id'] = 'I.shop_id = 0';
                if ($premoderation && isset($pa['filters']['moderated'])) {
                    if ($pa['filters']['moderated'] == 1) {
                        $filter[] = 'I.moderated = 1';
                        $filter['is_publicated'] = 'I.is_publicated = 1';
                    } else {
                        $filter[] = '(I.moderated = 0 OR I.moderated = 2)';
                    }
                }
            }
        } else {
            $filter[] = 'I.status = 3'; # publicated only
            $filter['is_publicated'] = 'I.is_publicated = 1';
            if ($premoderation) $filter[] = 'I.moderated = 1';
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['shop_id'])) {
            $filter['shop_id'] = 'I.shop_id = :shop_id';
            $aBinds['shop_id'] = (int)$pa['filters']['shop_id'];

            if (!empty($this->sesrow['user_id'])) {
                $aUserData = Users::model()->userData($this->sesrow['user_id'], ['shop_id']);
                if ($aUserData['shop_id'] == $pa['filters']['shop_id']) {
                    $qs[] = 'I.views_item_total, I.views_contacts_total, I.messages_total, I.blocked_reason, I.publicated_to, I.svc_up_free';
                    if (isset($pa['filters']['!status'])) $filter[] = 'I.status != ' . $pa['filters']['!status'];
                    if (isset($pa['filters']['status']) && $pa['filters']['status']) $filter[] = 'I.status = ' . $pa['filters']['status'];
                    if ($premoderation && isset($pa['filters']['moderated'])) {
                        if ($pa['filters']['moderated'] == 1) {
                            $filter[] = 'I.moderated = 1';
                            $filter['is_publicated'] = 'I.is_publicated = 1';
                        } else {
                            $filter[] = '(I.moderated = 0 OR I.moderated = 2)';
                        }
                    }
                }
            }
        }


        if ($flagUseOtherFilters && !empty($pa['filters']['myfav']) && !empty($this->sesrow['user_id'])) {
            $qj['F'] = 'INNER JOIN ' . TABLE_BBS_ITEMS_FAV . ' F ON F.item_id = I.id AND F.user_id = :user_id_fav';
            $qs['F'] = 'IF(IFNULL(F.user_id, 0) > 0, 1, 0) as fav';
            $aBinds['user_id_fav'] = $this->sesrow['user_id'];
            //unset($filter['filters']['!premium']);
            $this->favCounterCorrect((int)$this->sesrow['user_id']);
        } else if (!empty($this->sesrow['user_id'])) {
            $qj['F'] = 'LEFT JOIN ' . TABLE_BBS_ITEMS_FAV . ' F ON F.item_id = I.id AND F.user_id = :user_id_fav';
            $qs['F'] = 'IF(IFNULL(F.user_id, 0) > 0, 1, 0) as fav';
            $aBinds['user_id_fav'] = $this->sesrow['user_id'];
        } else {
            $qs['F'] = 'IFNULL(NULL, 0) as fav';
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['title'])) {
            $pa['filters']['title'] = '%' . trim($pa['filters']['title']) . '%';
            if (strlen($pa['filters']['title']) > 1) {
                $filter[] = '(I.title LIKE :title OR I.descr LIKE :desc)';
                $aBinds['title'] = $pa['filters']['title'];
                $aBinds['desc'] = $pa['filters']['title'];
            }
        }
        if ($flagUseOtherFilters && !empty($pa['filters']['created_more'])) {
            $t = $this->date2time($pa['filters']['created_more']);
            if ($t) {
                $filter[] = 'I.created > :createdm';
                $aBinds['createdm'] = date('Y.m.d H:i:s', $t);
            }
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['owner_type']) && $pa['filters']['owner_type'] > 0) {
            $filter[] = 'I.owner_type = :owner_type';
            $aBinds['owner_type'] = (int)$pa['filters']['owner_type'];
        }

        if ($flagUseOtherFilters && !empty($pa['filters']['is_image'])) {
            $filter[] = ($pa['filters']['is_image'] == 1) ? 'I.imgcnt > 0' : 'I.img_m = 0';
        }


        if ($flagUseOtherFilters && !empty($pa['filters']['created_less'])) {
            $t = $this->date2time($pa['filters']['created_less']);
            if ($t) {
                $filter[] = 'I.created < :createdl';
                $aBinds['createdl'] = date('Y.m.d H:i:s', $t);
            }
        }
        if ($flagUseOtherFilters && !empty($pa['filters']['city_id'])) {
            $filter[] = '(I.reg1_country = :city_id OR I.reg2_region = :city_id OR I.reg3_city = :city_id OR I.city_id = :city_id)';
            $aBinds['city_id'] = (int)$pa['filters']['city_id'];
        }


        if ($flagUseOtherFilters && !empty($pa['filters']['props']) && is_array($pa['filters']['props'])) {
            $aProps = $pa['filters']['props'];
            $aDynprops = array();
            foreach ($aProps as $n => $prop) {
                $aDynprops[$prop['n']] = $n;
            }

            $props = BBS::i()->dp()->getByID(array_keys($aDynprops), true);
            foreach ($aDynprops as $id => $n) {
                $aProps[$n]['n'] = $props[$id]['data_field']; //f1 - f....
                $aProps[$n]['id'] = $id; //add
            }

            foreach ($aProps as $v)
                if ((@$v['n'] > 0) && ($v['n'] < 20)) {
                    if (isset($v['v'])) {
                        if (in_array($props[$v['id']]['type'], [6, 8, 9]) && !empty($props[$v['id']]['multi'])) { //$v['id'] insted $id

                            $filter_path = array();
                            if (in_array($props[$v['id']]['type'], [9])) {
                                $av = array_sum($v['v']);
                                $filter_path[] = '(FLOOR(I.f' . (int)$v['n'] . ') & ' . (int)$av . ')=' . (int)$av;
                            } else {
                                foreach ($v['v'] as $f) {
                                    $filter_path[] = 'I.f' . (int)$v['n'] . '=' . (int)$f;
                                }
                            }
                            $filter[] = '(' . implode(' OR ', $filter_path) . ')';

                        } else {
                            $filter[] = 'I.f' . (int)$v['n'] . '=' . (int)$v['v']; //types 3,4,5
                        }
                    }
                    //types 10,11
                    if (isset($v['v_more']))
                        $filter[] = 'I.f' . (int)$v['n'] . ' >= ' . (int)$v['v_more'];
                    if (isset($v['v_less']))
                        $filter[] = 'I.f' . (int)$v['n'] . ' <= ' . (int)$v['v_less'];
                }
        }

        // price props
        if ($flagUseOtherFilters && isset($pa['filters']['p_more']))
            $filter[] = 'I.price >= ' . (int)$pa['filters']['p_more'];
        if ($flagUseOtherFilters && isset($pa['filters']['p_less']))
            $filter[] = 'I.price <= ' . (int)$pa['filters']['p_less'];

        $sort = ' ORDER BY I.svc_fixed DESC, I.svc_fixed_order DESC';

        if (isset($pa['filters']['sort'])) {
            if ($pa['filters']['sort'] == 'publicated_more')
                $sort .= ', I.publicated_order';
            if ($pa['filters']['sort'] == 'publicated_less')
                $sort .= ', I.publicated_order DESC';
            if ($pa['filters']['sort'] == 'price_more')
                $sort .= ', I.price_search';
            if ($pa['filters']['sort'] == 'price_less')
                $sort .= ', I.price_search DESC';
        } else {
            $sort .= ', I.publicated_order DESC';
        }

        $limit = ' LIMIT ' . (int)$pa['page'] * (int)$pa['onpage'] . ',' . (int)$pa['onpage'];
        $aDistricts = array();
        $sQuery = 'SELECT ' . implode(',', $qs) . ' FROM ' . TABLE_BBS_ITEMS . ' I ' . implode(' ', $qj) . ' WHERE ' . implode(' AND ', $filter) . $sort . $limit;
        $aItems = $this->DB->select($sQuery, $aBinds);

        if ($sort == 0) {
            $sQuery = 'SELECT COUNT(I.id) as count_items FROM ' . TABLE_BBS_ITEMS . ' I ' . implode(' ', $qj) . ' WHERE ' . implode(' AND ', $filter);
            $rez['count_items'] = $this->DB->one_data($sQuery, $aBinds);
        }

        if ($flagFavouritesList) {
            $queryForActualItems = 'SELECT I.id FROM ' . TABLE_BBS_ITEMS . ' I WHERE ' . implode(' AND ', $filter);
            $rez['actual_items'] = $this->DB->select_one_column($queryForActualItems);
        }

        //Fix for IOs
        foreach ($aItems as $value) {
            $tmp = $value;
            $this->modelItemRow($tmp);
            $tmpcat = $this->modelLoneCatInfo($tmp['id'], $pa['lang']);
            $tmp['price_mod'] = isset($tmpcat['price_sett']['mod_title']) ? $tmpcat['price_sett']['mod_title'] : $tmp['price_mod'];
            $dat[] = $tmp;
            $aDistricts[$tmp['district_id']] = $tmp['district_id'];
        }
        if (count($aDistricts)) {
            $rez['districts'] = $this->DB->select('SELECT id, title_' . $pa['lang'] . ' as t FROM ' . TABLE_REGIONS_DISTRICTS . ' WHERE id IN (' . implode(',', $aDistricts) . ')');
        }

        if (isset($pa['filters']['user_id']) && isset($this->sesrow['user_id']) && $this->sesrow['user_id'] == $pa['filters']['user_id']) {
            $rez['my_counters'] = array();
            $rez['my_counters']['active'] = $this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_BBS_ITEMS . ' WHERE status = 3 AND shop_id = 0 AND user_id = :user_id AND deleted = 0', ['user_id' => $pa['filters']['user_id']]);
            $rez['my_counters']['not_active'] = $this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_BBS_ITEMS . ' WHERE status != 3 AND shop_id = 0 AND user_id = :user_id AND deleted = 0', ['user_id' => $pa['filters']['user_id']]);
            if (!empty($pa['shop_id'])) {
                $rez['my_counters']['shop_active'] = $this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_BBS_ITEMS . ' WHERE status = 3 AND shop_id > 0 AND user_id = :user_id AND deleted = 0', ['user_id' => $pa['filters']['user_id']]);
                $rez['my_counters']['shop_not_active'] = $this->DB->one_data('SELECT COUNT(*) FROM ' . TABLE_BBS_ITEMS . ' WHERE status != 3 AND shop_id > 0 AND user_id = :user_id AND deleted = 0', ['user_id' => $pa['filters']['user_id']]);
            }

            if (BBS::svcUpFreePeriod() > 0 && isset($pa['filters']['status']) && $pa['filters']['status'] == BBS::STATUS_PUBLICATED) {
                $days = BBS::svcUpFreePeriod();
                $upTo = strtotime('-' . $days . ' days');
                foreach ($dat as $key => $item) {
                    $data = BBS::model()->itemData($item['id'], array('svc_up_free'));
                    $dat[$key]['up_free'] = strtotime($data['svc_up_free']) > $upTo ? 0 : 1;
                }
            }

            foreach ($dat as $key => $item) {
                if ((strtotime($item['publicated_to']) - time()) < (86400 * 3)) {
                    $dat[$key]['refresh'] = 1;
                } else {
                    $dat[$key]['refresh'] = 0;
                }
            }

            $userData = Users::model()->userDataByFilter(array('user_id' => $pa['filters']['user_id']), array('shop_id'));
            $nShopID = (int)$userData['shop_id'];
            if ($nShopID && bff::shopsEnabled() && Shops::abonementEnabled()) {
                $rez['limitsPayed'] = Shops::i()->abonementLimitExceed($nShopID);
            } else if (BBS::limitsPayedEnabled()) {
                $rez['limitsPayed'] = BBS::model()->limitsPayedCategoriesForUser(array(
                    'user_id' => $pa['filters']['user_id'],
                    'shop_id' => 0,
                ), true);
            }
        } else if (isset($pa['filters']['user_id'])) {
            $rez['user_data'] = $this->modelUserGetInfo($pa['filters']['user_id']);/*Users::model()->userData($pa['filters']['user_id'], array('user_id as id', 'name', 'login', 'last_activity', 'shop_id', 'created', 'avatar', 'sex',
                'phones', 'contacts', 'region_id', 'addr_lat', 'addr_lon'));*/
            $rez['user_data'] = empty($rez['user_data']) ? null : $rez['user_data'];
            /*if (!empty($rez['user_data']['avatar']))
                $rez['user_data']['avatar'] = $this->SET['domain'] . $this->modelUserSet['AvatarSitePath'] . ((int)(substr(str_pad($rez['user_data']['id'], 10, '0', STR_PAD_LEFT), 0, 7))) . '/' . $rez['user_data']['id'] . 'n' . $rez['user_data']['avatar'];
            */
            if (empty(floatval($rez['user_data']['addr_lat'])) && empty(floatval($rez['user_data']['addr_lon'])) && !empty($rez['user_data']['region_id'])) {
                $tmp = $this->getRegionDataById(['id' => $rez['user_data']['region_id'], 'lang' => $pa['lang']]);
                if (!empty($tmp['yc'])) {
                    list($rez['user_data']['addr_lat'], $rez['user_data']['addr_lon']) = explode(',', $tmp['yc']);
                }
            }
        } else if (isset($pa['filters']['shop_id'])) {
            $rez['shop_data'] = Shops::model()->shopData($pa['filters']['shop_id'], array('title', 'descr', 'logo', 'region_id', 'addr_lat', 'addr_lon', 'phones', 'site'));
            $rez['shop_data'] = empty($rez['shop_data']) ? null : $rez['shop_data'];
            if (!empty($rez['shop_data']['phones'])) {
                $aPhones = array();
                foreach ($rez['shop_data']['phones'] as $phoneData) {
                    $aPhones[] = $phoneData['v'];
                }
                $rez['shop_data']['phones'] = $aPhones;
            }
            if (!empty($rez['shop_data']['logo']))
                $rez['shop_data']['logo'] = $this->SET['http_'] . ShopsLogo::url($pa['filters']['shop_id'], $rez['shop_data']['logo'], ShopsLogo::szView);
            if (empty(floatval($rez['shop_data']['addr_lat'])) && empty(floatval($rez['shop_data']['addr_lon'])) && !empty($rez['shop_data']['region_id'])) {
                $tmp = $this->getRegionDataById(['id' => $rez['shop_data']['region_id'], 'lang' => $pa['lang']]);
                if (!empty($tmp['yc'])) {
                    list($rez['shop_data']['addr_lat'], $rez['shop_data']['addr_lon']) = explode(',', $tmp['yc']);
                }
            }
        }

        return $rez;
    }

    function modelItemRow(&$v)
    {
        if ($v['price_on'] = (!empty($v['price_on']))) {
            if ($v['price_mod'] = ($v['price_ex'] & 1)) {
                $v['price_sett'] = unserialize($v['price_sett']);
                $v['price_mod'] = (!empty($v['price_sett']['mod_title']['ru']) ? $v['price_sett']['mod_title']['ru'] : 'Торг возможен');
            } else {
                $v['price_mod'] = '';
            }
        } else {
            $v['price_mod'] = '';
        }

        unset($v['price_sett']);
        if (!empty($v['img_s'])) {
            $se = explode('/', $v['img_s']);
            if (strpos(array_pop($se), '_') && isset($this->modelUserSet['ItemImgPath2']))
                $v['img_s'] = str_replace($this->modelUserSet['ItemImgPath'], $this->modelUserSet['ItemImgPath2'], rawurlencode($v['img_s']));
            if (strpos($v['img_s'], 'http') === false) $v['img_s'] = $this->SET['http_'] . $v['img_s'];
            $v['img_s'] = str_replace(' ', '%20', $v['img_s']);
        }
        if (!empty($v['img_m'])) {
            $se = explode('/', $v['img_m']);
            if (strpos(array_pop($se), '_') && isset($this->modelUserSet['ItemImgPath2']))
                $v['img_m'] = str_replace($this->modelUserSet['ItemImgPath'], $this->modelUserSet['ItemImgPath2'], rawurlencode($v['img_m']));
            if (strpos($v['img_m'], 'http') === false) $v['img_m'] = $this->SET['http_'] . $v['img_m'];
            $v['img_m'] = str_replace(' ', '%20', $v['img_m']);
        }
        if (isset($v['link']))
            $v['link'] = str_replace('//{sitehost}/', $this->SET['domain'], $v['link']);
        if (!empty($v['phones']))
            $v['phones'] = unserialize($v['phones']);
        if (!empty($v['contacts']))
            $v['contacts'] = json_decode($v['contacts'], true);

        if (!empty($v['title']))
            $v['title'] = html_entity_decode($v['title']);
        if (!empty($v['title_edit']))
            $v['title_edit'] = html_entity_decode($v['title_edit']);
        $v['price'] = number_format((float)$v['price'], 2, ".", " ");
        $v['price_search'] = number_format((float)$v['price_search'], 2, ".", " ");
        $v['currency_default'] = Site::currencyDefault();

        $v['price_ex'] = array(
            'mod' => $v['price_ex'] & 1 ? 1 : 0, # PRICE_EX_MOD
            'exchange' => $v['price_ex'] & 2 ? 1 : 0, # PRICE_EX_EXCHANGE
            'free' => $v['price_ex'] & 4 ? 1 : 0, # PRICE_EX_FREE
            'agreed' => $v['price_ex'] & 8 ? 1 : 0, # PRICE_EX_AGREED
        );
        if ($v['price']) {
            $v['item_price_display'] = str_replace('.00', '', $v['price']) . ' ' . $v['curr_display'];
        } else {
            $v['item_price_display'] = '';
        }

        return $v;
    }

    function modelItemImages($id)
    {
        $dat = $this->DB->select('SELECT * FROM ' . TABLE_BBS_ITEMS_IMAGES . ' WHERE item_id = :id order by num', ['id' => (int)$id]);
        foreach ($dat as $key => $value) {
            if (substr($value['filename'], 0, 1) == '_' && isset($this->modelUserSet['ItemImgPath2'])) {
                $dat[$key]['path'] = $this->SET['domain'] . 'images/' . $value['dir'] . '/' . $value['item_id'] . 'z' . rawurlencode($value['filename']);
            } else {
                $dat[$key]['path'] = $this->SET['domain'] . $this->modelUserSet['ItemImgPath'] . $value['dir'] . '/' . $value['item_id'] . 'z' . rawurlencode($value['filename']);
            }
        }
        return $dat;
    }

    function modelItemClaimAdd($pa)
    {
        $inf = $this->DB->one_data('SELECT id FROM ' . TABLE_BBS_ITEMS . ' WHERE id = :id', ['id' => @$pa['item_id']]);
        if (empty($inf))
            return array('err' => 'NOT_FOUND');

        $pa['user_id'] = (int)$pa['user_id'];
        $pa['item_id'] = $inf;
        $pa['reason'] = (int)@$pa['reason'];
        $reasons = array(
            1 => _t('item-claim', 'Неверная рубрика'),
            2 => _t('item-claim', 'Запрещенный товар/услуга'),
            4 => _t('item-claim', 'Объявление не актуально'),
            8 => _t('item-claim', 'Неверный адрес'),
            1024 => _t('item-claim', 'Другое'),
        );
        if (($pa['reason'] & 1024) && mb_strlen($pa['message']) < 3)
            return array('err' => 'CLAIM_EMPTY_MESSAGE');
        if (!$pa['reason'] > 0)
            return array('err' => 'CLAIM_EMPTY_REASON');
        $pa['message'] = isset($pa['message']) ? mb_substr(trim(@$pa['message']), 0, 1000) : '';

        $aParams = array(
            'item_id' => $pa['item_id'],
            'user_id' => $pa['user_id'],
            'user_ip' => $pa['user_ip'],
            'reason' => $pa['reason'],
            'message' => $pa['message'],
            'viewed' => 0,
            'created' => date('Y-m-d H:i:s')
        );
        $result = $this->DB->insert(TABLE_BBS_ITEMS_CLAIMS, $aParams);
        return $result ? true : false;
    }

    function modelFavList($nUserID)
    {
        return $this->DB->select_one_column('SELECT item_id FROM ' . TABLE_BBS_ITEMS_FAV . ' WHERE user_id = :id', ['id' => $nUserID]);
    }

    function modelFavUpdate($pa)
    {
        if (!empty($pa['items']) && is_array($pa['items'])) {
            $this->DB->delete(TABLE_BBS_ITEMS_FAV, ['user_id' => (int)$pa['user_id']]);
            $pa['items_add'] = $pa['items'];
        }
        if (!empty($pa['items_add']) && is_array($pa['items_add'])) {
            $ar = array_flip($pa['items_add']);
            foreach ($ar as $k => $v) {
                $this->DB->insert(TABLE_BBS_ITEMS_FAV, ['user_id' => (int)$pa['user_id'], 'item_id' => (int)$k]);
            }
        }
        if (!empty($pa['items_del']) && is_array($pa['items_del'])) {
            $ar = array_flip($pa['items_del']);
            foreach ($ar as $k => $v) {
                $this->DB->delete(TABLE_BBS_ITEMS_FAV, ['user_id' => (int)$pa['user_id'], 'item_id' => (int)$k]);
            }
        }

        return $this->favCounterCorrect((int)$pa['user_id']);
    }

    function favCounterCorrect($nUserId)
    {
        $aFavoritesID = $this->modelFavList($nUserId);
        $aFilter = array('id' => $aFavoritesID, 'status' => BBS::STATUS_PUBLICATED);
        if (BBS::premoderation()) {
            $aFilter[':mod'] = 'moderated > 0';
        }
        # корректируем счетчик избранных ОБ пользователя
        $aFavoritesExists = BBS::model()->itemsDataByFilter($aFilter, array('id'));
        if (sizeof($aFavoritesID) != sizeof($aFavoritesExists)) {
            $aDeleteID = array();
            foreach ($aFavoritesID as $v) {
                if (!array_key_exists($v, $aFavoritesExists)) $aDeleteID[] = $v;
            }
            if (!empty($aDeleteID)) {
                BBS::model()->itemsFavDelete($nUserId, $aDeleteID);
            }
        }
        \bff::security()->userCounter('items_fav', sizeof($aFavoritesExists), $nUserId, false);

        return sizeof($aFavoritesExists);
    }

    /**
     * Формирование SQL запроса для сохранения дин.свойств
     * @param array $aData массив доп. параметров
     * @return array
     */
    function dpSavePrepare($aData)
    {
        if (empty($aData)) return NULL;

        $aDynpropsData = array();
        foreach ($aData as $props) {
            foreach ($props as $id => $v) {
                $aDynpropsData[$id] = $v;
            }
        }
        $aDynprops = BBS::i()->dp()->getByID(array_keys($aDynpropsData), true);

        if (empty($aDynprops)) return array();

        return BBS::i()->dp()->prepareSaveDataByID($aDynpropsData, $aDynprops, 'update', true);
    }

    /**
     * Поиск "минус слов" в строке
     * @param  string $sString строка
     * @param string $sWord @ref найденное слово
     * @return bool true - нашли минус слово, false - нет
     */
    protected function spamMinusWordsFound($sString, & $sWord = '', $lang = null)
    {
        if (empty($lang) || !in_array($lang, $this->languages)) $lang = $this->languageDefault;
        static $aMinusWords;
        if (!isset($aMinusWords)) {
            $aMinusWords = func::unserialize(config::get('bbs_items_spam_minuswords', ''));
        }
        if (empty($aMinusWords[LNG])) return false;
        return \bff\utils\TextParser::minuswordsSearch($sString, $sWord, $aMinusWords[$lang]);
    }

    /**
     * Проверка дублирования пользователем объявлений
     * @param integer $nUserID ID пользователя
     * @param array $aData @ref данные ОБ, ищем по заголовку 'title' и/или описанию 'descr'
     * @return bool true - нашли похожее объявление, false - нет
     */
    protected function spamDuplicatesFound($nUserID, &$aData)
    {
        if (!$nUserID) return false;
        if (!config::get('bbs_items_spam_duplicates', false)) return false;

        $query = array(0 => array());
        if (!empty($aData['title'])) {
            $query[0][] = 'title LIKE :title';
            $query[':title'] = $aData['title'];
        }
        if (!empty($aData['descr'])) {
            $query[0][] = 'descr LIKE :descr';
            $query[':descr'] = $aData['descr'];
        }
        if (!empty($query[0])) {
            $query[0] = '(' . join(' OR ', $query[0]) . ')';
            if (isset($aData['id'])) {
                $query[0] = '(id != ' . $aData['id'] . ' AND ' . $query[0] . ')';
            }
            if (BBS::model()->itemsCount(array(
                'user_id' => $nUserID,
                'deleted' => 0,
                ':query' => $query,
            ))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Инициализация компонента ApiBBSItemImages
     * @param integer $nUserID ID пользователя
     * @param integer $nItemID ID объявления
     * @return ApiBBSItemImages component
     */
    public function itemImagesObj($nUserId, $nItemID = 0)
    {
        BBS::i()->itemImages();
        require_once 'Api_Items_Images.php';

        static $i;
        if (!isset($i)) {
            $i = new ApiBBSItemImages();
        }
        $i->setRecordID($nItemID);
        $i->setUserID($nUserId);

        return $i;
    }

    /**
     * Инициализация компонента BBSItemVideo
     * @return BBSItemVideo component
     */
    public function itemVideo()
    {
        static $i;
        if (!isset($i)) {
            include_once PATH_MODULES . 'bbs/bbs.item.video.php';
            $i = new BBSItemVideo();
        }
        return $i;
    }

    /**
     * Валидация данных объявления для создания/редактирования
     * @param $pa
     * @param $aData
     * @param int $nItemID
     * @param array $aItemData
     * @return int
     */
    protected function validateItemData($pa, &$aData, $nItemID = 0, $aItemData = array())
    {
        if (empty($pa['lang']) || !in_array($pa['lang'], $this->languages)) {
            $lang = $this->languageDefault;
        } else {
            $lang = $pa['lang'];
        }
        bff::locale()->setCurrentLanguage($lang, true);

        $aKeyPars = array(
            'lat' => 'addr_lat', # => TYPE_NUM адрес, координата LAT
            'lon' => 'addr_lon', # => TYPE_NUM адрес, координата LON
            'dop' => ' ',          # пропустить
            'images' => ' ',
            'ps' => ' ',
        );

        foreach ($pa as $key => $value) {
            $dkey = isset($aKeyPars[$key]) ? $aKeyPars[$key] : $key;
            if ($dkey == ' ') continue;
            $aData[$dkey] = $value;
        }

        $aItemData = BBS::model()->itemData($nItemID, array('user_id',
            'city_id',
            'shop_id',
            'cat_id',
            'status',
            'publicated_order',
            'video',
            'imgcnt',
            'title',
            'descr',
            'moderated')
        );

        # Категория
        $catID = empty($aData['cat_id']) ? 0 : $aData['cat_id'];
        if ($catID === 0) {
            $this->errAddMsg('NOT_CATEGORY');
            return 0;
        }

        # достаем данные категории
        $catData = BBS::model()->catData($catID,
            array(
                'id',
                'pid',
                'subs',
                'numlevel',
                'numleft',
                'numright',
                'addr',
                'price',
                'keyword',
                'photos',
                'price_sett',
                'tpl_title_enabled',
            )
        );
        # если данных о категории нет либо категория имеет подкатегории
        if (empty($catData) || $catData['subs'] > 0) {
            $this->errAddMsg('NOT_CORRECT_CATEGORY');
            return 0;
        }

        # Автоматическая генерация заголовка (если включена для данной категории)
        if ($catData['tpl_title_enabled']) {
            $generatedTitles = $this->generateTitle($pa);
            $aData = array_merge($aData, $generatedTitles);
        }

        # проверяем, можно ли изменять категорию товара
        if ($nItemID && !BBSBase::categoryFormEditable() && $catID != $aItemData['cat_id']) {
            $this->errAddMsg('CAN_NOT_CHANGE_CATEGORY');
            return 0;
        } else {
            # подготавливаем ID категорий ОБ для сохранения в базу:
            # cat_id(выбранная, самая глубокая), cat_id1, cat_id2, cat_id3 ...
            $catParents = BBS::model()->catParentsID($catData, true);
            foreach ($catParents as $k => $v) {
                $aData['cat_id' . $k] = $v;
            }
            # заполняем все оставшиеся уровни категорий нулями
            for ($i = BBS::CATS_MAXDEEP; $i > 0; $i--) {
                if (!isset($aData['cat_id' . $i])) {
                    $aData['cat_id' . $i] = 0;
                }
            }
        }

        # Модерация
        if ($aItemData['status'] == BBS::STATUS_BLOCKED) {
            # объявление заблокировано, помечаем на проверку модератору
            $aData['moderated'] = 0;
        }
        # помечаем на модерацию при изменении: названия, описания, категории
        if ($aData['title'] != $aItemData['title'] || $aData['descr'] != $aItemData['descr'] ||
            (BBSBase::categoryFormEditable() && $aData['cat_id'] != $aItemData['cat_id'])) {
            if ($aItemData['moderated']) $aData['moderated'] = 2;
        }

        # Заголовок
        if (empty($aData['title'])) {
            $this->errAddMsg('NOT_TITLE');
            return 0;
        } elseif (mb_strlen($aData['title']) < 5) {
            $this->errAddMsg('SHORT_TITLE');
            return 0;
        }
        $aData['title'] = trim(preg_replace('/\s+/', ' ', $aData['title']));
        $aData['title'] = \bff\utils\TextParser::antimat($aData['title']);

        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        # antispam: banned words in title
        if ($this->spamMinusWordsFound($aData['title'], $sWord, $pa['lang'])) {
            $this->errAddMsg('BANNED_TITLE_WORD:' . $sWord);
            return 0;
        }

        # Описание
        $aData['descr'] = bff\base\app::i()->input()->cleanTextPlain($aData['descr'], false, false);
        if (empty($aData['descr']) || mb_strlen($aData['descr']) < 12) {
            $this->errAddMsg('NOT_OR_SHORT_DESCR');
            return 0;
        }
        $aData['descr'] = trim(preg_replace('/\s{2,}$/m', '', $aData['descr']));
        $aData['descr'] = preg_replace('/ +/', ' ', $aData['descr']);
        $aData['descr'] = \bff\utils\TextParser::antimat($aData['descr']);
        # antispam: banned words in description
        if ($this->spamMinusWordsFound($aData['descr'], $sWord, $pa['lang'])) {
            $this->errAddMsg('BANNED_DESCR_WORD:' . $sWord);
            return 0;
        }
        # antispam:  dublicate text
        if ($this->spamDuplicatesFound($aData['user_id'], $aData)) {
            $this->errAddMsg('DUPLICATE_TEXT');
            return 0;
        }

        # Данные пользователя
        Users::i()->cleanUserData($aData, array('name', 'skype', 'icq'));
        if (!empty($aData['phones'])) {
            $nCountBefore = count($aData['phones']);
            $aData['phones'] = Users::validatePhones($aData['phones'], Users::i()->profilePhonesLimit);
            if (count($aData['phones']) != $nCountBefore) {
                $this->errAddMsg('NOT_VALID_PHONE');
                return 0;
            }
        } else {
            $aData['phones'] = array();
        }
        if (empty($aData['name']) || mb_strlen($aData['name']) < 2) {
            $this->errAddMsg('NOT_OR_SHORT_NAME');
            return 0;
        }

        # Город
        if (!$aData['city_id']) {
            $this->errAddMsg('NOT_CITY');
            return 0;
        } else {
            if (!Geo::isCity($aData['city_id'])) {
                $this->errAddMsg('CITY_NOT_CORRECT');
                return 0;
            }
        }

        if (!empty($aData['shop_id'])) {
            if ($nItemID && $aItemData['shop_id'] != $aData['shop_id']) {
                $this->errAddMsg('SHOP_ID_MISSMATCH');
                return 0;
            }
            $aShopData = Users::model()->userData($this->sesrow['user_id'], array('shop_id'));
            if ($aShopData['shop_id'] != $aData['shop_id']) {
                $this->errAddMsg('SHOP_OWNER_MISSMATCH');
                return 0;
            }
            if ($aData['shop_id'] && !Shops::model()->shopActive($aData['shop_id'])) {
                $this->errAddMsg('SHOP_NOT_ACTIVE');
                return 0;
            }
        }

        if (!Geo::coveringType(Geo::COVERING_COUNTRY)) {
            $cityData = Geo::regionData($aData['city_id']);
            if (!$cityData || !Geo::coveringRegionCorrect($cityData)) {
                $this->errAddMsg('CITY_NOT_CORRECT');
                return 0;
            }
        }
        if ($aData['city_id'] && isset($aData['district_id']) && $aData['district_id']) {
            $aDistricts = Geo::districtList($aData['city_id']);
            if (empty($aDistricts) || !array_key_exists($aData['district_id'], $aDistricts)) {
                $aData['district_id'] = 0;
            }
        } else {
            $aData['district_id'] = 0;
        }
        # Доставка в регионы
        if (array_key_exists('regions_delivery', $catData) && !$catData['regions_delivery']) {
            $aData['regions_delivery'] = 0;
        }

        if (!empty($catData['addr']) && empty($aData['addr_lat']) && empty($aData['addr_lon'])) {
            $cityData = Geo::model()->regionData(['id' => $aData['city_id']], ['ycoords']);
            if ($cityData) {
                $coords = explode(",", $cityData['ycoords']);
                $aData['addr_lat'] = $coords[0];
                $aData['addr_lon'] = $coords[1];
            }
        }

        # OWNER TYPE
        if (empty($aData['owner_type'])) $aData['owner_type'] = BBS::OWNER_PRIVATE;

        # Видео
        if (empty($aData['video'])) {
            $aData['video_embed'] = '';
        } else {
            if (!$nItemID || ($nItemID && $aData['video'] != $aItemData['video'])) {
                $aVideo = BBS::i()->itemVideo()->parse($aData['video']);
                $aData['video_embed'] = serialize($aVideo);
                if (!empty($aVideo['video_url'])) {
                    $aData['video'] = $aVideo['video_url'];
                }
            }
        }

        # Адрес
        if (empty($catData['addr'])) {
            unset($aData['addr_addr'], $aData['addr_lat'], $aData['addr_lon']);
        }

        # Контакты (masked версия)
        $aContacts = array(
            'phones' => array(),
            'skype' => (!empty($aData['skype']) ? mb_substr($aData['skype'], 0, 2) . 'xxxxx' : ''),
            'icq' => (!empty($aData['icq']) ? mb_substr($aData['icq'], 0, 2) . 'xxxxx' : ''),
        );
        if (!empty($aData['phones'])) {
            foreach ($aData['phones'] as $v) {
                $aContacts['phones'][] = $v['m'];
            }
        }
        $aData['contacts'] = json_encode($aContacts);
        unset($aContacts);

        # разворачиваем данные о регионе: city_id => reg1_country, reg2_region, reg3_city
        $aRegions = Geo::model()->regionParents($aData['city_id']);
        $aData = array_merge($aData, $aRegions['db']);

        $catPriceSett = $catData['price_sett'];

        # корректируем цену:
        if (!empty($aData['price_ex'])) {
            foreach ($aData['price_ex'] as $i) {
                if ($i && !($i & $catPriceSett['ex'])) {
                    $this->errAddMsg('PRICE_EX_NOT_CORRECT');
                    return 0;
                }
            }
            $aData['price_ex'] = array_sum($aData['price_ex']);
        } else {
            $aData['price_ex'] = 0;
        }
        if (empty($aData['price'])) $aData['price'] = 0;
        $aData['price'] = str_replace(' ', '', $aData['price']);

        if ($catData['price']) {
            # конвертируем цену в основную по курсу (для дальнейшего поиска)
            if (empty($aData['price_curr'])) $aData['price_curr'] = $catPriceSett['curr'] ? $catPriceSett['curr'] : config::sysAdmin('currency.default', 1, TYPE_UINT);
            $aData['price_search'] = Site::currencyPriceConvertToDefault($aData['price'], $aData['price_curr']);
        }

        # эскейпим заголовок
        $aData['title_edit'] = $aData['title'];
        $aData['title'] = HTML::escape($aData['title_edit']);

        # формируем URL-keyword на основе title
        $aData['keyword'] = mb_strtolower(func::translit($aData['title_edit']));
        $aData['keyword'] = preg_replace("/\-+/", '-', preg_replace('/[^a-z0-9_\-]/', '', $aData['keyword']));

        # формируем URL объявления (@items.search@translit-ID.html)
        $sLink = BBSBase::url('items.search', array(
            'keyword' => $catData['keyword'],
            'region' => $aRegions['keys']['region'],
            'city' => $aRegions['keys']['city'],), true
        );
        $sLink .= $aData['keyword'] . '-';
        if ($nItemID) {
            $sLink .= $nItemID . '.html';
        }
        $aData['link'] = $sLink;

        # Готовим динамические свойства к сохранению
        if (!empty($pa['dop'])) {
            $aDop = array();
            foreach ($pa['dop'] as $keyCat => $aParams) {
                $aDopParams = array();
                $nfObj = -1;
                foreach ($aParams as $key => $dopParam) {
                    if ($nfObj == -1) {
                        $nfObj = ($key == 0 ? 0 : 1);
                    }
                    if ($nfObj == 1) {
                        $iDop = $key;
                        $vDop = $dopParam;
                    } else {
                        $iDop = array_keys($dopParam)[0];
                        $vDop = $dopParam[$iDop];
                    }
                    $aDopParams[$iDop] = $vDop;
                }
                $aDop[$keyCat] = $aDopParams;
            }

            $aDataDP = $this->dpSavePrepare($aDop);
            if (!empty($aDataDP))
                $aData = array_merge($aData, $aDataDP);
            else {
                $this->errAddMsg('ERROR_DYN_PROPERTIES');
                return 0;
            }
        }
        return 1;
    }

    function uploadImages($oImages, $aImages, $nItemID)
    {
        if (!$aImages || empty($aImages)) return;
        if (count($aImages) > $oImages->getLimit()) {
            $this->errAddMsg('COUNT_IMAGES_MORE_LIMIT');
        }

        $tmp = $oImages->getItemsImagesData([$nItemID]);
        $oldImages = array_key_exists($nItemID, $tmp) ? $tmp[$nItemID] : [];
        $aItemImagesNew = [];

        foreach ($aImages as $key => $value) {
            if (strpos($value, '://') !== false) {
                $t = strpos($value, $nItemID . 'z');
                $imgName = substr($value, $t + strlen($nItemID . 'z'));
                foreach ($oldImages as $img) {
                    if ($img["filename"] == $imgName) {
                        $aItemImagesNew[$img["id"]] = $img["filename"];
                    }
                }
            } else {
                $dir_img = $this->SET['public_site_path'] . $this->modelUserSet['TmpImgPath'];
                bff::errors()->clear();
                $rez = $oImages->uploadFromFile($dir_img . $value, FALSE);
                if (!$rez) {
                    bff::log(print_r($this->errors, true), 'upload_errors.log');
                    bff::errors()->clear();
                    $this->errAddMsg('ERROR_IMAGE_FILE_UPLOAD:N' . ($key + 1));
                } else {
                    $aItemImagesNew[$rez["id"]] = $rez["filename"];
                }
            }
        }

        foreach ($oldImages as $img) {
            if (!array_key_exists($img['id'], $aItemImagesNew)) $oImages->deleteImage($img['id']);
        }

        if (count($oldImages)) $oImages->saveOrder($aItemImagesNew);
    }

    /**
     * Корректировка URL текущего запроса с последующим 301 редиректом
     * @param string $correctURL корректный URL
     */
    public function urlCorrection($correctURL)
    {
        $landing = \SEO::landingPage();
        if ($landing !== false) {
            $correctURL = static::urlBase(LNG) . $landing['landing_uri'];
        }
        $aURI = parse_url(\Request::uri());
        if (!empty($aURI)) {
            if (stripos($aURI['path'], '%') !== false) {
                $aURI['path'] = urldecode($aURI['path']);
            }
            if (strpos($correctURL, '//') === 0) {
                $correctURL = \Request::scheme() . ':' . $correctURL;
            }
            # выполняем редирект на https версию
            if (config::sys('https.redirect', false) && mb_stripos($correctURL, 'http:') === 0) {
                $correctURL = 'https:' . mb_substr($correctURL, 5);
            }
        }
    }

    /**
     * Продвижение ОБ
     * @param array $aItem
     * @return integer
     */
    public function itemSvc($aItem)
    {
        if (!bff::servicesEnabled()) {
            $this->errAddMsg('SVC_NOT_ENABLED');
            return 0;
        }

        $nUserID = $this->sesrow['user_id'];
        $nItemID = $aItem['id'];
        $aItemData = BBS::model()->itemData($nItemID, array('user_id', 'cat_id', 'city_id', 'deleted', 'status'));
        if (!$nItemID || empty($aItem) || $aItemData['deleted'] || in_array($aItemData['status'], array(
                    BBS::STATUS_BLOCKED,
                    BBS::STATUS_PUBLICATED_OUT
                )
            )
        ) {
            $this->errAddMsg('ITEM_NOT_OR_BLOCKED');
            return 0;
        }
        if ($aItemData['user_id'] != $this->sesrow['user_id']) {
            $this->errAddMsg('ITEM_OWNER_MISSMATCH');
            return 0;
        }

        $aSvc = BBS::model()->svcData();
        $nSvcID = $aItem['svc_id'];
        if (!$nSvcID || !isset($aSvc[$nSvcID])) {
            $this->errAddMsg('SVC_NOT_OR_FAILED');
            return 0;
        }

        $ps = $aItem['ps'];
        if ($ps != 'balance') {
            $this->errAddMsg('PAYMENT_METHOD_NOT_AVAILABLE');
            return 0;
        }

        $aSvcPrices = BBS::model()->svcPricesEx(array_keys($aSvc), $aItemData['cat_id'], $aItemData['city_id']);
        foreach ($aSvcPrices as $k => $v) {
            if (!empty($v)) $aSvc[$k]['price'] = $v;
        }

        $aSvcSettings = array();
        $nSvcPrice = $aSvc[$nSvcID]['price'];

        $nUserBalance = Users::model()->userBalance($nUserID, true);

        if ($nUserBalance < $nSvcPrice) {
            $this->errAddMsg('LOW_BALANCE');
            return 0;
        }
        # конвертируем сумму в валюту для оплаты по курсу
        $pay = Bills::getPayAmount($nSvcPrice, $ps);
        $activated = Svc::i()->activate('bbs', $nSvcID, false, $nItemID, $nUserID, $nSvcPrice, $pay['amount'], $aSvcSettings);
        if ($activated)
            return 1;

        $this->errAddMsg('SVC_NOT_PAIED: ' . implode(', ', bff::errors()->get(true)));
        return 0;
    }

    /**
     * Формируем ключ активации ОБ
     * @return array (code, link, expire)
     */
    protected function getActivationInfo()
    {
        $aData = array();
        $aData['key'] = md5(uniqid(SITEHOST . 'ASDAS(D90--00];&%#97665.,:{}' . BFF_NOW, true));
        $aData['link'] = BBS::url('item.activate', array('c' => $aData['key']));
        $aData['expire'] = date('Y-m-d H:i:s', strtotime('+1 day'));

        return $aData;
    }

    /**
     * Получаем срок публикации объявления в днях
     * @param mixed $mFrom дата, от которой выполняется подсчет срока публикации
     * @param string $mFormat тип требуемого результата, строка = формат даты, false - unixtime
     * @return int
     */
    public function getItemPublicationPeriod($mFrom = false, $mFormat = 'Y-m-d H:i:s')
    {
        $nDays = config::get('bbs_item_publication_period', 30, TYPE_UINT);
        if ($nDays <= 0) {
            $nDays = 7;
        }

        if (empty($mFrom) || is_bool($mFrom)) {
            $mFrom = time();
        } else if (is_string($mFrom)) {
            $mFrom = strtotime($mFrom);
            if ($mFrom === false) {
                $mFrom = time();
            }
        }

        $nPeriod = strtotime('+' . $nDays . ' days', $mFrom);
        if (!empty($mFormat)) {
            return date($mFormat, $nPeriod);
        } else {
            return $nPeriod;
        }
    }

    function modelItemAdd($pa)
    {
        $this->rez['REZULT'] = 0;
        $nUserID = $pa['user_id'];

        $users = Users::i();

        $aUserData = $this->modelUserGetInfo($nUserID);

        if (empty($aUserData)) {
            $this->errAddMsg('AUTHOR_NOT_FOUND');
            return;
        }

        // is user active?
        $mBanned = $users->checkBan(true, false, $nUserID);
        if ($mBanned) {
            $this->errAddMsg('AUTHOR_NOT_ACTIVE: ' . $mBanned);
            return;
        }
        if (!empty($aUserData['blocked'])) {
            $this->errAddMsg('AUTHOR_NOT_ACTIVE: ' . $aUserData['blocked_reason']);
            return;
        }

        if ($aUserData['activated'] != 1) {
            $this->errAddMsg('AUTHOR_NOT_ACTIVE');
            return;
        }

        //antispam: 20s new add for one IP
        if (Site::i()->preventSpam('bbs-add', 20, false)) {
            $this->errAddMsg('ADD_PERIOD_20S');
            return;
        }

        $aData = array();
        $res = $this->validateItemData($pa, $aData);
        if (!$res) {
            return;
        }

        $aData['status'] = BBS::STATUS_PUBLICATED;
        if ($aData['shop_id'] && bff::shopsEnabled() && Shops::abonementEnabled()) {
            # проверим превышение лимита лимитирования по абонементу
            if (Shops::i()->abonementLimitExceed($aData['shop_id'])) {
                $aData['status'] = BBS::STATUS_PUBLICATED_OUT;
            }
        } else if (BBS::limitsPayedEnabled()) {
            $limit = BBS::model()->limitsPayedCategoriesForUser(array(
                'user_id' => $nUserID,
                'shop_id' => $aUserData['shop_id'],
                'cat_id' => $aData['cat_id']
            ));
            if (!empty($limit)) {
                $limit = reset($limit);
                if ($limit['cnt'] >= $limit['limit']) {
                    $aData['status'] = BBS::STATUS_PUBLICATED_OUT;
                } else {
                    $aData['status'] = BBS::STATUS_PUBLICATED;
                }
            }
        }

        $aData['publicated'] = $this->DB->now();
        $aData['publicated_order'] = $this->DB->now();
        if (BBS::svcUpFreePeriod()) {
            $aData['svc_up_free'] = $this->DB->now();
            $aData['svc_up_date'] = $this->DB->now();
        }
        $aData['publicated_to'] = $this->getItemPublicationPeriod();
        $aData['moderated'] = 0; # помечаем на модерацию

        unset($aData['files']);

        $nItemID = BBS::model()->itemSave(0, $aData);

        if ($nItemID == 0) {
            $this->errAddMsg('DATA_SAVE_ERROR');
            return;
        }

        if (!empty($pa['images'])) {
            $aImages = $pa['images'];
            $oImages = $this->itemImagesObj($nUserID, $nItemID);
            $this->uploadImages($oImages, $aImages, $nItemID);

            /*if(Count($this->errTxtAr)>0) {
                $oImages->deleteAllImages(false);
            }*/
        }
        /*if (Count($this->errTxtAr) > 0) {
            $this->DB->delete(TABLE_BBS_ITEMS, array('id' => $nItemID));
            return;
        }*/

        $registerPhone = Users::registerPhone();
        # обновляем счетчик объявлений "на модерации"
        if (isset($aData['moderated']) && empty($aData['moderated'])) {
            config::saveCount('bbs_items_moderating', 1, true);
        }

        $aResult['add_id'] = $nItemID;
        $aResult['item_status'] = $aData['status'];

        # SEO: Добавление объявления
        $this->urlCorrection(BBS::url('item.add'));
        bff::module('SEO')->canonicalUrl(BBS::url('item.add', array(), true));

        if (!empty($pa['ps'])) {
            $aData[] = $pa['ps'];
            $nBillID = $this->itemSvc($aData);
            $aResult['bill_id'] = $nBillID;
        }

        $aData = BBS::model()->itemData($nItemID, array('link'));
        $aResult['item_link'] = str_replace('//{sitehost}/', $this->SET['domain'], $aData['link']);

        BBS::model()->limitsPayedUserUnpublicate($nUserID);

        BBS::model()->itemsIndexesUpdate(array($nItemID));

        $this->rez['RESULT'] = 1;
        return $aResult;
    }

    /**
     * Обновляем объявление
     */
    function modelItemUpdate($pa)
    {
        $this->rez['REZULT'] = 0;
        $nUserID = $pa['user_id'];
        $users = Users::i();

        if (empty($pa['id'])) {
            $this->errAddMsg('ID_NOT_FOUND');
            return;
        } else {
            $nItemID = $pa['id'];
            $aItemData = BBS::model()->itemData($pa['id'], array(
                    'user_id',
                    'status',
                    'title',
                    'descr',
                    'cat_id',
                    'moderated'
                )
            );
            if (empty($aItemData['user_id'])) {
                $this->errAddMsg('ITEM_NOT_FOUND');
                return;
            }
            if ($aItemData['user_id'] != $nUserID) {
                $this->errAddMsg('AUTHOR_NOT_VALID');
                return;
            }
        }

        $aUserData = $this->modelUserGetInfo($nUserID);
        if (empty($aUserData)) {
            $this->errAddMsg('AUTHOR_NOT_FOUND');
            return;
        } else {

        }
        // is user active?
        $mBanned = $users->checkBan(true, false, $nUserID);
        if ($mBanned) {
            $this->errAddMsg('AUTHOR_NOT_ACTIVE:' . $mBanned);
            return;
        }
        if (!empty($aUserData['blocked'])) {
            $this->errAddMsg('AUTHOR_NOT_ACTIVE: ' . $aUserData['blocked_reason']);
            return;
        }
        //antispam: 20s new add for one IP
        if (Site::i()->preventSpam('bbs-add', 20, false)) {
            $this->errAddMsg('ADD_PERIOD_20S');
            return;
        }

        $aData = array();
        $res = $this->validateItemData($pa, $aData, $pa['id']);
        if (!$res) {
            return;
        }

        unset($aData['files']);
        $nUpdateResult = BBS::model()->itemSave($pa['id'], $aData);

        if ($nUpdateResult == 0) {
            $this->errAddMsg('DATA_SAVE_ERROR');
            return;
        }

        if (!empty($pa['images'])) {
            $aImages = $pa['images'];
            $oImages = $this->itemImagesObj($nUserID, $nItemID);
            $this->uploadImages($oImages, $aImages, $nItemID);

            /*if (count($this->errTxtAr) > 0) {
                $oImages->deleteAllImages(false);
            }*/
        } else {
            $oImages = $this->itemImagesObj($nUserID, $nItemID);
            $oImages->deleteAllImages(true);
            $this->resetImgCounter($nItemID);
        }

        # обновляем счетчик объявлений "на модерации"
        if (isset($aData['moderated']) && empty($aData['moderated'])) {
            config::saveCount('bbs_items_moderating', 1, true);
        }

        $aResult['updated'] = $nUpdateResult;
        BBS::model()->itemsIndexesUpdate(array($nItemID));
        $this->rez['RESULT'] = 1;
        return $aResult;
    }

    private function resetImgCounter($nItemID)
    {
        return $this->DB->update(TABLE_BBS_ITEMS, array("imgcnt" => 0), array("id" => $nItemID));
    }

    private function updateTitleImg($nItemID)
    {
        $aImgData = $this->DB->one_array("SELECT filename, dir FROM " . TABLE_BBS_ITEMS_IMAGES . " WHERE item_id = :id AND num = 1", array("id" => $nItemID));
        if (!empty($aImgData)) {
            $sImgNameM = '//' . config::sys('site.host') . '/files/images/items/' . $aImgData['dir'] . '/' . $nItemID . 's' . $aImgData['filename'];
            $sImgNameS = '//' . config::sys('site.host') . '/files/images/items/' . $aImgData['dir'] . '/' . $nItemID . 's' . $aImgData['filename'];
            return $this->DB->update(TABLE_BBS_ITEMS, array("img_s" => $sImgNameS, "img_m" => $sImgNameM), array("id" => $nItemID));
        }
        return false;
    }

    function modelItemChangeStatus($pa)
    {
        $this->rez['REZULT'] = 0;
        $nUserID = $pa['user_id'];
        $bbs = BBS::model();

        if (empty($pa['id'])) {
            $this->errAddMsg('ITEM_ID_NOT_FOUND');
            return;
        } else {
            $nItemID = $pa['id'];
            $aItemData = $bbs->itemData($pa['id'], array(
                    'user_id',
                    'shop_id',
                    'cat_id',
                    'status',
                    'deleted',
                    'publicated_to',
                    'publicated_order'
                )
            );
            if (empty($aItemData['user_id'])) {
                $this->errAddMsg('ITEM_NOT_FOUND');
                return;
            }
            if ($aItemData['user_id'] != $nUserID) {
                $this->errAddMsg('AUTHOR_NOT_VALID');
                return;
            }
        }

        switch ($pa['status']) {
            case 'unpublicate': { # снятие с публикации
                if ($aItemData['status'] != BBS::STATUS_PUBLICATED) {
                    $this->errAddMsg('ITEM_NOT_PUBLICATED');
                    return;
                }
                $res = $bbs->itemSave($nItemID, array(
                        'status' => BBS::STATUS_PUBLICATED_OUT,
                        'status_prev' => $aItemData['status'],
                        'publicated_to' => $this->DB->now(),
                    )
                );
                if (empty($res)) {
                    $this->errAddMsg('ITEM_PUBLICATE_OUT_ERROR');
                    return;
                } else {
                    $aResult = [
                        'item_id' => $nItemID,
                        'new_status_id' => BBS::STATUS_PUBLICATED_OUT,
                        'new_status' => 'STATUS_PUBLICATED_OUT'];
                }
            }
                break;
            case 'publicate': { # публикация
                if ($aItemData['status'] != BBS::STATUS_PUBLICATED_OUT) {
                    $this->errAddMsg('ITEM_NOT_PUBLICATED_OUT');
                    return;
                }

                if ($aItemData['shop_id'] && bff::shopsEnabled() && Shops::abonementEnabled()) {
                    # проверим превышение лимита лимитирования по абонементу
                    if (Shops::i()->abonementLimitExceed($aItemData['shop_id'])) {
                        $this->errAddMsg('SHOP_LIMIT_EXCEED');
                        return;
                    }
                } else if (BBS::limitsPayedEnabled()) {
                    # проверим превышение лимита
                    $limit = $bbs->limitsPayedCategoriesForUser(array(
                        'user_id' => $aItemData['user_id'],
                        'shop_id' => $aItemData['shop_id'],
                        'cat_id' => $aItemData['cat_id'],
                    ));
                    if (!empty($limit)) {
                        $limit = reset($limit);
                        if ($limit['cnt'] >= $limit['limit']) {
                            $this->errAddMsg('CATEGORY_LIMIT_EXCEED');
                            return;
                        }
                    }
                }

                $aUpdate = array(
                    'status' => BBS::STATUS_PUBLICATED,
                    'status_prev' => $aItemData['status'],
                    'publicated' => $this->DB->now(),
                    'publicated_to' => $this->getItemPublicationPeriod(), # от текущей даты
                );
                /**
                 * Обновляем порядок публикации (поднимаем наверх)
                 * только в случае если разница между датой publicated_order и текущей более 7 дней
                 * т.е. тем самым закрываем возможность бесплатного поднятия за счет
                 * процедуры снятия с публикации => возобновления публикации (продления)
                 */
                if ((time() - strtotime($aItemData['publicated_order'])) >= (86400 * 7)) {
                    $aUpdate['publicated_order'] = $this->DB->now();
                }
                $res = $bbs->itemSave($nItemID, $aUpdate);
                if (empty($res)) {
                    $this->errAddMsg('ITEM_PUBLICATE_ERROR');
                    return;
                } else {
                    $aResult = [
                        'item_id' => $nItemID,
                        'new_status_id' => BBS::STATUS_PUBLICATED,
                        'new_status' => 'STATUS_PUBLICATED'];
                }
            }
                break;
            case 'refresh': { # продление публикации
                if ($aItemData['status'] != BBS::STATUS_PUBLICATED) {
                    $this->errAddMsg('ITEM_NOT_PUBLICATED');
                    return;
                }

                # от даты завершения публикации
                $res = $bbs->itemSave($nItemID, array(
                        'publicated_to' => $this->getItemRefreshPeriod($aItemData['publicated_to']),
                    )
                );
                if (empty($res)) {
                    $this->errAddMsg('ITEM_REFRESH_ERROR');
                    return;
                } else {
                    $aResult = [
                        'item_id' => $nItemID,
                        'new_status' => 'ITEM_REFRESH_SUCCESS'];
                }
            }
                break;
            case 'delete': # удаление
            {
                if ($aItemData['status'] == BBS::STATUS_PUBLICATED) {
                    $this->errAddMsg('ITEM_MUST_BE_NOT_PUBLICATED');
                    return;
                }
                if ($aItemData['deleted']) {
                    $this->errAddMsg('ITEM_ALREADY_DELETED');
                    return;
                }
                $res = $bbs->itemSave($nItemID, array(
                        # помечаем как удаленное
                        'deleted' => 1,
                        # снимаем с публикации
                        'status' => BBS::STATUS_PUBLICATED_OUT,
                        'status_prev' => $aItemData['status'],
                        'publicated_to' => $this->DB->now(),
                    )
                );
                if (empty($res)) {
                    $this->errAddMsg('ITEM_DELETE_ERROR');
                    return;
                } else {
                    $aResult = [
                        'item_id' => $nItemID,
                        'new_status' => 'ITEM_DELETE_SUCCESS'];
                }
            }
                break;
        }

        $this->rez['RESULT'] = 1;
        return $aResult;
    }

    /**
     * Получаем срок продления объявления в днях
     * @param mixed $mFrom дата, от которой выполняется подсчет срока публикации
     * @param string $mFormat тип требуемого результата, строка = формат даты, false - unixtime
     * @return int
     */
    public function getItemRefreshPeriod($mFrom = false, $mFormat = 'Y-m-d H:i:s')
    {
        $nDays = config::get('bbs_item_refresh_period', 0, TYPE_UINT);
        if ($nDays <= 0) {
            $nDays = 7;
        }

        if (empty($mFrom) || is_bool($mFrom)) {
            $mFrom = $this->DB->now();
        }
        if (is_string($mFrom)) {
            $mFrom = strtotime($mFrom);
            if ($mFrom === false) {
                $mFrom = strtotime($this->DB->now());
            }
        }
        $nPeriod = strtotime('+' . $nDays . ' days', $mFrom);
        if (!empty($mFormat)) {
            return date($mFormat, $nPeriod);
        } else {
            return $nPeriod;
        }
    }

    function catsMainList($langKey = 'ru')
    {
        if (!isset($langKey) || !in_array($langKey, $this->languages)) $langKey = $this->languageDefault;
        return $this->DB->select('SELECT C.id, C.pid, C.icon_s as i, CL.title as t, C.keyword as k, C.items,
        	(C.numright-C.numleft) > 1 as subs, C.numlevel as lvl FROM ' . TABLE_BBS_CATEGORIES . ' C,
            ' . TABLE_BBS_CATEGORIES_LANG . ' CL WHERE C.enabled = 1 AND C.pid != 0 AND C.numlevel = 1
            AND ' . $this->DB->langAnd(false, 'C', 'CL', $langKey) . 'ORDER BY C.numleft ASC', NULL, 60
        );
    }

    function getChildSort($langKey = 'ru', $aFields = array())
    {

        if (defined("TABLE_BBS_ITEMS_COUNTERS") && $this->DB->isTable(TABLE_BBS_ITEMS_COUNTERS)) {
            $region = Geo::i()->defaultRegion();
            $deliveryCountry = 1;

            $filter['enabled'] = 1;
            $filter[] = $this->DB->langAnd(false, 'C', 'CL', $langKey);
            $filter = BBS::model()->prepareFilter($filter, '', array(':region' => $region));

            $data = $this->DB->select('SELECT C.id, C.pid, C.icon_s as i, CL.title as t, C.keyword as k, CL.title, N.items,
            		C.numleft, C.numright, C.numlevel as lvl, (C.numright-C.numleft) > 1 as subs, C.photos as photos_max_count,
                    C.addr, C.price, C.price_sett
            	FROM ' . TABLE_BBS_CATEGORIES . ' C LEFT JOIN ' . TABLE_BBS_ITEMS_COUNTERS . ' N ON C.id = N.cat_id
            	AND N.delivery = 0 AND N.items != 0 AND N.region_id = :region, ' . TABLE_BBS_CATEGORIES_LANG . ' CL ' . $filter['where'] . '
            	ORDER BY C.numleft', $filter['bind']);

            $this->openPriceSettings($data, $langKey);

            # добавим счетчики объявлений с доставкой по всей стране
            if (!empty($data) && $deliveryCountry) {
                $cats = array();
                for ($c = 0; $c < count($data); $c++) {
                    $cats[] = $data[$c]['id'];
                    //Add path img
                    if (!empty($data[$c]['i']))
                        $data[$c]['i'] = 'http://' . $_SERVER['SERVER_NAME'] . '/files/images/cats/' . $data[$c]['id'] . 'o' . $data[$c]['i'];
                }
                $counters = BBS::model()->itemsCountByFilter(array('cat_id' => $cats, 'region_id' => $deliveryCountry, 'delivery' => 1), array('cat_id', 'items'), false);
                if (!empty($counters)) {
                    $counters = func::array_transparent($counters, 'cat_id', true);
                    foreach ($data as &$v) {
                        if (!isset($counters[$v['id']])) continue;
                        $v['items'] += $counters[$v['id']]['items'];
                    }
                    unset($v);
                }
            }

            return array_values($data);
        }

        $cats = $this->DB->select_key('SELECT C.id, C.pid, C.icon_s as i, CL.title as t, C.keyword as k, C.items,
        		C.numleft, C.numright, C.numlevel as lvl, (C.numright-C.numleft) > 1 as subs, C.photos as photos_max_count,
                C.addr, C.price, C.price_sett
        	FROM ' . TABLE_BBS_CATEGORIES . ' C LEFT JOIN ' . TABLE_BBS_CATEGORIES_LANG . ' CL USING (id)
        	WHERE CL.lang = :lang  ORDER BY C.numleft ASC', NULL, array(':lang' => $langKey)
        );
        $this->openPriceSettings($cats, $langKey);
        return $cats;
    }

    function openPriceSettings(&$data, $lang)
    {
        foreach ($data as &$cat) {
            if ($cat['price'] == 0) {
                $cat['price_sett'] = new \stdClass();
                continue;
            }
            $sett = unserialize($cat['price_sett']);
            $cat['price_sett'] = array(
                'price_title' => $sett['title'][$lang],
                'curr' => $sett['curr'],
                'mod' => $sett['ex'] & 1 ? 1 : 0, # PRICE_EX_MOD
                'exchange' => $sett['ex'] & 2 ? 1 : 0, # PRICE_EX_EXCHANGE
                'free' => $sett['ex'] & 4 ? 1 : 0, # PRICE_EX_FREE
                'agreed' => $sett['ex'] & 8 ? 1 : 0, # PRICE_EX_AGREED
                'mod_title' => $sett['mod_title'][$lang]
            );

        }
    }

    function catChildList($categoryID, $langKey = 'ru', $aFields = array())
    {
        if (!in_array($langKey, $this->languages)) $langKey = $this->languageDefault;
        $data = BBS::model()->catData($categoryID);
        $numleft = $data['numleft'];
        $numright = $data['numright'];

        if ($numright - $numleft <= 1)
            return array();

        $level = $data['numlevel'] + 1;

        return $this->DB->select_key('SELECT C.id, C.pid, C.icon_s as i, CL.title as t, C.keyword as k, C.items,
        		(C.numright-C.numleft)>1 as subs, C.numlevel as lvl FROM ' . TABLE_BBS_CATEGORIES . ' C
                LEFT JOIN ' . TABLE_BBS_CATEGORIES_LANG . ' CL USING (id)
                WHERE C.numleft > :left AND C.numright < :right AND CL.lang = :lang
                AND C.numlevel = :level ORDER BY C.numleft ASC', 'id',
            array(':left' => $numleft, ':right' => $numright, ':lang' => $langKey, ':level' => $level)
        );
    }

    function getDynProperties(&$cat, $lang = 'ru')
    {
        bff::locale()->setCurrentLanguage($lang, true);
        $dynps = BBS::i()->dp()->getByOwner($cat['id']);
        if (empty($dynps))
            return;
        foreach ($dynps as $k => $v) {
            $list = array();

            if (isset($v['multi']) && !empty($v['multi'])) {
                foreach ($v['multi'] as $mk => $mv) {
                    $list[] = array('n' => $mv['name'], 'v' => $mv['value']);
                }
            }

            $cat['props'][$v['id']] = array(
                'cat_id' => $v['cat_id'],
                'id' => $v['id'],
                't' => $v['title'],
                'field' => $v['data_field'],
                'd' => $v['description'],
                'type' => $v['type'],
                'def' => $v['default_value'],
                'req' => $v['req'],
                'sea' => $v['is_search'],
                'val' => (isset($v['value']) ? $v['value'] : ''),
                'list' => $list,
                'num_first' => (int)$v['num_first'],
            );

            $extra = array();
            if (isset($v['search_range_user']))
                $extra['search_range_user'] = $v['search_range_user'];
            if (isset($v['search_ranges']))
                $extra['search_ranges'] = $v['search_ranges'];
            if (isset($v['group_one_row']))
                $extra['group_one_row'] = $v['group_one_row'];
            if (isset($v['type'])) {
                if ($v['type'] == 8)
                    $extra['group_one_row'] = 1;
                if ($v['type'] == 9)
                    $extra['group_one_row'] = 0;
            }
            if (isset($v['start']))
                $extra['start'] = $v['start'];
            if (isset($v['step']))
                $extra['step'] = $v['step'];
            if (isset($v['end']))
                $extra['end'] = $v['end'];
            if (count($extra))
                $cat['props'][$v['id']]['extra'] = $extra;
        }
    }

    function getAllCatDynProperties($pa)
    {
        $nCatId = $pa['id'];
        $aParentsID = BBS::model()->treeCategories->getNodeParentsID($nCatId, '', TRUE, array('id'));
        $inf = array();
        for ($i = 0; $i < count($aParentsID); $i++) {
            $props['id'] = $aParentsID[$i];
            $this->getDynProperties($props, $pa['lang']);
            if (isset($props['props'])) {
                $inf = array_merge($inf, $props['props']);
                unset($props['props']);
            }
        }
        $tmp1 = array();
        $tmp2 = array();
        foreach ($inf as $key => $value) {
            if ((int)$value['num_first']) {
                $tmp1[] = $value;
            } else {
                $tmp2[] = $value;
            }
        }
        $inf = array_merge($tmp1, $tmp2);
        return $inf;
    }

    function buildChildsTree($aData, $nRow = 0)
    {
        $aTree = array();
        $nLeft = $aData[$nRow]['numleft'];
        $nRight = $aData[$nRow]['numright'];
        $nRow++;
        while ($nRow < count($aData) && ($nCurRight = $aData[$nRow]['numright']) < $nRight) {
            $nCurLeft = $aData[$nRow]['numleft'];
            $aTree[] = $aData[$nRow];
            $nRow++;
        }

        return array($aTree, $nRow);
    }

    function modelTreeCats($pa)
    {
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        $aData = $this->getChildSort($pa['lang']);
        list($aTree) = $this->buildChildsTree($aData);
        return $aTree;
    }

    /**
     * Получаем список доступных регионов
     * @return array
     */
    function modelRegionList($pa)
    {

        $rez = array('rows' => array());
        $dat = &$rez['rows'];

        $nDefault = Geo::defaultCountry();
        //$nDefault = 5050;

        $qa = array('R.enabled = 1');
        $qf = array('R.enabled = 1');
        $aBinds = array();

        switch ($this->modelGeoCovering()) {
            case 1: # одна страна
                //$qa[] = '(R.country = '.$this->modelGeoCoveringLvl1().' OR R.id = '.$this->modelGeoCoveringLvl1().')';
                $qa[] = '(R.country = ' . $nDefault . ' OR R.id = ' . $nDefault . ')';
                $qf[] = '(R.country = ' . $nDefault . ' OR R.id = ' . $nDefault . ')';
                break;
            case 2: # одна область
                $qa[] = '((R.country = ' . $this->modelGeoCoveringLvl1() . ' AND R.pid = ' . $this->modelGeoCoveringLvl2() . ') OR R.id IN (' . $this->modelGeoCoveringLvl1() . ',' . $this->modelGeoCoveringLvl2() . '))';
                break;
            case 3: # список городов
                $aRegionsLvl2 = $this->DB->select_one_column('SELECT DISTINCT pid FROM ' . TABLE_REGIONS .
                    ' WHERE id IN (' . $this->modelGeoCoveringLvl3() . ')');
                $aRegionsLvl2[] = $this->modelGeoCoveringLvl2();
                $tmp = array($this->modelGeoCoveringLvl1(), implode(',', array_unique($aRegionsLvl2)), $this->modelGeoCoveringLvl3());
                $qa[] = 'R.id IN (' . implode(',', $tmp) . ')';
                break;
            case 4: # один город
                $tmp = array($this->modelGeoCoveringLvl1(), $this->modelGeoCoveringLvl2(), $this->modelGeoCoveringLvl3());
                $qa[] = 'R.id IN (' . implode(',', $tmp) . ')';
                break;
            case 5: # несколько стран
                $qa[] = '(R.country IN (' . $this->modelGeoCoveringLvl1() . ') OR R.id IN (' . $this->modelGeoCoveringLvl1() . '))';
                break;
            case 0: # некорректная/неполученная настройка гео покрытия
            default:
                break;
        }

        if (isset($pa['pid'])) {
            $qa[] = 'R.pid = :pid';
            $aBinds['pid'] = (int)$pa['pid'];
        }
        if (isset($pa['c'])) {
            $qa[] = 'R.country = :country';
            $aBinds['country'] = (int)$pa['c'];
        }
        if (isset($pa['lvl'])) {
            if (!is_array($pa['lvl']))
                $pa['lvl'] = array($pa['lvl']);
            $se = array();
            foreach ($pa['lvl'] as $v)
                $se[(int)$v] = (int)$v;
            if (count($se))
                $qa[] = 'R.numlevel IN(' . implode(',', $se) . ')';
            $qf[] = 'R.numlevel IN(3)';
        }

        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;

        if (isset($pa['fav']) && $pa['fav'] > 0 && count($pa['lvl']) == 1 && $pa['lvl'][0] == 2) {//Favorite cities
            $dat_f_cities = $this->DB->select('SELECT R.id, R.pid, R.country as c, R.title_' . $pa['lang'] . ' as t, R.ycoords as yc,
			IF(R.country = 0 AND R.id = ' . $nDefault . ', 1, 0) as def, (R.main > 0) as fav_city FROM ' . TABLE_REGIONS . ' R
			WHERE R.main > 0 AND ' . implode(' AND ', $qf) . ' ORDER BY R.id', $aBinds);

        } else {
            $dat_f_cities = array();
        }

        $dat_r_list = $this->DB->select('SELECT R.id, R.pid, R.country as c, R.title_' . $pa['lang'] . ' as t, R.ycoords as yc,
			IF(R.country = 0 AND R.id = ' . $nDefault . ', 1, 0) as def FROM ' . TABLE_REGIONS . ' R
			WHERE ' . implode(' AND ', $qa) . ' ORDER BY R.id', $aBinds);

        $dat = array_merge($dat_f_cities, $dat_r_list);

        return $rez;
    }

    /**
     * Получаем настройку географического покрытия
     * @return int
     */
    function modelGeoCovering()
    {
        return config::get('geo_covering', 0, TYPE_UINT);
    }

    /**
     * Получаем настройку географического покрытия, lvl 1
     * @return string
     */
    function modelGeoCoveringLvl1()
    {
        return config::get('geo_covering_lvl1', '', TYPE_STR);
    }

    /**
     * Получаем настройку географического покрытия, lvl 2
     * @return string
     */
    function modelGeoCoveringLvl2()
    {
        return config::get('geo_covering_lvl2', '', TYPE_STR);
    }

    /**
     * Получаем настройку географического покрытия, lvl 3
     * @return string
     */
    function modelGeoCoveringLvl3()
    {
        return config::get('geo_covering_lvl3', '', TYPE_STR);
    }

    /**
     * Получаем список доступных валют
     * @return array
     */
    function modelCurrenciesList($pa)
    {
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        return $this->DB->select('SELECT C.id, C.rate, R.title, R.title_short FROM ' . TABLE_CURRENCIES
            . ' C JOIN ' . TABLE_CURRENCIES_LANG . ' R ON R.id = C.id AND R.lang = :lang
			WHERE C.enabled = 1 ORDER BY C.id', ['lang' => $pa['lang']]);
    }

    /**
     * Отправляем push-нотификацию для пользователя на android устройстве
     * @return string
     */
    public function FCMsendNotification($ids = array(), $aData = [])
    {
        if (empty($ids)) return 'Wrong ids';
        $fields = array(
            'registration_ids' => $ids,
            'data' => array(
                'new_mess' => 1,
                'vibrate' => 1,
                'sound' => 1,
                'message_text' => !empty($aData['message_text']) ? $aData['message_text'] : "",// заголовок объявления
                'name_user' => !empty($aData['name_user']) ? $aData['name_user'] : "",//имя юзера
                'avatar' => !empty($aData['avatar']) ? $aData['avatar'] : $this->SET['http_'] . UsersAvatar::url(0, '', UsersAvatar::szNormal),//аватарка юзера
                'user_id' => !empty($aData['user_id']) ? $aData['user_id'] : 0,
                'for_user_id' => !empty($aData['for_user_id']) ? $aData['for_user_id'] : 0,
            )
        );
        $headers = array('Authorization: key=' . $this->androidPushKey, 'Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * Отправляем push-нотификацию для пользователя на ios устройстве
     * @return string
     */
    public function iOSsendNotification($ids = array(), $message = 'New internal message')
    {
        if (empty($ids)) return 'Wrong ids';

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', PATH_BASE . $this->cert);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passphrase);
        // $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195',
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195',
            $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            bff::log("Failed to connect: " . $err . ' ' . $errstr);
            return;
        }
        $body['aps'] = array('alert' => $message, 'sound' => 'default', 'category' => 'NEW_MESSAGE');
        $payload = json_encode($body);
        $msg = chr(0) . pack('n', 32) . pack('H*', $ids[0]) . pack('n', strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg));
        fclose($fp);
        return $result;
    }

    /**
     * Формируем WHERE-условие из массива
     */
    function sql_ArrayToWhere(&$qa, $ar, $type = 'i', $fld, $fg = array())
    {
        if (!is_array($ar)) {
            $ar = array($ar);
        }
        if (sizeof($ar) == 0)
            return $qa[$fld] = $fld . ' = 0';

        $se = array();
        foreach ($ar as $v) {
            $v = (int)$v;
            if (isset($fg['>'])) {
                if ($v > $fg['>'])
                    $se[$v] = $v;
            } else {
                $se[$v] = $v;
            }
        }

        if (sizeof($se) == 1) {
            $qa[$fld] = $fld . '=' . current($se);
        } elseif (sizeof($se) > 0) {
            $qa[$fld] = $fld . ' IN (' . implode(',', $se) . ')';
        }

        return true;
    }

    /**
     * Увеличиваем количество просмотров контактных данных в объявлении
     */
    public function modelItemContactsViewUp($nItemID)
    {
        if (empty($nItemID)) {
            $this->errAddMsg('ITEM_ID_NOT_DEFINED');
            return 0;
        }
        $bbs = BBS::model();
        $aData = $bbs->itemData($nItemID,
            array('user_id')
        );
        if ($aData['user_id'] == $this->sesrow['user_id']) {
            $this->errAddMsg('VIEW_BY_OWNER');
            return 0;
        }
        if (!$bbs->itemViewsIncrement($nItemID, 'contacts')) {
            $this->errAddMsg('INCREMENT_ERROR');
            return 0;
        };
        return 1;
    }

    function modelCountryCode($pa)
    {
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;


        $this->DB->select_iterator('SELECT id, title_' . $pa['lang'] . ', country_code, phone_code FROM ' . TABLE_REGIONS .
            ' WHERE numlevel = 1 AND enabled = 1 ORDER BY num', array(),
            function ($value) use (&$aCountries, $pa) {
                $aCountries[] = array(
                    'title' => $value['title_' . $pa['lang']],
                    'code' => '+' . $value['phone_code'],
                    'country_code' => $value['country_code']
                );
            });
        return $aCountries;
    }

    function modelLoneCatInfo($categoryID, $langKey = 'ru')
    {
        if (!in_array($langKey, $this->languages)) $langKey = $this->languageDefault;
        $data = BBS::model()->catData($categoryID);
        $result = array(
            'id' => $data['id'],
            'pid' => $data['pid'],
            'i' => $data['icon_s'],
            't' => '',
            'k' => $data['keyword'],
            'title' => $data['title'],
            'items' => $data['items'],
            'numleft' => $data['numleft'],
            'numright' => $data['numright'],
            'lvl' => $data['numlevel'],
            'subs' => (($data['numright'] - $data['numleft']) > 1) ? 1 : 0,
            'photos_max_count' => $data['photos'],
            'addr' => $data['addr'],
            'price' => $data['price'],
            'tpl_title_enabled' => $data['tpl_title_enabled'],
        );
        $result['price_sett'] = array(
            'price_title' => $data['price_sett']['title'][$langKey],
            'curr' => $data['price_sett']['curr'],
            'mod' => $data['price_sett']['ex'] & 1 ? 1 : 0, # PRICE_EX_MOD
            'exchange' => $data['price_sett']['ex'] & 2 ? 1 : 0, # PRICE_EX_EXCHANGE
            'free' => $data['price_sett']['ex'] & 4 ? 1 : 0, # PRICE_EX_FREE
            'agreed' => $data['price_sett']['ex'] & 8 ? 1 : 0, # PRICE_EX_AGREED
            'mod_title' => $data['price_sett']['mod_title'][$langKey]
        );
        $result['t'] = $this->DB->one_data('SELECT title FROM ' . TABLE_BBS_CATEGORIES_LANG . ' WHERE id = :id AND lang = :lang',
            array(':lang' => $langKey, ':id' => $categoryID));

        return $result;
    }

    function modelFreeItemUp($nItemID)
    {
        if (empty($nItemID)) {
            $this->errAddMsg('ITEM_ID_NOT_DEFINED');
            return 0;
        }

        $days = BBS::svcUpFreePeriod();
        if (!$days) {
            $this->errAddMsg('FREE_UP_NOT_ALLOWED');
            return 0;
        }

        $upTo = strtotime('-' . $days . ' days');
        $bbs = BBS::model();
        $data = $bbs->itemData($nItemID, array('user_id', 'status', 'svc_up_free'));
        if (empty($data)) {
            $this->errAddMsg('ITEM_NOT_FOUND');
            return 0;
        }
        if ($data['user_id'] != $this->sesrow['user_id']) {
            $this->errAddMsg('ITEM_OWNER_MISSMATCH');
            return 0;
        }
        if ($data['status'] != BBS::STATUS_PUBLICATED) {
            $this->errAddMsg('ITEM_STATUS_MISSMATCH');
            return 0;
        }
        if (strtotime($data['svc_up_free']) > $upTo) {
            $this->errAddMsg('FREE_UP_NOT_READY');
            return 0;
        }

        $now = $this->DB->now();
        $res = $bbs->itemSave($nItemID, array(
            'svc_up_free' => $now,
            'publicated_order' => $now,
            'svc_up_date' => $now,
        ));
        if (empty($res)) {
            $this->errAddMsg('FREE_UP_ERROR');
            return 0;
        }

        return 1;
    }

    function getRegionDataById($aData)
    {
        if (empty($aData['id'])) {
            $this->errAddMsg('REGION_ID_NOT_DEFINED');
            return 0;
        }
        if (!isset($aData['lang']) || !in_array($aData['lang'], $this->languages)) $aData['lang'] = $this->languageDefault;

        return $this->DB->one_array('SELECT R.id, R.pid, R.country as c, R.title_' . $aData['lang'] . ' as t, R.ycoords as yc
			FROM ' . TABLE_REGIONS . ' R WHERE R.id = :id', ['id' => $aData['id']]);
    }

    public function svcList($pa)
    {
        if (empty($pa['lang']) || !in_array($pa['lang'], $this->languages)) {
            $lang = $this->languageDefault;
        } else {
            $lang = $pa['lang'];
        }
        $aData = BBS::model()->svcData();
        if (empty($aData)) {
            return array();
        }
        $svcKeys = array_keys($aData);
        $aData = Svc::model()->svcData($svcKeys);
        $sCurrDisplay = Site::currencyDefault();

        if (!empty($pa['item_id'])) {
            $aItemData = BBS::model()->itemData($pa['item_id'], ['cat_id', 'city_id']);
            $aSvcPrices = BBS::model()->svcPricesEx($svcKeys, $aItemData['cat_id'], $aItemData['city_id']);
            foreach ($aData as $k => $svc) {
                if (!empty($aSvcPrices[$svc['id']])) $aData[$k]['price'] = $aSvcPrices[$svc['id']];
            }
        }

        $oIcon = BBS::svcIcon();
        foreach ($aData as $k => $v) {
            if (is_null($v['add_form'])) {
                unset($aData[$k]);
            } else {
                $aData[$k]["icon_b"] = $this->SET['http_'] . $oIcon->url($v['id'], $v['icon_b'], BBSSvcIcon::BIG);
                $aData[$k]["icon_s"] = $this->SET['http_'] . $oIcon->url($v['id'], $v['icon_s'], BBSSvcIcon::SMALL);
                if (is_array($v['title_view']) && isset($v['title_view'][$lang]))
                    $aData[$k]['title_view'] = $v['title_view'][$lang];
                if (is_array($v['description']) && isset($v['description'][$lang]))
                    $aData[$k]['description'] = $v['description'][$lang];
                if (is_array($v['description_full']) && isset($v['description_full'][$lang]))
                    $aData[$k]['description_full'] = $v['description_full'][$lang];
                $aData[$k]['price_display'] = $v['price'] . ' ' . $sCurrDisplay;
            }
        }
        if (!BBS::PRESS_ON && isset($aData['press'])) {
            unset($aData['press']);
        }

        return array_values($aData);
    }

    public function modelUserBalance($pa)
    {
        if (empty($pa['lang']) || !in_array($pa['lang'], $this->languages)) {
            $lang = $this->languageDefault;
        } else {
            $lang = $pa['lang'];
        }

        $aUser = Users::model()->userData($this->sesrow['user_id'], ['balance']);
        $sCurrDisplay = Site::currencyDefault();
        $aUser['balance_display'] = $aUser['balance'] . ' ' . $sCurrDisplay;
        if (!empty($pa['get_bills'])) {
            $nOnPage = empty($pa['onpage']) ? 10 : $pa['onpage'];
            $nPage = empty($pa['page']) ? 0 : (integer)$pa['page'] * $nOnPage;
            $aFilter = array('user_id' => $this->sesrow['user_id'], 'status' => Bills::STATUS_COMPLETED);
            $aData = Bills::model()->billsList($aFilter, false, ' LIMIT ' . $nPage . ',' . $nOnPage, 'created DESC, id DESC');
            foreach ($aData as $v) {
                $aItem['id'] = $v['id'];
                $aItem['svc_id'] = $v['svc_id'];
                $aItem['svc'] = SVC::model()->svcData($v['svc_id']);
                if (is_array($aItem['svc']['title_view']) && isset($aItem['svc']['title_view'][$lang]))
                    $aItem['svc']['title_view'] = $aItem['svc']['title_view'][$lang];
                if (is_array($aItem['svc']['description']) && isset($aItem['svc']['description'][$lang]))
                    $aItem['svc']['description'] = $aItem['svc']['description'][$lang];
                if (is_array($aItem['svc']['description_full']) && isset($aItem['svc']['description_full'][$lang]))
                    $aItem['svc']['description_full'] = $aItem['svc']['description_full'][$lang];
                if (empty($aItem['svc'])) $aItem['svc'] = new stdClass();
                $aItem['item_id'] = $v['item_id'];
                $aItem['item_title'] = BBS::model()->itemData($v['item_id'], array('title'));
                if (empty($aItem['item_title'])) $aItem['item_title'] = new stdClass();
                $aItem['type'] = $v['type'];
                $aItem['amount'] = $v['amount'] . ' ' . $sCurrDisplay;
                $aItem['date'] = $v['payed'];
                $aItem['title'] = $v['description'];
                $aItem['user_balance'] = $v['user_balance'] . ' ' . $sCurrDisplay;
                $aUser['log'][] = $aItem;
            }
        }
        return $aUser;
    }

    public function modelShopsList($pa)
    {
        if (!isset($pa['lang']) || !in_array($pa['lang'], $this->languages)) $pa['lang'] = $this->languageDefault;
        bff::locale()->setCurrentLanguage($pa['lang'], true);

        $aFilter = [];
        if (!empty($pa['filters']['title'])) {
            if (strlen(trim($pa['filters']['title'])) >= 3) {
                $aFilter[':title'] = array('(SL.title LIKE (:title) OR SL.descr LIKE (:title))', ':title' => '%' . trim($pa['filters']['title']) . '%');
            } else {
                $this->errAddMsg('SHOP_TITLE_TOO_SMALL');
                return 0;
            }
        }
        if (!empty($pa['filters']['region'])) {
            $aRegion = Geo::regionData($pa['filters']['region']);
            switch ($aRegion['numlevel']) {
                case Geo::lvlCountry:
                    $aFilter['reg1_country'] = $pa['filters']['region'];
                    break;
                case Geo::lvlRegion:
                    $aFilter['reg2_region'] = $pa['filters']['region'];
                    break;
                case Geo::lvlCity:
                    $aFilter['reg3_city'] = $pa['filters']['region'];
                    break;
            }
        }

        $nOnPage = empty($pa['onpage']) ? 10 : $pa['onpage'];
        $nPage = empty($pa['page']) ? 0 : (integer)$pa['page'] * $nOnPage;
        $sLimit = ' LIMIT ' . $nPage . ',' . $nOnPage;
        $sSizePrefix = (empty($pa['icon']) || !in_array($pa['icon'], array('l', 'm', 's', 'v'))) ? 'l' : $pa['icon'];
        $aFilter['status'] = Shops::STATUS_ACTIVE;
        $nCategoryId = !empty($pa['filters']['cats_id']) ? $pa['filters']['cats_id'] : 0;

        $aData = Shops::model()->shopsList($aFilter, $nCategoryId, false, $sLimit);
        if (empty($aData)) {
            $this->errAddMsg('SHOPS_NOT_FOUND');
            return 0;
        } else {
            foreach ($aData as &$shop) {
                $shop['logo'] = !empty($shop['logo'])
                    ? $this->SET['http_'] . Shops::i()->shopLogo($shop['id'])->url($shop['id'], $shop['logo'], $sSizePrefix)
                    : $this->SET['http_'] . Shops::i()->shopLogo($shop['id'])->urlDefault($sSizePrefix);
                $shop['contacts'] = json_decode($shop['contacts']);
                $shop['contacts'] = empty($shop['contacts']) ? new \stdClass() : $shop['contacts'];
                if (!empty($shop['social'])) $shop['social'] = unserialize($shop['social']);
                if (!empty($shop['phones'])) {
                    $aPhones = array();
                    foreach (unserialize($shop['phones']) as $phoneData) {
                        $aPhones[] = $phoneData['v'];
                    }
                    $shop['phones'] = $aPhones;
                }
                unset($shop['link']);
            }
        }

        return $aData;
    }

    public function modelOpenShop($aData)
    {
        $aUserData = Users::model()->userData($this->sesrow['user_id'], ['shop_id']);
        if ($aUserData['shop_id'] > 0) {
            $this->errAddMsg('USER_ALREADY_HAVE_SHOP');
            return 0;
        }
        $aData['title'] = trim($aData['title'], ' -');
        $sLogoFilename = $aData['logo'];
        unset($aData['logo']);
        if (empty($aData['title'])) {
            $this->errAddMsg('SHOP_TITLE_EMPTY');
            return 0;
        } else {
            if (mb_strlen($aData['title']) < config::sys('shops.form.title.min', 2, TYPE_UINT)) {
                $this->errAddMsg('SHOP_TITLE_TOO_SHORT');
                return 0;
            }
            $aData['title'] = bff::filter('shops.form.title.validate', $aData['title']);
        }
        $title = $aData['title'];

        # чистим описание, дополнительно:
        if (mb_strlen($aData['descr']) < config::sys('shops.form.descr.min', 12, TYPE_UINT)) {
            $this->errAddMsg('SHOP_DESCR_TOO_SHORT');
            return 0;
        }
        $aData['descr'] = bff::filter('shops.form.descr.validate', $aData['descr']);

        # проквочиваем название
        $aData['title_edit'] = $aData['title'];
        $aData['title'] = HTML::escape($aData['title_edit']);

        # URL keyword
        $aData['keyword'] = trim(preg_replace('/[^a-z0-9\-]/', '', mb_strtolower(
                func::translit($title)
            )
        ), '- '
        );

        # категории
        if (!Shops::categoriesEnabled()) {
            unset($aData['cats']);
        } else {
            if (empty($aData['cats'])) {
                $this->errAddMsg('SHOP_CATS_EMPTY');
                return 0;
            }
        }

        Users::i()->cleanUserData($aData, array('phones', 'contacts', 'site'), array(
                'phones_limit' => Shops::phonesLimit(),
            )
        );

        # соц. сети (корректируем ссылки)
        $aSocial = array();
        $aSocialTypes = Shops::socialLinksTypes();
        foreach ($aData['social'] as $v) {
            if (strlen($v['v']) >= 5 && array_key_exists($v['t'], $aSocialTypes)) {
                if (stripos($v['v'], 'http') !== 0) {
                    $v['v'] = 'http://' . $v['v'];
                }
                $v['v'] = str_replace(array('"', '\''), '', $v['v']);
                $aSocial[] = array(
                    't' => $v['t'],
                    'v' => $v['v'],
                );
            }
        }
        $limit = Shops::socialLinksLimit();
        if ($limit > 0 && sizeof($aSocial) > $limit) {
            $aSocial = array_slice($aSocial, 0, $limit);
        }
        $aData['social'] = $aSocial;

        # регион
        if ($aData['region_id']) {
            if (!Geo::isCity($aData['region_id'])) {
                $this->errAddMsg('SHOP_REGION_INCORRECT');
                return 0;
            }
        }
        if (!$aData['region_id']) {
            $this->errAddMsg('SHOP_REGION_EMPTY');
            return 0;
        }
        if (!Geo::coveringType(Geo::COVERING_COUNTRY)) {
            $regionData = Geo::regionData($aData['region_id']);
            if (!$regionData || !Geo::coveringRegionCorrect($regionData)) {
                $this->errAddMsg('SHOP_REGION_INCORRECT');
                return 0;
            }
        }

        # разворачиваем регион: region_id => reg1_country, reg2_region, reg3_city
        $aRegions = Geo::model()->regionParents($aData['region_id']);
        $aData = array_merge($aData, $aRegions['db']);

        # формируем URL магазина
        $sLink = Shops::url('shop.view', array(
            'region' => $aRegions['keys']['region'],
            'city' => $aRegions['keys']['city']
        ), true
        );
        $aData['link'] = $sLink;

        $aData['user_id'] = $this->sesrow['user_id'];
        $aData['moderated'] = 0; # помечаем на модерацию
        if (Shops::premoderation()) {
            $aData['status'] = Shops::STATUS_REQUEST;
        } else {
            $aData['status'] = Shops::STATUS_ACTIVE;
        }

        # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
        if (Site::i()->preventSpam('shops-open', 60)) {
            $this->errAddMsg('SHOP_OPEN_REQUEST_SPAM');
            return 0;
        }

        # создаем магазин
        unset($aData['files']);
        $nShopID = Shops::model()->shopSave(0, $aData);
        if (!$nShopID) {
            $this->errAddMsg('SHOP_OPEN_ERROR');
            return 0;
        } else {
            # связываем пользователя с магазином
            Shops::i()->onUserShopCreated($this->sesrow['user_id'], $nShopID);
            # сохраняем логотип

            if (!empty($sLogoFilename)) {
                //Shops::i()->shopLogo($nShopID)->untemp($sLogoFilename, true);
                $dir_img = $this->SET['domain'] . $this->modelUserSet['TmpImgPath'];
                Shops::i()->shopLogo($nShopID)->uploadURL($dir_img . $sLogoFilename, true, true);
            }
            if (Shops::premoderation()) {
                Shops::i()->updateRequestsCounter(1);
            } else {
                Shops::i()->updateModerationCounter(1);
            }
        }

        return 1;
    }

    public function modelEditShop($aData)
    {
        $nShopID = $aData['id'];
        $aData['title'] = trim($aData['title'], ' -');
        $sLogoFilename = $aData['logo'];
        unset($aData['logo']);
        if (empty($aData['title'])) {
            $this->errAddMsg('SHOP_TITLE_EMPTY');
            return 0;
        } else {
            if (mb_strlen($aData['title']) < config::sys('shops.form.title.min', 2, TYPE_UINT)) {
                $this->errAddMsg('SHOP_TITLE_TOO_SHORT');
                return 0;
            }
            $aData['title'] = bff::filter('shops.form.title.validate', $aData['title']);
        }
        $title = $aData['title'];

        # чистим описание, дополнительно:
        if (mb_strlen($aData['descr']) < config::sys('shops.form.descr.min', 12, TYPE_UINT)) {
            $this->errAddMsg('SHOP_DESCR_TOO_SHORT');
            return 0;
        }
        $aData['descr'] = bff::filter('shops.form.descr.validate', $aData['descr']);

        # проквочиваем название
        $aData['title_edit'] = $aData['title'];
        $aData['title'] = HTML::escape($aData['title_edit']);

        # URL keyword
        $aData['keyword'] = trim(preg_replace('/[^a-z0-9\-]/', '', mb_strtolower(
                func::translit($title)
            )
        ), '- '
        );

        # категории
        if (!Shops::categoriesEnabled()) {
            unset($aData['cats']);
        } else {
            if (empty($aData['cats'])) {
                $this->errAddMsg('SHOP_CATS_EMPTY');
                return 0;
            }
        }

        Users::i()->cleanUserData($aData, array('phones', 'contacts', 'site'), array(
                'phones_limit' => Shops::phonesLimit(),
            )
        );

        # соц. сети (корректируем ссылки)
        $aSocial = array();
        $aSocialTypes = Shops::socialLinksTypes();
        foreach ($aData['social'] as $v) {
            if (strlen($v['v']) >= 5 && array_key_exists($v['t'], $aSocialTypes)) {
                if (stripos($v['v'], 'http') !== 0) {
                    $v['v'] = 'http://' . $v['v'];
                }
                $v['v'] = str_replace(array('"', '\''), '', $v['v']);
                $aSocial[] = array(
                    't' => $v['t'],
                    'v' => $v['v'],
                );
            }
        }
        $limit = Shops::socialLinksLimit();
        if ($limit > 0 && sizeof($aSocial) > $limit) {
            $aSocial = array_slice($aSocial, 0, $limit);
        }
        $aData['social'] = $aSocial;

        # регион
        if ($aData['region_id']) {
            if (!Geo::isCity($aData['region_id'])) {
                $this->errAddMsg('SHOP_REGION_INCORRECT');
                return 0;
            }
        }
        if (!$aData['region_id']) {
            $this->errAddMsg('SHOP_REGION_EMPTY');
            return 0;
        }
        if (!Geo::coveringType(Geo::COVERING_COUNTRY)) {
            $regionData = Geo::regionData($aData['region_id']);
            if (!$regionData || !Geo::coveringRegionCorrect($regionData)) {
                $this->errAddMsg('SHOP_REGION_INCORRECT');
                return 0;
            }
        }

        # разворачиваем регион: region_id => reg1_country, reg2_region, reg3_city
        $aRegions = Geo::model()->regionParents($aData['region_id']);
        $aData = array_merge($aData, $aRegions['db']);

        # формируем URL магазина
        $sLink = Shops::url('shop.view', array(
            'region' => $aRegions['keys']['region'],
            'city' => $aRegions['keys']['city']
        ), true
        );
        $aData['link'] = $sLink;

        # не чаще чем раз в {X} секунд с одного IP (для одного пользователя)
        if (Site::i()->preventSpam('shops-open', 60)) {
            $this->errAddMsg('SHOP_EDIT_REQUEST_SPAM');
            return 0;
        }
        unset($aData['files']);

        $aDataPrev = Shops::model()->shopData($nShopID, array('title', 'descr', 'status'));
        foreach (array('title', 'descr') as $k) {
            if ($aDataPrev[$k] != $aData[$k]) {
                $aData['moderated'] = 2;
            }
        }
        if ($aDataPrev['status'] == Shops::STATUS_BLOCKED) {
            $aData['moderated'] = 2;
        }

        # сохраняем настройки магазина
        Shops::model()->shopSave($nShopID, $aData);
        if (!empty($sLogoFilename)) {
            //Shops::i()->shopLogo($nShopID)->untemp($sLogoFilename, true);
            $dir_img = $this->SET['domain'] . $this->modelUserSet['TmpImgPath'];
            Shops::i()->shopLogo($nShopID)->uploadURL($dir_img . $sLogoFilename, true, true);
        }

        return 1;
    }

    public function shopCategoriesList($aData)
    {
        if (!bff::moduleExists('shops')) {
            $this->errAddMsg('SHOPS_NOT_EXIST');
            return 0;
        }
        if (!Shops::categoriesEnabled()) {
            $this->errAddMsg('SHOPS_CATEGORIES_DISABLED');
            return 0;
        }

        /*$aData['pid'] = $aData['pid'] > 0 ? $aData['pid'] : 0;

        $aList = Shops::model()->catsList('form', bff::DEVICE_DESKTOP, $aData['pid'], ShopsCategoryIcon::BIG);
        $oShopCat = Shops::categoryIcon(0);
        if (!empty($aList)) {
            foreach ($aList as &$cat) {
                $cat['i'] = $this->SET['http_'] . $oShopCat->url($cat['id'], $cat['i'], ShopsCategoryIcon::BIG);
            }
        }

        return $aList;*/

        $aFilter = array(
            'numlevel > 0',
            'enabled' => 1
        );
        if (isset($aData['pid'])) {
            $aFilter['pid'] = $aData['pid'];
        }

        $aList = Shops::model()->catsListing($aFilter);
        $oShopCat = Shops::categoryIcon(0);
        if (!empty($aList)) {
            foreach ($aList as &$cat) {
                $aCatData = Shops::model()->catData($cat['id'], ['icon_b']);
                $sImgLink = $this->SET['http_'] . $oShopCat->url($cat['id'], $aCatData['icon_b'], ShopsCategoryIcon::BIG);
                $cat['img'] = $cat["numlevel"] == 1 && !empty($aCatData['icon_b']) ? $sImgLink : '';
            }
        }

        return $aList;
    }

    public function shopSvcList($pa)
    {
        if (empty($pa['lang']) || !in_array($pa['lang'], $this->languages)) {
            $lang = $this->languageDefault;
        } else {
            $lang = $pa['lang'];
        }

        $aData = Shops::model()->svcData();
        if (empty($aData)) {
            return array();
        }

        $svcKeys = array_keys($aData);
        $aData = Svc::model()->svcData($svcKeys);
        $sCurrDisplay = Site::currencyDefault();

        if (!empty($pa['shop_id'])) {
            $aShopData = Shops::model()->shopData($pa['shop_id'], ['reg3_city']);
            $aSvcPrices = Shops::model()->svcPricesEx($svcKeys, $aShopData['reg3_city']);
            foreach ($aData as $k => $svc) {
                if (!empty($aSvcPrices[$svc['id']])) $aData[$k]['price'] = $aSvcPrices[$svc['id']];
            }
        }

        $oIcon = Shops::svcIcon();
        foreach ($aData as $k => $v) {
            if ($k === 'abonement') {
                unset($aData[$k]);
                continue;
            }
            $aData[$k]["icon_b"] = $this->SET['http_'] . $oIcon->url($v['id'], $v['icon_b'], ShopsSvcIcon::BIG);
            $aData[$k]["icon_s"] = $this->SET['http_'] . $oIcon->url($v['id'], $v['icon_s'], ShopsSvcIcon::SMALL);
            if (is_array($v['title_view']) && isset($v['title_view'][$lang]))
                $aData[$k]['title_view'] = $v['title_view'][$lang];
            if (is_array($v['description']) && isset($v['description'][$lang]))
                $aData[$k]['description'] = $v['description'][$lang];
            if (is_array($v['description_full']) && isset($v['description_full'][$lang]))
                $aData[$k]['description_full'] = $v['description_full'][$lang];
            $aData[$k]['price_display'] = $v['price'] . ' ' . $sCurrDisplay;
        }

        return array_values($aData);
    }

    public function promoteShop($aShop)
    {
        if (!bff::servicesEnabled()) {
            $this->errAddMsg('SVC_NOT_ENABLED');
            return 0;
        }

        $nUserID = $this->sesrow['user_id'];
        $nShopID = $aShop['id'];
        $aShopData = Shops::model()->shopData($nShopID, array('user_id', 'reg3_city', 'status'));
        if (!$nShopID || empty($aShop) || (int)$aShopData['status'] !== Shops::STATUS_ACTIVE) {
            $this->errAddMsg('SHOP_NOT_OR_BLOCKED');
            return 0;
        }
        if ($aShopData['user_id'] != $this->sesrow['user_id']) {
            $this->errAddMsg('SHOP_OWNER_MISSMATCH');
            return 0;
        }

        $aSvc = Shops::model()->svcData();
        $nSvcID = $aShop['svc_id'];
        if (!$nSvcID || !isset($aSvc[$nSvcID])) {
            $this->errAddMsg('SVC_NOT_OR_FAILED');
            return 0;
        }

        $ps = $aShop['ps'];
        if ($ps != 'balance') {
            $this->errAddMsg('PAYMENT_METHOD_NOT_AVAILABLE');
            return 0;
        }

        $aSvcPrices = Shops::model()->svcPricesEx(array_keys($aSvc), $aShopData['reg3_city']);
        foreach ($aSvcPrices as $k => $v) {
            if (!empty($v)) $aSvc[$k]['price'] = $v;
        }

        $aSvcSettings = array();
        $nSvcPrice = $aSvc[$nSvcID]['price'];

        $nUserBalance = Users::model()->userBalance($nUserID, true);

        if ($nUserBalance < $nSvcPrice) {
            $this->errAddMsg('LOW_BALANCE');
            return 0;
        }
        # конвертируем сумму в валюту для оплаты по курсу
        $pay = Bills::getPayAmount($nSvcPrice, $ps);
        $activated = Svc::i()->activate('shops', $nSvcID, false, $nShopID, $nUserID, $nSvcPrice, $pay['amount'], $aSvcSettings);
        if ($activated)
            return 1;

        $this->errAddMsg('SVC_NOT_PAIED: ' . implode(', ', bff::errors()->get(true)));
        return 0;
    }

    public function shopSvcAbonementList($pa)
    {
        $currency = Site::currencyDefault();

        if (empty($pa['lang']) || !in_array($pa['lang'], $this->languages)) {
            $lang = $this->languageDefault;
        } else {
            $lang = $pa['lang'];
        }
        $shopId = isset($pa['shop_id']) ? $pa['shop_id'] : 0;


        $shopData = Shops::model()->shopData(
            $shopId,
            [
                'svc_abonement_id',
                'svc_abonement_expire',
                'svc_abonement_one_time',
            ]
        );

        $oneTimeAbonementsWhereUsed = unserialize($shopData['svc_abonement_one_time']);

        bff::locale()->setCurrentLanguage($lang, true);

        $abonements = Shops::model()->abonementsList([], false, '', 'num');
        if (!empty($abonements)) {
            foreach ($abonements as $key => &$abonement) {
                if (!$abonement['enabled']) {
                    unset($abonements[$key]);
                    continue;
                }

                if ($abonement['one_time'] && in_array($abonement['id'], $oneTimeAbonementsWhereUsed)) {
                    unset($abonements[$key]);
                    continue;
                }

                $abonement = $this->removeLangFields($abonement, ['title'], $lang);

                unset(
                    $abonement['discount'],
                    $abonement['icon_b'],
                    $abonement['icon_s'],
                    $abonement['num'],
                    $abonement['is_default'],
                    $abonement['enabled']
                );

                $abonement['img'] = Request::scheme() . ':' . $abonement['img'];

                if ($abonement['price_free'] && $abonement['price_free_period'] == 0) {
                    unset($abonement['price']);
                } else {
                    foreach ($abonement['price'] as $key => &$aPrice) {
                        $aPrice['period'] = $key;
                        $aPrice['pr'] .= (' ' . $currency);
                    }
                    $abonement['price'] = array_values($abonement['price']);
                }
            }
        }

        return array_values($abonements);
    }

    protected function removeLangFields($aData, array $fieldNames = [], $lang)
    {
        $langsAvailable = bff::locale()->getLanguages();

        foreach ($fieldNames as $fieldName) {
            $fieldLangKey = $fieldName . '_' . $lang;
            if (
                !empty($lang) &&
                in_array($lang, $langsAvailable) &&
                array_key_exists($fieldLangKey, $aData) &&
                !empty($aData[$fieldLangKey])
            ) {
                $aData[$fieldName] = $aData[$fieldLangKey];
            }

            foreach ($langsAvailable as $lang) {
                $fieldLangKey = $fieldName . '_' . $lang;
                if (array_key_exists($fieldLangKey, $aData)) {
                    unset($aData[$fieldLangKey]);
                }
            }
        }

        return $aData;
    }

    public function buyAbonement($shopId, $abonementId, $abonementPeriod, $paymentMethod)
    {
        $moduleName = 'shops';
        $userId = $this->sesrow['user_id'];

        if (!bff::servicesEnabled()) {
            $this->errAddMsg('SVC_NOT_ENABLED');
            return 0;
        }

        $shopData = Shops::model()->shopData($shopId, array('user_id', 'reg3_city', 'status'));
        if (!$shopId || (int) $shopData['status'] !== Shops::STATUS_ACTIVE) {
            $this->errAddMsg('SHOP_NOT_OR_BLOCKED');
            return 0;
        }
        if ($shopData['user_id'] != $userId) {
            $this->errAddMsg('SHOP_OWNER_MISSMATCH');
            return 0;
        }
        if ($paymentMethod != 'balance') {
            $this->errAddMsg('PAYMENT_METHOD_NOT_AVAILABLE');
            return 0;
        }

        $nSvcID = Shops::SERVICE_ABONEMENT;
        $aSvc = Shops::model()->abonementData($abonementId);
        $aSvc['module'] = $moduleName;
        if (!$aSvc['price_free']) {
            $nSvcPrice = $aSvc['price'][$abonementPeriod];
        } else {
            if (!$aSvc['price_free_period']) {
                $shop = Shops::model()->shopData($shopId, array('svc_abonement_id'));
                if ($shop['svc_abonement_id'] == $abonementId) {
                    $this->errAddMsg('ABONEMENT_IS_ALREADY_ACTIVE');
                    return 0;
                }
            }
            $nSvcPrice = 0;
        }
        $svcSettings['abonement_id'] = $abonementId;
        $svcSettings['abonement_period'] = $abonementPeriod;

        $nUserBalance = Users::model()->userBalance($userId, true);
        if ($nUserBalance < $nSvcPrice) {
            $this->errAddMsg('LOW_BALANCE');
            return 0;
        }

        # конвертируем сумму в валюту для оплаты по курсу
        $pay = Bills::getPayAmount($nSvcPrice, $paymentMethod);
        $activated = Svc::i()->svc()->activate($moduleName, $nSvcID, $aSvc, $shopId, $userId, $nSvcPrice, $pay['amount'], $svcSettings);
        if ($activated) {
            return 1;
        }

        $this->errAddMsg('SVC_NOT_PAIED: ' . implode(', ', bff::errors()->get(true)));
        return 0;
    }

    public function generateTitle($itemData)
    {
        $titles = [];
        $catId = $itemData['cat_id'];
        $currentLanguage = bff::locale()->getCurrentLanguage();
        $defaultLanguage = bff::locale()->getDefaultLanguage();
        $lang = null;
        $catData = BBS::model()->catDataLang($catId, ['title', 'tpl_title_list', 'tpl_title_view', 'tpl_descr_list']);
        if (array_key_exists($currentLanguage, $catData)) {
            $catData = $catData[$currentLanguage];
            $lang = $currentLanguage;
        } else {
            $catData = $catData[$defaultLanguage];
            $lang = $defaultLanguage;
        }

        if (!empty($catData['tpl_title_list'])) {
            $titles['title_list'] = $this->parseTemplate($catData['tpl_title_list'], $itemData, $catData, $lang);
        }
        if (!empty($catData['tpl_title_view'])) {
            $titles['title'] = $this->parseTemplate($catData['tpl_title_view'], $itemData, $catData, $lang);
        }
        if (!empty($catData['tpl_descr_list'])) {
            $titles['descr_list'] = $this->parseTemplate($catData['tpl_descr_list'], $itemData, $catData, $lang);
        }

        return $titles;
    }

    protected function parseTemplate($template, $itemData, $catData, $lang)
    {
        $delimiter = '|';
        $macroPrice = '{price}';
        $macroCategory = '{category}';
        $macroCity = '{geo.city}';
        $macroCityIn = '{geo.city.in}';
        $macroMetro = '{geo.metro}';
        $macroDistrict = '{geo.district}';
        $macroDynamicProperty = '/\{\d+\}/';
        $macroDynamicPropertyCacheKey = '/\{[\w:\.]+\}/';

        $macros = [
            $macroPrice,
            $macroCategory,
            $macroCity,
            $macroCityIn,
            $macroMetro,
            $macroDistrict,
        ];

        $dynamicProperties = [];
        $dynamicPropertyCategoryIds = [];

        if (array_key_exists('dop', $itemData) && !empty($itemData['dop'])) {
            foreach ($itemData['dop'] as $categoryId => $categoryProperties) {
                $dynamicPropertyCategoryIds[] = $categoryId;
                foreach ($categoryProperties as $property) {
                    $id = key($property);
                    $macro = '{' . $id . '}';
                    $macros[] = $macro;
                    $dynamicProperties[$id] = $property[$id];
                }
            }
        }

        $cityData = Geo::regionData($itemData['city_id']);

        foreach ($macros as $macro) {
            if (strpos($template, $macro) !== false) {
                $flag = false;
                $titleLang = 'title_' . $lang;

                switch ($macro) {
                    case $macroPrice:
                        if (array_key_exists('price', $itemData) && !empty($itemData['price'])) {
                            $template = str_replace($macroPrice, $itemData['price'], $template);
                            $flag = true;
                        }
                        break;
                    case $macroCategory:
                        if (array_key_exists('title', $catData) && !empty($catData['title'])) {
                            $template = str_replace($macroCategory, $catData['title'], $template);
                            $flag = true;
                        }
                        break;
                    case $macroCity:
                        if (array_key_exists($titleLang, $cityData) && !empty($cityData[$titleLang])) {
                            $template = str_replace($macroCity, $cityData[$titleLang], $template);
                            $flag = true;
                        }
                        break;
                    case $macroCityIn:
                        if (!empty($cityData['declension'])) {
                            $template = str_replace($macroCityIn, $cityData['declension'], $template);
                            $flag = true;
                        }
                        break;
                    case $macroMetro:
                        if (array_key_exists('metro_id', $itemData) && !empty($itemData['metro_id'])) {
                            $metroData = Geo::model()->metroData($itemData['metro_id']);
                            if (!empty($metroData) && !empty($metroData['title'])) {
                                $template = str_replace($macroMetro, $metroData['title'], $template);
                                $flag = true;
                            }
                        }
                        break;
                    case $macroDistrict:
                        if (array_key_exists('district_id', $itemData) && !empty($itemData['district_id'])) {
                            $districtData = Geo::model()->districtData($itemData['district_id']);
                            if (!empty($districtData) && !empty($districtData[$titleLang])) {
                                $template = str_replace($macroMetro, $districtData[$titleLang], $template);
                                $flag = true;
                            }
                        }
                        break;
                    case (preg_match($macroDynamicProperty, $macro) ? true : false):
                        $dynamicPropertyId = (int) filter_var($macro, FILTER_SANITIZE_NUMBER_INT);
                        if (!empty($dynamicPropertyId)) {
                            $template = str_replace($macro, $dynamicProperties[$dynamicPropertyId], $template);
                            $flag = true;
                        }
                        break;
                }

                if (!$flag) {
                    $template = $this->removeMacroFromTemplate($macro, $delimiter, $template);
                }
            }
        }

        $otherMacros = [];

        preg_match_all($macroDynamicPropertyCacheKey, $template, $otherMacros);
        if (!empty($otherMacros[0])) {
            foreach ($otherMacros[0] as $otherMacro) {
                $template = $this->replaceOtherMacro(
                    $otherMacro,
                    $delimiter,
                    $template,
                    $dynamicPropertyCategoryIds,
                    $dynamicProperties
                );
            }
        }

        return str_replace($delimiter, '', $template);
    }

    protected function removeMacroFromTemplate($macro, $delimiter, $template)
    {
        if (strpos($template, $delimiter) !== false) {
            $templateParts = explode($delimiter, $template);
            foreach ($templateParts as $key => &$part) {
                if (strpos($part, $macro) !== false) {
                    unset($templateParts[$key]);
                    break;
                }
            }
            $template = implode($delimiter, $templateParts);
        } else {
            if (strpos($template, $macro) !== false) {
                $template = str_replace($macro, '', $template);
            }
        }

        return $template;
    }

    protected function replaceOtherMacro($macro, $delimiter, $template, $dynamicPropertyCategoryIds, $dynamicProperties)
    {
        $flag = false;
        if (!empty($dynamicPropertyCategoryIds)) {
            foreach ($dynamicPropertyCategoryIds as $categoryId) {
                $dynamicPropertyId = $this->DB->one_data(
                    'SELECT id FROM ' . TABLE_BBS_CATEGORIES_DYNPROPS . ' WHERE cat_id=:cat_id AND cache_key=:cache_key',
                    [
                        ':cat_id' => $categoryId,
                        ':cache_key' => trim($macro, '{}'),
                    ]
                );
                if (!empty($dynamicPropertyId) && array_key_exists($dynamicPropertyId, $dynamicProperties)) {
                    $template = str_replace($macro, $dynamicProperties[$dynamicPropertyId], $template);
                    $flag = true;
                }
            }
        }

        if (!$flag) {
            $template = $this->removeMacroFromTemplate($macro, $delimiter, $template);
        }

        return $template;
    }
}