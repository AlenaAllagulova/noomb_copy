<?php

return array(
    array(
        'file' => '/modules/internalmail/internalmail.model.php',
        'search' => 'if (!empty($messageID)) {',
        'replace' => "bff::hook('mapi.send.message.push', array('author' => \$authorID, 'receiver' => \$recipientID, 'message' => \$message));",
        'position' => 'after',
        'offset' => 0,
        'index' => 1,
    ),
);