<?php

class Plugin_Mobile_api_p10ba27 extends Plugin
{
    const TABLE_APPSESS = 'api_sess_v2';
    const TABLE_APPREGID = 'api_appregid_v2';
    const TABLE_APPLEREGID = 'api_appleregid_v2';

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Gauranga Revolution Mobile API',
            'plugin_version' => '1.0.0',
            'extension_id'   => 'p10ba2736bff1d0627d98b7c0787d49def458cac',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'premium' => array(
                'title' => 'число',
                'description' => 'Количество отображаемых Премиум объявлений',
                'input' => 'number',
                'min' => 1, // Минимально допустимое значение
                'max' => 4, // Максимально допустимое значение
                'onChange'  => function($newValue, $oldValue) {
                    # пример проверки нового значения:
                    if (empty($newValue)) {
                        $this->errors->set('Число должно быть от 1 до 4');
                        return $oldValue;
                    }else if($newValue > 4 || $newValue < 1){
                        $this->errors->set('Введите число от 1 до 4');
                        return $oldValue;
                    }
                    return $newValue;
                }
            ),
            'regionFalse' => array(
                'title' => 'ID регионов',
                'description' => 'ID регионов через ", " которые не будут отображатся в прилождении. Пример: 5050, 5059, 5060',
                'input' => 'text',
            ),
            'android_push_key' => array(
                'title' => 'Firebase ключ для пушей, Android',
                'input' => 'text',
                'default' => 'AAAAegj1Z5A:APA91bGp6Nm7DgCCV8Yt14tx0BvMOuCqlpCyUJ3h29INIIfIZLdMy1r3aaZFxEnDavyB9MvwCMHWClChXP-B8v845wy8-v8FZH82jvIIe3qRZ1VYgiIfyDATF49Z0wWNhngq-KCrtT6l',
            ),
        ));
    }

    protected function install()
    {
        $this->db->exec("CREATE TABLE IF NOT EXISTS `" . $this->tableAppSessions() . "` (
            sid char(32) PRIMARY KEY,
            time int unsigned not null default 0,
            type char(1) not null default '',
            user_id int unsigned not null default 0,
            ip char(32) not null default '',
            imcode char(32) not null default '',
            key (user_id)
            )ENGINE=MyISAM DEFAULT CHARSET=utf8;");

        $this->db->exec("CREATE TABLE IF NOT EXISTS `" . $this->tableAppRegID() . "` (
            id int PRIMARY KEY,
            appregid char(240) not null default ''
            )ENGINE=MyISAM DEFAULT CHARSET=utf8;");

        $this->db->exec("CREATE TABLE IF NOT EXISTS `" . $this->tableAppleRegID() . "` (
            id int PRIMARY KEY,
            appleregid char(240) not null default ''
            )ENGINE=MyISAM DEFAULT CHARSET=utf8;");

        return parent::install();
    }

    protected function uninstall()
    {

        return parent::uninstall();
    }

    function tableAppSessions() {
        return self::TABLE_APPSESS;
    }

    function tableAppRegID() {
        return self::TABLE_APPREGID;
    }

    function tableAppleRegID() {
        return self::TABLE_APPLEREGID;
    }

    protected function start()
    {
        $this->routeAdd($this->getName(), array(
            'pattern' => 'api_v2/(.*)',
            'callback' => $this->routeAction('request', 'com=$1'),
        ));

        bff::hookAdd('mapi.send.message.push', function($aData) {

            $sUserAddress = $this->db->one_data('SELECT appregid FROM ' . $this->tableAppRegID() . ' WHERE id = :id', array('id' => $aData['receiver']));

            if (!$sUserAddress) {
                return false;
            }

            $bHTTPS = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off';
            $sHTTPS = $bHTTPS ? 'https:' : 'http:';

            $message = (mb_strlen($aData['message']) > 20) ? substr($aData['message'], 0, 17) . '...' : $aData['message'];
            $aUser = Users::model()->userData($aData['author'], array('name', 'login', 'avatar'));
            $sFromName = !empty($aUser['name']) ? $aUser['name'] : $aUser['login'];
            $fields = array(
                'registration_ids' => array($sUserAddress),
                'data' => array(
                    'new_mess' => 1,
                    'vibrate' => 1,
                    'sound' => 1,
                    'message_text' => !empty($message) ? $message : "",// заголовок объявления
                    'name_user' => $sFromName,//имя юзера
                    'avatar' => !empty($aUser['avatar']) ? $sHTTPS . UsersAvatar::url($aData['author'], $aUser['avatar'], UsersAvatar::szNormal) : $sHTTPS . UsersAvatar::url(0, '', UsersAvatar::szNormal),//аватарка юзера
                    'user_id' => $aData['author'],
                    'for_user_id' => $aData['receiver'],
                )
            );
            $headers = array('Authorization: key=' . $this->config('android_push_key', '') , 'Content-Type: application/json');
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch);
            curl_close( $ch );
        });

        bff::hookAdd('users.avatar.sizes', function($aData) {
            //$aData['o'] = array('width' => 150, 'height' => 150);
            $aData['n'] = array('width' => 250, 'height' => 250);

            return $aData;
        });
    }

    public function request()
    {
        $command = $this->input->get('com', TYPE_NOTAGS);

        if ($command === 'img') return $this->img();

        require 'com/'.$command.'.php';

        $m = new Api_FN();
        $m->run_init();
        $m->start();

        bff::shutdown();
    }

    public function img() {
        $AllowFileExtension = array('jpg', 'png', 'jpeg', 'gif');
        $uploaddir = PATH_PUBLIC . 'files/images/tmp/';
        header('Content-Type: application/json');

        // Создадим папку если её нет
        if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0775 );

        // переместим файлы из временной директории в указанную
        $aData = array();
        foreach( $_FILES as $file ){
            $path_parts = pathinfo($file['name']);
            // Проверка если расширение файла находится в массиве доступных
            $FileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);
            if(!in_array(strtolower($FileExtension), $AllowFileExtension)) {
                echo json_encode(['RESULT' => 0, 'error' => 'NOT_ALLOWED_FILE_EXTENTION']);
                return false;
            }
            if($file['error'] > 0) {
                switch ((integer)$file['error']){
                    case UPLOAD_ERR_INI_SIZE:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_INI_SIZE';
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_FORM_SIZE';
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_PARTIAL';
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_NO_FILE';
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_NO_TMP_DIR';
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_CANT_WRITE';
                        break;
                    case UPLOAD_ERR_EXTENSION:
                        $sUploadErrorReason = 'PHP_UPLOAD_ERR_EXTENSION';
                        break;
                    default:
                        $sUploadErrorReason = 'UNKNOWN_UPLOAD_ERROR';
                }
                echo json_encode(['RESULT' => 0, 'error' => $sUploadErrorReason]);
                return false;
            }
            $sNewFilename = substr(md5(uniqid(mt_rand(), true)), 0, 8) . '_u' . User::id();
            if (move_uploaded_file($file['tmp_name'], $uploaddir . $sNewFilename . '.' . $path_parts['extension'] ) ){
                $aData[] = array('orig' => $file['name'], 'new' => $sNewFilename . '.' . $path_parts['extension']);
            } else {
                echo json_encode(['RESULT' => 0, 'error' => 'ERROR_DURING_UPLOAD']);
                return false;
            }
        }

        $aResponse['RESULT'] = 1;
        $aResponse['img_files'] = $aData;
        $aResponse['img_files_count'] = count($aResponse['img_files']);
        echo json_encode($aResponse);

        bff::shutdown();
    }
}