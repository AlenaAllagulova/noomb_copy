<?php
class Plugin_Bbs_images_server_p06345e_model extends Model
{
    /**
     * Данные о изображениях объявлений по фильтру
     * @param array $filter фильтр
     * @param bool $fields поля
     * @param bool $oneArray только для одной записи
     * @param bool $group сгрупировать по полю GROUP BY
     * @return int|mixed
     */
    public function bbsItemsImagesByFilter(array $filter, $fields = false,  $oneArray = true, $group = false)
    {
        BBS::model();
        $from = ' FROM '.TABLE_BBS_ITEMS_IMAGES.' M ';

        if ( ! empty($filter['publicated'])) {
            $from .= ', '.TABLE_BBS_ITEMS.' I ';
            $filter[':j'] = 'M.item_id = I.id ';
            $filter[':s'] = array('I.status = :publicated', ':publicated' => BBS::STATUS_PUBLICATED);
        }
        unset($filter['publicated']);

        $filter = $this->prepareFilter($filter);
        if (empty($fields)) {
            return (int)$this->db->one_data('SELECT COUNT(*) ' . $from . $filter['where'], $filter['bind']);
        } else {
            if ($oneArray) {
                return $this->db->one_array('SELECT ' . join(',', $fields) . $from . $filter['where'] . ($group ? ' GROUP BY '.$group : ' LIMIT 1'), $filter['bind']);
            } else {
                return $this->db->select('SELECT ' . join(',', $fields) . $from . $filter['where'] . ($group ? ' GROUP BY '.$group : ''), $filter['bind']);
            }
        }
    }

    /**
     * Применить фкнкцию итератор к выборке объявлений с изображениями
     * @param array $filter фильтр
     * @param array $fields поля
     * @param callable $callback функция обработчик
     */
    public function bbsItemsByFilterIterator(array $filter, array $fields, callable $callback)
    {
        BBS::model();
        $from = ' FROM '.TABLE_BBS_ITEMS_IMAGES.' M, '.TABLE_BBS_ITEMS.' I ';
        $filter[':j'] = 'M.item_id = I.id ';

        if ( ! empty($filter['publicated'])) {
            $filter[':s'] = array('I.status = :publicated', ':publicated' => BBS::STATUS_PUBLICATED);
        }
        unset($filter['publicated']);

        $filter = $this->prepareFilter($filter);
        $this->db->select_iterator('SELECT ' . join(',', $fields) . $from . $filter['where'].' GROUP BY I.id ', $filter['bind'], $callback);
    }
}
