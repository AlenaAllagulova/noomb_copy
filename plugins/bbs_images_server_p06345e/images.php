<?php
class Plugin_Bbs_images_server_p06345e_Images extends Plugin_Bbs_images_server_p06345e_Images_Base
{
    /** @var  Plugin_Bbs_images_server_p06345e */
    protected $pluginExStor;
    protected $folderExStor;
    protected $externalSalt;
    protected $externalUrl;

    protected function initSettings()
    {
        parent::initSettings();
        # сообщаем об использовании внешнего сервера
        $this->externalSave = true;
        $this->pluginExStor = bff::plugin('bbs_images_server_p06345e');
        $this->folderExStor = $this->pluginExStor->config('bbs_items_folder');
        $this->externalSalt = $this->pluginExStor->config('salt');
        $this->externalUrl = $this->pluginExStor->config('url');
    }

    /**
     * Формирование пути к изображению
     * @param array $aImage : filename - название файла gen(N).ext, dir - # папки, srv - ID сервера
     * @param string $sSize префикс размера
     * @param boolean $bTmp tmp-изображение
     * @return string Путь
     */
    public function getPath($aImage, $sSize, $bTmp = false)
    {
        if ( ! empty($aImage['ext_stor'])) {
            return ($this->pathTmp . $sSize . $aImage['filename']);
        }
        return parent::getPath($aImage, $sSize, $bTmp);
    }

    /**
     * Формирование URL изображения
     * @param array $aImage : filename - название файла gen(N).ext, dir - # папки, srv - ID сервера
     * @param string|array $mSize префикс размера или массив префиксов размеров
     * @param boolean $bTmp tmp-изображение
     * @return string|array URL
     */
    public function getURL($aImage, $mSize, $bTmp = false)
    {
        if (is_array($mSize)) {
            $aResult = array();
            foreach ($mSize as $sz) {
                $aResult[$sz] = $this->getURL($aImage, $sz, $bTmp);
            }

            return $aResult;
        }
        if ( ! empty($aImage['ext_stor'])) {
            return $aImage['extstor_img_'.$mSize];
        }
        return parent::getURL($aImage, $mSize, $bTmp);
    }

    /**
     * Удаление tmp изображения(-й)
     * @param string|array $mFilename имя файла (нескольких файлов)
     * @return boolean
     */
    public function deleteTmpFile($filename)
    {
        if ( ! is_array($filename)) {
            $filename = array($filename);
        }
        $filter = array(
            $this->fRecordID => 0,
            'filename' => $filename,
        );
        $filter = Model::filter($filter);
        $data = $this->db->select('SELECT * FROM '.$this->tableImages.$filter['where'], $filter['bind']);
        $this->db->delete($this->tableImages, array($this->fRecordID => 0, 'filename' => $filename));
        foreach ($data as $v) {
            $this->deleteFile($v);
        }
    }

    /**
     * Удаление файлов изображения (всех размеров)
     * @param array $aImage информация о файле изображения: filename[, dir, srv]
     * @param boolean $bTmp tmp-изображение
     * @return boolean
     */
    public function deleteFile($aImage, $bTmp = false)
    {
        if (empty($aImage) || empty($aImage['filename'])) {
            return false;
        }
        if ( ! empty($aImage['ext_stor'])) {
            return $this->deleteExternal($aImage);
        }

        return parent::deleteFile($aImage, $bTmp);
    }

    /**
     * Загрузка файла на внешний сервер
     * @param array $save данные об отпраяоемых файлах
     * @param string $filename имя файла
     * @return array
     */
    protected function uploadExternal($save, $filename)
    {
        $post = array(
            'act'       => 'upload',
            'key'       => md5($filename.$this->externalSalt),
            'filename'  => $filename,
            'folder'    => $this->folderExStor,
        );
        $unlink = array();
        foreach ($save as $k => $v) {
            if (function_exists('curl_file_create')) {
                $post[$k] = curl_file_create($v['filename']);
            } else {
                $post[$k] = '@'.$v['filename'];
            }
            $unlink[] = $v['filename'];
        }

        $result = array();
        do {
            $status = $this->pluginExStor->externalApi($post);

            if ( ! empty($unlink)) {
                foreach ($unlink as $v) {
                    if (file_exists($v)) {
                        @unlink($v);
                    }
                }
            }
            $error = $this->pluginExStor->lang('Не удалось передать изображение');
            if (empty($status)) {
                if ($this->assignErrors) {
                    $this->errors->set($error);
                    break;
                }
            }

            if (empty($status['files'])) {
                if ($this->assignErrors) { $this->errors->set($error); }
                $this->pluginExStor->log(__FUNCTION__.' External response files is empty');
                break;
            }

            if (empty($status['filename'])) {
                if ($this->assignErrors) { $this->errors->set($error); }
                $this->pluginExStor->log(__FUNCTION__.' External filename is empty');
                break;
            }

            $result['filename'] = $status['filename'];
            $result['ext_stor'] = 1;
            foreach ($status['files'] as $k => $v) {
                $result['extstor_img_'.$k] = $v;
            }
        } while(false);
        return $result;
    }

    /**
     * Удаление файла на внешнем сервере
     * @param string $image имя файла
     * @return bool
     */
    protected function deleteExternal($image)
    {
        if (empty($image['filename'])) {
            return false;
        }
        $filename = $image['filename'];
        $post = array(
            'act' => 'delete',
            'key'       => md5($filename.$this->externalSalt),
            'filename'  => $filename,
            'sizes'     => join(',', array_keys($this->sizes)),
            'folder'    => $this->folderExStor,
        );
        $status = $this->pluginExStor->externalApi($post);
        if (empty($status['success'])) {
            if ($this->assignErrors) {
                $this->errors->set($this->pluginExStor->lang('Не удалось удалить изображение'));
            }
        }
        return ! empty($status['success']);
    }

    /**
     * Скачивание файла с внешнего сервера
     * @param array $image данные о файле изображения
     * @param mixed $size ключ требуемого размера
     * @param string $tmpName путь к файлу для сохранения
     */
    protected function downloadExternal($image, $size, $tmpName)
    {
        $url = $this->getURL($image, $size);
        $parseUrl = parse_url($url);
        if (empty($parseUrl['scheme']) && ! empty($parseUrl['host']) && ! empty($parseUrl['path'])) {
            $external = parse_url($this->externalUrl);
            $url = $external['scheme'].'://'.$parseUrl['host'].$parseUrl['path'];
        }
        file_put_contents($tmpName, fopen($url, 'r'));
    }

    /**
     * Поворот изображения
     * @param array $image данные изображения
     * @param integer $angle угол поворота
     * @param boolean $tmp временное изображений
     * @return boolean|array
     */
    public function rotateImage($image, $angle, $tmp = false)
    {
        if ( ! $tmp && isset($image['ext_stor']) && $image['ext_stor'] == 0) {
            $this->externalSave = false;
            $result = parent::rotateImage($image, $angle, $tmp);
            $this->externalSave = true;
            return $result;
        }
        return parent::rotateImage($image, $angle, $tmp);
    }

    /**
     * Загрузка всех изображений для записи
     * @param int $imageID - только конкретное изображение
     */
    public function uploadAllImagesExternal($imageID = 0)
    {
        # данные о изображениях у записи
        $data = $this->getData();
        if ( empty($data)) return;

        $fav = false;
        if ($this->useFav) {
            # id fav изображения
            $fav = $this->db->one_data('SELECT '.$this->tableRecords_fav.' FROM ' . $this->tableRecords .
                ' WHERE ' . $this->tableRecords_id . ' = :id', array(':id' => $this->recordID));
        }

        foreach ($data as $v) {
            if ($v['ext_stor']) continue;
            if ($imageID && $v['id'] != $imageID) continue;

            # список файлов размеров
            $save = array();
            foreach ($this->sizes as $kk => $vv) {
                $path = $this->getPath($v, $kk);
                if ( ! file_exists($path)) continue 2;
                $save[$kk] = array('filename' => $path);
            }
            if (empty($save)) continue;

            # загружаем
            $update = $this->uploadExternal($save, $v['filename']);
            if (empty($update['filename'])) continue;

            # обновляем в БД
            $this->db->update($this->tableImages, $update, array('id' => $v['id']));

            # для fav обновляем кеш fav размеров
            if ($v['id'] == $fav) {
                $this->setFavoriteImage($fav);
            }
        }
    }

    /**
     * Сохранение всех изображений для записи
     */
    public function downloadAllImagesExternal()
    {
        # данные о изображениях у записи
        $data = $this->getData();
        if ( empty($data)) return;

        $fav = false;
        if ($this->useFav) {
            # id fav изображения
            $fav = $this->db->one_data('SELECT '.$this->tableRecords_fav.' FROM ' . $this->tableRecords .
                ' WHERE ' . $this->tableRecords_id . ' = :id', array(':id' => $this->recordID));
        }

        foreach ($data as $v) {
            if ( ! $v['ext_stor']) continue;

            $ext = pathinfo($v['filename'], PATHINFO_EXTENSION);
            $external = $v; # данные о изображени на внешнем сервере

            # новое название локального файла
            $v['filename'] = func::generator($this->filenameLetters) . '.' . $ext;
            $v['ext_stor'] = 0;

            $update = array();
            foreach ($this->sizes as $kk => $vv) {
                $path = $this->getPath($v, $kk);
                $this->downloadExternal($external, $kk, $path);
                if ( ! file_exists($path)) continue;
                $update['extstor_img_'.$kk] = '';
            }

            # обновляем данные о изображении
            if ( ! empty($update)) {
                $update['ext_stor'] = 0;
                $update['filename'] = $v['filename'];
            }
            $this->db->update($this->tableImages, $update, array('id' => $v['id']));

            # для fav обновляем кеш fav размеров
            if ($v['id'] == $fav) {
                $this->setFavoriteImage($fav);
            }

            # удаляем файлы на внешнем сервере
            $this->deleteExternal($external);
        }
    }

}