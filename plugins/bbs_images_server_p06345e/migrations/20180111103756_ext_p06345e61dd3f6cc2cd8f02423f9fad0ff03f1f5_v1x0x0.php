<?php

use bff\db\migrations\Migration as Migration;
use Phinx\Db\Adapter\MysqlAdapter;

class ExtP06345e61dd3f6cc2cd8f02423f9fad0ff03f1f5V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        # Создаем доп. поля в таблице TABLE_BBS_ITEMS_IMAGES
        # для хранения URL изображений на s3
        $bbsImages = BBS::i()->itemImages(0);
        $sizes = $bbsImages->getSizes();
        foreach ($sizes as $k => $v) {
            $this->table(TABLE_BBS_ITEMS_IMAGES)
                ->addColumn('extstor_img_'.$k, 'string', ['limit' => 250, 'default' => ''])
                ->update();
        }

        # флаг, что данное изображение хранится на s3
        $this->table(TABLE_BBS_ITEMS_IMAGES)
            ->addColumn('ext_stor', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'default'=>0, 'signed'=>false])
            ->update();

    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {

    }
}