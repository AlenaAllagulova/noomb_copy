<?php
/** @var $this Plugin_Bbs_images_server_p06345e */
?>
<div class="j-tab j-tab-move hidden" id="j-plugin-images-server-move">
    <? if($verified): ?>
    <div class="well well-small well-success" style="margin-bottom: 10px;">
        Подключение к серверу было успешно протестировано <?= tpl::dateFormat((int)$verified_date, '%d.%m.%Y в %H:%M') ?><br />
        <a href="#" class="btn btn-mini j-testing">Протестировать повторно</a>
        <a class="btn btn-mini" href="<?= $this->adminLink('download', $this->getName()) ?>" target="_blank">Скачать скрипт</a>
    </div>
    <? else: ?>
    <div>
        Требования к внешнему серверу:<br />
        Nginx + php-fpm или Apache + mod_php<br />
        PHP >= 5.6.x: gd2, curl, json<br /><br />
        Порядок установки скрипта:<br />
        1. <a href="<?= $this->adminLink('download', $this->getName()) ?>" target="_blank">Скачайте</a> и установите cкрипт загрузки на внешний сервер.<br />
        2. URL доступа к данному скрипту должен соответствовать указанному в настройке плагина "<b>URL скрипта</b>".<br />
        3. Протестируйте доступ к скрипту: <a href="#" class="btn btn-mini btn-success j-testing">Протестировать</a>
    </div>
    <? endif; ?>
    <div class="j-status">
        <?= $status ?>
    </div>
<script type="text/javascript">
$(function(){
    var $block = $('#j-plugin-images-server-move');
    var $progress = false;
    var $status = $block.find('.j-status');
    var ajaxUrl = '<?= $this->adminLink('massActions', $this->getName()) ?>&ajax=1';

    $block.on('click', '.j-act', function(e){
        e.preventDefault();
        if ( ! confirm('Вы уверены?')) return;
        var $el = $(this);
        bff.ajax(ajaxUrl, {act:$el.data('act')}, function(data){
            if (data && data.success) {
                status();
            }
        }, $progress);
    });

    var timeout, process = false;
    function status()
    {
        if(process) return; process = true;
        bff.ajax(ajaxUrl, {}, function(data){
            process = false;
            if (data && data.success) {
                if (data.status) {
                    $status.html(data.status);
                    $progress = $status.find('.j-progress');
                }
                if (intval(data.working)) {
                    clearTimeout(timeout);
                    timeout = setTimeout(function(){
                        status();
                    }, 7000);
                }
            }
        }, $progress);
    }
    <?= ! empty($working) ? 'status();' : '' ?>

    $block.on('click', '.j-testing', function(e){
        e.preventDefault();
        var $el = $(this);
        $el.text('Тестирование...');
        bff.ajax(ajaxUrl, {act:'testing'}, function(data){
            $el.text('<?= $verified ? 'Протестировать повторно' : 'Протестировать' ?>');
            if (data && data.success) {
                bff.success('Тестирование прошло успешно.');
                setTimeout(function(){
                    location.reload();
                }, 1500);
            }
        }, $progress);

    });
});
</script>
</div>
