<?php
/** @var $this Plugin_Bbs_images_server_p06345e */
$allowActions = empty($working);
?>
<table class="admtbl table table-condensed table-hover ">
<thead>
<tr>
    <th width="200"></th>
    <th>На диске</th>
    <th>На внешнем сервере</th>
    <th>Переместить</th>
</tr>
</thead>
<tr>
    <td class="right">Всего изображений:</td>
    <td><?= $all['cnt'] ?></td>
    <td><?= $all['ext'] ?></td>
    <td>
        <? if ($allowActions): ?>
        <a class="btn btn-mini btn-success j-act" data-act="move-all-ext">диск &rarr; внешний сервер</a>
        <a class="btn btn-mini btn-warning j-act" data-act="move-all-local">внешний сервер &rarr; диск</a>
        <? endif; ?>
    </td>
</tr>
<tr>
    <td class="right">Опубликовано изображений:</td>
    <td><?= $pub['cnt'] ?></td>
    <td><?= $pub['ext'] ?></td>
    <td>
        <? if ($allowActions): ?>
        <a class="btn btn-mini btn-success j-act" data-act="move-pub-ext">диск &rarr; внешний сервер</a>
        <a class="btn btn-mini btn-warning j-act" data-act="move-pub-local">внешний сервер &rarr; диск</a>
        <? endif; ?>
    </td>
</tr>
<tr>
    <td class="right">Свободно:</td>
    <td><?= tpl::filesize($free) ?></td>
    <td><?= tpl::filesize($freeext) ?></td>
    <td></td>
</tr>
</table>
<? if( ! empty($log) && ! empty($updated) && $updated > time() - 60*60): ?>
<div class="well well-small" style="margin-top: 10px;">
    <? if ( ! $allowActions): ?>
        <a class="btn btn-mini btn-danger j-act" data-act="move-break" style="margin-bottom: 5px;">Прервать перемещение</a>
        <span class="progress j-progress" style="top: 0; margin-left: 5px; display: none;"></span>
        <br />
    <? endif; ?>
    <?= join('<br />', $log) ?>
</div>
<? endif; ?>

