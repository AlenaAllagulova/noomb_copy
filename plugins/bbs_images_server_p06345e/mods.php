<?php

return array(
    array(
        'file' => '/modules/bbs/bbs.class.php',
        'search' => '$aImages = $aData[\'img\']->getData($aData[\'imgcnt\']);',
        'replace' => '$aImages = bff::filter(\'bbs.form.item.images.data\', $aData[\'img\']->getData($aData[\'imgcnt\']));',
        'position' => 'replace-line',
    ),
);