<?php

define('DS', DIRECTORY_SEPARATOR);

class externalImagesStorage
{
    const HOST = '{__HOST__}';
    const SALT = '{__SALT__}';
    public static $path = '';
    public static $url = '';

    /**
     * Инициализация
     */
    public static function main()
    {
        static::$path = dirname(__FILE__).DS.'files'.DS.'images'.DS;
        static::$url = '//'.static::HOST.'/files/images/';

        if (empty($_POST['filename']) || empty($_POST['key']) || empty($_POST['act'])) {
            return;
        }

        $result = array();
        do {
            if ($_POST['key'] != md5($_POST['filename'].static::SALT)) {
                if ($_POST['act'] == 'testing') {
                    $result['success'] = false;
                    $result['errors'][] = 'Обновите файл скрипта на сервере, скачав его повторно.';
                    break;
                }
                return;
            }

            $folder = ! empty($_POST['folder']) ? $_POST['folder'] : 'items';
            static::$path .= $folder;
            static::$url .= $folder.'/';

            if ( ! static::checkDir(static::$path)) {
                $result['success'] = false;
                $result['errors'][] = 'Path not exist '.static::$path;
                break;
            }
            static::$path .= DS;

            $filename = $_POST['filename'];
            $act = $_POST['act'];

            switch ($act) {
                case 'upload':
                    $result = static::upload($filename);
                    break;
                case 'delete':
                    $result = static::delete($filename);
                    break;
                case 'free':
                    $result = static::free();
                    break;
                case 'testing':
                    $result = array(
                        'success' => true,
                        'errors' => array(),
                    );
                    break;
                default:
                    $result['success'] = false;
                    $result['errors'][] = 'Unknown action';
                    break;
            }
        } while(false);

        echo json_encode($result);
    }

    public function path($dir, $name, $size, $ext)
    {
        return $dir.$name.$size.'.'.$ext;
    }

    /**
     * Загрузка изображения
     * @param string $filename
     * @return array
     */
    public static function upload($filename)
    {
        if (empty($_FILES)) {
            return array(
                'success' => false,
                'errors' => array('$_FILES is empty'),
            );
        }

        $dir = static::generator(3);
        if (!file_exists(static::$path.$dir)) {
            @mkdir(static::$path.$dir, 0775, true);
        }
        $dir .= DS;
        $sizes = array_keys($_FILES);
        $start = time() + 5;
        $ext = mb_strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        do {
            $name = static::generator(15);
            $exist = false;
            foreach ($sizes as $s) {
                $fn = static::$path.static::path($dir, $name, $s, $ext);
                if (file_exists($fn)) {
                    $exist = true;
                    break;
                }
            }
        } while(time() < $start && $exist);

        $result = array('files' => array());
        $unlink = array();
        do {
            if ($exist) {
                $result['success'] = false;
                $result['errors'][] = 'File already exists';
                break;
            }

            foreach ($_FILES as $size => $data) {
                if ($data['error'] != UPLOAD_ERR_OK) {
                    $result['success'] = false;
                    $result['errors'][] = 'File upload error';
                    break 2;
                }
                $fn = static::path($dir, $name, $size, $ext);
                if (move_uploaded_file($data['tmp_name'], static::$path.$fn)) {
                    $result['files'][$size] = static::$url.$fn;
                    $unlink[] = static::$path.$fn;
                } else {
                    $result['success'] = false;
                    $result['errors'][] = 'File move error';
                    break 2;
                }
            }
            $result['success'] = true;
            $result['filename'] = static::path($dir, $name, '', $ext);;
            $unlink = array();
        } while(false);

        if (!empty($unlink)) {
            foreach ($unlink as $v) {
                @unlink($v);
            }
        }
        return $result;
    }

    public static function free()
    {
        return array(
            'success'   => true,
            'free'      => disk_free_space(static::$path),
        );
    }

    /**
     * Удаление
     * @param string $filename
     * @return array
     */
    public static function delete($filename)
    {
        $result = array();
        do {
            if (empty($filename)) {
                $result['success'] = false;
                $result['errors'][] = 'Filename is empty';
                break;
            }

            if (empty($_POST['sizes'])) {
                $result['success'] = false;
                $result['errors'][] = 'Sizes is empty';
                break;
            }
            $sizes = explode(',', $_POST['sizes']);

            $pos = strpos($filename, DS);
            if ( ! $pos) {
                $result['success'] = false;
                $result['errors'][] = 'Unknown dir';
                break;
            }
            $dir = substr($filename, 0, $pos);
            if ( ! file_exists(static::$path.$dir)) {
                $result['success'] = false;
                $result['errors'][] = 'Dir not exists';
                break;
            }
            $ext = mb_strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            $dir .= DS;
            $name = substr($filename, $pos + 1);
            $name = pathinfo($name, PATHINFO_FILENAME);
            foreach ($sizes as $s) {
                $fn = static::$path.static::path($dir, $name, $s, $ext);
                if (file_exists($fn)) {
                    @unlink($fn);
                }
            }
            $result['success'] = true;
        } while(false);
        return $result;
    }

    /**
     * Генератор имени файла
     * @param int $nLength
     * @param bool $bNumbersOnly
     * @return int|string
     */
    public static function generator($nLength = 10, $bNumbersOnly = false)
    {
        if ($nLength > 32) {
            $nLength = 32;
        }
        if ($bNumbersOnly) {
            return mt_rand(($nLength > 1 ? pow(10, $nLength - 1) : 1), ($nLength > 1 ? pow(10, $nLength) - 1 : 9));
        }

        return substr(md5(uniqid(mt_rand(), true)), 0, $nLength);
    }

    public function checkDir($path)
    {
        if (file_exists($path)) {
            return true;
        }
        try {
            @mkdir($path, 0775, true);
        } catch (Exception $e) {
            return false;
        }
        return file_exists($path);
    }


    /**
     * Логирование
     * @param string $message
     * @param string $logFile
     */
    public static function log($message = '', $logFile = 'default.log')
    {
        $dir = dirname(__FILE__).DS.'logs';
        @mkdir($dir, 0775, true);
        if (!empty($message)) {
            if (is_array($message)) $message = print_r($message, true);
            if (is_object($message)) $message = print_r($message, true);
            $message = date('Y-m-d H:i:s') . ' ' . $message;
            $message .= "\n\r";
            if ($fp = @fopen($dir.DS.$logFile, 'ab')) {
                fwrite($fp, $message);
                fclose($fp);
            }
        }
    }
}

externalImagesStorage::main();