<?php

return array(
  # desktop:
  array(
    'file' => '/modules/bbs/tpl/def/item.view.php',
    'search' => '<?= $this->viewPHP($aData, \'item.view.owner\') ?>',
    'replace' => '<?php bff::hook(\'plugin_bbs_item_price_suggest_block_desktop\', array(\'data\'=>&$aData)); ?>',
    'position' => 'before',
    'index' => 1,
    'offset' => 0,
  ),
  # mobile:
  array(
    'file' => '/modules/bbs/tpl/def/item.view.php',
    'search' => '<div id="j-view-contact-mobile-block">',
    'replace' => '<?php bff::hook(\'plugin_bbs_item_price_suggest_block_mobile\', array(\'data\'=>&$aData)); ?>',
    'position' => 'before',
    'index' => 1,
    'offset' => 0,
  ),
  # themes: start + nelson
  array(
    'file' => array(
        '/themes/start_do2_t062d90/modules/bbs/tpl/def/item.view.owner.php',
        '/themes/nelson_t001e26/modules/bbs/tpl/def/item.view.owner.php',
    ),
    'search' => '# $contacts[\'has\']',
    'replace' => '<?php bff::hook(\'plugin_bbs_item_price_suggest_block_desktop\', array(\'data\'=>&$aData,\'theme\'=>\'start\')); ?>',
    'position' => 'after',
    'index' => 1,
    'offset' => 0,
  ),
);