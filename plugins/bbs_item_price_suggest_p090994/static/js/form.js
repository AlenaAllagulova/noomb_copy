var jPluginBBSItemPriceSuggest = (function(){
    return {
        init: function(options)
        {
            var o = $.extend({lang:{}, captcha:false, prefix:'', plugin_name:''}, options || {});
            var popup, f, $captcha;
            $(function(){
                app.popup('v-'+o.prefix, '.j-v-'+ o.prefix+'-popup', '.j-v-'+o.prefix+'-link', {onInit: function($p){
                    popup = this;
                    f = app.form($p.find('form:first'), function($f) {
                        if( ! f.checkRequired() ) return false;
                        f.ajax(bff.ajaxURL(o.plugin_name+'&ev=submit',''), {}, function(data, errors){
                            if (data && data.success) {
                                popup.hide(function(){
                                    f.alertSuccess(data.success_message, {reset: true});
                                    if (o.captcha) {
                                        $captcha.triggerHandler('click');
                                    }
                                });
                            } else {
                                f.fieldsError(data.fields, errors);
                                if (o.captcha && data.captcha) {
                                    $captcha.triggerHandler('click');
                                }
                                if (data.later) {
                                    popup.hide(function(){
                                        f.reset();
                                    });
                                }
                            }
                        });
                    }, {onInit: function($f){
                        if (o.captcha) {
                            $captcha = $('.j-captcha', $f);
                            if ($captcha.length) {
                                $captcha.on('click', function(){
                                    $(this).attr('src', $(this).data('url')+Math.random());
                                });
                            } else {
                                o.captcha = false;
                            }
                        }
                    }});
                }, onShow: function($p){
                    $p.fadeIn(100);
                    if (o.captcha) {
                        $captcha.triggerHandler('click');
                    }
                }});
            });
        }
    };
}());