<?php
    /**
     * Предложить свою цену: форма
     * @var $this Plugin_BBS_Item_Price_Suggest_p090994
     * @var $isDesktop boolean форма для desktop-версии
     * @var $isMobile boolean форма для mobile-версии
     * @var $data array данные об объявлении
     */
     $this->css('css/style.css');
     $this->js('js/form.js');
     $captcha = false;
?>
<div class="v-actions rel bbs-item-suggest-price mrgt0">
    <? if ($isDesktop) { ?>
        <a href="javascript:void(0);" class="btn btn-large btn-success j-v-<?= $this->jsPrefix(); ?>-desktop-link"><?= $this->lang('Предложите свою цену') ?></a>
        <div class="mrgt0 mrgr0 v-popup dropdown-block dropdown-block-right box-shadow abs j-v-<?= $this->jsPrefix(); ?>-desktop-popup" style="display:none; min-width: 320px; position: absolute;background: white;border: 1px solid #c0cbd2; z-index: 10">
    <? } else { ?>
        <a href="javascript:void(0);" class="btn btn-success j-v-<?= $this->jsPrefix(); ?>-mobile-link"><?= $this->lang('Предложите цену') ?></a>
        <div class="mrgt0 mrgr0 v-popup dropdown-block dropdown-block-top box-shadow abs j-v-<?= $this->jsPrefix(); ?>-mobile-popup" style="display:none; max-width: 100%;">
    <? } ?>
        <div>
            <p><?= $this->lang('Предложите свою цену') ?></p>
        </div>
        <div class="v-popup__form">
            <form action="">
                <input type="hidden" name="item_id" value="<?= $data['id'] ?>" />
                <div class="form-group flex flex_center">
                    <input type="text" name="price" style="width: 65%;" class="form-control price-input j-required" placeholder="<?= $this->lang('Ваша цена'); ?>" maxlength="10" />
                    <select name="price_currency" style="width: 35%;" class="form-control mrgl10"><?= Site::currencyOptions($data['price_curr']); ?></select>
                </div>
                <div class="form-group">
                    <input type="tel" name="phone_number" class="form-control j-required j-phone" placeholder="<?= $this->lang('Ваш номер телефона'); ?>" maxlength="14" />
                </div>
                <? if($this->captcha()): if (Site::captchaCustom($this->getName())): ?>
                    <div class="form-group">
                        <? bff::hook('captcha.custom.view', $this->getName(), __FILE__) ?>
                    </div>
                <? else: $captcha = true; ?>
                <div class="form-group">
                    <div><p><?= _t('', 'Введите результат с картинки') ?></p></div>
                    <input type="text" name="captcha" class="input-small j-required" value="" pattern="[0-9]*" /> <img src="" alt="" class="j-captcha" data-url="<?= tpl::captchaURL('math', array('key'=>$this->captchaCookieKey(), 'bg'=>'FFFFFF', 'rnd'=>'')) ?>" />
                </div>
                <? endif; endif; ?>
                <button type="submit" class="btn btn-default-accent send-button j-submit">
                    <?= $this->lang('Предложить'); ?>
                </button>
            </form>
        </div>
    </div>
    <script type="text/javascript">
    <? js::start(); ?>
        $(function(){
            jPluginBBSItemPriceSuggest.init(<?= func::php2js(array(
                'prefix' => $this->jsPrefix().'-'.($isDesktop ? 'desktop' : 'mobile'),
                'plugin_name' => $this->getName(),
                'captcha' => $captcha,
            )) ?>);
        });
    <? js::stop(); ?>
    </script>
</div>
